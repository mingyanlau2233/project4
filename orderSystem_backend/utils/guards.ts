import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import express from "express";
import jwt from "./jwt";
import { userService } from "../routes/user/userRoutes"
// import { User } from "../routes/user/models"; 
const permit = new Bearer({
  query: "access_token",
});

export async function isLoggedInAPI(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const token = permit.check(req);
    //console.log(token)

    if (!token) {
      return res.status(401).json({ msg: "Permission Denied" });
    }
    // console.log("token : ", token)

    const payload = jwtSimple.decode(token, jwt.jwtSecret);
    const user = await userService.getUserById(payload.staff_id);
    //console.log(payload)

    if (!user) {
      return res.status(401).json({ msg: "Wrong Payload" });
    }

    const { id, name, company_id, branch_id,position} = user;
    req.user = { id, username: name, company_id, branch_id ,position};
    return next();
  } catch (err) {
    return res.status(401).json({ msg: err.message });
  }
}

// export async function isAdmin(role: string) {
//   return (req: express.Request, res: express.Response, next: express.NextFunction) => {
//     if (req.body.position !== role) {
//       // res.status(401)
//       return res.status(401).send('not allow')
//     }
//     return next()
//   }
// }



// export async function isAdmin(  req: express.Request,
//   res: express.Response,
//   next: express.NextFunction){
//     try{
//       return (role: string)=>{
//         if (req.body.position !== role) {
//           //       // res.status(401)
//                 return res.status(401).send('not allow')
//       }
//     }catch(e){

//     }

//   }