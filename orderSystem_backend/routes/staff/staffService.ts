import { Knex } from "knex";
import { hashPassword } from "../../utils/hash";

export class StaffService {
    constructor(private knex: Knex) { }

    async getAllStaff(company_id: number, branch_id: number) {
        try {
            // console.log('get all staff')
            const result = await this.knex.raw
                (`select * from staff where company_id = ? and branch_id = ?;`,
                    [company_id, branch_id])
            return result
        } catch (e) {
            console.log(e.error)
        }

    }

    async postStaff(name:string,company_id:number,branch_id:number,passwordRaw:string,position:string,is_onboard:boolean) {
        const trx = await this.knex.transaction()
        const password = await hashPassword(passwordRaw)
        const data = { name,company_id,branch_id,password,position,is_onboard}
        //console.log('service',data )
        try {
            const id = (await trx("staff").insert(data).returning('id'))[0]
            await trx.commit();
            return { id: id, message: "post success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("error")
            return false
        }
    }
    async deleteStaff(id: number) {

        const trx = await this.knex.transaction()
        try {
            await trx("staff").where("id", "=", id).del()
            await trx.commit();
            return { message: "delete success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("error")
            return false
        }
    }

    async putStaffForAdmin(data:{},id:number) {

        const trx = await this.knex.transaction()
        try {
            console.log(id)
            const u = await trx("staff").update(data).where("id", id).returning('*')
            await trx.commit();
            console.log('f',u)
            return { message: "update success" }
        } catch (err) {
            console.log('fuck', err.message)
            await trx.rollback()
            console.log("error")
            return false
        }
    }

}
