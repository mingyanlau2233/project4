import express from "express";
import {knex} from '../../app'
import { isLoggedInAPI } from "../../utils/guards"
import {StaffService} from './staffService'
import { StaffController } from "./staffController";


const staffService = new StaffService(knex)
const staffController = new StaffController(staffService)

export const staffRoutes = express.Router()

// console.log("route")
staffRoutes.get('/getAllStaff',isLoggedInAPI ,staffController.getAllStaff)
staffRoutes.post('/postStaff',isLoggedInAPI ,staffController.postStaff)
staffRoutes.delete('/deleteStaff',isLoggedInAPI ,staffController.deleteStaff)
staffRoutes.put('/putStaff',isLoggedInAPI ,staffController.putStaffForAdmin)
