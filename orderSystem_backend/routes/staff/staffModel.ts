export interface IStaff{
    id:number,
    name:string,
    password:string,
    position:string,
    is_onboard:boolean,
    register_at:Date,
    create_at:Date,
    update_at:Date,
}