import { Request, Response } from 'express'
import { StaffService } from './staffService'
import { logger } from "../../utils/logger"

export class StaffController {
  constructor(private staffService: StaffService) { }
  getAllStaff = async (req: Request, res: Response) => {
    try {
      console.log(req.user)
      if (req.user.position !== 'admin') {
        // res.status(401).send('not allow')
        return
      }
      const company_id = req.user.company_id
      const branch_id = req.user.branch_id
      if (!company_id || !branch_id) {
        res.status(404).json({ message: "This is wrong company" })
        return
      }
      const result = await this.staffService.getAllStaff(company_id, branch_id)
      const rows = result.rows
      console.log(rows)
      res.json(rows)
      return
    } catch (err) {
      logger.error(err.message)
      res.status(500).json({ message: `internal server error` })
    }
  }
  postStaff = async (req: Request, res: Response) => {
    console.log('tesreq.bodyting', req.body)
    try {
      const name = req.body.name
      const password = req.body.password
      const position = req.body.position
      const is_onboard = req.body.is_onboard
      const company_id = req.user.company_id
      const branch_id = req.user.branch_id
      // const data = { name,company_id,branch_id,password,position,is_onboard}
      if (!company_id || !branch_id) {
        res.status(404).json({ message: "This is wrong company" })
        return
      }
      // console.log('data1111',data)
      // console.log('tesreq.bodyting',req.body)
      const result = await this.staffService.postStaff(name, company_id, branch_id, password, position, is_onboard)
      res.json(result)
      return
    } catch (err) {
      logger.error(err.message)
      res.status(500).json({ message: `internal server error` })
    }
  }

  deleteStaff = async (req: Request, res: Response) => {
    try {
      const id = req.user.id
      console.log(id)
      const result = await this.staffService.deleteStaff(id)
      res.json(result)
      return
    } catch (err) {
      logger.error(err.message)
      res.status(500).json({ message: `internal server error` })
    }
  }

  putStaffForAdmin = async (req: Request, res: Response) => {
    console.log('testing')
    try {
      const name = req.body.name
      const id = req.body.id
      const position = req.body.position
      const is_onboard = req.body.is_onboard
      const company_id = req.user.company_id
      const branch_id = req.user.branch_id
      const data = { name, company_id, branch_id, position, is_onboard }
      console.log(data)
      const result = await this.staffService.putStaffForAdmin(data, id)
      res.json(result)
      return
    } catch (err) {
      logger.error(err.message)
      res.status(500).json({ message: `internal server error` })
    }
  }


}