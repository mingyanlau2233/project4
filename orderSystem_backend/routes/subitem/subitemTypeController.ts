import { Request, Response } from "express";
import { subitemTypeService } from "./subitemTypeService";
import { logger } from "../../utils/logger";

export class subitemTypeController {
    constructor(private subitemTypeService: subitemTypeService) { }
    
    postSetSubitemType =async(req:Request,res:Response)=>{
        try{
            const company_id = req.user.company_id
            const branch_id = req.user.branch_id
            const max_limit = req.body.max_limit
            const set_id = req.body.set_id
            const is_launched = req.body.is_launched
            const name = req.body.name
            const photoName = req.body.photoName
            let data = {max_limit,set_id,is_launched,name,company_id,branch_id}
            const product_category_id =req.body.product_category_id
            const subitem_type_id = req.body.subitem_type_id
            const product_id = req.body. product_id
            if (product_category_id){data["product_category_id"] = product_category_id}
            if (subitem_type_id){data["subitem_type_id"] = subitem_type_id}
            if (product_id){data["product_id"] = product_id}
          const result = await this.subitemTypeService
          .postSetSubitemType(company_id,branch_id,photoName,data,name)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
 
      putSetSubitemType =async(req:Request,res:Response)=>{
        try{
            const company_id = req.user.company_id
            const branch_id = req.user.branch_id
            const max_limit = req.body.max_limit
            const is_launched = req.body.is_launched
            const name = req.body.name
            const photoName = req.body.photoName
            const deletedPhotoName = req.body.deletedPhotoName
            const set_subitem_type_id = req.body.id
            let data = {max_limit,is_launched,company_id,branch_id}
            const product_category_id =req.body.product_category_id
            const subitem_type_id = req.body.subitem_type_id
            const product_id = req.body. product_id
            if (product_category_id){data["product_category_id"] = product_category_id}
            if (subitem_type_id){data["subitem_type_id"] = subitem_type_id}
            if (product_id){data["product_id"] = product_id}
          const result = await this.subitemTypeService
          .putSetSubitemType(set_subitem_type_id,company_id,branch_id,photoName,data,name,deletedPhotoName,)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      deleteSetSubitemType =async(req:Request,res:Response)=>{

 
        try{
          let set_subitem_type_id = req.body.id 
  
          const result = await this.subitemTypeService.deleteSetSubitemType(set_subitem_type_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      
  }