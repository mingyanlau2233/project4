import {Knex} from 'knex';

export class subitemService{
    constructor(private knex:Knex){}

    async getSubitem_type(company_id:number,branch_id:number){
        
        const trx = await this.knex.transaction()
        try {
            let result =  await trx("subitem_type").select("name","id")
                          .where('company_id',"=",company_id).where("branch_id","=",branch_id)
            await trx.commit();
            return result
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("subitemServices encounter error")
            return false
        }
    }

    async putSubitem_type(id:number,data:Object){
        
        const trx = await this.knex.transaction()
        try {
            await trx("subitem_type").update(data)
                          .where('id',"=",id)
            await trx.commit();
            return {message: "put success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("subitemServices encounter error")
            return false
        }
    }

    async postSubitem_type(company_id:number,branch_id:number,data:Object){
        
        const trx = await this.knex.transaction()
        try {
            await trx("subitem_type").insert(data)
            .where('company_id',"=",company_id).where("branch_id","=",branch_id)
            await trx.commit();
            return {message: "post success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("subitemServices encounter error")
            return false
        }
    }

    async deleteSubitem_type(id:number){
       
        const trx = await this.knex.transaction()
        try {
            const subitem_ids = await trx("subitem").select("id").where("subitem_type_id","=",id)
            console.log(subitem_ids)
            for (let id of subitem_ids){
                await trx("product_available_period").where("subitem_id","=",id.id).del()
                await trx("photo_list").where("subitem_id","=",id.id).del()
                await trx("price_list").where("subitem_id","=",id.id).del()
                await trx("remarks_details").where("subitem_id","=",id.id).del()

            }

            await trx("remarks").where("subitem_type_id","=",id).del()   
            await trx("subitem").where("subitem_type_id","=",id).del()  
            await trx("subitem_type").where("id","=",id).del() 
            await trx.commit();
            return {message: "delete success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("subitemServices encounter error")
            return false
        }
    }

    async postSubitem(company_id:number,branch_id:number,available_period:[],remarks_details:[],photoName:[],
        price:number,data:object){
            
        const trx = await this.knex.transaction()
        try {
            
            // let available_period = [1,2,3]
            // let remarks_details = [1,2,3] 
            // let photoName = ["9.jpg","10.jpg"]
            let discount_set_id = 0
            let product_id = 0
            let set_id = 0 
            let set_subitem_type_id = 0 
            let remarks_id =0 
            let subitem_id = await trx("subitem").insert(data).returning("id")
            subitem_id= subitem_id[0]
        if (available_period){
        for (let available_periods of available_period){
            let available_period_id=available_periods
            let data = {available_period_id,subitem_id,company_id,branch_id}
            await trx("product_available_period").insert(data)
        }}
        if (remarks_details){
        for (let remarks of remarks_details){
            let remarks_id = remarks
            let data = {remarks_id,subitem_id,company_id,branch_id}
            await trx("remarks_details").insert(data)
        }}
        if(photoName){
        for (let photo of photoName){
            let data = {photo,product_id,company_id,branch_id,discount_set_id,subitem_id,set_id,set_subitem_type_id}
            await trx("photo_list").insert(data)
        }}

        let priceData = {price,product_id,company_id,branch_id,discount_set_id,subitem_id,set_id,set_subitem_type_id,remarks_id}
        await trx("price_list").insert(priceData)
  
            await trx.commit();
            return {message: "post success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("subitemServices encounter error")
            return false
        }
    }

    
    async putSubitem(data:object,available_period:[],remarks_details:[],photoName:[],
        price:number,subitem_id:number,company_id:number,branch_id:number,deletedPhotoName:[]){
        //console.log(data,available_period,remarks_details,photoName,deletedPhotoName)
      
        const trx = await this.knex.transaction()
        try {
            // console.log(data)
            //  let available_period = [1]
            //  let remarks_details = [1] 
            //  let photoName = ["tony.jpg","tony.jpg"]
            //  let deletedPhotoName = ["4.jpg"]
             //console.log(product_id)

             if (available_period){
            let oldProduct_available_period = await trx("product_available_period").select("available_period_id").where("subitem_id","=",subitem_id)
           // console.log(oldProduct_available_period)
            for (let old of oldProduct_available_period){
                let remain =false
                //console.log(old)
                for(let news of available_period){
                    //console.log(old,news)
                   if( news == old.available_period_id ){
                    
                       remain = true
                   }
                }
                if(remain == false){
                  await trx("product_available_period").where("id","=",old.available_period_id).del()
                }
            }

            for (let news of available_period){
                let newOne = true
                for(let old of oldProduct_available_period){
                    if (news == old.available_period_id ) { newOne = false}
                }
                if(newOne == true){
                    let available_period_id = news
                    let data = {available_period_id,subitem_id,company_id,branch_id}
                    await trx("product_available_period").insert(data)
                }
            }}
            
            if (remarks_details){
            let oldRemarks_details = await trx("remarks_details").where("subitem_id","=",subitem_id).returning("remarks_id")
            oldRemarks_details = oldRemarks_details
            for (let old of oldRemarks_details){
                let remain =false
                for(let news of remarks_details){
                   if( news == old.remarks_id ){
                       remain = true
                   }
                }
                if(remain == false){
                  await trx("remarks_details").where("subitem_id","=",old.remarks_id).del()
                }
            }

            for (let news of remarks_details){
                let newOne = true
                for(let old of oldRemarks_details){
                    if (news == old.remarks_id ) { newOne = false}
                }
                if(newOne == true){
                    let remarks_id = news
                    let data = {remarks_id,subitem_id,company_id,branch_id}
                    await trx("remarks_details").insert(data)
                }
            }}

            let price_list_data = {price}
            await trx("price_list").update(price_list_data).where("subitem_id","=",subitem_id).where("discount_set_id","=",0)
            .where("subitem_id","=",0).where("set_id","=",0).where("set_subitem_type_id","=",0) 

            if (photoName){
            for (let photo of photoName){
                let data = {photo,subitem_id,company_id,branch_id}
                await trx("photo_list").insert(data)
            }
        }

           if(deletedPhotoName){
            for (let photo of deletedPhotoName){
                await trx("photo_list").where("photo","=",photo).del()
            }
        }
           await trx("subitem").update(data).where("id","=",subitem_id)
             
                     
            await trx.commit();
            return {message:"put success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    } 

    async deleteSubitem(subitem_id:number){
       
      
        const trx = await this.knex.transaction()
        try{
            
            await trx("photo_list").where("subitem_id","=",subitem_id).del()
            await trx("price_list").where("subitem_id","=",subitem_id).where("product_id","=",0).where("discount_set_id","=",0)
            .where("set_id","=",0).where("set_subitem_type_id","=",0).del()
            await trx("product_available_period").where("subitem_id","=",subitem_id).del()
            //await trx("discount_set_details").where("subitem_id","=",subitem_id).del()
            await trx("remarks_details").where("subitem_id","=",subitem_id).del()
            //await trx("order_cart").where("subitem_id","=",subitem_id).del()
            //await trx("set_subitem_type").where("subitem_id","=",subitem_id).del()
            await trx("subitem").where("id","=",subitem_id).del()
            await trx.commit();
            return {message:"delete success"}
          }catch(err){
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
           } 
        }


      async getAllSubitem(company_id:number,branch_id:number){
       
      
            const trx = await this.knex.transaction()
            try{
                const subitem_type_idData = await trx("subitem_type").select("id","name")
            .where('company_id',"=",company_id).where("branch_id","=",branch_id)
          let result = []
            for (let subitem_type_id of subitem_type_idData){
                 let dataWithSameSubitem_type=[]
                 const subitem_idData = await trx("subitem").select("id").where("subitem_type_id","=",subitem_type_id.id)
                 for (let subitem_id of subitem_idData){
                     let data = await trx("subitem").select("id","name","is_launched",
                     "is_time_consuming","subitem_type_id")
                     .where("id","=",subitem_id.id)

                     data= data[0]
                     data["subitem_id"]= data["id"]
                     data['product_category_name']=subitem_type_id.name
                     delete data["id"]
                     let price= await trx("price_list").select("price").where("subitem_id","=",subitem_id.id)
                     .where("subitem_id","=",0).where("discount_set_id","=",0).where("set_id","=",0)
                     .where('set_subitem_type_id',"=",0)
                     if(price.length==1){
                         price=price[0].price
                         data["price"]= price}
                     
                     let photos = await trx("photo_list").select("photo").where("subitem_id","=",subitem_id.id)
                     .where("product_id","=",0).where("discount_set_id","=",0).where("set_id","=",0)
                     .where('set_subitem_type_id',"=",0)
                     let photoList =[]
                     for(let photo of photos ){
                        photoList.push(photo.photo)
                     }
                     data["photo"]= photoList
                     let remarks = await trx("remarks_details").select("remarks_id").where("subitem_id","=",subitem_id.id)
                     let remarksList =[]
                     for(let remark of remarks ){
                        remarksList.push(remark.remarks_id)
                     }
                     data["remarks_id"]=remarksList
                     let period = await trx("product_available_period").select("available_period_id").where("subitem_id","=",subitem_id.id)
                     let periodList =[]
                     for(let periods of period ){
                        periodList.push(periods.available_period_id)
                     }
                    data['available_period'] =periodList
                    dataWithSameSubitem_type.push(data)
                 }
                 result.push(dataWithSameSubitem_type)
            }
                console.log(result)

            await trx.commit();
            return {result}
               
                return {message:"success"}
              }catch(err){
                console.log(err.message)
                await trx.rollback()
                console.log("menuSettingService encounter error")
                return false
               } 
            }

    async postSet(company_id:number,branch_id:number,data:Object,available_period:[],photoName:[],
        ){
        
        const trx = await this.knex.transaction()
        try {

            let available_period = [1]
        
            let photoName = ["1222.jpg","1126.jpg"]
            let discount_set_id = 0
            let product_id = 0
            let set_subitem_type_id = 0 
            let subitem_id =0
            let set_id = await trx("set").insert(data)
            .where('company_id',"=",company_id).where("branch_id","=",branch_id).returning("id")
            set_id= set_id[0]

            if(available_period){
            for (let available_periods of available_period){
                let available_period_id=available_periods
                let data = {available_period_id,set_id,company_id,branch_id}
                await trx("product_available_period").insert(data)
            }
            }

            if(photoName){
            for (let photo of photoName){
                let data = {photo,set_id,company_id,branch_id,product_id,discount_set_id,subitem_id,set_subitem_type_id}
                await trx("photo_list").insert(data)
            }}

            await trx.commit();
            
            return {message: "post success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("subitemServices encounter error")
            return false
        }
    }


    async putSet(data:object,available_period:[],photoName:[],
        set_id:number,company_id:number,branch_id:number,deletedPhotoName:[]){
        //console.log(data,available_period,remarks_details,photoName,deletedPhotoName)
      
        const trx = await this.knex.transaction()
        try {
           
             let available_period = [1,2,3]
             let photoName = ["tony.jpg","tony.jpg"]
             let deletedPhotoName = ["116.jpg"]
           

             if (available_period){
            let oldProduct_available_period = await trx("product_available_period").select("available_period_id").where("set_id","=",set_id)
           
            for (let old of oldProduct_available_period){
                let remain =false
               
                console.log("old.id",old.available_period_id)
                for(let news of available_period){
                  
                   if( news == old.available_period_id ){
                   
                       remain = true
                   }
                }
                if(remain == false){
                   
                  await trx("product_available_period").where("set_id","=",set_id).where("id","=",old.available_period_id).del()
                }
            }

            for (let news of available_period){
                let newOne = true
                for(let old of oldProduct_available_period){
                    if (news == old.available_period_id ) { newOne = false}
                }
                if(newOne == true){
                    let available_period_id = news
                    let data = {available_period_id,set_id,company_id,branch_id}
                    await trx("product_available_period").insert(data)
                }
            }}
            


            if (photoName){
            for (let photo of photoName){
                let data = {photo,set_id,company_id,branch_id}
                await trx("photo_list").insert(data)
            }
        }

           if(deletedPhotoName){
            for (let photo of deletedPhotoName){
                await trx("photo_list").where("photo","=",photo).del()
            }
        }
           await trx("set").update(data).where("id","=",set_id)
             
                     
            await trx.commit();
            return {message:"put success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    } 

    async deleteSet(set_id:number){
       
      
        const trx = await this.knex.transaction()
        try{
            console.log(set_id)
            await trx("photo_list").where("set_id","=",set_id).del()
            await trx("price_list").where("set_id","=",set_id).where("product_id","=",0).where("discount_set_id","=",0)
            .where("subitem_id","=",0).where("set_subitem_type_id","=",0).del()
            await trx("product_available_period").where("set_id","=",set_id).del()
    
            await trx("set_subitem_type").where("set_id","=",set_id).del()
     
            await trx("set").where("id","=",set_id).del()
            await trx.commit();
            return {message:"delete success"}
          }catch(err){
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
           } 
        }
    
    async getAllSet(company_id:number,branch_id:number){
       
      
      const trx = await this.knex.transaction()
       try{
           console.log(branch_id,company_id)
         let allSetIds = await trx("set").select("id","name","is_launched","price")
          .where('company_id',"=",company_id).where("branch_id","=",branch_id)
         let result = []
         for (let setIds of allSetIds){
             setIds["set_id"]=setIds.id
             setIds["set_name"]=setIds.name
             delete setIds["id"]
             delete setIds["name"]
            //  let dataInSameSet = await trx("set_subitem_type").select("id","sequence","max_limit",
            //  "name","set_id","product_category_id","subitem_type_id",
            //  "product_id","is_launched").where("set_id","=",setIds.set_id)
             //console.log(dataInSameSet)
             let dataInSameSet = await trx("set_subitem_type").select(
             "name").where("set_id","=",setIds.set_id)
            
            // console.log(dataInSameSet)
             let names = []
             for (let i of dataInSameSet){
                   names.push(i["name"])
             }
             let newNames:string[] = []
             for (let i of names){
                  if(!newNames.includes(i)){
                    newNames.push(i)
                  }
             }
            // console.log("names",newNames)

             let setPhoto =  await trx("photo_list").select("photo").where("set_id","=",setIds.set_id)
             setIds["set_photo"] =setPhoto
             //console.log(setPhoto)
             let oneSetData =[]
             oneSetData.push(setIds)
             console.log(oneSetData)
             for(let j of newNames){
                  let dataAsSameName = await trx("set_subitem_type").select("id","sequence","max_limit",
                   "name","product_category_id","subitem_type_id",
                   "product_id","is_launched").where("set_id","=",setIds.set_id).where("name","=",j)
                let oneNameData = []
                for(let i of dataAsSameName){
                    
                if (i.subitem_type_id){
                   
                    let subitem_typeData = 
                    await trx("set_subitem_type").join("subitem_type","subitem_type.id","=","set_subitem_type.subitem_type_id")
                    .select("subitem_type.name","set_subitem_type.id","subitem_type.id").where("set_subitem_type.id","=",i.id)
                    subitem_typeData=subitem_typeData[0]
                
                    i["subitem_type_name"]=subitem_typeData["name"]
                   
                }
                if (i.product_category_id){
                
                    let subitem_typeData = 
                    await trx("set_subitem_type").join("product_category","product_category.id","=","set_subitem_type.product_category_id")
                    .select("product_category.name","set_subitem_type.id","product_category_id").where("set_subitem_type.id","=",i.id)
                    subitem_typeData=subitem_typeData[0]
                    
                    i["product_category_name"]=subitem_typeData["name"]
                    
                }
                if (i.product_id){
                
                    let subitem_typeData = 
                    await trx("set_subitem_type").join("product","product.id","=","set_subitem_type.product_id")
                    .select("product.name","set_subitem_type.id","product_category_id").where("set_subitem_type.id","=",i.id)
                    subitem_typeData=subitem_typeData[0]
                    
                    i["product_name"]=subitem_typeData["name"]
                }
                 let photo = await trx("photo_list").select("photo").where("set_subitem_type_id","=",i.id)
                 i["set_subitem_type_id"]= i.id
                 i["set_subitem_type_name"] = i.name
                 i["photo"]= photo
                 delete i.id
                 delete i.name
                 oneNameData.push(i)
                   }
                   console.log(oneNameData)

             }
             
             result.push(oneSetData)
              
            }
           // console.log(result)
            await trx.commit();
             
                return result
              }catch(err){
                console.log(err.message)
                await trx.rollback()
                console.log("menuSettingService encounter error")
                return false
               } 
            }

    
}