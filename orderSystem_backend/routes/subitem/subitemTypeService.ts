import {Knex} from 'knex';


export class subitemTypeService{
    constructor(private knex:Knex){}

    async postSetSubitemType(company_id:number,branch_id:number,photoName:[],data:object,name:string){
        
        const trx = await this.knex.transaction()
        try {
            // let available_period = [1,2,3]
             let photoName = ["9.jpg","10.jpg"]
            
            let sequence = await trx('set_subitem_type').where("name","=",name).count('name', {as: name})
    
            data["sequence"] = Number(sequence[0][name])+1

            let set_subitem_type_id = await trx("set_subitem_type").insert(data).returning("id")
            set_subitem_type_id = set_subitem_type_id[0]
            console.log(set_subitem_type_id)

            if(photoName){
               for (let photo of photoName){
                     let data = {photo,set_subitem_type_id,company_id,branch_id}
                      await trx("photo_list").insert(data)
                    }}
            await trx.commit();
            return {message:"insert success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("waitingTicketService encounter error")
            return false
        }
    }

    async putSetSubitemType(set_subitem_type_id:number,company_id:number,branch_id:number,photoName:[],data:object,name:string,deletedPhotoName:[]){
        
        const trx = await this.knex.transaction()
        try {
           
             let photoName = ["9.jpg","10.jpg",]
             let deletedPhotoName = ["116.jpg"]
            await trx('set_subitem_type').update(data).where("id","=",set_subitem_type_id)

            if (photoName){
                for (let photo of photoName){
                    let data = {photo,set_subitem_type_id,company_id,branch_id}
                    await trx("photo_list").insert(data)
                }
            }
    
               if(deletedPhotoName){
                for (let photo of deletedPhotoName){
                    await trx("photo_list").where("photo","=",photo).where("set_subitem_type_id","=",set_subitem_type_id).del()
                }
            }
            await trx.commit();
            return {message:"put success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("waitingTicketService encounter error")
            return false
        }
    }

    async deleteSetSubitemType(set_subitem_type_id:number){
    
      
        const trx = await this.knex.transaction()
        try{
            
            await trx("photo_list").where("set_subitem_type_id","=",set_subitem_type_id).del()
            await trx("set_subitem_type").where("id","=",set_subitem_type_id).del()
            await trx.commit();
            return {message:"delete success"}
          }catch(err){
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
           } 
        }
}