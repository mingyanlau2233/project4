import { Request, Response } from "express";
import { subitemService } from "./subitemService";

import { logger } from "../../utils/logger";

export class subitemController {
    constructor(private subitemService: subitemService) { }
    
    getSubitem_type =async(req:Request,res:Response)=>{
      try{
        const company_id = req.user.company_id
        const branch_id = req.user.branch_id
        const result = await this.subitemService.getSubitem_type(company_id,branch_id)
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }

    putSubitem_type =async(req:Request,res:Response)=>{
      try{
        const id = req.body.id
        const name = req.body.name
        let data ={name}
        console.log(id)
        const result = await this.subitemService.putSubitem_type(id,data)
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }
 
    postSubitem_type =async(req:Request,res:Response)=>{
      try{
        const company_id = req.user.company_id
        const branch_id = req.user.branch_id
        const name = req.body.name
        let data ={name,company_id,branch_id}
        const result = await this.subitemService.postSubitem_type(company_id,branch_id,data)
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }

    deleteSubitem_type =async(req:Request,res:Response)=>{
      try{
        const id = req.body.id
        
        const result = await this.subitemService.deleteSubitem_type(id)
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }

    postSubitem =async(req:Request,res:Response)=>{
      try{
    
        let company_id = req.user.company_id 
        let branch_id = req.user.branch_id 
        let name = req.body.name
        let is_launched = req.body.is_launched
        let is_time_consuming = req.body.is_time_consuming
        let subitem_type_id = req.body.product_category_id
        let available_period = req.body.available_period
        let remarks_details = req.body.remarks_details
        let photoName =req.body.photoName
        let price = req.body.price
        let data ={company_id,branch_id,name,is_launched,is_time_consuming,subitem_type_id}
        
        const result = await this.subitemService.postSubitem(company_id,branch_id,available_period,remarks_details,photoName,price,data)
       
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }

    putSubitem =async(req:Request,res:Response)=>{
      try{
        let company_id = req.user.company_id 
        let branch_id = req.user.branch_id 
        let name = req.body.name
        let is_launched = req.body.is_launched
        let is_time_consuming = req.body.is_time_consuming
        let subitem_id = req.body.id
        let available_period = req.body.available_period
        let remarks_details = req.body.remarks_details
        let photoName =req.body.photoName
        let price = req.body.price
        let deletedPhotoName = req.body.deletedPhotoName
  
        let data ={name,is_launched,is_time_consuming}
        
        const result = await this.subitemService.putSubitem(data,available_period,remarks_details,photoName,
          price,subitem_id,company_id,branch_id,deletedPhotoName)
       
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }

    deleteSubitem =async(req:Request,res:Response)=>{

 
      try{
        let subitem_id = req.body.id 

        const result = await this.subitemService.deleteSubitem(subitem_id)
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }

    getAllSubitem =async(req:Request,res:Response)=>{
      try{
        const company_id = req.user.company_id
        const branch_id = req.user.branch_id
        const result = await this.subitemService.getAllSubitem(company_id,branch_id)
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }

    postSet =async(req:Request,res:Response)=>{
      try{
        const company_id = req.user.company_id
        const branch_id = req.user.branch_id
        const name = req.body.name
        const is_launched = req.body.is_launched
        const price = req.body.price
        const data = {name,is_launched,price,company_id,branch_id}
        const available_period = req.body.available_period
        const photoName = req.body.photoName
       
        const result = await this.subitemService.postSet(company_id,branch_id,data,available_period,photoName)
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }

    putSet =async(req:Request,res:Response)=>{
      try{
        let company_id = req.user.company_id 
        let branch_id = req.user.branch_id 
        let name = req.body.name
        let is_launched = req.body.is_launched
        let set_id = req.body.id
        let available_period = req.body.available_period
       
        let photoName =req.body.photoName
        let price = req.body.price
        let deletedPhotoName = req.body.deletedPhotoName
  
        let data ={name,is_launched,price}
        
        const result = await this.subitemService.putSet(data,available_period,photoName,
          set_id,company_id,branch_id,deletedPhotoName)
       
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }

    deleteSet =async(req:Request,res:Response)=>{

 
      try{
        let set_id = req.body.id 

        const result = await this.subitemService.deleteSet(set_id)
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }

    getAllSet =async(req:Request,res:Response)=>{

 
      try{
        let company_id = req.user.company_id
        let branch_id = req.user.branch_id
        const result = await this.subitemService.getAllSet(company_id,branch_id)
        res.json(result)
        return
      }catch(err){
        logger.error(err.message)
        res.status(500).json({message:`internal server error`})
      }
    }
  }


