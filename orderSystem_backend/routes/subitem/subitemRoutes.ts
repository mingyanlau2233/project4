import express from "express";
import {knex} from '../../app'
import { subitemService } from "./subitemService";
import { subitemController } from "./subitemController"

import { subitemTypeService } from "./subitemTypeService";
import { subitemTypeController } from "./subitemTypeController";
import multer from 'multer';
import path from "path";
let storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve("./upload"));
    },
    filename: function (req, file, cb) {
      cb(
        null,
        `${file.fieldname}-${Date.now().toString().slice(0, -2)}.${
          file.mimetype.split("/")[1]
        }`
      );
      let photoName = `${file.fieldname}-${Date.now()
        .toString()
        .slice(0, -2)}.${file.mimetype.split("/")[1]}`;
      //console.log('file',Date.now().toString().slice(0,-2))
      req.body.photoName = photoName;
    },
  });
 export  const upload = multer({ storage });

export const subitemRoutes = express.Router()

const SubitemService = new subitemService(knex)
const SubitemController = new subitemController(SubitemService)

const SubitemTypeService = new subitemTypeService(knex)
const SubitemTypeController = new subitemTypeController(SubitemTypeService)

subitemRoutes.post('/getSubitem_type',SubitemController.getSubitem_type)
subitemRoutes.post('/putSubitem_type',SubitemController.putSubitem_type)
subitemRoutes.post('/postSubitem_type',SubitemController.postSubitem_type)
subitemRoutes.delete('/deleteSubitem_type',SubitemController.deleteSubitem_type)

subitemRoutes.post('/postSubitem',upload.array("image"),SubitemController.postSubitem)
subitemRoutes.post('/putSubitem',upload.array("image"),SubitemController.putSubitem)
subitemRoutes.delete('/deleteSubitem',SubitemController.deleteSubitem)
subitemRoutes.post('/getAllSubitem',SubitemController.getAllSubitem)

subitemRoutes.post('/postSet',upload.array("image"),SubitemController.postSet)
subitemRoutes.post('/putSet',upload.array("image"),SubitemController.putSet)
subitemRoutes.delete('/deleteSet',SubitemController.deleteSet)
subitemRoutes.post('/getAllSet',SubitemController.getAllSet)


subitemRoutes.post('/postSetSubitemType',upload.array("image"),SubitemTypeController.postSetSubitemType)
subitemRoutes.post('/putSetSubitemType',upload.array("image"),SubitemTypeController.putSetSubitemType)
subitemRoutes.delete('/deleteSetSubitemType',SubitemTypeController.deleteSetSubitemType)
// {"id":1,"name":"fish","start_time":"17:00:00","end_time":"22:00:00","company_id":1,"branch_id":1,"price":20,"product_category_id":2,"price":90,"department_id":1,"is_launched":true,"is_time_consuming":true,"is_kiosk":true,"end_time":"10:00:00","start_time":"20:00:00"} 