import { Request, Response } from "express";
import { getOrderService } from "./getOrderServices";
import { logger } from "../../utils/logger";
import { Server as SocketIO } from "socket.io";
export class getOrderController {
    constructor(private getOrderService: getOrderService,
      private io: SocketIO) { }
    
    getOrders =async(req:Request,res:Response)=>{
        try{
         // console.log(1)
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const stage = req.body.stage
          const result = await this.getOrderService.getOrders(company_id,branch_id,stage)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
 
      getOrderDetailsForProduct =async(req:Request,res:Response)=>{
        try{
            const company_id = req.body.company_id
            const branch_id = req.body.branch_id
            const stage = req.body.stage
            //console.log("getOrderDetailsForProduct",company_id,branch_id)
          const result = await this.getOrderService.getOrderDetailsForProduct(company_id,branch_id,stage)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      changeStageOrderDetailsForProductWaiter  =async(req:Request,res:Response)=>{
        try{
            const id = req.body.id
            
          //  console.log("changeStageOrderDetailsForProductWaiter",id)
          const result = await this.getOrderService.changeStageOrderDetailsForProductWaiter(id)
          this.io.emit("packed", "By Jupiter")
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }


      getOrderDetailsForDiscountSetWaiter =async(req:Request,res:Response)=>{
        try{
            const company_id = req.body.company_id
            const branch_id = req.body.branch_id
            const stage = req.body.stage
            //console.log("getOrderDetailsForDiscountSetWaiter",company_id,branch_id)
          const result = await this.getOrderService.getOrderDetailsForDiscountSet(company_id,branch_id,stage)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      getDiscountSetOrderDetailsWaiter =async(req:Request,res:Response)=>{
        try{
            const company_id = req.body.company_id
            const branch_id = req.body.branch_id
            const stage = req.body.stage
            console.log("getDiscountSetOrderDetailsWaiter",company_id,branch_id)
          const result = await this.getOrderService.getDiscountSetOrderDetails(company_id,branch_id,stage)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      changeStageDiscountSetOrderDetailsWaiter  =async(req:Request,res:Response)=>{
        try{
            const id = req.body.id
            
           // console.log("changeStageDiscountSetOrderDetailsWaiter ",id)
          const result = await this.getOrderService.changeStageDiscountSetOrderDetailsWaiter(id)
          this.io.emit("packed", "By Jupiter")
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }



  }
