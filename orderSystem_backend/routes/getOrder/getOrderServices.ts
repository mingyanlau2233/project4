import {Knex} from 'knex';


export class getOrderService{
    constructor(private knex:Knex){}

    async getOrders(company_id:number,branch_id:number,stage:number){
        
        const trx = await this.knex.transaction()
        try {
 
             let result =  await trx("orders")
             .select("id", "closed_at", "created_at", "updated_at ","session_id")
             .where('company_id', "=", company_id).where("branch_id", "=", branch_id)
             .orderBy("id","asc")
             let notYetFinish:any[] = []
             for (let i of result ){

                let check = await trx("order_details").select("*")
                .where("orders_id","=",i.id).where("stage","<",stage)
                if(check[0]){


                    notYetFinish.push(i)
     
                }
                
             }
                 
            await trx.commit();
            return notYetFinish
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("getOrderService encounter error")
            return false
        }
    }


    async getOrderDetailsForProduct(company_id:number,branch_id:number,stage:number){
        
        const trx = await this.knex.transaction()
        try {
 
             let result = await await trx("order_details")
             .select("")
             .where('company_id',"=",company_id)
             .where('branch_id',"=",branch_id)
             .where("stage","<",stage)
             .whereNotNull('product_id')
             .orderBy("id","asc")
                
             //console.log(result)
                 
            await trx.commit();
            return result
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("getOrderService encounter error")
            return false
        }
    }

    async changeStageOrderDetailsForProductWaiter(id:number){
        
        const trx = await this.knex.transaction()
        try {
             let stage = 3
             let data = {stage}
              await trx("order_details")
             .update(data)
             .where('id',"=",id)
     
            await trx.commit();
            return {message:"updated success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("getOrderService encounter error")
            return false
        }
    }

    async getOrderDetailsForDiscountSet(company_id:number,branch_id:number,stage:number){
        
        const trx = await this.knex.transaction()
        try {
 
             let result =  await trx("order_details")
             .select("")
             .where('company_id',"=",company_id)
             .where('branch_id',"=",branch_id)
             .where("stage","<",stage)
             .whereNotNull('discount_set_order_id')
             .orderBy("id","asc")
                
           //  console.log("getOrderDetailsForDiscountSet",result)
                 
            await trx.commit();
            return result
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("getOrderService encounter error")
            return false
        }
    }

    async getDiscountSetOrderDetails(company_id:number,branch_id:number,stage:number){
        
        const trx = await this.knex.transaction()
        try {
 
             let result =  await trx("discount_set_order_details")
             .select("")
             .where('company_id',"=",company_id)
             .where('branch_id',"=",branch_id)
             .where("stage","<",stage)
             .orderBy("id","asc")
                
            // console.log("getDiscountSetOrderDetails",result)
                 
            await trx.commit();
            return result
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("getOrderService encounter error")
            return false
        }
    }

    async changeStageDiscountSetOrderDetailsWaiter(id:number){
        
        const trx = await this.knex.transaction()
        try {
            //console.log("changeStageDiscountSetOrderDetailsWaiterService ",id)
             let stage = 3
             let data = {stage}
              await trx("discount_set_order_details")
             .update(data)
             .where('id',"=",id)
             let discount_set_order_id =
             await trx("discount_set_order_details").select("discount_set_order_id")
             .where('id',"=",id)
             discount_set_order_id =discount_set_order_id[0]["discount_set_order_id"] 

             let check = await trx("discount_set_order_details")
             .where('discount_set_order_id',"=",discount_set_order_id)
             .where("stage","<",3)
             //console.log("check",check)
             if(!check[0]){
                 console.log('here')
                let stage = 3
                let data = {stage}
                await trx("order_details")
                .update(data)
                .where('discount_set_order_id',"=",discount_set_order_id)
               
             }

            await trx.commit();
            return {message:"updated success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("getOrderService encounter error")
            return false
        }
    }
}

