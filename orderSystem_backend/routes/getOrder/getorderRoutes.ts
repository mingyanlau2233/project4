import express from "express";
import {knex,io} from '../../app'
import { getOrderService } from "./getOrderServices";
import {getOrderController} from "./getOrderController"

export const getOrderRoutes = express.Router()

const GetOrderService = new getOrderService(knex)
const GetOrderController = new getOrderController(GetOrderService,io)
getOrderRoutes.post('/getOrders',GetOrderController.getOrders )
getOrderRoutes.post('/getOrderDetailsForProduct',GetOrderController.getOrderDetailsForProduct )
getOrderRoutes.post('/changeStageOrderDetailsForProductWaiter',GetOrderController.changeStageOrderDetailsForProductWaiter )
getOrderRoutes.post('/getOrderDetailsForDiscountSetWaiter',GetOrderController.getOrderDetailsForDiscountSetWaiter )
getOrderRoutes.post('/getDiscountSetOrderDetailsWaiter',GetOrderController.getDiscountSetOrderDetailsWaiter )

getOrderRoutes.post('/changeStageDiscountSetOrderDetailsWaiter',GetOrderController.changeStageDiscountSetOrderDetailsWaiter )