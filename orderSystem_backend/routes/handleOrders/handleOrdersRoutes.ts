import express from "express";
import {knex,io} from '../../app'
import { handleOrdersService } from "./handleOrdersService";
import {handleOrdersController} from "./handleOrdersController"
import { isLoggedInAPI } from "../../utils/guards";

export const handleOrdersRoutes = express.Router()

const HandleOrdersService = new handleOrdersService(knex)
const HandleOrdersController = new handleOrdersController(HandleOrdersService,io)
handleOrdersRoutes.post('/getOrderDetailsDepartment',isLoggedInAPI,HandleOrdersController.getOrderDetailsDepartment)
handleOrdersRoutes.post('/putOrderDetailsStage',isLoggedInAPI,HandleOrdersController.putOrderDetailsStage)
handleOrdersRoutes.post('/getDiscountSetDepartment',isLoggedInAPI,HandleOrdersController.getDiscountSetDepartment)
handleOrdersRoutes.post('/putDiscountSetStage',isLoggedInAPI,HandleOrdersController.putDiscountSetStage)