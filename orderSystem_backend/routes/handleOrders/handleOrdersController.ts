import { Request, Response } from "express";
import { handleOrdersService } from "./handleOrdersService";
import { logger } from "../../utils/logger";
import { Server as SocketIO } from "socket.io";
export class handleOrdersController {
  constructor(private handleOrdersService: handleOrdersService,
    private io: SocketIO) { }

  getOrderDetailsDepartment = async (req: Request, res: Response) => {
    try {
      const company_id = req.user?.company_id;
      const branch_id = req.user?.branch_id;
      const department_id = req.body.department_id;
      if (!company_id || !branch_id) {
        res.status(404).json({ message: "Wrong company or branch!" });
        return;
      }
      const result = await this.handleOrdersService.getOrderDetailsDepartment(company_id, branch_id, department_id);
      res.json(result);
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: `internal server error` });
      return;
    }
  }

  putOrderDetailsStage = async (req: Request, res: Response) => {
    try {
      const company_id = req.user?.company_id;
      const branch_id = req.user?.branch_id;
      const id = req.body.id;
      if (!company_id || !branch_id) {
        res.status(404).json({ message: "Wrong company or branch!" });
        return;
      }
      const result = await this.handleOrdersService.putOrderDetailsStage(id);
      this.io.emit("handledOrder", "By Jupiter")
      res.json(result);
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: `internal server error` });
    }
  }

  getDiscountSetDepartment = async (req: Request, res: Response) => {
    try {
      const company_id = req.user?.company_id;
      const branch_id = req.user?.branch_id;
      const department_id = req.body.department_id;
      if (!company_id || !branch_id) {
        res.status(404).json({ message: "Wrong company or branch!" });
        return;
      }
      const result = await this.handleOrdersService.getDiscountSetDepartment(company_id, branch_id, department_id);
      res.json(result);
      return;
    } catch (err) {
      logger.error(err.message);
      res.status(500).json({ message: `internal server error` });
    }
  }

  putDiscountSetStage = async (req: Request, res: Response) => {
    try {
      const company_id = req.user?.company_id;
      const branch_id = req.user?.branch_id;
      const id = req.body.id
      if (!company_id || !branch_id) {
        res.status(404).json({ message: "Wrong company or branch!" });
        return;
      }
      const result = await this.handleOrdersService.putDiscountSetStage(id)
      this.io.emit("handledOrder", "By Jupiter")
      res.json(result)
      return
    } catch (err) {
      logger.error(err.message)
      res.status(500).json({ message: `internal server error` })
    }
  }



}
