import { Knex } from 'knex';
import { logger } from '../../utils/logger';

export class handleOrdersService {
    constructor(private knex: Knex) { }

    async getOrderDetailsDepartment(company_id: number, branch_id: number, department_id: number) {
        try {
            // console.log(44444444)
            let result = await this.knex("order_details")
                 .select("order_details.id as order_details_id",
                 "order_details.qty","order_details.orders_id","product.name as product_name",
                 "product.department_id","order_details.stage")
                .leftJoin("product", "order_details.product_id", "product.id")
                //.where("product.department_id", department_id)
                .where("order_details.company_id", company_id)
                .where("order_details.branch_id", branch_id)
                .where("stage", "=", 1)
                //  .whereNotNull('product_id')
                .orderBy("order_details.id", "asc")
               // console.log(result)
            return result;
        } catch (err) {
            logger.error(err.message);
            console.log("handleOrdersService encounter error")
            return false;
        }
    }

    async putOrderDetailsStage(id: number) {
        const trx = await this.knex.transaction()
        try {
            //console.log(id)
            let stage = 2
           let data = { stage }
            await trx("order_details")
                .update(data)
                .where('id', id)
            await trx.commit();
            return { message: "updated success" }
        } catch (err) {
            logger.error(err.message);
            await trx.rollback()
            console.log("handleOrdersService encounter error")
            return false
        }
    }

    async getDiscountSetDepartment(company_id: number, branch_id: number, department_id: number) {
        //console.log("helhdfghgfgjgvdrcrtgbsahdbasygvygl",company_id, branch_id, department_id)
        try {
            let result = []
            let discount_set_order_id = await this.knex("discount_set_order")
            .select("id as discount_set_order_id")
              .where("is_confirm","=",true)
            .where('company_id', company_id)
            .where('branch_id', branch_id)
            .orderBy("id","asc")

            for(let id of discount_set_order_id){
                let orders_id = await this.knex("order_details").
                where("discount_set_order_id",id.discount_set_order_id)
               //console.log("orders_id",orders_id[0]["orders_id"])
               orders_id=orders_id[0]["orders_id"]

            let discount_set_order_details = await this.knex("discount_set_order_details")
            .where("discount_set_order_id","=",id.discount_set_order_id)
            .where("stage", "=", 1)
            .orderBy("id", "asc")
                 for (let id2 of discount_set_order_details){
                     let details = 
                     await this.knex("discount_set_order_details")
                     
                     .leftJoin("product","product.id","=","discount_set_order_details.product_id")
                     .where("discount_set_order_details.id","=",id2.id)
                     details=details[0]
                     details["orders_id"]=orders_id
                     details["id"]=id2.id
                   //  console.log(details)
                     result.push(details)
                 }
            }


            
            return result;

        
        } catch (err) {
            logger.error(err.message);
            console.log("handleOrdersService encounter error")
            return false
        }
    }

    async putDiscountSetStage(id: number) {

        const trx = await this.knex.transaction()
        try {
            let stage = 2
            let data = { stage }
           // console.log(id)
            await trx("discount_set_order_details")
                .update(data)
                .where('id', id)
            let discount_set_order_id =
                await trx("discount_set_order_details").select("discount_set_order_id")
                    .where('id', id)
            discount_set_order_id = discount_set_order_id[0]["discount_set_order_id"]

            let check = await trx("discount_set_order_details")
                .where('discount_set_order_id', "=", discount_set_order_id)
                .where("stage", "<", 3)
            //console.log("check",check)
            if (!check[0]) {
            //    console.log('here')
                let stage = 2
                let data = { stage }
                await trx("order_details")
                    .update(data)
                    .where('discount_set_order_id', discount_set_order_id)
            }
            await trx.commit();
            return { message: "updated success" }
        } catch (err) {
            logger.error(err.message);
            await trx.rollback()
            console.log("handleOrdersService encounter error")
            return false
        }
    }
}

