import { Knex } from 'knex';
import { logger } from '../../utils/logger';

export class deskAndSessionService {
    constructor(private knex: Knex) { }

    async getDesk(company_id: number, branch_id: number) {
        try {
            console.log("getDesk")
            const sql = `SELECT id, sequence, (SELECT JSON_AGG(seat.*) from seat where seat.desk_id = desk.id) as seats from desk where desk.company_id = ? and desk.branch_id = ? group by desk.id order by sequence ASC`
            const result = await this.knex.raw(sql, [company_id, branch_id])
            //console.log(sql)
            //console.log(result)
            return result;
        } catch (err) {
            logger.error(err.message);
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }



    async postDesk(data: {}) {
        const trx = await this.knex.transaction();
        try {

            console.log("postDesk",data)
            let maxSequence = await trx("desk").max("sequence")
            if (maxSequence) {
                maxSequence = maxSequence[0]["max"] + 1
                console.log(maxSequence)

                data["sequence"] = maxSequence
                console.log("postDesk",data)
                const id = (await trx("desk").insert(data).returning("id"))[0];
                await trx.commit();
                return { id: id, message: "post success" };
            }
            data["sequence"] = 1
            const id = (await trx("desk").insert(data).returning("id"))[0];
            await trx.commit();
            return { id: id, message: "post success" };
        } catch (err) {
            logger.error(err.message);
            await trx.rollback();
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }


    async deleteDesk(desk_id: number) {
        const trx = await this.knex.transaction()
        try {
            await trx("seat").where("desk_id", desk_id).del();
            await trx("desk").where("id", desk_id).del();
            await trx.commit();
            return { message: "Delete success" }
        } catch (err) {
            logger.error(err.message);
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }

     async deleteAllDesk(branch_id: number) {
        const trx = await this.knex.transaction()
        try {
            await trx("seat").where("branch_id", branch_id).del();
            await trx("desk").where("branch_id", branch_id).del();
            await trx.commit();
            return { message: "Delete success" }
        } catch (err) {
            logger.error(err.message);
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }


    async getSeat(company_id: number, branch_id: number, desk_id: number) {
        try {
            let result = await this.knex("seat").select("id", "sequence")
                .where('company_id', company_id)
                .where("branch_id", branch_id)
                .where("desk_id", desk_id);
            console.log("getSeat",result)
                return result;
        } catch (err) {
            logger.error(err.message);
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }

    async postSeat(data: object) {
        const trx = await this.knex.transaction();
        try {
            console.log("postSeatvvvvvvv", data)

            let maxSequence = await trx("seat").where("desk_id","=",data["desk_id"]).max("sequence")
            console.log(maxSequence)
            // let maxSequence = await this.knex.raw('SELECT max(sequence) FROM seat WHERE desk_id = ? ORDER BY sequence DESC', [data["desk_id"]]);
            if (maxSequence) {
                maxSequence = maxSequence[0]["max"] + 1
                data["sequence"] = maxSequence
                data["is_occupied"] = false
                await trx("seat").insert(data);
                await trx.commit();
                return
            }
            data["sequence"] = 1
            await trx("seat").insert(data);
            await trx.commit();
            return { message: "post success" };
        } catch (err) {
            console.log(err.message);
            await trx.rollback();
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }

    async putSeat(id: number, data: object) {
        const trx = await this.knex.transaction();
        try {
            await trx("seat").update(data).where("id", id);
            await trx.commit();
            return { message: "seat update success" };
        } catch (err) {
            logger.error(err.message);
            await trx.rollback();
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }

    async deleteSeat(id: number) {
        const trx = await this.knex.transaction()
        try {
            await trx("seat").where("id", id).del();
            await trx.commit();
            return { message: "Delete success" }
        } catch (err) {
            logger.error(err.message);
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }

    async getSession(company_id: number, branch_id: number) {
        try {
            let result = await this.knex("session").select("id", "is_client_left", "is_takeaways", "total_price")
                .where("company_id", company_id)
                .where("branch_id", branch_id);
            return result;
        } catch (err) {
            logger.error(err.message);
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }

    async postSession(data: object,seatData:[]) {
        const trx = await this.knex.transaction();
        try {
            // console.log(data,seatData)
            // let deskData= await trx("desk").join()
            // .select("id","sequence").where("id")
            await trx.commit();
            return { message: "post success" };
        } catch (err) {
            logger.error(err.message);
            await trx.rollback();
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }

    async putSession(id: string, data: object) {
        const trx = await this.knex.transaction();
        try {
            await trx("session").update(data).where("id", id);
            await trx.commit();
            return { message: "session update success" };
        } catch (err) {
            logger.error(err.message);
            await trx.rollback();
            console.log("deskAndSessionService encounter error");
            return false;
        }
    }


async getSeating (company_id: number, branch_id: number) {
    const trx = await this.knex.transaction();
    try {

        let desk = await  trx("desk").where("company_id","=", company_id).where("branch_id","=", branch_id)
        //console.log(desk)
        let data = []

        for (let i of desk ){

            let seats = await  trx("seat").where("desk_id","=",i.id)
            i["seats"]=seats
            data.push(i)
        }
            
        console.log("getSeat",data)
            return data;
    } catch (err) {
        logger.error(err.message);
        console.log("deskAndSessionService encounter error");
        return false;
    }
}

}
        // async getArea(company_id:number,branch_id:number){
    //     try {
    //         let result = await this.knex("area")
    //                     .select("id", "sequence", "description")
    //                     .whereRaw('status=?',["active"])
    //                     .where('company_id', company_id)
    //                     .where("branch_id", branch_id);
    //         return result;
    //     } catch (err) {
    //         logger.error(err.message);
    //         console.log("deskAndSessionService encounter error");
    //         return false;
    //     }
    // }

    // async postArea(data:object){
    //     const trx = await this.knex.transaction();
    //     try {
    //         await trx("area").insert(data);         
    //         await trx.commit();
    //         return {message:"post success"};
    //     } catch (err) {
    //         logger.error(err.message);
    //         await trx.rollback();
    //         console.log("deskAndSessionService encounter error");
    //         return false;
    //     }
    // }

    // async putArea(id:number, data:object){
    //     const trx = await this.knex.transaction();
    //     try {
    //         await trx("area").update(data).where("id",id);         
    //         await trx.commit();
    //         return {message:"area update success"};
    //     } catch (err) {
    //         logger.error(err.message);
    //         await trx.rollback();
    //         console.log("deskAndSessionService encounter error");
    //         return false;
    //     }
    // }

    // async putAreaStatus(id:number, status:string, company_id:number, branch_id:number){
    //     const trx = await this.knex.transaction();
    //     try {
    //         await trx("area").update({status})
    //         .where("id",id)
    //         .where('company_id', company_id)
    //         .where("branch_id", branch_id);         
    //         await trx.commit();
    //         return {message:"area update success"};
    //     } catch (err) {
    //         logger.error(err.message);
    //         await trx.rollback();
    //         console.log("deskAndSessionService encounter error");
    //         return false;
    //     }
    // }

    // async deleteSession(id:string){
    //     const trx = await this.knex.transaction();
    //     try {
    //         await trx("session").where("id", id).del();         
    //         return {message:"delete success"};
    //     } catch (err) {
    //         logger.error(err.message);
    //         await trx.rollback();
    //         console.log("deskAndSessionService encounter error");
    //         return false;
    //     }
    // }

      // async getAllDesk(company_id:number, branch_id:number){
    //     try {
    //         const desk_idData = await this.knex.select("id", "sequence").from("desk")
    //                             .where('company_id', company_id)
    //                             .where("branch_id", branch_id)

    //         return desk_idData;
    //     } catch (err) {
    //         logger.error(err.message);
    //         console.log("deskAndSessionService encounter error");
    //         return false;
    //     }
    // }

       // async getAllSeat(company_id:number,branch_id:number){
    //     try {
    //         const seat_idData = await this.knex("seat").select("id", "sequence")
    //         .where('company_id', company_id).where("branch_id", branch_id);
    //         let result = []
    //         for (let key of seat_idData){
    //             let idData = await this.knex("seat").select("id", "sequence", "desk_id")
    //                          .where("desk_id", key.id);
    //             if (idData.length > 0) {
    //                 result.push(idData)
    //             }}
    //         return result;
    //     } catch (err) {
    //         logger.error(err.message);
    //         console.log("deskAndSessionService encounter error");
    //         return false;
    //     }
    // }

    // async putDeskSequence(id:number, sequence:number, company_id:number, branch_id:number){
    //     const trx = await this.knex.transaction();
    //     try{
    //         await trx("desk").update(sequence)
    //         .where("id", id)
    //         .where('company_id', company_id)
    //         .where("branch_id", branch_id);
    //         await trx.commit();
    //         return {message:"sequence update success"};
    //     } catch (err) {
    //         logger.error(err.message);
    //         await trx.rollback();
    //         console.log("deskAndSessionService encounter error");
    //         return false;
    //     }
    // }

    // async putDeskStatus(id:number, status:string, company_id:number, branch_id:number){
    //     const trx = await this.knex.transaction();
    //     try{
    //         await trx("desk").update(status)
    //         .where("id", id)
    //         .where('company_id', company_id)
    //         .where("branch_id", branch_id);
    //         await trx("seat").update(status)
    //         .where("desk_id", id)
    //         .where('company_id', company_id)
    //         .where("branch_id", branch_id);
    //         await trx.commit();
    //         return {message:"status update success"};
    //     } catch (err) {
    //         logger.error(err.message);
    //         await trx.rollback();
    //         console.log("deskAndSessionService encounter error");
    //         return false;
    //     }
    // }

    // async putAllDeskStatus(status:string, company_id:number, branch_id:number){
    //     const trx = await this.knex.transaction();
    //     try{
    //         await trx("desk").update(status)
    //         .where('company_id', company_id)
    //         .where("branch_id", branch_id);
    //         await trx("seat").update(status)
    //         .where('company_id', company_id)
    //         .where("branch_id", branch_id);
    //         await trx.commit();
    //         return {message:"status update success"};
    //     } catch (err) {
    //         logger.error(err.message);
    //         await trx.rollback();
    //         console.log("deskAndSessionService encounter error");
    //         return false;
    //     }
    // }