import express from "express";
import { knex } from '../../app';
import { deskAndSessionService } from "./deskAndSessionService";
import { deskAndSessionController } from "./deskAndSessionController";
import { isLoggedInAPI } from "../../utils/guards";
export const deskAndSessionRoutes = express.Router()

const DeskAndSessionService = new deskAndSessionService(knex);
const DeskAndSessionController = new deskAndSessionController(DeskAndSessionService);
// deskAndSessionRoutes.get('/getArea',DeskAndSessionController.getArea);
// deskAndSessionRoutes.post('/postArea',DeskAndSessionController.postArea);
// deskAndSessionRoutes.put('/putArea',DeskAndSessionController.putArea);
// deskAndSessionRoutes.put('/putAreaStatus',DeskAndSessionController.putAreaStatus);

deskAndSessionRoutes.get('/getDesk', isLoggedInAPI,DeskAndSessionController.getDesk);
deskAndSessionRoutes.post('/postDesk', isLoggedInAPI, DeskAndSessionController.postDesk);
// deskAndSessionRoutes.put('/putDeskSequence',DeskAndSessionController.putDeskSequence);
deskAndSessionRoutes.delete('/deleteDesk', isLoggedInAPI, DeskAndSessionController.deleteDesk);
deskAndSessionRoutes.delete('/deleteAllDesk', isLoggedInAPI, DeskAndSessionController.deleteAllDesk);

deskAndSessionRoutes.get('/getSeat', isLoggedInAPI, DeskAndSessionController.getSeat);
deskAndSessionRoutes.post('/postSeat', isLoggedInAPI, DeskAndSessionController.postSeat);
deskAndSessionRoutes.put('/putSeat', isLoggedInAPI, DeskAndSessionController.putSeat);
deskAndSessionRoutes.delete('/deleteSeat', isLoggedInAPI, DeskAndSessionController.deleteSeat);

deskAndSessionRoutes.get('/getSession', isLoggedInAPI, DeskAndSessionController.getSession);
deskAndSessionRoutes.post('/postSession', isLoggedInAPI, DeskAndSessionController.postSession);
deskAndSessionRoutes.put('/putSession', isLoggedInAPI, DeskAndSessionController.putSession);

deskAndSessionRoutes.get('/getSeating', isLoggedInAPI, DeskAndSessionController.getSeating);
