import {Request, Response} from "express";
import {deskAndSessionService} from "./deskAndSessionService";
import {logger} from "../../utils/logger"

export class deskAndSessionController {
    constructor(private deskAndSessionService: deskAndSessionService) { }

    
    getDesk = async(req:Request, res: Response) => {
        try {
          const company_id = req.user?.company_id;
          const branch_id = req.user?.branch_id;
          console.log("getDesk",company_id)
          if (!company_id || !branch_id) {
            res.status(404).json({ message: "Wrong company or branch!"})
          }
          const result = (await this.deskAndSessionService.getDesk(company_id, branch_id)).rows;
          res.json({ result });
          return;
        } catch(err) {
          logger.error(err.message);
          res.status(500).json({message:`internal server error`})
        }
    } 

    postDesk = async(req:Request, res: Response) => {
        try {
          const company_id = req.user?.company_id;
          const branch_id = req.user?.branch_id;
          if (!company_id || !branch_id) {
            res.status(404).json({ message: "Wrong company or branch!"})
          }
          const data = {
            company_id, branch_id
          }
          console.log("postDeskController",)
          const result = await this.deskAndSessionService.postDesk(data);
          //console.log(result);

          res.json({ result });
          return;
        } catch(err) {
          logger.error(err.message);
          res.status(500).json({message:`internal server error`})
        }
    } 

    deleteAllDesk = async (req:Request, res: Response) => {
      try {
        const company_id = req.user?.company_id;
        const branch_id = req.user?.branch_id;
        if (!company_id || !branch_id) {
          res.status(404).json({ message: "Wrong company or branch!"})
        }
        await this.deskAndSessionService.deleteAllDesk(branch_id);
        res.json({ message: "Delete success"})
      } catch(err) {
        logger.error(err.message);
        res.status(500).json({message:`internal server error`})
      }
    }

    deleteDesk = async (req:Request, res: Response) => {
      try {
        const company_id = req.user?.company_id;
        const branch_id = req.user?.branch_id;
        if (!company_id || !branch_id) {
          res.status(404).json({ message: "Wrong company or branch!"})
        }
        const { id}= req.body;
        await this.deskAndSessionService.deleteDesk(id);
        res.json({ message: "Delete success"})
      } catch(err) {
        logger.error(err.message);
        res.status(500).json({message:`internal server error`})
      }
    }

    getSeat = async(req:Request, res: Response) => {
        // try {
        //   const { desk_id} = req.body;
        //   const company_id = req.user?.company_id;
        //   const branch_id = req.user?.branch_id;
        //   if (!company_id || !branch_id) {
        //     res.status(404).json({ message: "Wrong company or branch!"})
        //   }
        //   const result = await this.deskAndSessionService.getSeat(company_id, branch_id, desk_id);
        //   res.json({ result });
        //   return;
        // } catch(err) {
        //   logger.error(err.message);
        //   res.status(500).json({message:`internal server error`})
        // }
    } 

    postSeat = async(req:Request, res: Response) => {
        try {
          const company_id = req.user?.company_id;
          const branch_id = req.user?.branch_id;
        //  console.log("post Seat", company_id)
          if (!company_id || !branch_id) {
            res.status(404).json({ message: "Wrong company or branch!"})
          }
          const data = req.body;
          data["company_id"]=company_id
          data["branch_id"] =branch_id
          const result = await this.deskAndSessionService.postSeat(data);
          res.json({ result });
          return;
        } catch(err) {
          logger.error(err.message);
          res.status(500).json({message:`internal server error`})
        }
    } 

    putSeat = async(req:Request, res: Response) => {
      try {
        const company_id = req.user?.company_id;
        const branch_id = req.user?.branch_id;
        if (!company_id || !branch_id) {
          res.status(404).json({ message: "Wrong company or branch!"})
        }
        const data = req.body;
        const result = await this.deskAndSessionService.putSeat(data.id, data);
        res.json({ result });
        return;
      } catch(err) {
        logger.error(err.message);
        res.status(500).json({message:`internal server error`})
      }    
    } 

    deleteSeat = async (req:Request, res: Response) => {
      try {
        const company_id = req.user?.company_id;
        const branch_id = req.user?.branch_id;
        if (!company_id || !branch_id) {
          res.status(404).json({ message: "Wrong company or branch!"})
        }
        const id = req.body.id;
        await this.deskAndSessionService.deleteSeat(id)
        res.json({ message: "Delete success"})
      } catch(err) {
        logger.error(err.message);
        res.status(500).json({message:`internal server error`})
      }
    }

    getSession = async(req:Request, res: Response) => {
        try {
          const company_id = req.user?.company_id;
          const branch_id = req.user?.branch_id;
          if (!company_id || !branch_id) {
            res.status(404).json({ message: "Wrong company or branch!"})
          }
          const result = await this.deskAndSessionService.getSession(company_id, branch_id);
          res.json({ result });
          return;
        } catch(err) {
          logger.error(err.message);
          res.status(500).json({message:`internal server error`})
        }
    } 
    
    postSession = async(req:Request, res: Response) => {
        try {
          const company_id = req.user?.company_id;
          const branch_id = req.user?.branch_id;
          const seatData = req.body.seatData
          if (!company_id || !branch_id) {
            res.status(404).json({ message: "Wrong company or branch!"})
          }
          const data = req.body;
          console.log(data);
          const result = await this.deskAndSessionService.postSession(data,seatData);
          res.json({ result });
          return;
        } catch(err) {
          logger.error(err.message);
          res.status(500).json({message:`internal server error`})
        }
    } 
    
    putSession = async(req:Request, res: Response) => {
        try {
          const company_id = req.user?.company_id;
          const branch_id = req.user?.branch_id;
          if (!company_id || !branch_id) {
            res.status(404).json({ message: "Wrong company or branch!"})
          }
          const data = req.body;
          const result = await this.deskAndSessionService.putSession(data.id, data);
          res.json({ result });
          return;
        } catch(err) {
          logger.error(err.message);
          res.status(500).json({message:`internal server error`})
        }
    } 

    getSeating = async(req:Request, res: Response) => {
      try {
        console.log("Seating")
        const company_id = req.user?.company_id;
        const branch_id = req.user?.branch_id;
        console.log("Seating",company_id)
        if (!company_id || !branch_id) {
          res.status(404).json({ message: "Wrong company or branch!"})
        }
        const result = await this.deskAndSessionService.getSeating(company_id, branch_id)
        res.json({ result });
        return;
      } catch(err) {
        logger.error(err.message);
        res.status(500).json({message:`internal server error`})
      }
  } 
}
    // getArea = async(req:Request, res: Response) => {
    //     try{
    //       const { company_id, branch_id } = req.body;
    //       const result = await this.deskAndSessionService.getArea(company_id, branch_id);
    //       res.json({ result });
    //       return;
    //     } catch(err) {
    //       logger.error(err.message);
    //       res.status(500).json({message:`internal server error`})
    //     }
    // } 

    // postArea = async(req:Request, res: Response) => {
    //     try{
    //       const data = req.body;
    //       const result = await this.deskAndSessionService.postArea(data);
    //       res.json({ result });
    //       return;
    //     } catch(err) {
    //       logger.error(err.message);
    //       res.status(500).json({message:`internal server error`})
    //     }
    // } 

    // putArea = async(req:Request, res: Response) => {
    //     try{
    //       const data = req.body;
    //       const result = await this.deskAndSessionService.putArea(data.id, data);
    //       res.json({ result });
    //       return;
    //     } catch(err) {
    //       logger.error(err.message);
    //       res.status(500).json({message:`internal server error`})
    //     }
    // }

    // putAreaStatus = async(req:Request, res: Response) => {
    //     try{
    //       const { status, id, company_id, branch_id } = req.body;
    //       const result = await this.deskAndSessionService.putAreaStatus(id, status, company_id, branch_id);
    //       res.json({ result });
    //       return;
    //     } catch(err) {
    //       logger.error(err.message);
    //       res.status(500).json({message:`internal server error`})
    //     }    
    // }

      // putDeskSequence = async(req:Request, res: Response) => {
    //     try{
    //       const { id, sequence, company_id, branch_id } = req.body;
    //       await this.deskAndSessionService.putDeskSequence(id, sequence, company_id, branch_id);
    //       res.json({message:`update success`});
    //       return;
    //     } catch(err) {
    //       logger.error(err.message);
    //       res.status(500).json({message:`internal server error`})
    //     }
    // }

    // getAllSeat = async(req:Request, res: Response) => {
    //   try {
    //     const { company_id, branch_id } = req.body;
    //     const result = await this.deskAndSessionService.getAllSeat(company_id, branch_id);
    //     res.json({ result });
    //     return;
    //   } catch(err) {
    //     logger.error(err.message);
    //     res.status(500).json({message:`internal server error`})
    //   }
    // } 

    



