import { Knex } from 'knex';
export class menuSettingService {
    constructor(private knex: Knex) { }

    async getAvailable_period(company_id: number, branch_id: number) {

        const trx = await this.knex.transaction()
        try {
            let result = await trx("available_period").select("name", "start_time", "end_time", "id")
                .where('company_id', "=", company_id).where("branch_id", "=", branch_id)
            await trx.commit();
            return result
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async putAvailable_period(id: number, data: object) {
        const trx = await this.knex.transaction()
        try {
            await trx("available_period").update(data).where("id", "=", id)

            await trx.commit();
            return { message: "renew success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async deleteAvailable_period(id: number) {

        const trx = await this.knex.transaction()
        try {
            await trx("available_period").where("id", "=", id).del()
            await trx.commit();
            return { message: "delete success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async postAvailable_period(data: object) {

        const trx = await this.knex.transaction()
        try {
            await trx("available_period").insert(data)
            await trx.commit();
            return { message: "post success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async getDepartment(company_id: number, branch_id: number) {

        const trx = await this.knex.transaction()
        try {
            console.log("services",company_id)
            console.log(branch_id)
            
            let result = await trx("department").select("name", "id")
                .where('company_id', "=", company_id).where("branch_id", "=", branch_id)
                // console.log(result)
                await trx.commit();
            return result
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async postDepartment(data: object) {
        const trx = await this.knex.transaction()
        try {
            await trx("department").insert(data)
            await trx.commit();
            return { message: "post success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async putDepartment(id: number, data: object) {
        const trx = await this.knex.transaction()
        try {
            await trx("department").update(data).where("id", "=", id)
            await trx.commit();
            return { message: "renew success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async deleteDepartment(id: number) {

        const trx = await this.knex.transaction()
        try {
            await trx("department").where("id", "=", id).del()
            await trx.commit();
            return { message: "delete success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error encounter error")
            return false
        }
    }

    async postRemarks(data: object) {
        const trx = await this.knex.transaction()
        try {
            await trx("remarks").insert(data)
            await trx.commit();
            return { message: "post success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async deleteRemarks(id: number) {

        const trx = await this.knex.transaction()
        try {
            await trx("order_cart_remarks").where("remarks_id", "=", id).del()
            await trx("remarks_details").where("remarks_id", "=", id).del()
            await trx("remarks").where("id", "=", id).del()
            await trx.commit();
            return { message: "delete success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error encounter error")
            return false
        }
    }

    async putRemarks(id: number, data: object) {
        const trx = await this.knex.transaction()
        //console.log(data)
        try {
            await trx("remarks").update(data).where("id", "=", id)

            await trx.commit();
            return { message: "renew success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async getAllRemarks(company_id: number, branch_id: number) {
        const trx = await this.knex.transaction()
        try {
            const product_category_idData = await trx("product_category").select("id")
                .where('company_id', "=", company_id).where("branch_id", "=", branch_id)
            const subitem_type_idData = await trx("subitem_type").select("id")
                .where('company_id', "=", company_id).where("branch_id", "=", branch_id)
            //console.log(product_category_idData, subitem_type_idData)

            let product_category = []
            for (let id of product_category_idData) {
                let idData = await trx('remarks').select('id', "name", "price", "product_category_id").where("product_category_id", '=', id.id)
                if (idData.length > 0) {
                    product_category.push(idData)
                }
            }
            let subitem_type = []
            for (let id of subitem_type_idData) {
                let idData = await trx('remarks').select('id', "name", "price", "subitem_type_id").where("subitem_type_id", '=', id.id)
                if (idData.length > 0) {
                    subitem_type.push(idData)
                }
            }
            await trx.commit();
            return { product_category, subitem_type }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async getCategoryAndTypeWithoutRemarks(company_id: number, branch_id: number) {
        const trx = await this.knex.transaction()
        try {
            const product_category_idData = await trx("product_category").select("id", "name")
                .where('company_id', "=", company_id).where("branch_id", "=", branch_id)
            const subitem_type_idData = await trx("subitem_type").select("id", "name")
                .where('company_id', "=", company_id).where("branch_id", "=", branch_id)
           // console.log(product_category_idData, subitem_type_idData)

            let product_category = []
            for (let id of product_category_idData) {
                let idData = await trx('remarks').select('id', "name", "price", "product_category_id").where("product_category_id", '=', id.id)
                if (idData.length == 0) {
                    product_category.push(id)
                }
            }
            let subitem_type = []
            for (let id of subitem_type_idData) {
                let idData = await trx('remarks').select('id', "name", "price", "subitem_type_id").where("subitem_type_id", '=', id.id)
                if (idData.length == 0) {
                    subitem_type.push(id)
                }
            }
            await trx.commit();
            return { product_category, subitem_type }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }



    async postProductCategory(data: object) {

        const trx = await this.knex.transaction()
        try {
            await trx("product_category").insert(data)
            await trx.commit();
            return { message: "post success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async putProductCategory(id: number, data: object) {
        const trx = await this.knex.transaction()
        try {
            await trx("product_category").update(data).where("id", "=", id)

            await trx.commit();
            return { message: "renew success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async getProductCategory(company_id: number, branch_id: number) {

        const trx = await this.knex.transaction()
        try {
            let result =  await trx('product_category').select("name","id","is_inSetOnly")
                          .where('company_id',"=",company_id).where("branch_id","=",branch_id).orderBy("id","asc")
            //console.log("getProductCategory services",result)
            await trx.commit();
            return result
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async deleteProductCategory(id: number) {

        const trx = await this.knex.transaction()
        try {

            const product_ids = await trx("product").select("id").where("product_category_id", "=", id)
            //console.log(product_ids)
            for (let id of product_ids) {
                await trx("product_available_period").where("product_id", "=", id.id).del()
                await trx("photo_list").where("product_id", "=", id.id).del()
                await trx("price_list").where("product_id", "=", id.id).del()
                await trx("set_subitem_type").where("product_id", "=", id.id).del()
                //await trx("discount_set_order_details").where("product_id","=",id.id).del()
                await trx("discount_set_details").where("product_id", "=", id.id).del()
                await trx("order_cart").where("product_id", "=", id.id).del()
                //await trx("order_details").where("product_id","=",id.id).del()
                await trx("remarks_details").where("product_id", "=", id.id).del()

                //await trx("ingredient_expected_consumption").where("product_id","=",id.id).del()
                //await trx("set_order_details").where("product_id","=",id.id).del()
            }


            await trx("remarks").where("product_category_id", "=", id).del()
            await trx("product").where("product_category_id", "=", id).del()
            await trx("product_category").where("id", "=", id).del()
            await trx.commit();
            return { message: "delete success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error encounter error")
            return false
        }
    }

    async postProductData(data:object,available_period:[],remarks_details:[],
        price:number,company_id:number,branch_id:number){
        console.log(data,available_period,remarks_details,company_id,branch_id)
      
        const trx = await this.knex.transaction()
        try {

            // let available_period = [1,2,3]
            // let remarks_details = [1,2,3] 
            
           
          //  console.log("service",photoName)
      
            let product_id = await trx("product").insert(data).returning("id")
                product_id= product_id[0]
          
            if (available_period){
            for (let available_periods of available_period){
                let available_period_id=available_periods
                let data = {available_period_id,product_id,company_id,branch_id}
                await trx("product_available_period").insert(data)
            }}

                                 
            await trx.commit();
            return { message: "post success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    } 
    async putProductData(data:object,available_period:[],remarks_details:[],
        price:number,company_id:number,branch_id:number,deletedPhotoName:[],product_id:number,){
        //console.log(data,available_period,remarks_details,photoName,deletedPhotoName)

        const trx = await this.knex.transaction()
        try {

            //  let available_period = [1]
            //  let remarks_details = [1] 
            //  let photoName = ["4.jpg","5.jpg"]
            //  let deletedPhotoName = ["2.jpg"]
            
             if(available_period){
            let oldProduct_available_period = await trx("product_available_period").select("available_period_id").where("product_id","=",product_id)
            console.log("old",oldProduct_available_period)
            console.log("new",available_period)
            

                  await trx("product_available_period").where("product_id","=",product_id).del()
                
            
                for (let news of available_period) {
             
                        let available_period_id = news
                        let data = { available_period_id, product_id, company_id, branch_id }
                        await trx("product_available_period").insert(data)
                    
                }
            
        }
        //     if (remarks_details){
        //     let oldRemarks_details = await trx("remarks_details").where("product_id","=",product_id).returning("remarks_id")
        //     oldRemarks_details = oldRemarks_details
        //     for (let old of oldRemarks_details){
        //         let remain =false
        //         for(let news of remarks_details){
        //            if( news == old.remarks_id ){
        //                remain = true
        //            }
        //         }
        //         if(remain == false){
        //           await trx("remarks_details").where("product_id","=",old.remarks_id).del()
        //         }
        //     }

        //     for (let news of remarks_details){
        //         let newOne = true
        //         for(let old of oldRemarks_details){
        //             if (news == old.remarks_id ) { newOne = false}
        //         }
        //         if(newOne == true){
        //             let remarks_id = news
        //             let data = {remarks_id,product_id,company_id,branch_id}
        //             await trx("remarks_details").insert(data)
        //         }
        //     }
        // }
        //     let price_list_data = {price}
        //     await trx("price_list").update(price_list_data).where("product_id","=",product_id).where("discount_set_id","=",0)
        //     .where("subitem_id","=",0).where("set_id","=",0).where("set_subitem_type_id","=",0)

        //     if (photoName){
        //     for (let photo of photoName){
        //         let data = {photo,product_id,company_id,branch_id}
        //         await trx("photo_list").insert(data)
        //     }}
           
        //     if (deletedPhotoName){
        //     for (let photo of deletedPhotoName){
        //         await trx("photo_list").where("photo","=",photo).del()
        //     }}
         
           await trx("product").update(data).where("id","=",product_id)
             
                     
            await trx.commit();
            return { message: "put success" }
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async deleteProductData(product_id: number) {


        const trx = await this.knex.transaction()
        try {
            console.log("delete_product", product_id, typeof product_id)


            await trx("photo_list").where("product_id", "=", product_id).del()
            await trx("price_list").where("product_id", "=", product_id).del()
            await trx("product_available_period").where("product_id", "=", product_id).del()
            await trx("discount_set_details").where("product_id", "=", product_id).del()
            await trx("remarks_details").where("product_id", "=", product_id).del()
            await trx("order_cart").where("product_id", "=", product_id).del()
            await trx("set_subitem_type").where("product_id", "=", product_id).del()
            await trx("product").where("id", "=", product_id).del()
            await trx.commit();
            return { message: "delete success" }

        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }

    async getAllProductData(company_id: number, branch_id: number, product_category_id: number) {
        const trx = await this.knex.transaction()
        try {
        //     const product_category_idData = await trx("product_category").select("id","name")
        //     .where('company_id',"=",company_id).where("branch_id","=",branch_id).where("product_catergory_id","=",product_catergory_id)
        //   let result = []
                   
                 let dataWithSameProduct_category=[]
                 const product_idData = await trx("product").select("id").where('company_id',"=",company_id)
                 .where("branch_id","=",branch_id).orderBy("id","asc")
                 for (let product_id of product_idData){
                     let data = await trx("product").select("id","name","is_launched","price","photo",
                     "is_time_consuming","is_kiosk","department_id","product_category_id")
                     .where("id","=",product_id.id)

                     data= data[0]
                     data["product_id"]= data["id"]
                     
                     delete data["id"]
                    //  let price= await trx("price_list").select("price").where("product_id","=",product_id.id)
                    //  .where("subitem_id","=",0).where("discount_set_id","=",0).where("set_id","=",0)
                    //  .where('set_subitem_type_id',"=",0)
                    //  if(price.length==1){
                    //      price=price[0].price
                    //      data["price"]= price}
                     
                    //  let photos = await trx("photo_list").select("photo").where("product_id","=",product_id.id)
                    //  .where("subitem_id","=",0).where("discount_set_id","=",0).where("set_id","=",0)
                    //  .where('set_subitem_type_id',"=",0)
                    //  let photoList =[]
                    //  for(let photo of photos ){
                    //     photoList.push(photo.photo)
                    //  }
                    //  data["photo"]= photoList
                    //  let remarks = await trx("remarks_details").select("remarks_id").where("product_id","=",product_id.id)
                    //  let remarksList =[]
                    //  for(let remark of remarks ){
                    //     remarksList.push(remark.remarks_id)
                    //  }
                    //  data["remarks_id"]=remarksList
                     let period = await trx("product_available_period").select("available_period_id").where("product_id","=",product_id.id)
                     let periodList =[]
                     for(let periods of period ){
                        periodList.push(periods.available_period_id)
                     }
                    data['available_period'] =periodList
                    

                    data['breakfast']=false
                    data['lunch']=false
                    data['afternoonTea']=false
                    data['dinner']=false
                    data['nightSnack']=false
                    
                    if(data['available_period'].includes(1)){data['breakfast']=true}
                    if(data['available_period'].includes(2)){data['lunch']=true}
                    if(data['available_period'].includes(3)){data['afternoonTea']=true}
                    if(data['available_period'].includes(4)){data['dinner']=true}
                    if(data['available_period'].includes(5)){data['nightSnack']=true}
                    delete data['available_period']
                    delete data['remarks_id']
                    dataWithSameProduct_category.push(data)
                 }
                 //result.push(dataWithSameProduct_category)
                // let result =
                //console.log(dataWithSameProduct_category)
            
            await trx.commit();
            return dataWithSameProduct_category
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("menuSettingService encounter error")
            return false
        }
    }
}