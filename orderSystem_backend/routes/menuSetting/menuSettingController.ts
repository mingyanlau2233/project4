import { Request, Response } from "express";
import { menuSettingService } from "./menuSettingService";
import { logger } from "../../utils/logger";

export class menuSettingController {
    constructor(private menuSettingService: menuSettingService) { }
    
    getAvailable_period =async(req:Request,res:Response)=>{
        try{
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const result = await this.menuSettingService.getAvailable_period(company_id,branch_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

    putAvailable_period =async(req:Request,res:Response)=>{
        try{
          const id = req.body.id
          const name = req.body.name
          const start_time =req.body.start_time
          const end_time = req.body.end_time
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const data = { name,start_time , end_time,company_id,branch_id}
          const result = await this.menuSettingService.putAvailable_period(id,data)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      deleteAvailable_period =async(req:Request,res:Response)=>{
        try{
          
          const id = req.body.id
          const result = await this.menuSettingService.deleteAvailable_period(id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      postAvailable_period =async(req:Request,res:Response)=>{
        try{
          const name = req.body.name
          const start_time =req.body.start_time
          const end_time = req.body.end_time
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const data = { name,start_time , end_time,company_id,branch_id}
          const result = await this.menuSettingService.postAvailable_period(data)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      getDepartment =async(req:Request,res:Response)=>{
        try{

          
          const company_id = req.user?.company_id 
          const branch_id = req.user?.branch_id
          // console.log("getDepartmentController",company_id)
          // console.log("getDepartmentController",branch_id)
          if (!company_id || !branch_id) {
            res.status(404).json({message: "This is wrong company"})
            return 
          }
          //console.log("getDepartmentController",company_id)
          const result = await this.menuSettingService.getDepartment(company_id,branch_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      postDepartment =async(req:Request,res:Response)=>{
        try{
          const name = req.body.name
          const company_id = req.user?.company_id
          const branch_id = req.user?.branch_id
          const data = { name,company_id,branch_id}
         // console.log(data)
          const result = await this.menuSettingService.postDepartment(data)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      putDepartment =async(req:Request,res:Response)=>{
        try{
          const id = req.body.id
          const name = req.body.name
          const company_id = req.user?.company_id
          const branch_id = req.user?.branch_id
          const data = { name,company_id,branch_id}
          const result = await this.menuSettingService.putDepartment(id,data)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      deleteDepartment =async(req:Request,res:Response)=>{
        try{
          const id = req.body.id
          const result = await this.menuSettingService.deleteDepartment(id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      postRemarks =async(req:Request,res:Response)=>{
        try{
          const name = req.body.name
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const price = req.body.price
          const data = { name,company_id,branch_id,price}
          const result = await this.menuSettingService.postRemarks(data)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      deleteRemarks =async(req:Request,res:Response)=>{
        try{
          const id = req.body.id
          const result = await this.menuSettingService.deleteRemarks(id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      putRemarks =async(req:Request,res:Response)=>{
        try{
          const id = req.body.id
          const name= req.body.name
          const price = req.body.price
          const data ={id,name,price}
          if (req.body.product_category_id !== undefined){ 
             data["product_category_id"]=req.body.product_category_id} 
          if (req.body.subitem_type_id !== undefined){ 
            data["subitem_type_id"]=req.body.subitem_type_id} 
          const result = await this.menuSettingService.putRemarks(id,data)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      getAllRemarks =async(req:Request,res:Response)=>{
        try{
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const result = await this.menuSettingService.getAllRemarks(company_id,branch_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      getCategoryAndTypeWithoutRemarks =async(req:Request,res:Response)=>{
        try{
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const result = await this.menuSettingService.getCategoryAndTypeWithoutRemarks(company_id,branch_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      postProductCategory =async(req:Request,res:Response)=>{
        try{
        //  console.log(" postProductCategory",req.body)
          const name = req.body.name
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const is_inSetOnly =req.body.subitem
          const data = { name,company_id,branch_id,is_inSetOnly}
        //  console.log(data)
          const result = await this.menuSettingService.postProductCategory(data)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      putProductCategory =async(req:Request,res:Response)=>{
        try{
          const id = req.body.id
          const name= req.body.name
          
          const is_inSetOnly =req.body.is_inSetOnly
          const data ={id,name, is_inSetOnly}
          //console.log(data)
          const result = await this.menuSettingService.putProductCategory(id,data)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      getProductCategory =async(req:Request,res:Response)=>{
        try{
        //  console.log("get Product Category ",req.body)
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const result = await this.menuSettingService.getProductCategory(company_id,branch_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      deleteProductCategory =async(req:Request,res:Response)=>{
        try{
             
          const id = req.body.id
        
          const result = await this.menuSettingService.deleteProductCategory(id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      postProductData =async(req:Request,res:Response)=>{

 
        try{
           console.log(req.body)
          let company_id = Number(req.body.company_id)
          let branch_id = Number(req.body.branch_id )
          let name = req.body.name
          let is_kiosk = req.body.is_kiosk
          let is_launched = req.body.is_launched
          let is_time_consuming = req.body.is_time_consuming
          let product_category_id = Number(req.body.product_category_id)
          let department_id= Number(req.body.department_id[0])
          let available_period = req.body.available_period
          available_period=available_period.split(',')
          let remarks_details = req.body.remarks_details
          let photoName =req.body.photoName
          let price = req.body.price
          
        
          console.log(available_period)
        
          let data = {company_id,branch_id,name,is_kiosk,is_launched,is_time_consuming,product_category_id,department_id,price}
          if (photoName){
              data["photo"] = photoName
          }
          const result = await this.menuSettingService.postProductData(data,available_period,
                     remarks_details,price,company_id,branch_id)
          res.json(result)
          return
        }catch(err){       
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      putProductData =async(req:Request,res:Response)=>{

 
        try{
          console.log("put",req.body)
          let company_id = Number(req.body.company_id) 
          let branch_id = Number(req.body.branch_id )
          let id = Number(req.body.product_id)
          let name = req.body.name
          let is_kiosk = req.body.is_kiosk
          let is_launched = req.body.is_launched
          let is_time_consuming = req.body.is_time_consuming
         
          let department_id= Number(req.body.department_id[0])
          let available_period = req.body.available_period
          available_period=available_period.split(',')
          let remarks_details = req.body.remarks_details
          let photo =req.body.photoName
          let price = req.body.price
          let deletedPhotoName = req.body.deletedPhotoName
          let data = {name,is_kiosk,is_launched,is_time_consuming,department_id,price}
          if(photo){
            data["photo"]=photo
          console.log(data)}
          const result = await this.menuSettingService.putProductData(data,available_period,remarks_details,
            price,company_id,branch_id,deletedPhotoName,id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
  
      deleteProductData =async(req:Request,res:Response)=>{

 
        try{

          let product_id = req.body.product_id
          console.log(product_id )
         const result = await this.menuSettingService.deleteProductData(product_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      getAllProductData =async(req:Request,res:Response)=>{
        try{
          console.log(req.body)
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const product_category_id = req.body.product_category_id
          const result = await this.menuSettingService.getAllProductData(company_id,branch_id,product_category_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

  }


