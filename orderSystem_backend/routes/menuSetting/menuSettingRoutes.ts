import express from "express";
import {knex} from '../../app'
import { menuSettingService } from "./menuSettingService";
import { menuSettingController } from "./menuSettingController";
import multer from 'multer';
import path from "path";
 import { isLoggedInAPI } from "../../utils/guards"
let storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve("./upload"));
    },
    filename: function (req, file, cb) {
      cb(
        null,
        `${file.fieldname}-${Date.now().toString().slice(0, -2)}.${
          file.mimetype.split("/")[1]
        }`
      );
      let photoName = `${file.fieldname}-${Date.now()
        .toString()
        .slice(0, -2)}.${file.mimetype.split("/")[1]}`;
      //console.log('file',Date.now().toString().slice(0,-2))
      req.body.photoName = photoName;
    },
  });
 export  const upload = multer({ storage });


export const menuSettingRoutes = express.Router()

const MenuSettingService = new menuSettingService(knex)
const MenuSettingController = new menuSettingController(MenuSettingService)
menuSettingRoutes.post('/getAvailable_period',MenuSettingController.getAvailable_period)
menuSettingRoutes.post('/putAvailable_period',MenuSettingController.putAvailable_period)
menuSettingRoutes.delete('/deleteAvailable_period',MenuSettingController.deleteAvailable_period)
menuSettingRoutes.post('/postAvailable_period',MenuSettingController.postAvailable_period)

menuSettingRoutes.get('/getDepartment', isLoggedInAPI,MenuSettingController.getDepartment)
menuSettingRoutes.post('/postDepartment', isLoggedInAPI,MenuSettingController.postDepartment)
menuSettingRoutes.post('/putDepartment', isLoggedInAPI,MenuSettingController.putDepartment)
menuSettingRoutes.delete('/deleteDepartment',isLoggedInAPI, MenuSettingController.deleteDepartment)

menuSettingRoutes.post('/getCategoryAndTypeWithoutRemarks',MenuSettingController.getCategoryAndTypeWithoutRemarks)
menuSettingRoutes.post('/postRemarks',MenuSettingController.postRemarks)
menuSettingRoutes.delete('/deleteRemarks',MenuSettingController.deleteRemarks)
menuSettingRoutes.post('/putRemarks',MenuSettingController.putRemarks)

menuSettingRoutes.post('/getAllRemarks',MenuSettingController.getAllRemarks)

menuSettingRoutes.post('/postProductCategory',MenuSettingController.postProductCategory)
menuSettingRoutes.post('/putProductCategory',MenuSettingController.putProductCategory)
menuSettingRoutes.post('/getProductCategory',MenuSettingController.getProductCategory)
menuSettingRoutes.delete('/deleteProductCategory',MenuSettingController.deleteProductCategory)

menuSettingRoutes.post('/postProductData',upload.array("image"),MenuSettingController.postProductData)
menuSettingRoutes.post('/putProductData',upload.array("image"),MenuSettingController.putProductData)

menuSettingRoutes.delete('/deleteProductData',MenuSettingController.deleteProductData)
menuSettingRoutes.post('/getAllProductData',MenuSettingController.getAllProductData)
// {"id":1,"name":"hot_dog","start_time":"17:00:00","end_time":"22:00:00","company_id":1,"branch_id":1,"price":20,"product_category_id":1} 