import { UserService } from "../user/UserService";
import { Request, Response } from "express";
import { logger } from "../../utils/logger";
import { checkPassword } from "../../utils/hash";
import jwtSimple from "jwt-simple";
import jwt from "../../utils/jwt";


export class UserController {
  private readonly tag = "UserController";
  constructor(private userService: UserService) {}

  login = async (req: Request, res: Response) => {
    try {
      console.log(req.body.name)
      if (!req.body.name || !req.body.password) {
        res.status(401).json({ msg: "Wrong Username/Password" });
        return;
      }

      const { name, password } = req.body;
      
      const user = await this.userService.getUserByUsername(name);
      
      if (!user || !(await checkPassword(password.toString(), user.password))) {
        console.log("Wrong Username/Password")
        res.status(401).json({ msg: "Wrong Username/Password" });
        return;
      }
    
 
      const payload = { staff_id: user.id , company_id :user.company_id ,name:name, 
        branch_id: user.branch_id ,is_onboard: user.is_onboard, position:user.position};
      
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
     // console.log("ok")
      res.json({ token });
    } catch (err) {
      logger.error(`[${this.tag}] ${err.message}`);
      res.status(500).json({ message: "internal server error" });
    }
  };

  // loginFacebook = async (req: Request, res: Response) => {
  //   try {
  //     if (!req.body.accessToken) {
  //       res.status(401).json({ msg: "Wrong Access Token!" });
  //       return;
  //     }
  //     const { accessToken } = req.body;
  //     const fetchResponse = await fetch(
  //       `https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`
  //     );
  //     const result = (await fetchResponse.json()) as any;
  //     if (result["error"]) {
  //       res.status(401).json({ msg: "Wrong Access Token!" });
  //       return;
  //     }
  //     // let user = (await this.userService.getUser(result.email))[0];

  //     // Create a new user if the user does not exist
  //     // if (!user) {
  //     //   user = (await this.userService.createUser(result.email))[0];
  //     // }

  //     // dummy --- for demo purpose
  //     const payload = { id: 1 };
  //     const token = jwtSimple.encode(payload, jwt.jwtSecret);
  //     res.json({ token: token });
  //   } catch (err) {
  //     logger.error(`[${this.tag}] ${err.message}`);
  //     res.status(500).json({ message: "internal server error" });
  //   }
  // };

  demo = async (req: Request, res: Response) => {
    res.json({ message: "demo" });
  };
}
