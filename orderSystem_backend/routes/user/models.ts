export interface TodoItem {
  id: number;
  description: string;
  is_checked: boolean;
}

export interface User {
  id: number;
  name: string;
  password: string;
  company_id:number;
  branch_id:number;
  position: string
}




declare global {
  namespace Express {
    interface Request {
      user: { id: number; username: string, company_id: number, branch_id: number ,position: string};
    }
  }
}
