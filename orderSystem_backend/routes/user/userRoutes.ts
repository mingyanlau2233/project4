import express from "express";
import { UserService } from "./UserService";
import { UserController } from "./UserController";
import { knex } from "../../app";
//import { isLoggedInAPI } from "../../utils/guards";

export const userService = new UserService(knex);
const userController = new UserController(userService);

export const userRoutes = express.Router();
userRoutes.post("/login", userController.login);
//userRoutes.post("/login/facebook", userController.loginFacebook);
userRoutes.get("/demo", userController.demo);
