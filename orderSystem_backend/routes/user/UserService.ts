import { Knex } from "knex";
import {  User } from "./models";

export class UserService {
  constructor(private knex: Knex) {}

  async getUserByUsername(name: string) {
    //console.log(123)
    const user = await this.knex("staff")
      .where({ name: name })
      .first();
      
    return user;
  }


  async getUserById(id: number) {
   // console.log("id",id)
    const user = await this.knex<User>("staff").where({ id }).first();
   // console.log("byId",user)
    return user;
  }

}
