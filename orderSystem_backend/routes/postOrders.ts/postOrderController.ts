import { Request, Response } from "express";
import { postOrderService } from "./postOrderService";
import { logger } from "../../utils/logger";
import { Server as SocketIO } from "socket.io";
export class postOrderController {
    constructor(private postOrderService: postOrderService,
      private io: SocketIO
      ) {}
    
    postProductToOrderCart =async(req:Request,res:Response,)=>{
        try{
          
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          //let  session_id = req.body.session_id
          const qty =req.body.qty 
          const price =req.body.price*qty
          const product_id=req.body.product_id
          const product_name = req.body.product_name
          
          let session_id = req.body.session_id
          let data={company_id,branch_id,session_id,qty,price,product_id, product_name}
                  
          const result = await this.postOrderService.postProductToOrderCart(data,session_id,product_id)
          res.json(result)
          
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      putProductToOrderCart =async(req:Request,res:Response)=>{
        try{
         // console.log(req.body)
          const id = req.body.id
          const qty =req.body.qty 
          const price =req.body.price
          let data={qty,price}
          //console.log(data,order_cart_id)
          const result = await this.postOrderService.putProductToOrderCart(data,id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
 
      deleteProductFromOrderCart =async(req:Request,res:Response)=>{
        try{
         // console.log(req.body)
          const order_cart_id = req.body.id
  
          //console.log(data,order_cart_id)
          const result = await this.postOrderService.deleteProductFromOrderCart(order_cart_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }


      getProductFromOrderCart =async(req:Request,res:Response)=>{
        try{
        // console.log(req.body)
          const session_id = req.body.session_id
          
         // console.log(session_id)
          const result = await this.postOrderService.getProductFromOrderCart(session_id)
         // console.log(result)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }



      postSubmitOrders =async(req:Request,res:Response)=>{
        try{
         // console.log(req.body)
         
          const branch_id = 1
          const company_id = 1
          const session_id = req.body.session_id
          //console.log(data,order_cart_id)
          const result = await this.postOrderService.postSubmitOrders(session_id,company_id,branch_id)
         // console.log("ioioioioioioioioioioioioio")
          this.io.emit("postedOrder", "By James")
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }


      postDiscounterSetToOrderCart =async(req:Request,res:Response)=>{
        try{
          
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const session_id = req.body.session_id
         
          const price =req.body.price
          const productData=req.body.productData
          const discount_set_name = req.body.discount_set_name
          const discount_set_id =req.body.discount_set_id
        
         ///postData = [ [product_id ,product_name, discount_set_sequence]]
          
    
         // console.log(productData)
          const result = await this.postOrderService.postDiscounterSetToOrderCarts(company_id,branch_id,session_id,
            price,discount_set_name,discount_set_id,productData)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      getDiscounterSetFromOrderCart =async(req:Request,res:Response)=>{
        try{
          
    
          const session_id = req.body.session_id
          
      
          const result = await this.postOrderService.getDiscountSetFromOrderCart(session_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      getDiscounterSetOrderDetailsFromOrderCart =async(req:Request,res:Response)=>{
        try{
          
         // console.log("discountSetOrderDetails",req.body.id)
          //const session_id = req.user?.session_id
          let discount_set_order_id =req.body.id
      
          const result = await this.postOrderService.getDiscountSetOrderDetailFromOrderCart(discount_set_order_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      deleteDiscountSetFromOrderCart =async(req:Request,res:Response)=>{
        try{
         // console.log(req.body)
          const discount_set_order_id = req.body.discount_set_order_id
  
          //console.log(data,order_cart_id)
          const result = await this.postOrderService.deleteDiscountSetFromOrderCart(discount_set_order_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
  }