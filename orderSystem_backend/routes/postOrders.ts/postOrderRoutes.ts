import express from "express";
import {knex,io} from '../../app'
import { postOrderController } from "./postOrderController";
import { postOrderService } from "./postOrderService";

export const postOrderRoutes = express.Router()

const PostOrderService = new postOrderService(knex)
const PostOrderController = new postOrderController(PostOrderService,io)
postOrderRoutes.post('/postProductToOrderCart',PostOrderController.postProductToOrderCart)
postOrderRoutes.post('/putProductToOrderCart',PostOrderController.putProductToOrderCart)
postOrderRoutes.delete('/deleteProductFromOrderCart',PostOrderController.deleteProductFromOrderCart)
postOrderRoutes.post('/getProductFromOrderCart',PostOrderController.getProductFromOrderCart)

postOrderRoutes.post('/postDiscounterSetToOrderCart',PostOrderController.postDiscounterSetToOrderCart)
postOrderRoutes.post('/getDiscounterSetFromOrderCart',PostOrderController.getDiscounterSetFromOrderCart)
postOrderRoutes.post('/getDiscounterSetOrderDetailFromOrderCart',PostOrderController.getDiscounterSetOrderDetailsFromOrderCart)
postOrderRoutes.post('/deleteDiscountSetFromOrderCart',PostOrderController.deleteDiscountSetFromOrderCart)

postOrderRoutes.post('/submitOrders',PostOrderController.postSubmitOrders)
//postOrderRoutes.post('/postOrderCart',PostOrderController.getProductToOrderCart)