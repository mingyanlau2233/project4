import {Knex} from 'knex';
//import { Server } from "socket.io";

//const io = new Server();

 
export class postOrderService{
    constructor(private knex:Knex){}

    async postProductToOrderCart(data:object,session_id:number,product_id:number,){
        
        const trx = await this.knex.transaction()
        try {
           // console.log("here",)
            let sameProductOrderedBefore = await trx("order_cart").select("product_id","qty","price")
            .where("session_id","=",session_id).where("product_id","=",product_id)
            sameProductOrderedBefore=sameProductOrderedBefore[0]
            //console.log(sameProductOrderedBefore)
            if(!sameProductOrderedBefore){
                //console.log(data)
                
                await trx("order_cart").insert(data)
                console.log("?")
               // io.emit("Occupied");
                await trx.commit();
                return {message:"insert success"}
            }
            data["qty"]=data['qty']+sameProductOrderedBefore["qty"]
            data["price"]=data["price"]+sameProductOrderedBefore['price']
            
            data["discount_set_name"]=""

            console.log("i ' am here ",)

            await trx("order_cart").update(data).where("session_id","=",session_id)
            .where("product_id","=",product_id)
                 
            await trx.commit();
            return {message:"insert success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("postOrderService encounter error")
            return false
        }
    }

    async putProductToOrderCart(data:object,order_cart_id:number){
        
        const trx = await this.knex.transaction()
        try {
            await trx("order_cart").update(data).where("id","=",order_cart_id)
                 
            await trx.commit();
            return {message:"put success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("postOrderService encounter error")
            return false
        }
    }
     
    async deleteProductFromOrderCart(order_cart_id:number){
        
        const trx = await this.knex.transaction()
        try {
            await trx("order_cart").where("id","=",order_cart_id).del()
                 
            await trx.commit();
            return {message:"delete success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("waitingTicketService encounter error")
            return false
        }
    }

    async getProductFromOrderCart(session_id:number){
        
        const trx = await this.knex.transaction()
        try {
            console.log(session_id)
           let result= await trx("order_cart")
            .select("id","qty","price","product_name","product_id")
            .where("session_id","=",session_id).whereNotNull("product_id").orderBy('id', 'asc')
            //console.log("getProductFromOrderCart",result)
                 
            await trx.commit();
            return result
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("postOrderService encounter error")
            return false
        }
    }


    async postSubmitOrders(session_id:number,company_id:number,branch_id:number){
        console.log("submitOrders",company_id,session_id,branch_id)
        const trx = await this.knex.transaction()
        try {
            console.log("????",session_id)
            let order_cartDetails=
            await trx("order_cart")
            .select("product_id","qty","price","product_id","product_name",
            "discount_set_order_id","discount_set_name","company_id","branch_id")
            .where("session_id","=",session_id)

            console.log("order_crat",order_cartDetails)
            let dataForOrders = {company_id,branch_id,session_id}
        
            let orders_id = await trx("orders").insert(dataForOrders).returning("id")
            console.log(orders_id)
              orders_id = orders_id[0]
            for(let order_carts of order_cartDetails){
                
                order_carts["stage"] = 1
                order_carts["orders_id"] = orders_id
                order_carts["company_id"] = company_id
                order_carts["branch_id"] = branch_id
                //console.log("IDC",order_carts)
                
                await trx("order_details").insert(order_carts)
                console.log("????")
                if(order_carts.discount_set_order_id){
                    console.log("????")
                    let is_confirm = true
                    let data={is_confirm}
                    await trx("discount_set_order").update(data).where("session_id","=",session_id).where("id","=",order_carts.discount_set_order_id)
                }
                

            }
            await trx("order_cart").where("session_id","=",session_id).del()
            await trx.commit();
            return {orders_id}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("postOrderService encounter error")
            return false
        }
    }


    async postDiscounterSetToOrderCarts(company_id:number,branch_id:number,session_id:number,
        price:number,discount_set_name:string,discount_set_id:number,productData:string[]){
        
        const trx = await this.knex.transaction()
        try {
            
            let is_confirm = false 
            let data={company_id,branch_id,session_id,discount_set_id,discount_set_name,is_confirm,price}
            console.log(data)
            //console.log(productData)
            let discount_set_order_id = await trx("discount_set_order").insert(data).returning("id")
            data["discount_set_order_id"]=discount_set_order_id
            discount_set_order_id = discount_set_order_id[0]     
            console.log("?????????",discount_set_order_id,"productData",productData)
          
            for(let datas of productData){
            
                       let arr = datas.split(",")
                       console.log(datas)
                     let discount_set_sequence_id = arr[0]
                     let product_id =arr[1]
                     let discount_set_sequence_name=arr[2]
                      let stage =1
                      console.log(arr,discount_set_name)
                      let product_name= await trx("product").select("name").where("id","=",product_id)
                      product_name = product_name[0].name
                      console.log("?",product_name)
                   let data2 = {product_id,discount_set_order_id,discount_set_name,discount_set_sequence_id,discount_set_sequence_name,company_id,branch_id,stage,product_name}
                   console.log(data2)
                    await trx("discount_set_order_details").insert(data2)
    
                 }
                
            let qty = 1
            data["qty"]= qty 
           
            
            let data3={qty,price,session_id,discount_set_order_id,company_id,branch_id,discount_set_name}
            await trx("order_cart").insert(data3)
            await trx.commit();
            return {message:"insert success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("postOrderService encounter error")
            return false
        }
    }


async getDiscountSetFromOrderCart(session_id:number){
        
    const trx = await this.knex.transaction()
    try {
       // console.log(session_id)
       let result= await trx("order_cart")
        .select("id","qty","price","product_name","product_id","discount_set_order_id","discount_set_name")
        .where("session_id","=",session_id).whereNotNull("discount_set_name").orderBy('id', 'asc')
        //console.log(result)
        //  let result2=[]
        // for(let order_cart of result){


        // }
        // console.log(result2)
        //console.log(result2)
        await trx.commit();
        return result
    } catch (err) {
        console.log(err.message)
        await trx.rollback()
        console.log("postOrderService encounter error")
        return false
    }
}

async getDiscountSetOrderDetailFromOrderCart(discount_set_order_id:number){
        
    const trx = await this.knex.transaction()
    try {
        
            let details = await trx("discount_set_order_details").select("discount_set_sequence_name",
            " product_name","discount_set_order_id")
            .where("discount_set_order_id","=",discount_set_order_id)
              console.log("details",details)
              
        

        await trx.commit();
        return details
    } catch (err) {
        console.log(err.message)
        await trx.rollback()
        console.log("postOrderService encounter error")
        return false
    }
}

async deleteDiscountSetFromOrderCart(discount_set_order_id:number){
        
    const trx = await this.knex.transaction()
    try {
        await trx("discount_set_order_details").where("discount_set_order_id","=",discount_set_order_id).del()
        await trx("discount_set_order").where("id","=",discount_set_order_id).del()  
        await trx("order_cart").where("discount_set_order_id ","=",discount_set_order_id).del() 
        await trx.commit();
        return {message:"delete success"}
    } catch (err) {
        console.log(err.message)
        await trx.rollback()
        console.log("waitingTicketService encounter error")
        return false
    }
}

async handleSocketIOData(data: any) {
    return true
}


}