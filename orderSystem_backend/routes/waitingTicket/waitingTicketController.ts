import { Request, Response } from "express";
import { waitingTicketService } from "./waitingTicketService";
import { logger } from "../../utils/logger";

export class waitingTicketController {
    constructor(private waitingTicketService: waitingTicketService) { }
    
    post =async(req:Request,res:Response)=>{
        try{
          const data = req.body.product_category
          const result = await this.waitingTicketService.post(data)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
 
  }


