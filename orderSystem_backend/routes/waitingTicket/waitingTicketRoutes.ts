import express from "express";
import {knex} from '../../app'
import {waitingTicketService} from "./waitingTicketService"
import {waitingTicketController} from "./waitingTicketController"

export const waitingTicketRoutes = express.Router()

const WaitingTicketService = new waitingTicketService(knex)
const WaitingTicketController = new waitingTicketController(WaitingTicketService)
waitingTicketRoutes.post('/post',WaitingTicketController.post)
