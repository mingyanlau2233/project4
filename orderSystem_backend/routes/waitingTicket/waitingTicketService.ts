import {Knex} from 'knex';

export class waitingTicketService{
    constructor(private knex:Knex){}

    async post(data:string){
        
        const trx = await this.knex.transaction()
        try {
 
             await trx.raw(`INSERT INTO product_category (name) values (?) returning order_id`, [data])
                 
            await trx.commit();
            return {message:"insert success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("waitingTicketService encounter error")
            return false
        }
    }

}