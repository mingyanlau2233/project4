import { Request, Response } from "express";
import { discountSetService } from "./discountSetService";
import { logger } from "../../utils/logger";

export class discountSetController {
    constructor(private discountSetServices : discountSetService) { }
    
    postDiscountSet =async(req:Request,res:Response)=>{
        try{
          
          const company_id = Number(req.body.company_id)
          const branch_id = Number(req.body.branch_id)
          const name =req.body.name
          const is_launched =req.body.is_launched
          const price = req.body.price
          let data = {name,is_launched,company_id,branch_id,price}
          let  available_period = req.body.available_period
          let  photo = req.body.photoName
          if (photo){
            data["photo"] =photo
          }
         available_period = available_period.split(",")

          const result = await this.discountSetServices.postDiscountSet(company_id,branch_id,data,available_period)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      putDiscountSet =async(req:Request,res:Response)=>{
        try{
          let company_id = req.body.company_id 
          let branch_id = req.body.branch_id 
          let name = req.body.name
          let is_launched = req.body.is_launched
          let discount_set_id = req.body.id
          let available_period = req.body.available_period
          let price = Number(req.body.price)
          let photo =req.body.photoName
          let data ={name,is_launched,price}
          available_period=available_period.split(",")
          if (photo){
            data["photo"] = photo
          }
         
          
          
          const result = await this.discountSetServices.putDiscountSet(data,available_period,
            discount_set_id,company_id,branch_id,)
         
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
 

      deleteDiscountSet =async(req:Request,res:Response)=>{

 
        try{
          let discount_set_id = req.body.id 
  
          const result = await this.discountSetServices.deleteDiscountSet(discount_set_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      postDiscountSetDetails =async(req:Request,res:Response)=>{
        try{
          let company_id = req.body.company_id 
          let branch_id = req.body.branch_id           
          let discount_set_sequence_id  = req.body.discount_set_sequence_id
          let description = req.body.description 
          let discount_set_id =req.body.discount_set_id
          let  product_category_id =Number(req.body.product_category_id)
        
          const product_id = req.body. product_id
          let data ={discount_set_sequence_id,discount_set_id,company_id,branch_id}
          if (product_category_id){data["product_category_id"] = product_category_id}
          if (product_id){data["product_id"] = product_id}
          
         
          
          const result = await this.discountSetServices.postDiscountSetDetails(data,description,company_id,branch_id)
         
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
      putDiscountSetDetails =async(req:Request,res:Response)=>{
        try{
            let discount_set_details_id  = req.body.id
            const product_category_id =req.body.product_category_id
            const product_id = req.body. product_id
            let data ={product_category_id,product_id}
            if (!product_category_id){data["product_category_id"] = null}
            if (!product_id){data["product_id"] = null}
          
          const result = await this.discountSetServices.putDiscountSetDetails(data,discount_set_details_id)
         
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
      deleteDiscountSetDetails =async(req:Request,res:Response)=>{

 
        try{
          let discount_set_details_id = req.body.id 
           
          const result = await this.discountSetServices.deleteDiscountSetDetails(discount_set_details_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
      putDiscountSetSequence =async(req:Request,res:Response)=>{

 
        try{
          let discount_set_sequence_id = req.body.id 
          let description= req.body.description 
          let data = {description}
          const result = await this.discountSetServices.putDiscountSetSequence(data,discount_set_sequence_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      deleteDiscountSetSequence =async(req:Request,res:Response)=>{

 
        try{
          let discount_set_sequence_id = req.body.id 
  
          const result = await this.discountSetServices.deleteDiscountSetSequence(discount_set_sequence_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      getAllDiscountSet =async(req:Request,res:Response)=>{
        try{
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const result = await this.discountSetServices.getAllDiscountSet(company_id,branch_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }

      getDiscountSet =async(req:Request,res:Response)=>{
        try{
         
          const company_id = req.body.company_id
          const branch_id = req.body.branch_id
          const result = await this.discountSetServices.getDiscountSet(company_id,branch_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }
    
      getDiscountSetDetails =async(req:Request,res:Response)=>{
        try{
          const discount_set_id = req.body.discount_set_id
          const result = await this.discountSetServices.getDiscountSetDetails(discount_set_id)
          res.json(result)
          return
        }catch(err){
          logger.error(err.message)
          res.status(500).json({message:`internal server error`})
        }
      }



    }
        



