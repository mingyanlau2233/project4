import {Knex} from 'knex';

export class discountSetService{
    constructor(private knex:Knex){}

    async postDiscountSet(company_id:number,branch_id:number,data:Object,available_period:[]
        ){
        
        const trx = await this.knex.transaction()
        try {

            let discount_set_id = await trx("discount_set").insert(data)
            .where('company_id',"=",company_id).where("branch_id","=",branch_id).returning("id")
            discount_set_id= discount_set_id[0]

            if(available_period){
            for (let available_periods of available_period){
                let available_period_id=available_periods
                let data = {available_period_id,discount_set_id,company_id,branch_id}
                await trx("product_available_period").insert(data)
            }
            }

            await trx.commit();
            
            return {message: "post success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("subitemServices encounter error")
            return false
        }
    }


    async putDiscountSet(data:object,available_period:[],
        discount_set_id:number,company_id:number,branch_id:number,){
        //console.log(data,available_period,remarks_details,photoName,deletedPhotoName)
      
        const trx = await this.knex.transaction()
        try {
           
    
           

             if (available_period){
            let oldProduct_available_period = await trx("product_available_period").select("available_period_id").where("discount_set_id","=",discount_set_id)
           
            for (let old of oldProduct_available_period){
                let remain =false
                for(let news of available_period){
                   if( news == old.available_period_id ){
                       remain = true
                   }
                }
                if(remain == false){
                   
                  await trx("product_available_period").where("discount_set_id","=",discount_set_id).where("available_period_id","=",old.available_period_id).del()
                }
            }

            for (let news of available_period){
                let newOne = true
                for(let old of oldProduct_available_period){
                    if (news == old.available_period_id ) { newOne = false}
                }
                if(newOne == true){
                    let available_period_id = news
                    let data = {available_period_id,discount_set_id,company_id,branch_id}
                    await trx("product_available_period").insert(data)
                }
            }}
            
        
           await trx("discount_set").update(data).where("id","=",discount_set_id)
             
                     
            await trx.commit();
            return {message:"put success"}
        } catch (err) {
            console.log(err.message)
            await trx.rollback()
            console.log("discount_set Service encounter error")
            return false
        }
    } 

    async deleteDiscountSet(discount_set_id:number){
       
      
        const trx = await this.knex.transaction()
        try{
           // console.log(discount_set_id)
            await trx("photo_list").where("discount_set_id","=",discount_set_id).del()
            await trx("product_available_period").where("discount_set_id","=",discount_set_id).del()
            await trx("discount_set_details").where("discount_set_id","=",discount_set_id).del()
            await trx("discount_set").where("id","=",discount_set_id).del()
            await trx.commit();
            return {message:"delete success"}
          }catch(err){
            console.log(err.message)
            await trx.rollback()
            console.log("discountSetService encounter error")
            return false
           } 
        }


   async postDiscountSetDetails(data:object,description:string,company_id:number,branch_id:number){
                
            const trx = await this.knex.transaction()
            try {
                
                if(!data["discount_set_sequence_id"]){
                    let datas ={description,company_id,branch_id}
                    let discount_set_sequence_id = await trx("discount_set_sequence").insert(datas).returning("id")
                    data["discount_set_sequence_id"]=discount_set_sequence_id[0]
                    //console.log(data)
                }

               await trx("discount_set_details").insert(data)
     
    
      
                await trx.commit();
                return {message: "post success"}
            } catch (err) {
                console.log(err.message)
                await trx.rollback()
                console.log("subitemServices encounter error")
                return false
            }
        }    


   async putDiscountSetDetails(data:object,
            discount_set_details_id:number,){
            //console.log(data,available_period,remarks_details,photoName,deletedPhotoName)
          
            const trx = await this.knex.transaction()
            try {
               
                 
               await trx("discount_set_details").update(data).where("id","=",discount_set_details_id)
                 
                         
                await trx.commit();
                return {message:"put success"}
            } catch (err) {
                console.log(err.message)
                await trx.rollback()
                console.log("discount_set Service encounter error")
                return false
            }
        } 

        async deleteDiscountSetDetails(discount_set_details_id:number){
       
      
            const trx = await this.knex.transaction()
            try{
                //console.log("hey",discount_set_details_id)
                await trx("discount_set_details").where("discount_set_sequence_id","=",discount_set_details_id).del()
                await trx.commit();
                return {message:"delete success"}
              }catch(err){
                console.log(err.message)
                await trx.rollback()
                console.log("discountSetService encounter error")
                return false
               } 
            }


        async putDiscountSetSequence(data:object,discount_set_sequence_id:number){
       
      
            const trx = await this.knex.transaction()
            try{
                //console.log(discount_set_sequence_id)
                await trx("discount_set_sequence").update(data).where("id","=",discount_set_sequence_id)
                await trx.commit();
                return {message:"put success"}
              }catch(err){
                console.log(err.message)
                await trx.rollback()
                console.log("discountSetService encounter error")
                return false
               } 
            }

        async deleteDiscountSetSequence(discount_set_sequence_id:number){
       
      
            const trx = await this.knex.transaction()
            try{
               // console.log(discount_set_sequence_id)
               await trx("discount_set_details").where("discount_set_sequence_id","=",discount_set_sequence_id).del()
                await trx("discount_set_sequence").where("id","=",discount_set_sequence_id).del()
                await trx.commit();
                return {message:"delete success"}
              }catch(err){
                console.log(err.message)
                await trx.rollback()
                console.log("discountSetService encounter error")
                return false
               } 
            }
    async getAllDiscountSet(company_id:number,branch_id:number){
       
      
      const trx = await this.knex.transaction()
       try{
          
         let discountSetIds = await trx("discount_set").select("id","name","is_launched")
          .where('company_id',"=",company_id).where("branch_id","=",branch_id).orderBy("id","asc")
          let result = []
         for (let discountSetId of discountSetIds){
            discountSetId["discount_set_id"]=discountSetId.id
            discountSetId["discount_name"]=discountSetId.name
             delete discountSetId["id"]
             delete discountSetId["name"]
            
             let discountSetPhoto = await trx("photo_list").select(
                "photo").where("set_id","=",discountSetId.discount_set_id)
                discountSetId["discountSetPhoto"]= discountSetPhoto
                //console.log(discountSetId)
               let dataInSameDiscountSet = []
               dataInSameDiscountSet.push(discountSetId)
              // console.log(dataInSameDiscountSet)
                let dataInSameSet = await trx("discount_set_details").select(
                    "discount_set_sequence_id").where("discount_set_id","=",discountSetId.discount_set_id)
                   
                    //console.log(dataInSameSet)
                    let sequence = []
                    for (let i of dataInSameSet){
                        sequence.push(i["discount_set_sequence_id"])
                    }
                    let newSequence:number[] = []
                    for (let i of sequence){
                         if(!newSequence.includes(i)){
                            newSequence.push(i)
                         }
                    }
                    let dataInOneSet = []
                    for(let j of newSequence){
                        let dataAsSameName = await trx("discount_set_details").select("id","discount_set_sequence_id",
                         "product_category_id","product_id")
                         .where("discount_set_id","=",discountSetId.discount_set_id).where("discount_set_sequence_id","=",j)
                      let oneNameData = []
                      //console.log(dataAsSameName)
                      for(let i of dataAsSameName){
                          
                      if (i.product_category_id){
                      
                          let discount_set_detailsData = 
                          await trx("discount_set_details").join("product_category","product_category.id","=","discount_set_details.product_category_id")
                          .select("product_category.name","discount_set_details.id","product_category_id").where("discount_set_details.id","=",i.id)
                          discount_set_detailsData=discount_set_detailsData[0]
                          
                          i["product_category_name"]=discount_set_detailsData["name"]
                          
                      }
                      if (i.product_id){
                      
                          let discount_set_detailsData = 
                          await trx("discount_set_details").join("product","product.id","=","discount_set_details.product_id")
                          .select("product.name","discount_set_details.id","product_category_id").where("discount_set_details.id","=",i.id)
                          discount_set_detailsData=discount_set_detailsData[0]
                          
                          i["product_name"]=discount_set_detailsData["name"]
                      }
                       let photo = await trx("photo_list").select("photo").where("set_subitem_type_id","=",i.id)
                       i["discount_set_details_id"]= i.id

                       i["photo"]= photo
                       delete i.id
                       oneNameData.push(i)
                         }
                         //console.log(oneNameData)
                         dataInOneSet.push(oneNameData)
                   }

                    //console.log(dataInOneSet)
                    dataInSameDiscountSet.push(dataInOneSet)

                    result.push(dataInSameDiscountSet)



                }
                
            
            await trx.commit();
               // console.log(result)
                return result 
              }catch(err){
                console.log(err.message)
                await trx.rollback()
                console.log("menuSettingService encounter error")
                return false
               } 
            }
    async getDiscountSet(company_id:number,branch_id:number){
       
              
                const trx = await this.knex.transaction()
                 try{
                    let result=[]
                   let discountSetData = await trx("discount_set").select("id")
                    .where('company_id',"=",company_id).where("branch_id","=",branch_id)
                    .orderBy("id","asc")
          
                    for (let discount_set_id of discountSetData){
                        let data = await trx("discount_set").select("id","name","is_launched","price","photo")
                        .where("id","=",discount_set_id.id)
   
                        data= data[0]
                 

                        // let remarks = await trx("remarks_details").select("remarks_id").where("discount_set_id","=",discount_set_id.id)
                        // let remarksList =[]
                        // for(let remark of remarks ){
                        //    remarksList.push(remark.remarks_id)
                        // }
                       // data["remarks_id"]=remarksList
                        let period = await trx("product_available_period").select("available_period_id").where("discount_set_id","=",discount_set_id.id)
                        let periodList =[]
                        for(let periods of period ){
                           periodList.push(periods.available_period_id)
                        }
                       data['available_period'] =periodList
                       
   
                       data['breakfast']=false
                       data['lunch']=false
                       data['afternoonTea']=false
                       data['dinner']=false
                       data['nightSnack']=false
                       
                       if(data['available_period'].includes(1)){data['breakfast']=true}
                       if(data['available_period'].includes(2)){data['lunch']=true}
                       if(data['available_period'].includes(3)){data['afternoonTea']=true}
                       if(data['available_period'].includes(4)){data['dinner']=true}
                       if(data['available_period'].includes(5)){data['nightSnack']=true}
                       delete data['available_period']
                     
                       result.push(data)
                    }
          
                          
                          
                      
                      await trx.commit();
                       //   console.log(result)
                          return result 
                        }catch(err){
                          console.log(err.message)
                          await trx.rollback()
                          console.log("menuSettingService encounter error")
                          return false
                         } 
                      }


                      async getDiscountSetDetails(discount_set_id:number){
       
                      // console.log("???",discount_set_id)
                        const trx = await this.knex.transaction()
                         try{
                            let result = await trx("discount_set_details").join("product_category","product_category.id","=","discount_set_details.product_category_id")
                            .select("product_category.name","discount_set_details.id","product_category_id","discount_set_sequence.description","discount_set_details.discount_set_sequence_id",
                            "discount_set_details.discount_set_id")
                            .join("discount_set_sequence","discount_set_sequence.id","=","discount_set_details.discount_set_sequence_id")
                            .orderBy("discount_set_sequence.id")
                  
                                  
                                  
                             // console.log(result)
                              await trx.commit();
                                 
                                  return result
                                }catch(err){
                                  console.log(err.message)
                                  await trx.rollback()
                                  console.log("menuSettingService encounter error")
                                  return false
                                 } 
                              }
}

