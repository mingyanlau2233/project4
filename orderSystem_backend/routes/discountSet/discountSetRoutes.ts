import express from "express";
import {knex} from '../../app'
import { discountSetController } from "./discountSetController";
import { discountSetService } from "./discountSetService";
import multer from 'multer';
import path from "path";
let storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve("./upload"));
    },
    filename: function (req, file, cb) {
      cb(
        null,
        `${file.fieldname}-${Date.now().toString().slice(0, -2)}.${
          file.mimetype.split("/")[1]
        }`
      );
      let photoName = `${file.fieldname}-${Date.now()
        .toString()
        .slice(0, -2)}.${file.mimetype.split("/")[1]}`;
      //console.log('file',Date.now().toString().slice(0,-2))
      req.body.photoName = photoName;
    },
  });
 export  const upload = multer({ storage });
export const discountSetRoutes = express.Router()

const DiscountSetService = new discountSetService(knex)
const DiscountSetController = new discountSetController(DiscountSetService)
discountSetRoutes.post('/postDiscountSet',upload.array("image"),DiscountSetController.postDiscountSet)
discountSetRoutes.post('/putDiscountSet',upload.array("image"),DiscountSetController.putDiscountSet)
discountSetRoutes.delete('/deleteDiscountSet',DiscountSetController.deleteDiscountSet)
discountSetRoutes.post('/getDiscountSet',DiscountSetController.getDiscountSet)

discountSetRoutes.post('/postDiscountSetDetails',upload.array("image"),DiscountSetController.postDiscountSetDetails)
discountSetRoutes.post('/putDiscountSetDetails',upload.array("image"),DiscountSetController.putDiscountSetDetails)
discountSetRoutes.delete('/deleteDiscountSetDetails',DiscountSetController.deleteDiscountSetDetails)

discountSetRoutes.post('/putDiscountSetSequence',upload.array("image"),DiscountSetController.putDiscountSetSequence)
discountSetRoutes.delete('/deleteDiscountSetSequence',upload.array("image"),DiscountSetController.deleteDiscountSetSequence)

discountSetRoutes.post('/getAllDiscountSet',DiscountSetController.getAllDiscountSet)


discountSetRoutes.post('/getDiscountSetDetails',DiscountSetController.getDiscountSetDetails)