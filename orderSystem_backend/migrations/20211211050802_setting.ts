import { Knex } from "knex";

export const tables2 = Object.freeze({
  COMPANY:"company",
  BRANCH:"branch",
  STAFF: "staff",
  AREA: "area",
  DESK: "desk",
  SESSION: "session",
  SEAT: "seat",
  SERVICE_CHARGE: "service_charge",
  TAKEAWAYS_CODE: "takeaways_code",
  TICKET_TYPE: "ticket_type",
  TICKET: "ticket",
});

export async function up(knex: Knex): Promise<void> {

  await knex.schema.createTable(tables2.COMPANY, (t) => {
    t.increments("id");
    t.string("name").notNullable();
    t.string("password").notNullable();
    t.boolean("is_subscribed").notNullable();
    t.date("resigned_at");
    t.timestamps(false, true);
  });

  await knex.schema.createTable(tables2.BRANCH, (t) => {
    t.increments("id");
    t.string("name").notNullable();
    t.boolean("is_subscribed").notNullable();
    t.date("resigned_at");
    t.integer("company_id").notNullable().unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.timestamps(false, true);
  });

  await knex.schema.createTable(tables2.STAFF, (t) => {
    t.increments("id");
    t.string("name").notNullable();
    t.string("password").notNullable();
    t.string("position").notNullable();
    t.boolean("is_onboard").notNullable();
    t.date("resigned_at");
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });

  await knex.schema.createTable(tables2.AREA, (t) => {
    t.increments("id");
    t.integer('sequence').notNullable().unique().unsigned();
    t.string("description").notNullable();
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });

  await knex.schema.createTable(tables2.DESK, (t) => {
    t.increments("id");
    t.integer('sequence').notNullable().unique().unsigned();
    t.integer("area_id").unsigned();
    // t.foreign("area_id").references(`${tables2.AREA}.id`);
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });

    await knex.schema.createTable(tables2.SESSION, (t) => {
      t.string("id").notNullable().unique().unsigned();
      t.boolean("is_client_left").notNullable();
      t.boolean("is_takeaways").notNullable();
      t.integer("total_price").unsigned();
      t.integer("company_id").unsigned();
      t.foreign("company_id").references(`${tables2.COMPANY}.id`);
      t.integer("branch_id").unsigned();
      t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
      t.timestamps(false, true);
    });

  await knex.schema.createTable(tables2.SEAT, (t) => {
    t.increments("id");
    t.integer('sequence').notNullable()
    t.boolean("is_occupied").notNullable();
    t.integer("session_id");
    // t.foreign("session_id").references(`${tables2.SESSION}.id`);
    t.integer("desk_id").notNullable().unsigned();
    t.foreign("desk_id")
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });

  await knex.schema.createTable(tables2.SERVICE_CHARGE, (t) => {
    t.boolean("is_charged").notNullable();
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });

  await knex.schema.createTable(tables2.TAKEAWAYS_CODE, (t) => {
    t.increments("id").notNullable().unique().unsigned();
    t.integer('code').notNullable().unique().unsigned();
    t.boolean("is_picked").notNullable();
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });

  await knex.schema.createTable(tables2.TICKET_TYPE, (t) => {
    t.increments("id");
    t.string("name").notNullable();
    t.string("num_group").notNullable();
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });

  await knex.schema.createTable(tables2.TICKET, (t) => {
    t.increments("id");
    t.boolean("is_seat_assigned").notNullable();
    t.integer("ticket_type_id").notNullable().unsigned();
    t.foreign("ticket_type_id").references(`${tables2.TICKET_TYPE}.id`);
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.raw(`drop table IF Exists ${tables2.COMPANY} cascade`);
  await knex.raw(`drop table IF Exists ${tables2.BRANCH} cascade`);
  await knex.raw(`drop table IF Exists ${tables2.TICKET} cascade`);
  await knex.raw(`drop table IF Exists ${tables2.TICKET_TYPE} cascade`);
  await knex.raw(`drop table IF Exists ${tables2.TAKEAWAYS_CODE} cascade`);
  await knex.raw(`drop table IF Exists ${tables2.SERVICE_CHARGE} cascade`);
  await knex.raw(`drop table IF Exists ${tables2.SEAT} cascade`);
  await knex.raw(`drop table IF Exists ${tables2.SESSION} cascade`)
  await knex.raw(`drop table IF Exists ${tables2.DESK} cascade`);
  await knex.raw(`drop table IF Exists ${tables2.AREA} cascade`);
  await knex.raw(`drop table IF Exists ${tables2.STAFF} cascade`);
}
