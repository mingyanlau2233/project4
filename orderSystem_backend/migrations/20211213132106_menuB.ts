import { Knex } from "knex";
//import { tables3 } from "./20211213132051_menuA";
import { tables2 } from "./20211211050802_setting";

export const tables = Object.freeze({
       SET:"set",
       SET_SUBITEM_TYPE:"set_subitem_type",
       SET_ORDER:"set_order",
       SET_ORDER_DETAILS:"set_order_details",
       PRICE_LIST:"price_list",
       PHOTO_LIST:"photo_list",
       PRODUCT_AVAILABLE_PERIOD:"product_available_period",
});

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(tables.SET, (t) => {
        t.increments("id");
        t.integer("price").notNullable();
        t.string("name").notNullable().unique();
        t.boolean("is_launched").notNullable();
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
    });

    await knex.schema.createTable(tables.SET_SUBITEM_TYPE, (t) => {
        t.increments("id");
        t.integer("sequence").notNullable();
        t.integer("max_limit").notNullable();
        t.string("name").notNullable()
        t.integer("set_id").notNullable()


        t.integer("product_category_id")
       

        t.integer("subitem_type_id")
   

        t.integer("product_id")
       

        t.boolean("is_launched").notNullable();

        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
    });

    await knex.schema.createTable(tables.SET_ORDER, (t) => {
        t.increments("id");
        t.integer("total_price")
        t.integer("top_up_price")
        t.boolean("is_confirm").notNullable();

        t.integer("set_id").notNullable();
        
        t.string("set_id_name")

        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);

        t.timestamps(false, true);
    });

      await knex.schema.createTable(tables.SET_ORDER_DETAILS, (t) => {
        t.increments("id");

        t.integer("sequence").notNullable()
        t.integer("top_up_price")

        t.integer("set_order_id").notNullable()

        t.integer("subitem_id")

        t.integer("product_id")

        t.string("subitem_name")

        t.string("subitem_type_name")

        t.string("product_name")

        t.string("product_category_name")
        
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
    });

      await knex.schema.createTable(tables.PRICE_LIST, (t) => {
        t.increments("id");
        t.integer("price");

        t.integer("discount_set_id");
        //t.foreign("discount_set_id").references(`${tables3.DISCOUNT_SET}.id`);

        t.integer("subitem_id")
       // t.foreign("subitem_id").references(`${tables3.SUBITEM}.id`);

        t.integer("product_id")
        //t.foreign("product_id").references(`${tables3.PRODUCT}.id`);

        t.integer("set_id")
       // t.foreign("set_id").references(`${tables.SET}.id`);

        t.integer("set_subitem_type_id")
       // t.foreign("set_subitem_id").references(`${tables.SET_SUBITEM_TYPE}.id`);
        
        t.integer("remarks_id")

        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
    });

      await knex.schema.createTable(tables.PHOTO_LIST, (t) => {
        t.increments("id");
        t.string("photo").notNullable()

        t.integer("discount_set_id")
        //t.foreign("discount_set_id").references(`${tables3.DISCOUNT_SET}.id`);

        t.integer("subitem_id")
       // t.foreign("subitem_id").references(`${tables3.SUBITEM}.id`);

        t.integer("product_id")
      //  t.foreign("product_id").references(`${tables3.PRODUCT}.id`);

        t.integer("set_id")
       // t.foreign("set_id").references(`${tables.SET}.id`);

        t.integer("subitem_type_id")
      //  t.foreign("set_subitem_id").references(`${tables.SET_SUBITEM_TYPE}.id`);

        
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
    });

      await knex.schema.createTable(tables.PRODUCT_AVAILABLE_PERIOD, (t) => {
        t.increments("id");
        t.integer("available_period_id").notNullable()

        t.integer("discount_set_id")
        //t.foreign("discount_set_id").references(`${tables3.DISCOUNT_SET}.id`);

        t.integer("subitem_id")
        //t.foreign("subitem_id").references(`${tables3.SUBITEM}.id`);

        t.integer("product_id")
        //t.foreign("product_id").references(`${tables3.PRODUCT}.id`);

        t.integer("set_id")
        //t.foreign("set_id").references(`${tables.SET}.id`);

        t.integer("set_subitem_type_id")
        //t.foreign("set_subitem_type_id").references(`${tables.SET_SUBITEM_TYPE}.id`);
        
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.raw(`drop table IF Exists ${tables.SET} cascade`)
    await knex.raw(`drop table IF Exists ${tables.SET_SUBITEM_TYPE} cascade`)
    await knex.raw(`drop table IF Exists ${tables.SET_ORDER} cascade`)
    await knex.raw(`drop table IF Exists ${tables.SET_ORDER_DETAILS} cascade`)
    await knex.raw(`drop table IF Exists ${tables.PRICE_LIST} cascade`)
    await knex.raw(`drop table IF Exists ${tables.PHOTO_LIST} cascade`)
    await knex.raw(`drop table IF Exists ${tables.PRODUCT_AVAILABLE_PERIOD} cascade`)
 
}
