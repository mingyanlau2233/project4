import { Knex } from "knex";
//import { tables3 } from "./20211213132051_menuA";
import { tables2 } from "./20211211050802_setting";

const tables = Object.freeze({
  INGREDIENT_TYPE: "ingredient_type",
  INGREDIENT: "ingredient",
  INGREDIENT_EXPECTED_CONSUMPTION: "ingredient_expected_consumption",
  INVENTORY: "inventory",
});

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tables.INGREDIENT_TYPE, (t) => {
    t.increments("id");
    t.string("name").notNullable().unique();
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });
    
  await knex.schema.createTable(tables.INGREDIENT, (t) => {
    t.increments("id");
    t.string("name").notNullable().unique();
    t.string("unit").notNullable().unsigned();
    t.integer("ingredient_type_id").notNullable().unsigned();
    t.foreign("ingredient_type_id").references(`${tables.INGREDIENT_TYPE}.id`);
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });
    
  await knex.schema.createTable(tables.INGREDIENT_EXPECTED_CONSUMPTION, (t) => {
    t.increments("id");
    t.integer('qty').notNullable().unsigned();
    t.integer("ingredient_id").notNullable().unsigned();
    t.string("ingredient_name")
    t.integer("product_id").unsigned();
    t.string("product_name")
    t.integer("subitem_id").unsigned();
    t.string("subitem_name")
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });    
    
  await knex.schema.createTable(tables.INVENTORY, (t) => {
    t.increments("id");
    t.integer('in_out_qty').notNullable().unsigned();
    t.integer('qty').notNullable().unsigned();
    t.integer('unit').notNullable().unsigned();
    t.integer("ingredient_id").notNullable().unsigned();
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });    
}


export async function down(knex: Knex): Promise<void> {
  await knex.raw(`drop table IF Exists ${tables.INGREDIENT} cascade`);
  await knex.raw(`drop table IF Exists ${tables.INGREDIENT_TYPE} cascade`);
  await knex.raw(`drop table IF Exists ${tables.INGREDIENT_EXPECTED_CONSUMPTION} cascade`);
  await knex.raw(`drop table IF Exists ${tables.INVENTORY} cascade`);
}

