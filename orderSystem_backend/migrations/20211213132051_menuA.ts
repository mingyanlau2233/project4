import { Knex } from "knex";
import { tables2 } from "./20211211050802_setting";

export const tables3 = Object.freeze({
    PRODUCT_CATEGORY: "product_category",
    DEPARTMENT:"department",
    AVAILABLE_PERIOD:"available_period",
    PRODUCT:"product",
    DISCOUNT_SET:"discount_set",
    DISCOUNT_SET_ORDER:"discount_set_order",
    DISCOUNT_SET_DETAILS:"discount_set_details",
    DISCOUNT_SET_ORDER_DETAILS:"discount_set_order_details",
    DISCOUNT_SET_SEQUENCE:"discount_set_sequence",
    SUBITEM_TYPE:"subitem_type",
    SUBITEM:"subitem",
  });


export async function up(knex: Knex): Promise<void> {

    await knex.schema.createTable(tables3.PRODUCT_CATEGORY, (t) => {
        t.increments("id");
        t.string("name").notNullable().unique();
        t.boolean("is_inSetOnly")
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });

      await knex.schema.createTable(tables3.DEPARTMENT, (t) => {
        t.increments("id");
        t.string("name").notNullable().unique();
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });

      await knex.schema.createTable(tables3.AVAILABLE_PERIOD, (t) => {
        t.increments("id");
        t.string("name").notNullable().unique();
        t.time('start_time').notNullable();
        t.time('end_time').notNullable();
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });

      await knex.schema.createTable(tables3.PRODUCT, (t) => {
        t.increments("id");
        t.string("name").notNullable().unique();
        t.boolean("is_launched").notNullable();
        t.boolean("is_kiosk").notNullable();
        t.boolean("is_time_consuming").notNullable();
        t.integer('estimated_cost');
        t.string("photo")
        t.integer("price")
        t.integer("product_category_id").notNullable()
        t.foreign("product_category_id").references(`${tables3.PRODUCT_CATEGORY}.id`);
        
        t.integer("department_id")
        t.foreign("department_id").references(`${tables3.DEPARTMENT}.id`);
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });
      
      await knex.schema.createTable(tables3.DISCOUNT_SET, (t) => {
        t.increments("id");
        t.string("name").notNullable().unique();
        t.boolean("is_launched").notNullable();
        t.string("photo")
        t.integer("price")
        t.integer("session_id")
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });

      await knex.schema.createTable(tables3.DISCOUNT_SET_SEQUENCE, (t) => {
        t.increments("id");
        t.string("description").notNullable();
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });

      await knex.schema.createTable(tables3.DISCOUNT_SET_DETAILS, (t) => {
        t.increments("id");
        t.integer("product_category_id");
     
        t.integer("product_id");
        
        t.integer("discount_set_id");
      
        t.integer("discount_set_sequence_id");
        
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });

      await knex.schema.createTable(tables3.DISCOUNT_SET_ORDER, (t) => {
        t.increments("id");
        t.integer("price").notNullable();
        t.boolean("is_confirm").notNullable();
        t.integer("session_id").notNullable();
        t.integer("discount_set_id");
        t.string("discount_set_name");
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });
      await knex.schema.createTable(tables3.SUBITEM_TYPE, (t) => {
        t.increments("id");
        t.string("name").notNullable().unique();
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });
      
      await knex.schema.createTable(tables3.DISCOUNT_SET_ORDER_DETAILS, (t) => {
        t.increments("id");
        t.integer("product_id");
        t.string("product_name")
        t.integer("discount_set_order_id");
        t.string("discount_set_name")
        t.integer("discount_set_sequence_id");
        t.string("discount_set_sequence_name")
        t.integer("stage");
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });
      
      await knex.schema.createTable(tables3.SUBITEM, (t) => {
        t.increments("id");
        t.string("name").notNullable().unique();
        t.boolean("is_launched").notNullable();
        t.boolean("is_time_consuming").notNullable();
        t.integer("subitem_type_id").notNullable();
       
        t.integer("department_id");
       
        t.integer("company_id").unsigned();
        t.foreign("company_id").references(`${tables2.COMPANY}.id`);
        t.integer("branch_id").unsigned();
        t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
        t.timestamps(false, true);
      });
}


export async function down(knex: Knex): Promise<void> {

    await knex.raw(`drop table IF Exists ${tables3.PRODUCT_CATEGORY} cascade`)
    await knex.raw(`drop table IF Exists ${tables3.DEPARTMENT} cascade`)
    await knex.raw(`drop table IF Exists ${tables3.AVAILABLE_PERIOD} cascade`)
    await knex.raw(`drop table IF Exists ${tables3.PRODUCT} cascade`)
    await knex.raw(`drop table IF Exists ${tables3.DISCOUNT_SET} cascade`)
    await knex.raw(`drop table IF Exists ${tables3.DISCOUNT_SET_ORDER} cascade`)
    await knex.raw(`drop table IF Exists ${tables3.DISCOUNT_SET_DETAILS} cascade`)
    await knex.raw(`drop table IF Exists ${tables3.DISCOUNT_SET_SEQUENCE} cascade`)
    await knex.raw(`drop table IF Exists ${tables3.DISCOUNT_SET_ORDER_DETAILS} cascade`)
    await knex.raw(`drop table IF Exists ${tables3.SUBITEM_TYPE} cascade`)
    await knex.raw(`drop table IF Exists ${tables3.SUBITEM} cascade`)
}

