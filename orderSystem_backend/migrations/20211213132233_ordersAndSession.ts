import { Knex } from "knex";
import { tables2 } from "./20211211050802_setting";
import { tables3 } from "./20211213132051_menuA";
//import { tables } from "./20211213132106_menuB";

export const tables4 = Object.freeze({
    ORDERS: "orders",
    ORDER_CART: "order_cart",
    ORDER_DETAILS: "order_details",
    REMARKS: "remarks",
    ORDER_CART_REMARKS: "order_cart_remarks",
    REMARKS_DETAILS: "remarks_details",
  });

export async function up(knex: Knex): Promise<void> {      
  await knex.schema.createTable(tables4.ORDERS, (t) => {
    t.increments("id");
    t.time("closed_at");
    t.boolean("is_takeaways")
    t.string("session_id").notNullable().unsigned();
    // t.foreign("session_id").references(`${tables4.SESSION}.id`);
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });

  await knex.schema.createTable(tables4.ORDER_CART, (t) => {
    t.increments("id");
    t.integer('qty').notNullable().unsigned();
    t.integer('price').notNullable().unsigned();
    t.integer("orders_id").unsigned();
    //t.foreign("orders_id").references(`${tables4.ORDERS}.id`);
    t.integer("session_id").notNullable().unsigned();
   // t.foreign("session_id").references(`${tables4.SESSION}.id`);
    t.integer("product_id").unsigned();
    t.string("product_name")
    t.integer("discount_set_order_id").unsigned();
    t.string("discount_set_name")
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });    

  await knex.schema.createTable(tables4.ORDER_DETAILS, (t) => {
    t.increments("id");
    t.integer('qty').notNullable().unsigned();
    t.integer('price').notNullable().unsigned();
    t.boolean("is_takeaways");
    t.integer("orders_id")
    t.integer("stage")
    // t.foreign("orders_id").references(`${tables4.ORDERS}.id`);
    t.integer("product_id").unsigned();
    t.string("product_name")
    // t.foreign("product_id").references(`${tables3.PRODUCT}.id`);
    t.integer("discount_set_order_id").unsigned();
    t.string("discount_set_name")
    // t.foreign("discount_set_order_id").references(`${tables3.DISCOUNT_SET_ORDER}.id`);
    t.integer("set_order_id").unsigned();
    t.string("set_order_name")
    // t.foreign("set_order_id").references(`${tables.SET_ORDER}.id`);
    
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });    

  await knex.schema.createTable(tables4.REMARKS, (t) => {
    t.increments("id");
    t.string("name").notNullable();
    t.integer("price").notNullable().unsigned();
    t.integer("product_category_id");
    t.foreign("product_category_id").references(`${tables3.PRODUCT_CATEGORY}.id`);
    t.integer("subitem_type_id");
    t.foreign("subitem_type_id").references(`${tables3.SUBITEM_TYPE}.id`);
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });   

  await knex.schema.createTable(tables4.ORDER_CART_REMARKS, (t) => {
    t.increments("id");
    t.integer("order_details_id").notNullable().unsigned();
    
    t.integer("remarks_id").notNullable().unsigned();
    
    t.integer("order_cart_id").notNullable().unsigned();
    
    t.integer("company_id").unsigned();
    t.foreign("company_id").references(`${tables2.COMPANY}.id`);
    t.integer("branch_id").unsigned();
    t.foreign("branch_id").references(`${tables2.BRANCH}.id`);
    t.timestamps(false, true);
  });    

  await knex.schema.createTable(tables4.REMARKS_DETAILS, (t) => {
    t.increments("id");
    t.integer("remarks_id").notNullable().unsigned();
    
    t.integer("product_id").unsigned();
    
    t.integer("subitem_id").unsigned();
   
    t.integer("company_id").unsigned();
    
    t.integer("branch_id").unsigned();
   
    t.timestamps(false, true);
  });    
}



export async function down(knex: Knex): Promise<void> {
  await knex.raw(`drop table IF Exists ${tables4.REMARKS_DETAILS} cascade`)
  await knex.raw(`drop table IF Exists ${tables4.ORDER_CART_REMARKS} cascade`)
  await knex.raw(`drop table IF Exists ${tables4.REMARKS} cascade`)
  await knex.raw(`drop table IF Exists ${tables4.ORDER_DETAILS} cascade`)
  await knex.raw(`drop table IF Exists ${tables4.ORDER_CART} cascade`)
  await knex.raw(`drop table IF Exists ${tables4.ORDERS} cascade`)
}

