// Update with your config settings.
import dotenv from 'dotenv';
dotenv.config();


const knexConfig  = {
    development: {
      debug: false,
      client: 'postgresql',
      connection: {
        database: process.env.DB_NAME,
        user:     process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD
      },
      pool: {
        min: 2,
        max: 10
      },
      migrations: {
        tableName: 'knex_migrations'
      } 
    },
  
    staging: {
      client: 'postgresql',
      debug: false,
      connection: {
        database: process.env.DB_NAME,
        user:     process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD
      },
      pool: {
        min: 2,
        max: 10
      },
      migrations: {
        tableName: 'knex_migrations'
      } 
    },
  
    production: {
      client: 'postgresql',
      debug: false,
      connection: {
        database: process.env.DB_NAME,
        user:     process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD
      },
      pool: {
        min: 2,
        max: 10
      },
      migrations: {
        tableName: 'knex_migrations'
      } 
    }
  };

  export default knexConfig
module.exports = knexConfig