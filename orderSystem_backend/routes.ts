import express from 'express';

import { waitingTicketRoutes } from './routes/waitingTicket/waitingTicketRoutes';
import { menuSettingRoutes } from './routes/menuSetting/menuSettingRoutes';
import { subitemRoutes } from './routes/subitem/subitemRoutes';
import { userRoutes } from './routes/user/userRoutes';
import { deskAndSessionRoutes } from './routes/deskAndSession/deskAndSessionRoutes';
import { discountSetRoutes } from './routes/discountSet/discountSetRoutes';
import { postOrderRoutes } from './routes/postOrders.ts/postOrderRoutes';
import { staffRoutes } from './routes/staff/staffRoutes';
import { getOrderRoutes } from './routes/getOrder/getorderRoutes';
import { handleOrdersRoutes } from './routes/handleOrders/handleOrdersRoutes';

export const routes = express.Router()

routes.use('/waitingTicket',waitingTicketRoutes);
routes.use('/menuSetting',menuSettingRoutes);
routes.use('/getOrder',getOrderRoutes)
routes.use('/subitem',subitemRoutes);
routes.use('/postOrder',postOrderRoutes);
routes.use("/users", userRoutes);
routes.use('/handleOrders',handleOrdersRoutes)
routes.use('/deskAndSession',deskAndSessionRoutes);
routes.use('/discountSet',discountSetRoutes)
// console.log("Route")
routes.use('/staff',staffRoutes)
// routes.use('/userSetting',userSettingRoutes)
// routes.use('/ingredient',ingredientRoutes)


