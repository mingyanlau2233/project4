import Knex from 'knex';
//import path from 'path';
import cors from "cors";
import express from 'express';
import {logger} from './utils/logger'
import grant from 'grant';
import knexConfig from './knexfile';
export const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])
import path from 'path';
import http from "http";
import { Server as SocketIO } from "socket.io";

const app = express()
app.use(cors());

app.use(express.urlencoded({extended:true}));
app.use(express.json({limit: '50mb'}))
   
app.use((req, res, next) => {
    logger.debug(`req path: ${req.path}, method: ${req.path}`)
    next()
})

const grantExpress = grant.express({
  defaults: {
    origin: "https://api.activibe.site",
    transport: "session",
    state: true
  }
})

app.use(express.static(path.join(__dirname,"upload")))

const server = new http.Server(app);
export const io = new SocketIO(server, {
  cors: {
    origin: "*",
    methods: "*"
  }
});
io.on("connection", (socket) => {
  console.log(`[INFO] socket: ${socket.id} is connected`);
});

import {routes} from './routes'
const PORT = 8080;
const apiRoute = '/api'
app.use(apiRoute,routes)

app.use(grantExpress as express.RequestHandler);

server.listen(PORT,()=>{
    logger.info(`listening to PORT ${PORT}`)
})

