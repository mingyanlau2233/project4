import { Knex } from "knex";
import path from "path";
import xlsx from "xlsx";

const tables = Object.freeze({
    // SESSION: "session",
    ORDERS: "orders",
    ORDER_CART: "order_cart",
    ORDER_DETAILS: "order_details",
    // REMARKS: "remarks",
    ORDER_CART_REMARKS: "order_cart_remarks",
    REMARKS_DETAILS: "remarks_details",
});
  
// interface SessionRow{
//     id: string;
//     is_client_left: boolean;
//     is_takeaways: boolean;
//     total_price: number;
//     company_id:number;
//     branch_id:number
// }
  
interface OrdersRow{
    id: number;
    closed_at: Date;
    is_takeaways: boolean;
    session_id: string;
    company_id: number;
    branch_id: number;
}
  
interface Order_cartRow{
    id: number;
    qty: number;
    price: number;
    orders_id: number;
    session_id:string;
    product_id: number;
    discount_set_order_id:number;
    company_id: number;
    branch_id: number;
}
  
interface Order_detailsRow {
    id: number;
    qty: number;
    price: number;
    is_takeaways: boolean;
    orders_id: number;
    product_id:number;
    discount_set_order_id: number;
    set_order_id: number;
    company_id: number;
    branch_id: number;
}
  
// interface RemarksRow {
//     id: number;
//     name: number;
//     price: number;
//     product_category_id: number;
//     subitem_type_id: number;
//     company_id: number;
//     branch_id:number;
// }
  
interface Order_cart_remarksRow{
    id: number;
    order_details_id: number;
    remarks_id: number;
    order_cart_id: number;
    company_id: number;
    branch_id:number;
}

interface Remarks_detailsRow{
    id: number;
    remarks_id: number;
    product_id: number;
    subitem_id: number;
    company_id: number;
    branch_id:number;
}

function readDatasets<T>(filename: string) {
    const BASE_PATH = path.join(__dirname, "ordersAndSession");
    const workbook = xlsx.readFile(path.join(BASE_PATH, filename), {
    cellDates: true,
    });
    const data = xlsx.utils.sheet_to_json<T>(workbook.Sheets["Sheet1"]);
    return data;
}

export async function seed(knex: Knex): Promise<void> {
    // const sessionData= readDatasets<SessionRow>("session.csv");
    const ordersData= readDatasets<OrdersRow>("orders.csv");
    const order_cartData= readDatasets<Order_cartRow>("order_cart.csv");
    const order_detailsData= readDatasets<Order_detailsRow>("order_details.csv");
    // const remarksData= readDatasets<RemarksRow>("remarks.csv");
    const order_cart_remarksData= readDatasets<Order_cart_remarksRow>("order_cart_remarks.csv");
    const remarks_detailsData= readDatasets<Remarks_detailsRow>("remarks_details.csv");

    const trx = await knex.transaction();
    try {
        // await trx.raw(/*sql*/ `TRUNCATE ${tables.SESSION} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.ORDERS} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.ORDER_CART} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.ORDER_DETAILS} RESTART IDENTITY CASCADE`);
        // await trx.raw(/*sql*/ `TRUNCATE ${tables.REMARKS} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.ORDER_CART_REMARKS} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.REMARKS_DETAILS} RESTART IDENTITY CASCADE`);

        // await trx(tables.SESSION).insert(sessionData);
        await trx(tables.ORDERS).insert(ordersData);
        await trx(tables.ORDER_CART).insert(order_cartData);
        await trx(tables.ORDER_DETAILS).insert(order_detailsData);
        // await trx(tables.REMARKS).insert(remarksData);
        await trx(tables.ORDER_CART_REMARKS).insert(order_cart_remarksData);
        await trx(tables.REMARKS_DETAILS).insert(remarks_detailsData);
        await trx.commit();
    }catch (err) {
        console.error(err.message);
        await trx.rollback();
    }
}

