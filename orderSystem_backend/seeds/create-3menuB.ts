import { Knex } from "knex";
import path from "path";
import xlsx from "xlsx";

const tables = Object.freeze({
  SET: "set",
  SET_SUBITEM_TYPE: "set_subitem_type",
  SET_ORDER: "set_order",
  SET_ORDER_DETAILS: "set_order_details",
  PRODUCT_AVAILABLE_PERIOD: "product_available_period",
  PRICE_LIST: "price_list",
  PHOTO_LIST: "photo_list",
});

interface SetRow {
  id: number;
  price: string;
  name: string;
  is_launched: boolean;
}

interface Set_subitem_typeRow {
  id: number;
  sequence: number;
  max_limit: number;
  set_id: number;
  product_category_id: number;
  subitem_type_id: number;
  product_id: number;
  is_launched: number;
}

interface Set_orderRow {
  id: number;
  is_confirm: boolean;
  top_up_price: number;
  set_id: number;
  total_price: number;
}

interface Set_order_detailsRow {
  id: number;
  sequence: number;
  subitem_type_name: string;
  product_category_name: string;
  product_name: string;
  subitem_name: string;
  set_order_id: number;
  product_id: number;
  subitem_id: number;
  top_up_price: number;
}

interface Product_available_periodRow {
  id: number;
  available_period_id: number;
  product_id: number;
  discount_set_id: number;
  set_id: number;
  subitem_id: number;
  set_subitem_type_id: number;
}

interface Price_listRow {
  id: number;
  price: number;
  product_id: number;
  discount_set_id: number;
  set_id: number;
  subitem_id: number;
  top_up_price: number;
}

interface Photo_listRow {
  id: number;
  photo: string;
  product_id: number;
  discount_set_id: number;
  set_id: number;
  subitem_id: number;
  subitem_type_id: number;
}

function readDatasets<T>(filename: string) {
  const BASE_PATH = path.join(__dirname, "menuB");
  const workbook = xlsx.readFile(path.join(BASE_PATH, filename), {
    cellDates: true,
  });
  const data = xlsx.utils.sheet_to_json<T>(workbook.Sheets["Sheet1"]);
  return data;
}

export async function seed(knex: Knex): Promise<void> {
  const setData = readDatasets<SetRow>("set.csv");
  const set_subitem_typeData = readDatasets<Set_subitem_typeRow>(
    "set_subitem_type.csv"
  );
  const set_orderData = readDatasets<Set_orderRow>("set_order.csv");
  const set_order_detailsData = readDatasets<Set_order_detailsRow>(
    "set_order_details.csv"
  );
  const product_available_periodData =
    readDatasets<Product_available_periodRow>("product_available_period.csv");
  const price_listData = readDatasets<Price_listRow>("price_list.csv");
  const photo_listData = readDatasets<Photo_listRow>("photo_list.csv");

  const trx = await knex.transaction();
  try {
    await trx.raw(/*sql*/ `TRUNCATE ${tables.SET} RESTART IDENTITY CASCADE`);
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.SET_SUBITEM_TYPE} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.SET_ORDER} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.SET_ORDER_DETAILS} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.PRODUCT_AVAILABLE_PERIOD} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.PRICE_LIST} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.PHOTO_LIST} RESTART IDENTITY CASCADE`
    );

    await trx(tables.SET).insert(setData);
    await trx(tables.SET_SUBITEM_TYPE).insert(set_subitem_typeData);
    await trx(tables.SET_ORDER).insert(set_orderData);
    await trx(tables.SET_ORDER_DETAILS).insert(set_order_detailsData);
    await trx(tables.PRODUCT_AVAILABLE_PERIOD).insert(
      product_available_periodData
    );
    await trx(tables.PRICE_LIST).insert(price_listData);
    await trx(tables.PHOTO_LIST).insert(photo_listData);

    await trx.commit();
  } catch (err) {
    console.error(err.message);
    await trx.rollback();
  }
}
