import { Knex } from "knex";
import path from "path";
import xlsx from "xlsx";
import fs from 'fs';

const tables = Object.freeze({
  PRODUCT_CATEGORY: "product_category",
  DEPARTMENT: "department",
  AVAILABLE_PERIOD: "available_period",
  PRODUCT: "product",
  DISCOUNT_SET: "discount_set",
  // DISCOUNT_SET_ORDER: "discount_set_order",
  DISCOUNT_SET_DETAILS: "discount_set_details",
  // DISCOUNT_SET_ORDER_DETAILS: "discount_set_order_details",
  DISCOUNT_SET_SEQUENCE:"discount_set_sequence",
  SUBITEM_TYPE: "subitem_type",
  SUBITEM: "subitem",
});

interface DepartmentRow {
  id: number;
  name: string;
}

interface Product_categoryRow {
  id: number;
  name: string;
}

interface Available_periodRow {
  id: number;
  name: string;
}

interface ProductRow {
  id: number;
  name: string;
  is_launched: boolean;
  is_time_consuming: boolean;
  is_kiosk: boolean;
  estimated_cost: number;
  product_category_id: number;
  department_id: number;
}

interface Discount_setRow {
  id: number;
  name: string;
  is_launched: boolean;
}

// interface Discount_set_orderRow {
//   id: number;
//   price: number;
//   is_confirm: boolean;
//   discount_set_id: number;
// }

interface Discount_set_detailsRow {
  id: number;
  product_category_id: number;
  discount_set_sequence_id: number;
  discount_set_id: number;
  product_id: number;
}

// interface Discount_set_order_detailsRow {
//   id: number;
//   product_id: number;
//   discount_set_order_id: number;
// }

interface Discount_set_sequenceRow {
  id: number;
  description: string;
}

interface Subitem_typeRow {
  id: number;
  name: string;
}

interface SubitemRow {
  id: number;
  name: string;
  is_time_consuming: boolean;
  is_launched: boolean;
  subitem_type_id: number;
}

function readDatasets<T>(filename: string) {
  const BASE_PATH = path.join(__dirname, "menuA");
  const workbook = xlsx.readFile(path.join(BASE_PATH, filename), {
    cellDates: true,
  });
  const data = xlsx.utils.sheet_to_json<T>(workbook.Sheets["Sheet1"]);
  console.log(data);
  return data;
}

export async function seed(knex: Knex): Promise<void> {
  const product_categoryData = readDatasets<Product_categoryRow>(
    "product_category.csv"
  );
  const available_periodData = readDatasets<Available_periodRow>(
    "available_period.csv"
  );
  const productData = readDatasets<ProductRow>("product.csv");
  const discount_setData = readDatasets<Discount_setRow>("discount_set.csv");
  // const discount_set_orderData = readDatasets<Discount_set_orderRow>(
  //   "discount_set_order.csv"
  // );
  const discount_set_detailsData = readDatasets<Discount_set_detailsRow>(
    "discount_set_details.csv"
  );
  // const discount_set_order_detailsData =
  //   readDatasets<Discount_set_order_detailsRow>(
  //     "discount_set_order_details.csv"
  //   );
  const discount_set_sequenceData = readDatasets<Discount_set_sequenceRow>("discount_set_sequence.csv");
  const subitem_typeData = readDatasets<Subitem_typeRow>("subitem_type.csv");
  const subitemData = readDatasets<SubitemRow>("subitem.csv");

  readDatasets<DepartmentRow>("./department.csv");
  console.log('Preparing to read csv from fs')
  const dataFromFS = fs.readFileSync(__dirname + '/menuA/department.csv', {encoding:'utf8', flag:'r'});
  const dataFromFSList = dataFromFS.split('\n')
  const heading = dataFromFSList[0].split(',')
  let dataObjectArray = []
  for (let i = 1; i < dataFromFSList.length; i++) {
    let dataContent = dataFromFSList[i].split(',')
    let dataObject  = {}
    dataObject[heading[0]] = dataContent[0];
    dataObject[heading[1]] = dataContent[1];
    dataObject[heading[2]] = dataContent[2];
    dataObjectArray.push(dataObject)
  }
  console.log(dataObjectArray)
  // console.log(`[dataObject]: ${Object.prototype.toString.call(dataObject)}\n`);
  // for (let val in dataObject) {
  //   console.log(val + ': ' + dataObject[val])
  // }

  
  const trx = await knex.transaction();
  try {
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.PRODUCT_CATEGORY} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.DEPARTMENT} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.AVAILABLE_PERIOD} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.PRODUCT} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.DISCOUNT_SET} RESTART IDENTITY CASCADE`
    );
    // await trx.raw(
    //   /*sql*/ `TRUNCATE ${tables.DISCOUNT_SET_ORDER} RESTART IDENTITY CASCADE`
    // );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.DISCOUNT_SET_DETAILS} RESTART IDENTITY CASCADE`
    );
    // await trx.raw(
    //   /*sql*/ `TRUNCATE ${tables.DISCOUNT_SET_ORDER_DETAILS} RESTART IDENTITY CASCADE`
    // );
    await trx.raw(/*sql*/ `TRUNCATE ${tables.DISCOUNT_SET_SEQUENCE} RESTART IDENTITY CASCADE`);
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.SUBITEM_TYPE} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
      /*sql*/ `TRUNCATE ${tables.SUBITEM} RESTART IDENTITY CASCADE`
    );

    await trx(tables.PRODUCT_CATEGORY).insert(product_categoryData);
    await trx(tables.DEPARTMENT).insert(dataObjectArray);
    // console.log(product_categoryData)
    // console.log(departmentData)
    // console.log(available_periodData)
    // console.log(productData)
    // console.log(discount_setData)
    // console.log(discount_set_orderData)
    // console.log(discount_set_detailsData)
    // console.log(discount_set_order_detailsData)
    // console.log(subitem_typeData)
    // console.log(subitemData)
    // console.log(discount_set_sequenceData)

    await trx(tables.AVAILABLE_PERIOD).insert(available_periodData);
    await trx(tables.PRODUCT).insert(productData);
    await trx(tables.DISCOUNT_SET).insert(discount_setData);
    // await trx(tables.DISCOUNT_SET_ORDER).insert(discount_set_orderData);
    await trx(tables.DISCOUNT_SET_SEQUENCE).insert(discount_set_sequenceData);
    await trx(tables.DISCOUNT_SET_DETAILS).insert(discount_set_detailsData);
    // await trx(tables.DISCOUNT_SET_ORDER_DETAILS).insert(discount_set_order_detailsData);
    await trx(tables.SUBITEM_TYPE).insert(subitem_typeData);
    await trx(tables.SUBITEM).insert(subitemData);

    await trx.commit();
  } catch (err) {
    console.error(err.message);
    await trx.rollback();
  }
}
