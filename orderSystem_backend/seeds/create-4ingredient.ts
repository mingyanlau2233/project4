import { Knex } from "knex";
import path from "path";
import xlsx from "xlsx";

const tables = Object.freeze({
  INGREDIENT_TYPE: "ingredient_type",
  INGREDIENT: "ingredient",
  INGREDIENT_EXPECTED_CONSUMPTION: "ingredient_expected_consumption",
  INVENTORY: "inventory",
});

interface Ingredient_typeRow{
  id: number;
  name: string;
  company_id:number;
  branch_id:number;
}

interface IngredientRow {
  id: number;
  name: string;
  unit:string;
  ingredient_type_id:number;
  company_id: number;
  branch_id:number;
}

interface Ingredient_expected_consumptionRow {
  id: number;
  qty: number;
  ingredient_id: number;
  product_id:number;
  subitem_id:number;
  company_id: number;
  branch_id:number;
}

interface InventoryRow {
  id: number;
  in_out_qty: number;
  qty:number;
  unit: number;
  ingredient_id:number;
  company_id:number;
  branch_id:number;
}

function readDatasets<T>(filename: string) {
  const BASE_PATH = path.join(__dirname, "ingredient");
  const workbook = xlsx.readFile(path.join(BASE_PATH, filename), {
    cellDates: true,
  });
  const data = xlsx.utils.sheet_to_json<T>(workbook.Sheets["Sheet1"]);
  return data;
}

export async function seed(knex: Knex): Promise<void> {
const ingredient_typeData= readDatasets<Ingredient_typeRow>("ingredient_type.csv");
const ingredientData= readDatasets<IngredientRow>("ingredient.csv");
const ingredient_expected_consumptionData= readDatasets<Ingredient_expected_consumptionRow>("ingredient_expected_consumption.csv");
const inventoryData= readDatasets<InventoryRow>("inventory.csv");

  const trx = await knex.transaction();
  try {
    await trx.raw(
    /*sql*/ `TRUNCATE ${tables.INGREDIENT_TYPE} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
    /*sql*/ `TRUNCATE ${tables.INGREDIENT} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
    /*sql*/ `TRUNCATE ${tables.INGREDIENT_EXPECTED_CONSUMPTION} RESTART IDENTITY CASCADE`
    );
    await trx.raw(
    /*sql*/ `TRUNCATE ${tables.INVENTORY} RESTART IDENTITY CASCADE`
    );
    await trx(tables.INGREDIENT_TYPE).insert(ingredient_typeData);
    await trx(tables.INGREDIENT).insert(ingredientData);
    await trx(tables.INGREDIENT_EXPECTED_CONSUMPTION).insert(ingredient_expected_consumptionData);
    await trx(tables.INVENTORY).insert(inventoryData);
    // console.log(product_categoryData)
    // console.log(departmentData)
    // console.log(available_periodData)
    // console.log(productData)
    // console.log(discount_setData)
    // console.log(discount_set_orderData)
    // console.log(discount_set_detailsData)
    // console.log(discount_set_order_detailsData)
    // console.log(subitem_typeData)
    // console.log(subitemData)
    // console.log(discount_set_sequenceData)
    await trx.commit();
} catch (err) {
    console.error(err.message);
    await trx.rollback();
}
}


