import { Knex } from "knex";
import path from "path";
import xlsx from "xlsx";

const tables = Object.freeze({
    COMPANY:"company",
    BRANCH:"branch",
    STAFF: "staff",
    AREA: "area",
    DESK: "desk",
    SEAT: "seat",
    SERVICE_CHARGE: "service_charge",
    TAKEAWAYS_CODE: "takeaways_code",
    NUM_OF_PERSON: "num_of_person",
    TICKET_TYPE: "ticket_type",
    TICKET: "ticket",
})

interface CompanyRow{
    id:number;
    name:string;
    password:string;
    is_subscribed:boolean;
    resigned_at:string;
}

interface BranchRow{
    id:number;
    name:string;
    is_subscribed:boolean;
    resigned_at:string;
    company_id:number;
}

interface StaffRow{
    id:number;
    name:string;
    password:string;
    position:string;
    is_onboard:boolean;
    resigned_at:string;
    company_id:number;
    branch_id:number;
}

interface AreaRow{
    id:number;
    sequence:number;
    description:string;
    status:string;
    company_id:number;
    branch_id:number;
}

interface DeskRow{
    id:number;
    sequence:number;
    status:string;
    area_id:number;
    company_id:number;
    branch_id:number;
}

interface SeatRow{
    id:number;
    sequence:number;
    is_occupied:boolean;
    status:string;
    desk_id:number;
    company_id:number;
    branch_id:number;
}

interface Service_chargeRow{
    is_charged:boolean;
    company_id:number;
    branch_id:number;
}

interface Takeaways_codeRow{
    id:number;
    code:number;
    is_picked:boolean;
    company_id:number;
    branch_id:number;
}


interface Ticket_typeRow{
    id:number;
    name:string;
    num_group:number;
    company_id:number;
    branch_id:number;
}

interface TicketRow{
    id:number;
    is_seat_assigned:boolean;
    ticket_type_id:number;
    company_id:number;
    branch_id:number;
}


function readDatasets<T>(filename: string) {
const BASE_PATH = path.join(__dirname, "baseSetting");
const workbook = xlsx.readFile(path.join(BASE_PATH, filename), {
    cellDates: true,
});
const data = xlsx.utils.sheet_to_json<T>(workbook.Sheets["Sheet1"]);
return data;
}

export async function seed(knex: Knex): Promise<void> {
    const companyData = readDatasets<CompanyRow>( "company.csv");
    const branchData = readDatasets<BranchRow>("branch.csv");
    const staffData = readDatasets<StaffRow>("staff.csv");
    const areaData = readDatasets<AreaRow>("area.csv");
    const deskData = readDatasets<DeskRow>("desk.csv");
    const seatData = readDatasets<SeatRow>("seat.csv");
    const service_chargeData = readDatasets<Service_chargeRow>("service_charge.csv");
    const takeaways_codeData = readDatasets<Takeaways_codeRow>("takeaways_code.csv");
    const ticket_typeData = readDatasets<Ticket_typeRow>("ticket_type.csv");
    const ticketData = readDatasets<TicketRow>("ticket.csv");
    
    const trx = await knex.transaction();
    try {
        await trx.raw(/*sql*/ `TRUNCATE ${tables.COMPANY} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.BRANCH} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.STAFF} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.AREA} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.DESK} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.SEAT} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.SERVICE_CHARGE} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.TAKEAWAYS_CODE} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.TICKET_TYPE} RESTART IDENTITY CASCADE`);
        await trx.raw(/*sql*/ `TRUNCATE ${tables.TICKET} RESTART IDENTITY CASCADE`);

        await trx(tables.COMPANY).insert(companyData);
        await trx(tables.BRANCH).insert(branchData);
        await trx(tables.STAFF).insert(staffData);
        await trx(tables.AREA).insert(areaData);
        await trx(tables.DESK).insert(deskData);
        await trx(tables.SEAT).insert(seatData);
        await trx(tables.SERVICE_CHARGE).insert(service_chargeData);
        await trx(tables.TAKEAWAYS_CODE).insert(takeaways_codeData);
        await trx(tables.TICKET_TYPE).insert(ticket_typeData);
        await trx(tables.TICKET).insert(ticketData);
    
        await trx.commit();
    } catch (err) {
        console.error(err.message);
        await trx.rollback();
    }
    }






















// export async function seed(knex: Knex): Promise<void> {
//     // Deletes ALL existing entries
//     // await knex("table_name").del();

//     // // Inserts seed entries
//     // await knex("table_name").insert([
//     //     { id: 1, colName: "rowValue1" },
//     //     { id: 2, colName: "rowValue2" },
//     //     { id: 3, colName: "rowValue3" }
//     // ]);
// };