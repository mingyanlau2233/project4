# dot2easy

## 1. set up database 
   please install PSQL and create a dataBase 

## 2. set up backend Server 
    2.1 cd to folder "orderSystem_backend" in terminal
    2.2 run "yarn install " in terminal
    2.3 set up .env and input your DB Name (please reference to file ".env.example" in the sample folder )
    2.2 run "yarn knex migrate:latest " in terminal
    2.3 run "yarn run dev" in terminal 

## 3. set up frontend Server (Please do it in  other terminal )
     3.1 cd to folder "my-app"
     3.2  run "yarn install " in terminal
     3.3  set up .env and input your DB Name (please reference to file ".env.example" in the sample folder )
     3.4  run "yarn start " in terminal 


## 4. login 
    4.1 loginName : admin 
        PW:12345678
        level: admin 
    4.2 loginName : staff
        PW:12345678
        level: staff 
    4.3 loginName : customer  
        PW:12345678
        level: customer  

