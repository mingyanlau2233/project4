

export function getDesk(deskData:any) {
     console.log("action",deskData);
    return {
        type: "@@EditDesk/GET_DESK" as const,
        deskData,
    };
}

export function getSeat(seatData:any) {
    return {
        type: "@@EditDesk/GET_SEAT" as const,
        seatData,
    };
}



export type IEditDeskAction =
    // | ReturnType<typeof getArea>
    | ReturnType<typeof getDesk>
    | ReturnType<typeof getSeat>
