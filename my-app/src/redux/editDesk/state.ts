
// export interface AreaState {
//     id: number;
//     sequence: number;
//     description: string;
// }

export interface DeskState {
    id: number;
    sequence: number;
    seats: SeatState[]
}

export interface SeatState {
    id: number;
    sequence: number;
    is_occupied: boolean;
    desk_id: number;
    session_id: string;
}



export interface IEditDeskState {
    // areaFromDB: AreaState [] | [];
    desks: DeskState[]
    seats: SeatState[]
}