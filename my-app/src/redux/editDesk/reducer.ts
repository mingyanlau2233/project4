import produce from "immer";
import { IEditDeskAction } from "./action";
import { IEditDeskState } from "./state";

// initialState
const initialState: IEditDeskState = {
    desks: [],
    seats:[],
};

//reducer [Function]
export const editDeskReducer = produce((state: IEditDeskState, action: IEditDeskAction) => {
    switch (action.type) {
        case "@@EditDesk/GET_DESK":
            state.desks = action.deskData;
            // console.log("reducer", state.desks)

        return;
     
    }

    switch (action.type) {
        case "@@EditDesk/GET_SEAT":
            state.seats = action.seatData;
            // console.log("reducer", state.desks)

        return;
     
    }
    return state;
}, initialState);

