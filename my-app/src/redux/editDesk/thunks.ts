import jwtDecode from "jwt-decode";
import { Dispatch } from "react";
import { IRootAction } from "../store";
import { getDesk, getSeat } from "./action";
import { fetchGetDesk, fetchPostDesk, fetchDeleteDesk, fetchGetSeat, fetchPostSeat, fetchDeleteSeat, fetchDeleteAllDesk, fetchGetSeating, fetchPostSession } from "./api";


export function getDeskThunk() {
    return async (dispatch: Dispatch<IRootAction>) => {
        const result = await fetchGetDesk()
        // console.log(result)
        dispatch(getDesk(result));
    }
}

export function postDeskThunk() {
    console.log("post Desk Thunk")
    return async (dispatch: Dispatch<IRootAction>) => {
        console.log("post Desk Thunk2")
        await fetchPostDesk()
        const result = await fetchGetDesk()
        dispatch(getDesk(result));
    }
}

export function deleteAllDeskThunk() {
    return async (dispatch: Dispatch<IRootAction>) => {
        console.log("deleteDesk")
        await fetchDeleteAllDesk();
        const result = await fetchGetDesk()
        // console.log(result)
        dispatch(getDesk(result));
    }
}

export function deleteDeskThunk(desk_id: number) {
    return async (dispatch: Dispatch<IRootAction>) => {
        console.log("deleteDesk")
        await fetchDeleteDesk(desk_id);
        const result = await fetchGetDesk()
        // console.log(result)
        dispatch(getDesk(result));

    }
}

export function getSeatThunk() {
    return async (dispatch: Dispatch<IRootAction>) => {
        const data = await fetchGetSeat()
        dispatch(getSeat(data));
    }
}

export function postSeatThunk(desk_id: number) {
    return async (dispatch: Dispatch<IRootAction>) => {
        const data = await fetchPostSeat(desk_id)
        const result = await fetchGetDesk()
        dispatch(getDesk(result));
    }
}

export function deleteSeatThunk(props: any) {
    return async () => {
        // console.log("deleteSeat")
        const token: string | null = localStorage.getItem("token");
        let result
        if (token) {
            result = await jwtDecode(token)
            // console.log("result", result)
            await fetchDeleteSeat(props.id);
        }
    }
}

export function getSeatingThunk() {
    return async (dispatch: Dispatch<IRootAction>) => {
        const result = await  fetchGetSeating()
        // console.log(result)
        dispatch(getDesk(result));
    }
}

export function postSessionThunk(seatData:[]) {
    return async (dispatch: Dispatch<IRootAction>) => {
        const result = await  fetchPostSession(seatData)
         console.log(result)
       // dispatch(getDesk(result));
    }
}