import axios from 'axios';
const { REACT_APP_BACKEND_BASE_URL } = process.env;

export async function fetchGetDesk() {
    console.log("fetchGetDesk")
    const resp = await axios({
        url:`${REACT_APP_BACKEND_BASE_URL}/deskAndSession/getDesk`,
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        method: 'GET',
    });
        let data = resp.data.result
        console.log("department",data)
        return data;
}

export async function fetchPostDesk() {
    // console.log("postDesk",company_id,branch_id)
    const resp = await axios({
        url:`${REACT_APP_BACKEND_BASE_URL}/deskAndSession/postDesk`,
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
        },
        method: 'POST',
    });
        let data = resp.data
        console.log(data)
        return data;
}

export async function fetchDeleteAllDesk() {
    const resp = await axios({
        url:`${REACT_APP_BACKEND_BASE_URL}/deskAndSession/deleteAllDesk`,
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        method: 'DELETE',
    });
        return;
}

export async function fetchDeleteDesk(id:number) {
    const resp = await axios({
        url:`${REACT_APP_BACKEND_BASE_URL}/deskAndSession/deleteDesk`,
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
        },
        method: 'DELETE',
        data: {
            id: id,
        }
    });
        return;
}


export async function fetchGetSeat() {
    console.log("getSeat")
    const resp = await axios({
        url:`${REACT_APP_BACKEND_BASE_URL}/deskAndSession/getSeat`,
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        method: 'GET',   
    });
        let data = resp.data.result
         console.log("getSeat",data)
        return data;
}

export async function fetchPostSeat(desk_id:number) {
    // console.log("postDesk",company_id,branch_id)
    const resp = await axios({
        url:`${REACT_APP_BACKEND_BASE_URL}/deskAndSession/postSeat`,
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
        },
        method: 'POST',
        data: {
            desk_id: desk_id
        }
    });
        let data = resp.data
        // console.log(data)
        return data;
}

export async function fetchDeleteSeat(id:number) {
    const resp = await axios({
        url:`${REACT_APP_BACKEND_BASE_URL}/deskAndSession/deleteSeat`,
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
        },
        method: 'DELETE',
        data: {
            id: id,
        }
    });
        return;
}

export async function fetchGetSeating() {
    console.log("fetchGetDesk")
    const resp = await axios({
        url:`${REACT_APP_BACKEND_BASE_URL}/deskAndSession/getSeating`,
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        method: 'GET',
    });
        let data = resp.data.result
        // console.log("getDesk",data)
        return data;
}

export async function fetchPostSession(seatData:[]) {
    // console.log("postDesk",company_id,branch_id)
    const resp = await axios({
        url:`${REACT_APP_BACKEND_BASE_URL}/deskAndSession/postSession`,
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
        },
        method: 'POST',
    });
        let data = resp.data
        console.log(data)
        return data;
}