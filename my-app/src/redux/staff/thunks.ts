import { Dispatch } from "redux"
import jwtDecode from "jwt-decode"
import { IRootAction } from "../store"
import { deleteStaffAction, getAllStaffAction, getStaffAction, postStaffAction, putStaffAction } from "./action";
import axios from "axios";
import { fetchPostStaff } from "./api";

const { REACT_APP_BACKEND_BASE_URL } = process.env;

export function getAllStaffThunk() {
    return async (dispatch: Dispatch<IRootAction>) => {
       
        const result = await axios.get(`${REACT_APP_BACKEND_BASE_URL}/staff/getAllStaff`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        })
        
        const data = result.data
        // console.log("hihihi")
       
        // const data = await axiosGetStaff(result.company_id,result.brand_id)
        // dispatch(getStaffAction(data))
        dispatch(getAllStaffAction(data))
    }

}

export function deleteStaff(id: number) {
    return async (dispatch: Dispatch<IRootAction>) => {
        const result = await axios({
            url: `${REACT_APP_BACKEND_BASE_URL}/staff/deleteStaff`,
            headers: {
                'content-type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            method: "DELETE",
            data: {
                id: id
            }

        })
        dispatch(deleteStaffAction(id))
    }
}

export function postStaffThunk(data:any) {
    return async (dispatch: Dispatch<IRootAction>) => {
        // console.log('thnbks',data)
        const result = await axios({
            url: `${REACT_APP_BACKEND_BASE_URL}/staff/postStaff`,
            headers: {
                'content-type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            method: "POST",
            data: data
        })
        const getAllStaff = await axios.get(`${REACT_APP_BACKEND_BASE_URL}/staff/getAllStaff`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        })
        
        const getAllStaffData = getAllStaff.data
        dispatch(getAllStaffAction(getAllStaffData))
    }
}

export function putStaffThunk(data:any,id:number){
    return async (dispatch: Dispatch<IRootAction>) =>{
        const result = await axios({
            url: `${REACT_APP_BACKEND_BASE_URL}/staff/putStaff`,
            headers: {
                'content-type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
            method: "PUT",
            data: {...data, id}
        })
        dispatch(putStaffAction(id, data))
        // console.log(result)
        // dispatch(putAllStaffAction())
        // const getAllStaff = await axios.get(`${REACT_APP_BACKEND_BASE_URL}/staff/getAllStaff`, {
        //     headers: {
        //         Authorization: `Bearer ${localStorage.getItem("token")}`,
        //     },
        // })
        
        // const getAllStaffData = getAllStaff.data
        // dispatch(getAllStaffAction(getAllStaffData))
    }

}




// export  function postStaffThunk(staffData:any) {
//     return async(dispatch:Dispatch<IRootAction>)=>{
//         console.log("thunk")
//         const token:string|null = localStorage.getItem("token");
//         console.log(token)
//          let result
//         if (token){
//              result = await jwtDecode(token)
//              staffData['company_id'] = result.company_id
//              staffData['branch_id'] = result.branch_id
//             const data = await fetchPostStaff(staffData)
//             console.log(result)
     
           
//         }
//   }
// }