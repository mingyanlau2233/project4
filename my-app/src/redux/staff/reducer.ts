import { IStaffState } from "./state";
import produce from "immer"
import { IStaffAction } from "./action";
import { Staff } from "./state";

const initialState: IStaffState = {
    staff: []
}

export const staffReducers = produce((state: IStaffState, action: IStaffAction) => {
    console.log(action.type)

    switch (action.type) {
        case "@@AllStaff/Get":
            state.staff = action.staff
            console.log(state.staff)
            return
        case "@@Staff/Get":
            state.staff = action.staff
            // console.log(state.staff)
            return

        case "@@Staff/Delete":
            state.staff = state.staff.filter((staff) => staff.id !== action.id)
            return
        case "@@Staff/Post":
            // const staff = {...action.staff, id: action.id}
            // console.log(staff)
            // state.staff.push(staff)
            return
        case "@@Staff/Put": {
            // const staff = {...action.staff, id: action.id}
            // console.log(staff)
            // state.staff.push(staff)
            console.log(state.staff)
            console.log(action)
            let idx = state.staff.findIndex(staff => staff.id === action.id);
            if (idx !== -1) {
                const toBeUpdateStaff = {...state.staff[idx], ...action.staff}
                state.staff[idx] = toBeUpdateStaff;
            }
            return
        }
        default:
            return

    }
}, initialState)