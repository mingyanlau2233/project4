export interface Staff{
    id:number,
    name: string,
    position:string,
    is_onboard:boolean,
    created_at:Date
}

export interface IStaffState{
    staff: Staff[]
}