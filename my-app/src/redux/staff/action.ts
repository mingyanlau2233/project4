import { Staff } from "./state";

export function getAllStaffAction(staff:any){
    console.log("staff",staff)
    return{
        
        type:"@@AllStaff/Get" as const,
        staff
    }
}
export function getStaffAction(staff:any){
    return{
        type:"@@Staff/Get" as const,
        staff
    }
}

export function deleteStaffAction(id:number){
    return{
        type:"@@Staff/Delete" as const,
        id
    }
}   

export function postStaffAction(id: number, staff:Staff){
    return{
        type:"@@Staff/Post" as const,
        staff,
        id
    }
}  

export function putStaffAction(id: number, staff:Staff){
    return{
        type:"@@Staff/Put" as const,
        staff,
        id
    }
}   

export type IStaffAction = 
ReturnType<typeof getAllStaffAction> |
ReturnType<typeof deleteStaffAction>|ReturnType <typeof postStaffAction>
|ReturnType <typeof getStaffAction>|ReturnType <typeof putStaffAction>
