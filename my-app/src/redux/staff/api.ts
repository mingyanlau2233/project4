const { REACT_APP_BACKEND_BASE_URL } = process.env;


export async function fetchPostStaff(staffData: any) {
    console.log("Fetchstaff",staffData)
     const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/staff/postStaff`, {
       method: "POST",
       headers: {
         Authorization: `Bearer ${localStorage.getItem("token")}`,
         "Content-Type": "application/json",
       },
       body: JSON.stringify(staffData)
     });
       let data = await resp.json()
     return data;
     // const data = await resp.json();
     // return data;
   }