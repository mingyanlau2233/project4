export const getAllCategoriesAction = function(categories:any){
    return {
        type: "@@AllCategory/Get" as const,
        categories
    }
}