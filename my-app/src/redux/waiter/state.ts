export interface GetOrders {
      
    id:number,
    closed_at:String,
    session_id:number,
    created_at:string,
    updated_at:string,
    close_at:string,
    desk_sequence:number,
    seat_sequence:number,
    
}

export interface ReturnOrdersId {
     orders_id:number,
 }

export interface GetOrderDetailsForProduct{

     id:number,
     qty:number,
     price:number,
     orders_id:number,
     stage:number,
     created_at:number,
     updated_at:number,
     product_name:string,
     product_id:number,
} 

export interface GetOrderDetailsForDiscountSetWaiter{

     id:number,
     price:number,
     orders_id:number,
     stage:number,
     created_at:number,
     updated_at:number,
     discount_set_name:string,
     discount_set_order_id:number,
} 

export interface GetDiscountSetOrderDetailsWaiter{

     id:number,
     discount_set_sequence_name:string,
     product_name:string,
     stage:number,
     created_at:number,
     updated_at:number,
} 
export interface IOrderState {
     getOrdersFromDb : GetOrders[]
     getOrderDetailsForProductFromDb: GetOrderDetailsForProduct[]
     getOrderDetailsForDiscountSetWaiterFromDb :GetOrderDetailsForDiscountSetWaiter[]
     getDiscountSetOrderDetailsWaiter:GetDiscountSetOrderDetailsWaiter[]
     returnOrderId: ReturnOrdersId | null
}