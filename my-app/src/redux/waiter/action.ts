export const getOrdersAction = function (orders: any) {
    // console.log( "getOrderProduct",OrderCartProduct)
     return {
         type : "@@getOrders" as const,
         orders
     }
 }

 export const getOrderDetailsForProductAction = function (ordersDetailsForProduct: any) {
    // console.log( "getOrderProduct",OrderCartProduct)
     return {
         type : "@@getOrdersDetailsForProduct" as const,
         ordersDetailsForProduct
     }
     
 }

 export const getOrderDetailsForDiscountSetWaiterAction = function (ordersDetailsForDiscountSet: any) {
     //console.log( "getOrderDetailsForDiscountSetWaiterAction",ordersDetailsForDiscountSet)
     return {
         type : "@@getOrdersDetailsForDiscountSetWaiter" as const,
         ordersDetailsForDiscountSet
     }
 }

 export const getDiscountSetOrderDetailsWaiterAction = function (discountSetOrderDetailsWaiter: any) {
   // console.log( "getDiscountSetOrderDetailsWaiterAction",discountSetOrderDetailsWaiter)
    return {
        type : "@@getDiscountSetOrderDetailsWaiter" as const,
        discountSetOrderDetailsWaiter
    }
}
export const returnOrderIdAction = function (returnOrderId: any) {
    //console.log( "getDiscountSetOrderDetailsWaiterAction",returnOrderId)
    return {
        type : "@@returnOrderIdAction" as const,
        returnOrderId
    }
}



 export type IOrderAction =

|ReturnType<typeof getOrdersAction> 
|ReturnType<typeof getOrderDetailsForProductAction> 
|ReturnType<typeof getOrderDetailsForDiscountSetWaiterAction> 
|ReturnType<typeof getDiscountSetOrderDetailsWaiterAction>
|ReturnType<typeof returnOrderIdAction>
