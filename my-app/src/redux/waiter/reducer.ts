import produce from "immer";
import { IOrderAction } from "./action";
import { IOrderState } from "./state";

const initialState: IOrderState = {
  getOrdersFromDb: [],
  getOrderDetailsForProductFromDb: [],
  getOrderDetailsForDiscountSetWaiterFromDb: [],
  getDiscountSetOrderDetailsWaiter: [],
  returnOrderId: null,
};

export const orderReducer = produce(
  (state: IOrderState, action: IOrderAction) => {
    switch (action.type) {
      case "@@getOrders":
        // console.log("getOrderaction",action.orders)
        state.getOrdersFromDb = action.orders;
        return;

      case "@@getOrdersDetailsForProduct":
        // console.log("getOrdersDetailsForProductaction",action.ordersDetailsForProduct)
        state.getOrderDetailsForProductFromDb = action.ordersDetailsForProduct;
        return;

      case "@@getOrdersDetailsForDiscountSetWaiter":
        // console.log("getOrdersDetailsForProductaction",action.ordersDetailsForProduct)
        state.getOrderDetailsForDiscountSetWaiterFromDb =
          action.ordersDetailsForDiscountSet;
        return;

      case "@@getDiscountSetOrderDetailsWaiter":
        // console.log("getDiscountSetOrderDetailsWaiter",action.discountSetOrderDetailsWaiter)
        state.getDiscountSetOrderDetailsWaiter =
          action.discountSetOrderDetailsWaiter;
        return;

      case "@@returnOrderIdAction":
        console.log("@@returnOrderIdAction", action.returnOrderId);
        state.returnOrderId = action.returnOrderId;
        return;
    }
  },
  initialState
);
