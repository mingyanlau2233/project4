import { Dispatch } from "redux";
import jwtDecode from "jwt-decode";

import { IRootAction } from "../store";
import { fetchGetOrders, fetchGetOrderDetailsForProduct, fetchChangeOrderCartStageWaiter, fetchGetOrderDetailsForDiscountSetOrderWaiter, fetchGetDiscountSetOrderDetailsWaiter, fetchStageDiscountSetOrderDetailsWaiter } from "./api";
import { getOrdersAction, getOrderDetailsForProductAction, getOrderDetailsForDiscountSetWaiterAction ,getDiscountSetOrderDetailsWaiterAction} from "./action";


export  function getOrdersThunk(stage:number) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        console.log("here2")
        const token:string|null = localStorage.getItem("token");
      //  console.log(token)
         let result
        if (token){
          //  console.log(token)
             result = await jwtDecode(token)
            // console.log("get category ",result.company_id,result.branch_id)
            const data = await fetchGetOrders (result.company_id,result.branch_id,stage)
          //  console.log("get category ",data)
     
            dispatch(getOrdersAction(data))
                 
        }
  }
}


export  function getOrderDetailsForProductThunk(stage:number) {
  return async(dispatch:Dispatch<IRootAction>)=>{
     // console.log("here2")
      const token:string|null = localStorage.getItem("token");
    //  console.log(token)
       let result
      if (token){
         // console.log(token)
           result = await jwtDecode(token)
          // console.log("get category ",result.company_id,result.branch_id)
          const data = await fetchGetOrderDetailsForProduct(result.company_id,result.branch_id,stage)
   
        dispatch(getOrderDetailsForProductAction(data) )
         
               
      }
}
}

export  function changeStageOrderCartWaiterThunk(id:number) {
  return async(dispatch:Dispatch<IRootAction>)=>{
      console.log("here2")
      const token:string|null = localStorage.getItem("token");
      //console.log(token)
       let result
      if (token){
          console.log(token)
           result = await jwtDecode(token)
          // console.log("get category ",result.company_id,result.branch_id)
           await fetchChangeOrderCartStageWaiter(id)
        //  console.log("get category ",data)
        const data = await fetchGetOrderDetailsForProduct(result.company_id,result.branch_id,4)
        dispatch(getOrderDetailsForProductAction(data))
        const data2 = await fetchGetOrders (result.company_id,result.branch_id,3)
        dispatch(getOrdersAction(data2))
      }
}
}


export  function getOrderDetailsForDiscountSetOrderWaiterThunk(stage:number) {
  return async(dispatch:Dispatch<IRootAction>)=>{
     // console.log("here2")
      const token:string|null = localStorage.getItem("token");
    //  console.log(token)
       let result
      if (token){
         // console.log(token)
           result = await jwtDecode(token)
          // console.log("get category ",result.company_id,result.branch_id)
          const data = await fetchGetOrderDetailsForDiscountSetOrderWaiter(result.company_id,result.branch_id,stage)
        //  console.log("get category ",data)
   
          dispatch(getOrderDetailsForDiscountSetWaiterAction(data))
   
               
      }
}
}

export  function getDiscountSetOrderDetailsWaiterThunk(stage:number) {
  console.log("getDiscountSetOrderDetailsWaiterThunk")
  return async(dispatch:Dispatch<IRootAction>)=>{
    
      const token:string|null = localStorage.getItem("token");
  
       let result
      if (token){
        
           result = await jwtDecode(token)
           console.log("getDiscountSetOrderDetailsWaiterThunk",result.company_id)
          const data = await fetchGetDiscountSetOrderDetailsWaiter(result.company_id,result.branch_id,stage)
        
   
          dispatch(getDiscountSetOrderDetailsWaiterAction(data))
               
      }
}
}

export  function changeStageDiscountSetOrderDetailsWaiterThunk(id:number) {
  return async(dispatch:Dispatch<IRootAction>)=>{
      console.log("here2")
      const token:string|null = localStorage.getItem("token");
      //console.log(token)
       let result
      if (token){
          console.log(token)
           result = await jwtDecode(token)
          // console.log("get category ",result.company_id,result.branch_id)
           await fetchStageDiscountSetOrderDetailsWaiter(id)
           const data = await fetchGetOrderDetailsForDiscountSetOrderWaiter(result.company_id,result.branch_id,4)
          
      
             dispatch(getOrderDetailsForDiscountSetWaiterAction(data))
             const data2 = await fetchGetOrders (result.company_id,result.branch_id,3)
        dispatch(getOrdersAction(data2))
      }
}
}