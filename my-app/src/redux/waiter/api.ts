const { REACT_APP_BACKEND_BASE_URL } = process.env;


export async function fetchGetOrders(company_id:number,branch_id:number,stage:number) {
   //console.log("how come",company_id)
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/getOrder/getOrders`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({company_id,branch_id,stage})
    });
      let data = await resp.json()
    return data;
  }
  // const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/getOrder/getOrders`, {
  //   method: "POST",
  //   headers: {
  //     Authorization: `Bearer ${localStorage.getItem("token")}`,
  //     "Content-Type": "application/json",
  //   },
  //   body: JSON.stringify({ company_id, branch_id })
  // });
  // let data = await resp.json()
  // console.log(data)
  // return data;

  export async function fetchGetOrderDetailsForProduct(company_id:number,branch_id:number,stage:number) {
     //console.log("fetchGetOrderDetailsForProduct")
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/getOrder/getOrderDetailsForProduct`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({company_id,branch_id,stage})
    });
      let data = await resp.json()
    return data;
}

export async function fetchChangeOrderCartStageWaiter(id: number) {
  //console.log("fetchChangeOrderCartStageWaiter")
  const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/getOrder/changeStageOrderDetailsForProductWaiter`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ id })
  });
  let data = await resp.json()
  return data;

}

 export async function fetchGetOrderDetailsForDiscountSetOrderWaiter(company_id:number,branch_id:number,stage:number) {
  //console.log("fetchGetOrderDetailsForDiscountSetOrderWaiter")
 const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/getOrder/getOrderDetailsForDiscountSetWaiter`, {
   method: "POST",
   headers: {
     Authorization: `Bearer ${localStorage.getItem("token")}`,
     "Content-Type": "application/json",
   },
   body: JSON.stringify({company_id,branch_id,stage})
 });
   let data = await resp.json()
 return data;

}

export async function fetchGetDiscountSetOrderDetailsWaiter(company_id:number,branch_id:number,stage:number) {
  //console.log("fetchGetDiscountSetOrderDetails",company_id)
 const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/getOrder/getDiscountSetOrderDetailsWaiter`, {
   method: "POST",
   headers: {
     Authorization: `Bearer ${localStorage.getItem("token")}`,
     "Content-Type": "application/json",
   },
   body: JSON.stringify({company_id,branch_id,stage})
 });
   let data = await resp.json()
   console.log("fetchGetDiscountSetOrderDetails2")
 return data;

}

export async function fetchStageDiscountSetOrderDetailsWaiter(id: number) {
 // console.log("fetchStageDiscountSetOrderDetailsWaiterThunk")
  const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/getOrder/changeStageDiscountSetOrderDetailsWaiter`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ id })
  });
  let data = await resp.json()
  return data;

}