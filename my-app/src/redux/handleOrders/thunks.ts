import { Dispatch } from "redux";
import { IRootAction } from "../store";
import { fetchGetOrderDetailsDepartment, fetchGetDiscountSetDepartment, fetchPutOrderDetailsStage, fetchPutDiscountSetStage } from "./api";
import { getOrderDetailsDepartmentAction, getDiscountSetDepartmentAction } from "./action";


export function getOrderDetailsDepartmentThunk(department_id: number) {
  return async (dispatch: Dispatch<IRootAction>) => {
    const data = await fetchGetOrderDetailsDepartment(department_id)
    dispatch(getOrderDetailsDepartmentAction(data))

  }
}

export function putOrderDetailsStageThunk(id: number,value:number) {
  return async (dispatch: Dispatch<IRootAction>) => {
    await fetchPutOrderDetailsStage(id);
    const data = await fetchGetDiscountSetDepartment(value)
    dispatch(getDiscountSetDepartmentAction(data))
    // setTimeout(() => {dispatch(getDiscountSetDepartmentAction(data)) }, 200);
  }
}

export function getDiscountSetDepartmentThunk(department_id: number) {
  return async (dispatch: Dispatch<IRootAction>) => {
    const data = await fetchGetDiscountSetDepartment(department_id);
      //console.log('getDiscountSetDepartmentThunk',data)
    dispatch(getDiscountSetDepartmentAction(data));
  }
}


export function putDiscountSetStageThunk(id: number,value:number) {
  return async (dispatch: Dispatch<IRootAction>) => {
    await fetchPutDiscountSetStage(id)
    const data = await fetchGetDiscountSetDepartment(value)
    // console.log(data)
    // dispatch(getOrderDetailsDepartmentAction(data))
  }
}