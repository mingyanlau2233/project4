const { REACT_APP_BACKEND_BASE_URL } = process.env;
 
export async function fetchGetOrderDetailsDepartment(department_id:number) {
      const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/handleOrders/getOrderDetailsDepartment`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({department_id})
     });
       let data = await resp.json();
       
     return data;
 
}

export async function fetchGetDiscountSetDepartment(department_id:number) {
    // console.log("api department_id",department_id)
   const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/handleOrders/getDiscountSetDepartment`, {
     method: "POST",
     headers: {
       Authorization: `Bearer ${localStorage.getItem("token")}`,
       "Content-Type": "application/json",
     },
     body: JSON.stringify({department_id})
   });
     let data = await resp.json()
    //  console.log('fetchGetDiscountSetDepartment',data)
   return data;
  
}

export async function fetchPutOrderDetailsStage(id:number) {
 const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/handleOrders/putOrderDetailsStage`, {
   method: "POST",
   headers: {
     Authorization: `Bearer ${localStorage.getItem("token")}`,
     "Content-Type": "application/json",
   },
   body: JSON.stringify({id})
 });
   let data = await resp.json()
 return data;

}

export async function fetchPutDiscountSetStage(id:number) {
 const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/handleOrders/putDiscountSetStage`, {
   method: "POST",
   headers: {
     Authorization: `Bearer ${localStorage.getItem("token")}`,
     "Content-Type": "application/json",
   },
   body: JSON.stringify({id})
 });
   let data = await resp.json()
 return data;

}