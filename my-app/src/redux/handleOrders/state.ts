export interface GetOrderDetailsDepartment{

     id:number,
     qty:number,
     price:number,
     orders_id:number,
     stage:number,
     created_at:number,
     updated_at:number,
     product_name:string,
     product_id:number,
     department_id:number,
     order_details_id:number,
} 

export interface GetDiscountSetDepartment{

     id:number,
     
     orders_id:number,
     stage:number,
     created_at:number,
     updated_at:number,
     discount_set_name:string,
     discount_set_order_id:number,
     department_id:number,
     product_name:string,
     discount_set_sequence_name:string,
} 

export interface IHandleOrdersState {
     getOrderDetailsDepartment: GetOrderDetailsDepartment[]
     getDiscountSetDepartment :GetDiscountSetDepartment[]
}