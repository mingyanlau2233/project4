import produce from "immer";
import { IHandleOrdersAction} from "./action";
import { IHandleOrdersState } from "./state";


const initialState: IHandleOrdersState = {
  getOrderDetailsDepartment:[],
  getDiscountSetDepartment: [],
};


export const handleOrdersReducer = produce((state: IHandleOrdersState, action: IHandleOrdersAction) => {

    switch (action.type) {
  
      case "@@HandleOrders/GET_ORDER_DETAILS_DEPARTMENT":
       state.getOrderDetailsDepartment  = action.ordersDetailsDepartment
        return ;

      case "@@HandleOrders/GET_DISCOUNT_SET_DEPARTMENT":
       state.getDiscountSetDepartment  = action.DiscountSetDepartment
        return ;
      
    }
  }, initialState);