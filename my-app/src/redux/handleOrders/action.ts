
 export const getOrderDetailsDepartmentAction = function (ordersDetailsDepartment: any) {
     return {
         type : "@@HandleOrders/GET_ORDER_DETAILS_DEPARTMENT" as const,
         ordersDetailsDepartment
     }
     
 }

 export const getDiscountSetDepartmentAction = function (DiscountSetDepartment: any) {
   
     return {
         type : "@@HandleOrders/GET_DISCOUNT_SET_DEPARTMENT" as const,
         DiscountSetDepartment
     }
 }




 export type IHandleOrdersAction =
|ReturnType<typeof getOrderDetailsDepartmentAction> 
|ReturnType<typeof getDiscountSetDepartmentAction> 
