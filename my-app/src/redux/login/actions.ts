export function loginSuccess() {
  return {
    type: "@@Auth/LOGIN_SUCCESS" as const,
  };
}

export function logoutSuccess() {
  return {
    type: "@@Auth/LOGOUT_SUCCESS" as const,
  };
}

export function decodeToken(decodedToken:any) {
  console.log("action",decodedToken)
  return {
    type: "@@Auth/decodeToken" as const,
    decodedToken
  };
}


export type IAuthAction =
  // | CallHistoryMethodAction
  ReturnType<typeof loginSuccess> | ReturnType<typeof logoutSuccess>| ReturnType<typeof decodeToken>;
