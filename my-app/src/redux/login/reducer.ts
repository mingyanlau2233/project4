import { SignalCellularConnectedNoInternet1Bar, StartTwoTone } from "@mui/icons-material";
import produce from "immer";
import { IAuthAction } from "./actions";
import { IAuthState } from "./state";




const initialState: IAuthState = {
  // isAuthenticated: null,
  
  isAuthenticated: localStorage.getItem("token") !=null,
  position: "" ,
  is_onboard: false,
  msg: "",
  name:""
};

export const authReducer = produce((state: IAuthState, action: IAuthAction) => {
  switch (action.type) {
    case "@@Auth/LOGIN_SUCCESS":
      state.isAuthenticated = true;
      return;
    case "@@Auth/LOGOUT_SUCCESS":
      state.isAuthenticated = false;
      state.position = "";
      state.name=""

      return;
      case "@@Auth/decodeToken":
      
        state.position = action.decodedToken["position"] ;
        state.name = action.decodedToken["name"] ;
        state.is_onboard = action.decodedToken["is_onboard"]
        state.isAuthenticated =true ;
        
        return;
  }
}, initialState);
