const { REACT_APP_BACKEND_BASE_URL } = process.env;

export const fetchLogin = async (name: string, password: string) => {
  console.log("232432543543634", REACT_APP_BACKEND_BASE_URL)
  const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/users/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ name, password }),
  });
  console.log(resp)

  return resp;
};

// export const fetchLoginFacebook = async (accessToken: string) => {
//   const resp = await fetch(
//     `${REACT_APP_BACKEND_BASE_URL}/users/login/facebook`,
//     {
//       method: "POST", 
//       headers: {
//         "Content-Type": "application/json; charset=utf-8",
//       },
//       body: JSON.stringify({ accessToken }),
//     }
//   );
//   return resp;
// };

export const fetchDemo = async () => {
  const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/users/demo`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });

  return resp;
};
