import { push } from "connected-react-router";
import { Dispatch } from "redux";
import { fetchLogin, fetchDemo} from "./api";
import { IRootAction } from "../store";
import { loginSuccess, logoutSuccess } from "./actions";
import jwtDecode from "jwt-decode";

export function loginThunk(username: string, password: string) {
  return async (dispatch: Dispatch<IRootAction>) => {
  
    const resp = await fetchLogin(username, password);
    const result = await resp.json();
    
    if (resp.status !== 200) {
      //   dispatch(failed("LOGIN_FAILED", result.msg));
    } else {
      localStorage.setItem("token", result.token);
      let token= localStorage.getItem("token")
    
      let ans = jwtDecode(result.token)
     
      dispatch(loginSuccess());
      dispatch(push("/menuPage"));
    }
  };
}

export function restoreLoginThunk() {
  return async (dispatch: Dispatch<IRootAction>) => {
    const token = localStorage.getItem("token");
    if (!token) {
      dispatch(logoutSuccess());
      return;
    }

    const resp = await fetchDemo();
    if (resp.status !== 200) {
      dispatch(logoutSuccess());
    } else {
      dispatch(loginSuccess());
      dispatch(push("/"));
    }
  };
}

export function logoutThunk(){
   return async (dispatch: Dispatch<IRootAction>)=>{
    dispatch(logoutSuccess())
    localStorage.removeItem('token');
    dispatch(push("/"));
   }

}

// export function loginFacebookThunk(accessToken: string) {
//   return async (dispatch: Dispatch<IRootAction>) => {
//     const resp = await fetchLoginFacebook(accessToken);
//     const result = await resp.json();

//     if (resp.status !== 200) {
//       // dispatch(failed("LOGIN_FAILED", result.msg));
//     } else {
//       localStorage.setItem("token", result.token);
//       dispatch(loginSuccess());
//       dispatch(push("/"));
//     }
//   };
// }
