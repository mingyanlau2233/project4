// import * as types from "./actionType"
// import axios from 'axios'
// import { Dispatch } from "redux"
// import { DEPARTMENT_FAIL, DEPARTMENT_LOADING, DEPARTMENT_SUCCESS } from "./state"

const { REACT_APP_BACKEND_BASE_URL } = process.env;

export const getDepartmentsSuccess = function (departments: any) {
   
    return {
        type : "@@Department/Get" as const,
        departments
    }
}

export const deleteDepartments = function (id: number) {
    return {
        type : "@@Department/Detele" as const,
        id
    }
}

export const postDepartments = function (departments: any) {
    return {
        type : "@@Department/Post" as const,
        departments
    }
}


export type IDepartmentAction = 
| ReturnType<typeof getDepartmentsSuccess>| ReturnType<typeof deleteDepartments> | ReturnType<typeof postDepartments> 
