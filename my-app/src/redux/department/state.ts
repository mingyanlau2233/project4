// export const DEPARTMENT_LOADING = "DEPARTMENT_LOADING"
// export const DEPARTMENT_FAIL = "DEPARTMENT_FAIL"
// export const DEPARTMENT_SUCCESS = "DEPARTMENT_SUCCESS"

export interface Department {
    id: number,
    name: string
}

export interface IDepartmentState {
    department: Department[] | []

}