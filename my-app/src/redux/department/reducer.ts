import { IDepartmentState } from './state'
import { IDepartmentAction } from "./actions";
import produce from "immer";

const initialState: IDepartmentState = {
    department: []
}

export const departmentReducers = produce((state: IDepartmentState, action: IDepartmentAction) => {
    switch (action.type) {
        case "@@Department/Get" : 
           
            state.department = action.departments
            return 
        case "@@Department/Detele" :
            state.department = state.department.filter((department)=>department.id !== action.id)
            return
        case "@@Department/Post":
            return 
        default :
            return
    }
}, initialState)