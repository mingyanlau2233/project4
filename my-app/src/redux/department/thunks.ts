
import { IDepartmentAction, getDepartmentsSuccess, deleteDepartments,postDepartments } from "./actions" 
import axios from 'axios'
import { Dispatch } from "redux";
import { IRootAction } from "../store";
import jwtDecode from "jwt-decode";
import {Department} from "./state";

const { REACT_APP_BACKEND_BASE_URL } = process.env;

export function getDepartments() {
    return async (dispatch: Dispatch<IRootAction>) => {
        
        const result = await axios ( {
            url:`${REACT_APP_BACKEND_BASE_URL}/menuSetting/getDepartment`,
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`, 
            },
            method: 'GET', 
        })
        const data = result.data
        
        dispatch(getDepartmentsSuccess(data))
        
    }
}

// export function loadDepartment (){
//     return asyn
// }

export function deleteDepartment(id:number){
    return async (dispatch: Dispatch<IRootAction>) => {
        const result = await axios ({
            url:`${REACT_APP_BACKEND_BASE_URL}/menuSetting/deleteDepartment`,
            headers:{
                'content-type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("token")}`, 
            },
            method: "DELETE",
            data:{
                id:id
            }
        })
        dispatch(deleteDepartments(id))
        
 
    }
}

export function addDepartment(name: string){
    return async (dispatch: Dispatch<IRootAction>) => {
        const result = await axios({
            url:`${REACT_APP_BACKEND_BASE_URL}/menuSetting/postDepartment`,
            headers:{
                'content-type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("token")}`, 
            },
            method: "POST",
            data: {name : name}
        })
        // dispatch(postDepartments(data))
        const deparments = await axios.get(`${REACT_APP_BACKEND_BASE_URL}/menuSetting/getDepartment`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`, 
            },
        })
        const data = deparments.data
        dispatch(getDepartmentsSuccess(data))

    }
}
