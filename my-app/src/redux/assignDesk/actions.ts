import { AreaState, DeskState, SeatState, SessionState } from "./state"

export function fetchDataPending() {
    return {
        type: "@@AssignDesk/FETCH_PENDING" as const,
    }
}

export function fetchDataSuccess(areaData: AreaState[], deskData: DeskState[], seatData: SeatState[], sessionData: SessionState[] ) {
    return {
        type: "@@AssignDesk/FETCH_SUCCESS" as const,
        areaData,
        deskData,
        seatData,
        sessionData,
    }
}

export function fetchDataFail(errMessage: string) {
    return {
        type: "@@AssignDesk/FETCH_FAIL" as const,
        errMessage,
    }
}



export type IAssignDeskAction = 
    | ReturnType<typeof fetchDataPending>
    | ReturnType<typeof fetchDataSuccess>
    | ReturnType<typeof fetchDataFail>
