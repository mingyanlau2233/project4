export enum ShowStatus {
    Active = "active",   
    Deleted = "deleted"
}
export interface AreaState {
    id: number;
    sequence: number;
    description: string;
    status: ShowStatus;
    company_id: number;
    branch_id: number
}

export interface DeskState {
    id: number;
    sequence: number;
    status: ShowStatus;
    area_id: number;
    company_id: number;
    branch_id: number
}

export interface SeatState {
    id: number;
    sequence: number;
    is_occupied: boolean;
    status: ShowStatus;
    desk_id: number;
    company_id: number;
    branch_id: number
}

export interface SessionState {
    id: string;
    is_client_left: boolean;
    is_takeaways: boolean;
    total_price: number;
    seat_id: number;
    company_id: number;
    branch_id: number
}

export interface IAssignDeskState {
    areaFromDB: AreaState [] | [];
    deskFromDB: DeskState [] | [];
    seatFromDB: SeatState [] | [];
    sessionFromDB: SessionState [] | [];
    status: ShowStatus;
    isLoading: boolean;
    errMessage: string;
}
