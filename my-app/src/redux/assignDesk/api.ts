const { REACT_APP_BACKEND_BASE_URL } = process.env;

export async function fetchGetArea(company_id:number, branch_id:number) {
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/deskAndSession/getArea`, { 
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ company_id, branch_id })
    });
        let data = await resp.json();
        return data;
}

export async function fetchPostArea(sequence:number, description:string, company_id:number, branch_id:number) {
    
}
