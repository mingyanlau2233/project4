import produce from "immer";
import { IAssignDeskAction } from "./actions";
import { IAssignDeskState } from "./state";
export{}
// const initialState: IAssignDeskState = {
//     areaFromDB: [],
//     deskFromDB: [],
//     seatFromDB: [],
//     sessionFromDB: [],
   
// };

// export const assignDeskReducer = produce((state: IAssignDeskState, action: IAssignDeskAction) => {
//     switch (action.type) {
//         case "@@AssignDesk/FETCH_PENDING":
//             state.isLoading = true;
//             state.errMessage = "'";
//             return;
//         case "@@AssignDesk/FETCH_SUCCESS":
//             state.isLoading = false;
//             state.areaFromDB = action.areaData;
//             state.deskFromDB = action.deskData;
//             state.seatFromDB = action.seatData;
//             state.sessionFromDB = action.sessionData;
//             return;
//         case "@@AssignDesk/FETCH_FAIL":
//             state.isLoading = false;
//             state.errMessage = action.errMessage;
//             return;
//     }
//     return state;
// }, initialState);