import {
  RouterState,
  connectRouter,
  routerMiddleware,
  CallHistoryMethodAction,
} from "connected-react-router";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import logger from "redux-logger";
import { createBrowserHistory } from "history";
import thunk, { ThunkDispatch } from "redux-thunk";

import { IAuthState } from "./login/state";
import { IAuthAction } from "./login/actions";
import {authReducer} from "./login/reducer"

import { ICategoryState } from "./productSetting/state";
import { categoryReducer } from "./productSetting/reducer";
import { ICategoryAction } from "./productSetting/actions";
import { IDepartmentState } from './department/state'
import { IDepartmentAction } from "./department/actions";
import { departmentReducers } from "./department/reducer";

import { IAssignDeskState } from "./assignDesk/state";
// import { assignDeskReducer } from "./assignDesk/reducer";
import { IAssignDeskAction } from "./assignDesk/actions";
import { IStaffState } from "./staff/state";
import { staffReducers } from "./staff/reducer";
import { IStaffAction } from "./staff/action";

import { IOrderCartState } from "./orderCart/state";
import { IOrderCartAction } from "./orderCart/action";
import { orderCartReducer } from "./orderCart/reducer";

import { IOrderAction } from "./waiter/action"
import {orderReducer } from "./waiter/reducer"
import { IOrderState } from "./waiter/state";

import {IEditDeskState} from "./editDesk/state"
import {editDeskReducer } from "./editDesk/reducer";
import { IEditDeskAction } from "./editDesk/action";

import { IHandleOrdersState } from "./handleOrders/state";
import { handleOrdersReducer } from "./handleOrders/reducer";
import { IHandleOrdersAction } from "./handleOrders/action";

import { IPassState } from "./passMessage/state";
import { IPassAction } from "./passMessage/action";
import  {passReducer}  from "./passMessage/reducer"


export const history = createBrowserHistory();

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

// IRootState
export interface IRootState {
  auth: IAuthState;
  router: RouterState;
  category:ICategoryState;
  department: IDepartmentState;
  // assignDesk: IAssignDeskState;
  editDesk: IEditDeskState;
  staff: IStaffState;
  orderCart: IOrderCartState;
  order: IOrderState
  handleOrders: IHandleOrdersState
  passMessage :IPassState
}

//  IRootAction

export type IRootAction = IAuthAction | CallHistoryMethodAction | ICategoryAction | IDepartmentAction | IAssignDeskAction | IStaffAction | IEditDeskAction
                         | IOrderCartAction | IOrderAction | IHandleOrdersAction | IPassAction

export type IRootThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>;

//  rootReducer
const rootReducers = combineReducers<IRootState>({
  auth: authReducer,
  department: departmentReducers,
  router: connectRouter(history),
  category: categoryReducer,
  // assignDesk: assignDeskReducer,
  editDesk: editDeskReducer,
  staff:staffReducers,
  orderCart: orderCartReducer,
  order:orderReducer,
  handleOrders: handleOrdersReducer,
  passMessage: passReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

//  createStore
export default createStore<IRootState, IRootAction, {}, {}>(
  rootReducers,
  composeEnhancers(
    applyMiddleware(logger),
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history))
  )
);
