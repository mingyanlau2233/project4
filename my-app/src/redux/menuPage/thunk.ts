import { Dispatch } from "redux";
import jwtDecode from "jwt-decode";
import { IRootAction } from "../store";
import { fetchPostDiscountSetToOrderCart, fetchPostToOrderCart } from "./api";
import { io } from "socket.io-client";
import socketClient  from "socket.io-client";
import { passOrderCartSubmit } from "../passMessage/action";



export  function postProductToOrderCartThunk(qty:number,price:number,product_id:number,product_name:string) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        
        const token:string|null = localStorage.getItem("token");
        //console.log(token)
        let result ;
        if (token){
          
            result  = await jwtDecode(token)
            const data = await fetchPostToOrderCart(qty,price,product_id,product_name,result.branch_id,result.company_id,result.staff_id)
          
           
            
        }
  }
  }

  export  function postDiscountSetToOrderCartThunk(price:number,discount_set_id:number,discount_set_name:string,productData:object) {
    return async(dispatch:Dispatch<IRootAction>)=>{
       
        const token:string|null = localStorage.getItem("token");
        //console.log(token)
        let result ;
        if (token){
         
            result  = await jwtDecode(token)
            const data = await fetchPostDiscountSetToOrderCart(price,discount_set_id,discount_set_name,productData,result.branch_id,result.company_id,result.staff_id)
          
           
            
        }
  }
  }