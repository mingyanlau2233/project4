import axios from 'axios'



const { REACT_APP_BACKEND_BASE_URL } = process.env;

export async function fetchPostToOrderCart(qty:number,price:number,product_id:number,product_name:string,
  company_id:number,branch_id:number,session_id:number) {
   
     const resp = await axios({
         url:`${REACT_APP_BACKEND_BASE_URL}/postOrder/postProductToOrderCart`,
         headers:{
             'content-type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem("token")}`, 
         },
         method: "POST",
         data:{
           
           qty:qty,
           price:price,
           product_id:product_id,
           product_name:product_name,
           company_id:company_id,
           branch_id:branch_id,
           session_id:session_id
         }
      
     })
       let result =  resp.data
 
       console.log("postOrderCartProduct",resp.data)
     return result;
     // const data = await resp.json();
     // return data;
   }


   export async function fetchPostDiscountSetToOrderCart(price:number,discount_set_id:number,
    discount_set_name:string,productData:object,company_id:number,branch_id:number,session_id:number) {
    
     const resp = await axios({
         url:`${REACT_APP_BACKEND_BASE_URL}/postOrder/postDiscounterSetToOrderCart`,
         headers:{
             'content-type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem("token")}`, 
         },
         method: "POST",
         data:{
           
           
           price:price,
           discount_set_id:discount_set_id,
           discount_set_name:discount_set_name,
           session_id:session_id,
           company_id:company_id,
           branch_id:branch_id,
           productData:productData
         }
      
     })
       let result =  resp.data
 
       console.log("postOrderCartProduct",resp.data)
     return result;
     // const data = await resp.json();
     // return data;
   }