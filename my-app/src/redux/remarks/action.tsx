const { REACT_APP_BACKEND_BASE_URL } = process.env;

export function getRemarksAction(remarks:any){
    return{
        type:"@@Remarks/Get" as const,
        remarks
    }
}

export function deleteRemarksAction(id:number){
    return {
        type:"@@Remarks/Delete" as const,
        id
    }
}

export function postRemarksAction(id:number,remarks:any){
    return{
        type:"@@Remarks/Post" as const,
        id,
        remarks
    }
}