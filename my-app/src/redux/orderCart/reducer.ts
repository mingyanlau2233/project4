import produce from "immer";
import { IOrderCartAction } from "./action";
import { IOrderCartState } from "./state";



const initialState: IOrderCartState = {
    GetOrderCartProduct:[],
    GetOrderCartDiscountSet:[],
    GetOrderCartDiscountSetOrderDetails:[]
};


export const orderCartReducer = produce((state: IOrderCartState, action: IOrderCartAction) => {

  switch (action.type) {

    case "@@getOrderCartProduct":
   
      state.GetOrderCartProduct = action.OrderCartProduct
      return ;
    
      case "@@getOrderCartDiscountSet":
       
         state.GetOrderCartDiscountSet = action.OrderCartDiscountSet
         return ;

     case "@@getOrderCartDiscountSetOrderDetails":
        
         state.GetOrderCartDiscountSetOrderDetails = action.OrderCartDiscountSetOrderDetails
         return ;    
    
  }
}, initialState);