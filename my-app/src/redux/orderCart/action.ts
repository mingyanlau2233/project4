
const { REACT_APP_BACKEND_BASE_URL } = process.env;

export const getOrderCartProduct = function (OrderCartProduct: any) {
   // console.log( "getOrderProduct",OrderCartProduct)
    return {
        type : "@@getOrderCartProduct" as const,
        OrderCartProduct
    }
}

export const getOrderCartDiscountSet = function (OrderCartDiscountSet: any) {
    //console.log("getOrderDiscount", OrderCartDiscountSet)
    return {
        type : "@@getOrderCartDiscountSet" as const,
        OrderCartDiscountSet
    }
}

export const getOrderCartDiscountSetOrderDetails = 
function (OrderCartDiscountSetOrderDetails: any) {
    console.log("getOrderDiscountOrderDetails", OrderCartDiscountSetOrderDetails)
    return {
        type : "@@getOrderCartDiscountSetOrderDetails" as const,
        OrderCartDiscountSetOrderDetails
    }
}
export type IOrderCartAction =
// | CallHistoryMethodAction
|ReturnType<typeof getOrderCartProduct> 
|ReturnType<typeof getOrderCartDiscountSet> 
|ReturnType<typeof getOrderCartDiscountSetOrderDetails> 
