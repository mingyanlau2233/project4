import axios from 'axios'



const { REACT_APP_BACKEND_BASE_URL } = process.env;


export async function fetchGetOrderCartProduct(session_id:number) {
   
    const resp = await axios({
        url:`${REACT_APP_BACKEND_BASE_URL}/postOrder/getProductFromOrderCart`,
        headers:{
            'content-type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem("token")}`, 
        },
        method: "POST",
        data:{
          session_id:session_id
        }
     
    })
     

      let data =  resp.data

      console.log("getOrderCartProduct",resp.data)
    return data;
    // const data = await resp.json();
    // return data;
  }

  export async function fetchGetOrderCartDiscountSet(session_id:number) {
    
     const resp = await axios({
         url:`${REACT_APP_BACKEND_BASE_URL}/postOrder/getDiscounterSetFromOrderCart`,
         headers:{
             'content-type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem("token")}`, 
         },
         method: "post",
         data:{
           session_id:session_id
         }
     })
       let data =  resp.data
 
       console.log("getOrderCartProduct",resp.data)
     return data;
     // const data = await resp.json();
     // return data;
   }

   export async function fetchGetOrderCartDiscountSetOrderDetails(id:number) {
    
     const resp = await axios({
         url:`${REACT_APP_BACKEND_BASE_URL}/postOrder/getDiscounterSetOrderDetailFromOrderCart`,
         headers:{
             'content-type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem("token")}`, 
         },
         method: "post",
         data:{
           id:id
         }
     })
       let data =  resp.data
 
       console.log("getOrderCartProduct",resp.data)
     return data;
     // const data = await resp.json();
     // return data;
   }

  export async function fetchPutOrderCartQty(id:number,qty:number,price:number) {
   
     const resp = await axios({
         url:`${REACT_APP_BACKEND_BASE_URL}/postOrder/putProductToOrderCart`,
         headers:{
             'content-type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem("token")}`, 
         },
         method: "POST",
         data:{
           id:id,
           qty:qty,
           price:price
         }
      
     })
       let result =  resp.data
 
      
     return result;
     // const data = await resp.json();
     // return data;
   }

   export async function fetchDeleteProductOrderCart(id:number) {
    
     const resp = await axios({
         url:`${REACT_APP_BACKEND_BASE_URL}/postOrder/deleteProductFromOrderCart`,
         headers:{
             'content-type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem("token")}`, 
         },
         method: "DELETE",
         data:{
           id:id,
      
         }
      
     })
       let result =  resp.data
 
       
     return result;
     // const data = await resp.json();
     // return data;
   }



   export async function fetchSubmitOrder(session_id:number) {
  
     const resp = await axios({
         url:`${REACT_APP_BACKEND_BASE_URL}/postOrder/submitOrders`,
         headers:{
             'content-type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem("token")}`, 
         },
         method: "POST",
         data:{
           session_id:session_id
      
         }
      
     })
       let result =  resp.data
 
      
     return result;
     // const data = await resp.json();
     // return data;
   }

   export async function fetchDeleteDiscountSetOrderCart(discount_set_order_id:number) {
    
     const resp = await axios({
         url:`${REACT_APP_BACKEND_BASE_URL}/postOrder/deleteDiscountSetFromOrderCart`,
         headers:{
             'content-type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem("token")}`, 
         },
         method: "POST",
         data:{
          discount_set_order_id:discount_set_order_id,
      
         }
      
     })
       let result =  resp.data
 
      
     return ;
     // const data = await resp.json();
     // return data;
   }