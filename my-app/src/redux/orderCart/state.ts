export interface OrderCartProduct {
    
    id: number,
    product_name:string
    product_id:number,
    qty:number,
    price:number
}


export interface OrderCartDiscountSet {
    
    id: number,
    discount_set_name:string
    discount_set_order_id:number,
    price:number
   // details:[]
}

export interface OrderCartDiscountSetOrderDetails {
    
    
    discount_set_sequence_name:string
    product_name:string
    
   // details:[]
}
export interface IOrderCartState {
    GetOrderCartProduct: OrderCartProduct[]
    GetOrderCartDiscountSet: OrderCartDiscountSet[]
    GetOrderCartDiscountSetOrderDetails :OrderCartDiscountSetOrderDetails[]
}