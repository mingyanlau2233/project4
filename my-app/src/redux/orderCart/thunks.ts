import { Dispatch } from "redux";
import jwtDecode from "jwt-decode";
import { IRootAction } from "../store";
import { fetchGetOrderCartProduct ,fetchPutOrderCartQty,fetchDeleteProductOrderCart,fetchSubmitOrder, fetchGetOrderCartDiscountSet, fetchGetOrderCartDiscountSetOrderDetails, fetchDeleteDiscountSetOrderCart} from "./api";
import { getOrderCartDiscountSet, getOrderCartDiscountSetOrderDetails, getOrderCartProduct } from "./action";
import { returnOrderIdAction } from "../waiter/action";
import { RestoreOutlined } from "@mui/icons-material";
import { passOrderCartSubmit } from "../passMessage/action";


export  function getOrderCartProductThunk() {
    return async(dispatch:Dispatch<IRootAction>)=>{
        
        const token:string|null = localStorage.getItem("token");
       let result;
        if (token){
          result  = await jwtDecode(token)
          // console.log("fewfcsadvcsa",result.staff_id)
            const data = await fetchGetOrderCartProduct(result.staff_id)
           
           
             dispatch(getOrderCartProduct(data))
        }
  }
}

export  function getOrderCartDiscountThunk() {
  return async(dispatch:Dispatch<IRootAction>)=>{
      
      const token:string|null = localStorage.getItem("token");
      
      let result;
      if (token){
        result  = await jwtDecode(token)
          const data = await fetchGetOrderCartDiscountSet(result.staff_id)
        // console.log("get DiscountThunk thunks",data)
         
           dispatch(getOrderCartDiscountSet(data))
      }
}
}

export  function getOrderCartDiscountSetOrderDetailsThunk(id:number) {
  return async(dispatch:Dispatch<IRootAction>)=>{
     // console.log("here2")
      const token:string|null = localStorage.getItem("token");
     // console.log(token)
      
      if (token){
  
          const data = await fetchGetOrderCartDiscountSetOrderDetails(id)
         //console.log("get DiscountSetOrderDetailsk thunks",data)
         
           dispatch(getOrderCartDiscountSetOrderDetails(data))
      }
}
}

export  function putOrderCartProductThunk(id:number,qty:number,price:number) {
  return async(dispatch:Dispatch<IRootAction>)=>{
      console.log("here2")
      const token:string|null = localStorage.getItem("token");
      console.log(token)
      
      if (token){
        
          const data = await fetchPutOrderCartQty(id,qty,price)
         
         
          
      }
}
}
export  function deleteOrderCartProductThunk(id:number) {
  return async(dispatch:Dispatch<IRootAction>)=>{
      
      const token:string|null = localStorage.getItem("token");
      
      
      if (token){
       
          const data = await fetchDeleteProductOrderCart(id)
       
        
          
      }
}
}

export  function deleteDiscountSetProductThunk(discount_set_order_id:number) {
  return async(dispatch:Dispatch<IRootAction>)=>{
     
      const token:string|null = localStorage.getItem("token");
     
      
      if (token){
       
          const data = await fetchDeleteDiscountSetOrderCart(discount_set_order_id)
        
         
          
      }
}
}


export  function submitOrderThunk() {
  return async(dispatch:Dispatch<IRootAction>)=>{
      
      const token:string|null = localStorage.getItem("token");
      
      let result ; 
      if (token){
    
        result  = await jwtDecode(token)
          const data = await fetchSubmitOrder(result.staff_id)
         
         
          dispatch(returnOrderIdAction(data))
          
         
      }
}
}
