
import { ICategoryState,originCategoryState} from "./state";



export function getCategoryAction(category:any) {
 
    return {
      type: "@@getCategory" as const,
      category,
      
    };
  }

  export function getProductAction(product:any) {
    //console.log(product)
      return {
        type: "@@getProduct" as const,
        product,
        
      };
    }

  
    export function getAllDiscountSetAction(discountSet:any) {
    //  console.log("discountSet",discountSet)
        return {
          type: "@@getAllDiscountSet" as const,
          discountSet,
          
        };
      }

      export function getDiscountSetDetailsAction(details:any) {
        
          return {
            type: "@@getAllDiscountDetailsSet" as const,
            details
            
          };
        }
  export type ICategoryAction =
    // | CallHistoryMethodAction
    |ReturnType<typeof getCategoryAction> 
    |ReturnType<typeof getProductAction>
    |ReturnType<typeof getAllDiscountSetAction>
    |ReturnType<typeof getDiscountSetDetailsAction>