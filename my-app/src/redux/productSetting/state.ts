export interface originCategoryState {
    // isAuthenticated: boolean | null;
    name: string,
    id: number,
    is_inSetOnly: boolean;
}


export interface DiscountSetState {
    // isAuthenticated: boolean | null;
    name: string,
    id: number,
    is_launched:boolean,
    breakfast : boolean ;
    lunch :boolean ;
    afternoonTea:boolean;
    dinner:boolean;
    nightSnack:boolean;
    photo:string;
    price:number;
}

// export interface IDiscountSetState {
//     // isAuthenticated: boolean | null;
//     pro
// }
export interface ProductState{

    name: string;
    product_category_id:number;
    is_launched:boolean;
    is_kiosk:boolean;
    is_time_consuming:boolean;
    department_id :number;
    price:number;
    breakfast : boolean ;
    lunch :boolean ;
    afternoonTea:boolean;
    dinner:boolean;
    nightSnack:boolean;
    photo:string
    product_id:number

}

export interface DiscountSetDetailsState{

    name: string;
    product_category_id:number;
    id:number;
    description:string;
    discount_set_id:number;
    discount_set_sequence_id:number;

}

export interface ICategoryState {
    dataFromDb: originCategoryState[]
    productFromDb:ProductState[]
    discountSetFromDb : DiscountSetState[]
    discountSetDetailsFromDB : DiscountSetDetailsState[]
}