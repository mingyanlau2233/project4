const { REACT_APP_BACKEND_BASE_URL } = process.env;


export async function fetchGetProductCategory(company_id:number,branch_id:number) {
   
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/menuSetting/getProductCategory`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({company_id,branch_id})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }

  export async function fetchPostProductCategory(company_id:number,branch_id:number,name:string,subitem:boolean) {
   //console.log("postcategory",subitem)
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/menuSetting/postProductCategory`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({company_id,branch_id,name, subitem})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }

  export async function fetchDeleteProductCategory(id:number) {
   
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/menuSetting/deleteProductCategory`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({id})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }

  export async function fetchPutProductCategory(id:number,name:string,is_inSetOnly:boolean) {
   
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/menuSetting/putProductCategory`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({id,name,is_inSetOnly})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }



  export async function fetchPostProduct(available_period:any,department_id:any,name:any,price:any,
    is_launched:any,is_kiosk:any,is_time_consuming:any,image:any,company_id:any,branch_id:any,
    product_category_id:any) {
       
     
    // const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/menuSetting/postProductData` ,{
    //   method: "POST",
    //   headers: {
    //     Authorization: `Bearer ${localStorage.getItem("token")}`,
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify({available_period,department_id,name,price,is_launched,
    //     is_kiosk,is_time_consuming,company_id,branch_id,product_category_id})
    // });
    const formData = new FormData()
    formData.append('image', image[0])
    formData.append('available_period', available_period)
    formData.append('department_id', department_id)
    formData.append('name', name)
    formData.append('price',price)
    formData.append('is_launched', is_launched)
    formData.append('is_kiosk', is_kiosk)
    formData.append('is_time_consuming', is_time_consuming)
    formData.append('department_id', department_id)
    formData.append('company_id', company_id)
    formData.append('branch_id', branch_id)
    formData.append('product_category_id', product_category_id)
    console.log(formData)
     const resp2 = await fetch(`${REACT_APP_BACKEND_BASE_URL}/menuSetting/postProductData` ,{
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        //"Content-Type": "application/json",
      },
      body: formData,
    });
      let data = await resp2.json()
    return data;
    // const data = await resp.json();
    // return data;
  }


  export async function fetchGetProduct(company_id:number,branch_id:number,product_category_id:number) {
    
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/menuSetting/getAllProductData`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({company_id,branch_id,product_category_id})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }



  export async function fetchPutProduct(product_id:any,available_period:any,department_id:any,name:any,price:any,
    is_launched:any,is_kiosk:any,is_time_consuming:any,image:any,company_id:any,branch_id:any,
    ) {
      
    const formData = new FormData()
    if(image){
      formData.append('image', image[0])
    }
    
    formData.append('available_period', available_period)
    formData.append('department_id', department_id)
    formData.append('name', name)
    formData.append('price',price)
    formData.append('is_launched', is_launched)
    formData.append('is_kiosk', is_kiosk)
    formData.append('is_time_consuming', is_time_consuming)
    formData.append('department_id', department_id)
    formData.append('company_id', company_id)
    formData.append('branch_id', branch_id)
    formData.append('product_id', product_id)
    
     const resp2 = await fetch(`${REACT_APP_BACKEND_BASE_URL}/menuSetting/putProductData` ,{
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        //"Content-Type": "application/json",
      },
      body: formData,
    });
      let data = await resp2.json()
    return data;
    // const data = await resp.json();
    // return data;
  }


  export async function fetchDeleteProduct(product_id:number) {
  
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/menuSetting/deleteProductData`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({product_id})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }




  export async function fetchPostDiscountSet(available_period:any ,name:any,is_launched:any,
    photo:any,company_id:any,branch_id:any,price:any) {
       console.log("Put DiscountSet api")
    const formData = new FormData()
    formData.append('image', photo[0])
    formData.append('available_period', available_period)
    formData.append('is_launched', is_launched)
    formData.append('name', name)
    formData.append('company_id', company_id)
    formData.append('branch_id', branch_id)
    formData.append('price', price )
     const resp2 = await fetch(`${REACT_APP_BACKEND_BASE_URL}/discountSet/postDiscountSet` ,{
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        //"Content-Type": "application/json",
      },
      body: formData,
    });
      let data = await resp2.json()
   
    return data;
    // const data = await resp.json();
    // return data;
  }




  export async function  fetchGetAllDiscountSet(company_id:number,branch_id:number) {
     
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/discountSet/getDiscountSet`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({company_id,branch_id})
    });
      let data = await resp.json()
     
    return data;
    // const data = await resp.json();
    // return data;
  }
  


  export async function fetchDeleteDiscountSet(id:number) {
    console.log("delete called")
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/discountSet/deleteDiscountSet`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({id})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }


  export async function fetchPostDiscountSetDetails(discount_set_id:number,product_category_id:number,company_id:number,branch_id:number,description:string) {
   
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/discountSet/postDiscountSetDetails`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({discount_set_id,product_category_id,company_id,branch_id,description})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }


  export async function fetchGetDiscountSetDetails(discount_set_id:number) {
   
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/discountSet/getDiscountSetDetails`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({discount_set_id})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }



  export async function fetchDeleteDiscountSetDetails(id:number) {
    console.log("delete called")
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/discountSet/deleteDiscountSetDetails`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({id})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }


  export async function fetchPostDiscountSetDetails2(discount_set_id:number,product_category_id:number,company_id:number,branch_id:number,discount_set_sequence_id:number) {
    console.log("called")
    const resp = await fetch(`${REACT_APP_BACKEND_BASE_URL}/discountSet/postDiscountSetDetails`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({discount_set_id,product_category_id,company_id,branch_id,discount_set_sequence_id})
    });
      let data = await resp.json()
    return data;
    // const data = await resp.json();
    // return data;
  }


  export async function fetchPutDiscountSet(id:any,available_period:any ,name:any,is_launched:any,
    photo:any,company_id:any,branch_id:any,price:any) {
     
    const formData = new FormData()
    if (photo){formData.append('image', photo[0])}
    formData.append('available_period', available_period)
    formData.append('is_launched', is_launched)
    formData.append('name', name)
    formData.append('company_id', company_id)
    formData.append('branch_id', branch_id)
    formData.append('price', price )
    formData.append('id', id )
     const resp2 = await fetch(`${REACT_APP_BACKEND_BASE_URL}/discountSet/putDiscountSet` ,{
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        //"Content-Type": "application/json",
      },
      body: formData,
    });
      let data = await resp2.json()
      
    return data;
    // const data = await resp.json();
    // return data;
  }