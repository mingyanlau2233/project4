import { Dispatch } from "redux";
import jwtDecode from "jwt-decode";
import { ICategoryAction ,getCategoryAction , getProductAction, getAllDiscountSetAction, getDiscountSetDetailsAction } from "./actions";
import { fetchDeleteDiscountSet, fetchDeleteDiscountSetDetails, fetchDeleteProduct, fetchDeleteProductCategory, fetchGetAllDiscountSet, fetchGetDiscountSetDetails, fetchGetProduct, fetchGetProductCategory, fetchPostDiscountSet, fetchPostDiscountSetDetails, fetchPostDiscountSetDetails2, fetchPostProduct, fetchPostProductCategory, fetchPutDiscountSet, fetchPutProduct, fetchPutProductCategory } from "./api";
import { IRootAction } from "../store";






export  function getAllCategoryThunk() {
    return async(dispatch:Dispatch<IRootAction>)=>{
       
        const token:string|null = localStorage.getItem("token");
       
         let result
        if (token){
            
             result = await jwtDecode(token)
            // console.log("get category ",result.company_id,result.branch_id)
            const data = await fetchGetProductCategory(result.company_id,result.branch_id)
          //  console.log("get category ",data)
     
            dispatch(getCategoryAction(data))
        }
  }
}

export  function postCategoryThunk(name:string,subitem:boolean) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        
        const token:string|null = localStorage.getItem("token");
       
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await fetchPostProductCategory(result.company_id,result.branch_id,name,subitem)
           
     
           
        }
  }
}

export  function deleteCategoryThunk(id:number) {
    return async(dispatch:Dispatch<IRootAction>)=>{
       
        const token:string|null = localStorage.getItem("token");
       
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await  fetchDeleteProductCategory(id)
        //    console.log(data)
     
            //dispatch(getCategoryAction(data))
        }
  }
}

export  function putCategoryThunk(id:number,name:string,is_inSetOnly:boolean) {
    return async(dispatch:Dispatch<IRootAction>)=>{
       
        const token:string|null = localStorage.getItem("token");
        
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await  fetchPutProductCategory(id,name,is_inSetOnly)
            //console.log(data)
     
            //dispatch(getCategoryAction(data))
        }
  }
}

export  function postProductThunk(breakfast : boolean ,lunch :boolean,afternoonTea:boolean,dinner:boolean,
    nightSnack:boolean,department_id:number,name:string,price:number,is_launched:boolean,
    is_kiosk:boolean,is_time_consuming:boolean,photo:any,product_category_id:number) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        let  available_period = []
        if(breakfast==true){available_period.push(1)}
        if(lunch==true){available_period.push(2)}
        if(afternoonTea==true){available_period.push(3)}
        if(dinner==true){available_period.push(4)}
        if(nightSnack==true){available_period.push(5)}
        const token:string|null = localStorage.getItem("token");
      //  console.log(token)
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await  fetchPostProduct(available_period,department_id,name,price,is_launched,
                is_kiosk,is_time_consuming,photo,result.company_id,result.branch_id,product_category_id)
          //  console.log(data)
     
            //dispatch(getCategoryAction(data))
        }
  }
}

export  function getProductDataThunk(product_category_id:number) {
    return async(dispatch:Dispatch<IRootAction>)=>{
       
        const token:string|null = localStorage.getItem("token");
       // console.log(token)
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await fetchGetProduct(result.company_id,result.branch_id,product_category_id)
        //    console.log(data)
     
            dispatch(getProductAction(data))
        }
  }
}


export  function putProductThunk(product_id:number,breakfast : boolean ,lunch :boolean,afternoonTea:boolean,dinner:boolean,
    nightSnack:boolean,department_id:number,name:string,price:number,is_launched:boolean,
    is_kiosk:boolean,is_time_consuming:boolean,photo:any) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        let  available_period = []
        if(breakfast==true){available_period.push(1)}
        if(lunch==true){available_period.push(2)}
        if(afternoonTea==true){available_period.push(3)}
        if(dinner==true){available_period.push(4)}
        if(nightSnack==true){available_period.push(5)}
        const token:string|null = localStorage.getItem("token");
        // console.log(token)
        // console.log(product_id)
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await  fetchPutProduct(product_id,available_period,department_id,name,price,is_launched,
                is_kiosk,is_time_consuming,photo,result.company_id,result.branch_id)
         //   console.log(data)
     
            //dispatch(getCategoryAction(data))
        }
  }
}


export  function deleteProductThunk(product_id:number) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        //console.log("here20")
        const token:string|null = localStorage.getItem("token");
      //  console.log(token)
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await  fetchDeleteProduct(product_id)
           // console.log(data)
     
            //dispatch(getCategoryAction(data))
        }
  }
}


export  function postDiscountSetThunk(breakfast : boolean ,lunch :boolean,afternoonTea:boolean,dinner:boolean,
    nightSnack:boolean,name:string,is_launched:boolean,photo:any,price:number) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        let  available_period = []
        if(breakfast==true){available_period.push(1)}
        if(lunch==true){available_period.push(2)}
        if(afternoonTea==true){available_period.push(3)}
        if(dinner==true){available_period.push(4)}
        if(nightSnack==true){available_period.push(5)}
        const token:string|null = localStorage.getItem("token");
        console.log(token)
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await  fetchPostDiscountSet(available_period ,name,is_launched,photo,result.company_id,result.branch_id,price)
     
            const data2 = await fetchGetAllDiscountSet(result.company_id,result.branch_id)
            console.log(data2)
     
            dispatch(getAllDiscountSetAction(data2))
        }
  }
}


export  function getAllDiscountSetThunk() {
    return async(dispatch:Dispatch<IRootAction>)=>{
        console.log("here2233")
        const token:string|null = localStorage.getItem("token");
        console.log(token)
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await fetchGetAllDiscountSet(result.company_id,result.branch_id)
           
     
            dispatch(getAllDiscountSetAction(data))
        }
  }

}


export  function deleteDiscountSetThunk(id:number) {
    return async(dispatch:Dispatch<IRootAction>)=>{
       
        const token:string|null = localStorage.getItem("token");
        
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await fetchDeleteDiscountSet(id)
            console.log(data)
     
           
        }
  }
}

export  function postDiscountSetDetailsThunk(discount_set_id:number,product_category_id:number,description:string) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        
        const token:string|null = localStorage.getItem("token");
       
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await fetchPostDiscountSetDetails(discount_set_id,product_category_id,result.company_id,result.branch_id,description)
            //console.log(data)
     
           
        }
  }
}

export  function getDiscountSetDetailsThunk(discount_set_id:number,) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        
        const token:string|null = localStorage.getItem("token");
        
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await fetchGetDiscountSetDetails(discount_set_id)
            //console.log("wow",data)
            dispatch(getDiscountSetDetailsAction(data))
     
           
        }
  }
}


export  function deleteDiscountSetDetails(id:number) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        
        const token:string|null = localStorage.getItem("token");
     
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await  fetchDeleteDiscountSetDetails(id)
            console.log(data)
     
           
        }
  }
}


export  function postDiscountSetDetailsThunk2(discount_set_id:number,product_category_id:number,discount_set_sequence_id:number) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        
        const token:string|null = localStorage.getItem("token");
        
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await fetchPostDiscountSetDetails2(discount_set_id,product_category_id,result.company_id,result.branch_id,discount_set_sequence_id)
            //console.log(data)
     
           
        }
  }
}


export  function putDiscountSetThunk(id:number,breakfast : boolean ,lunch :boolean,afternoonTea:boolean,dinner:boolean,
    nightSnack:boolean,name:string,price:number,is_launched:boolean,photo:any) {
    return async(dispatch:Dispatch<IRootAction>)=>{
        let  available_period = []
        if(breakfast==true){available_period.push(1)}
        if(lunch==true){available_period.push(2)}
        if(afternoonTea==true){available_period.push(3)}
        if(dinner==true){available_period.push(4)}
        if(nightSnack==true){available_period.push(5)}
        const token:string|null = localStorage.getItem("token");
     
         let result
        if (token){
             result = await jwtDecode(token)
            const data = await  fetchPutDiscountSet(id,available_period,name,is_launched,photo,result.company_id,result.branch_id,price)
           
     
            //dispatch(getCategoryAction(data))
        }
  }
}