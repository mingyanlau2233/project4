import produce from "immer";
import { ICategoryAction ,  } from "./actions";
import { ICategoryState } from "./state";
import { IRootState } from "../store";


const initialState: ICategoryState = {
  // dataFromDb:[{
  //   name: 'rice',
  //   id : 1
  // }],
  dataFromDb: [],
  productFromDb:[],
  discountSetFromDb : [],
  discountSetDetailsFromDB:[],
};


export const categoryReducer = produce((state: ICategoryState, action: ICategoryAction) => {

  switch (action.type) {

    case "@@getCategory":
     
      state.dataFromDb = action.category;
      return ;
      
    
    case "@@getProduct":
       //console.log(action.product)
       state.productFromDb = action.product;
       return ;
       
    case "@@getAllDiscountSet":
       // console.log(action)
        state.discountSetFromDb = action.discountSet;
      return;


    case  "@@getAllDiscountDetailsSet":
      
       state.discountSetDetailsFromDB= action.details;
       return ;
  }
}, initialState);
