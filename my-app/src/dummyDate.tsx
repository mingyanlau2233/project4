export const userRows = [
    { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35, dateIn: '11-08-2021', position: '店長', phoneNumner: '12312454' },
    { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42, dateIn: '11-08-2021', position: '水吧', phoneNumner: '12312454' },
    { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45, dateIn: '11-08-2021', position: '水吧', phoneNumner: '12312454' },
    { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16, dateIn: '11-08-2021', position: '廚房', phoneNumner: '12312454' },
    { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null, dateIn: '11-08-2021', position: '店長', phoneNumner: '12312454' },
    { id: 6, lastName: 'Melisandre', firstName: null, age: 150, dateIn: '11-08-2021', position: '店長', phoneNumner: '12312454' },
    { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44, dateIn: '11-08-2021', position: '店長', phoneNumner: '12312454' },
    { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36, dateIn: '11-08-2021', position: '店長', phoneNumner: '12312454' },
    { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65, dateIn: '11-08-2021', position: '店長', phoneNumner: '12312454' },
];

export const remarksRows = [
    {id:1,name:"少甜",price: 0},
    {id:2,name:"多甜",price: 0},
    {id:3,name:"少冰",price: 0},
    {id:4,name:"多冰",price: 0},
    {id:5,name:"少奶",price: 0},
    {id:6,name:"多奶",price: 0},
]