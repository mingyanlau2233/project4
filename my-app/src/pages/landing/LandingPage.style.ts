import styled from 'styled-components'
import image from './img/乾炒牛河-removebg-preview.png'

export const Hero = styled.div`
    color:white;
    text-align:center;
    padding: 15em 0;
    // background-color:#222;
    background-size: 130%;

    @supports(background-blend-mode:multiply){
        background-image:
            url(${image}),
            radial-gradient(#444,#111);
        background-blend-mode:multiply;
        background-attachment:fixed;
        background-repeat: no-repeat;
        background-position:center center;
    }
`

export const Container = styled.div`
    max-width:65em; 
    // *+*{
    //     margin-top: var(--spacer,2em);
    // }
`

export const PrimaryTitle = styled.h1`
    font-size:4em;
    font-size: clamp(3em,calc(5vw+1rm),4.5em);
    line-height:1;
`

export const MainTextP = styled.p`

margin-top: var(--spacer,2em);`

export const Btn = styled.a`
    display:inline-block;
    text-decoration:none;
    padding: 0.5em 1.25em;
    background: var(--clr-accent,blue);
    color:var(--clr-text,white);
    font-weight:700;
    border-radius:.25em;
    margin-top: var(--spacer,2em);
    transition: 
        transform 250ms ease-in-out,
        opacity 250ms linear;
    &:focus,&:hover{
        transform:scale(1.1);
        opacity:0.9;
    }
`

export const Main = styled.main``

export const Featured = styled.section`
    background:#eee;
`

export const SectionTitle = styled.h2`
    text-align:center;
    font-size: clamp(2.5em,calc(5vw+1rm),4em);
    line-height: 1;
    color: #17353d;
    margin-bottom:5rem;
`

export const Split = styled.div`
    display:flex;
    gap:1em;
    flex-wrap:wrap;
    justify-content: center;
    &>*{
        flex-basis:30%;
        min-width:15em;
    }
`

export const FeaturedItem = styled.a`
display:block;
position:relative;
transform: scale(.85);
transition: transform 250ms ease-in-out;


&:focus{
    transform: scale(1);
}
&:hover{
    transform: scale(1);
}
&::after{
    content: '';
    position:absolute;
    padding:100% 100% 0 0;
    background:red;
}
`

export const FeaturedImg = styled.img`
    width:100%;
`

export const FeaturedDetails = styled.p``

export const OurProducts = styled.section``

export const Product = styled.article``

export const ProductImg = styled.img``

export const ProductDecription = styled.p``

