import React from 'react'
import { Hero, PrimaryTitle, MainTextP, Btn, Featured, SectionTitle, Split, FeaturedItem, FeaturedImg, FeaturedDetails, OurProducts, Product, Container } from './LandingPage.style'
import PageModal from './LandingPageModal'
import image from './img/楊洲炒飯.jpg'

export default function Page() {
    return (
        <>
                                    <Hero>
                            <Container>
                                <PrimaryTitle>
                                    乾炒牛河
                                </PrimaryTitle>
                                <MainTextP>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatem rerum fugiat iste architecto, porro, nam repellat necessitatibus quia, at veritatis consequuntur dolorum dolorem deleniti. Distinctio necessitatibus fuga iste quis optio.
                                </MainTextP>
                                <Btn href="#">點餐開始</Btn>
                            </Container>
                        </Hero>
                        <Featured>
                            <Container>
                                <SectionTitle>產品
                                </SectionTitle>
                                <Split>
                                    <FeaturedItem>

                                    <FeaturedImg src={image}>
                                    </FeaturedImg>
                                    <FeaturedDetails>
                                    </FeaturedDetails>
                                    </FeaturedItem>
                                    <FeaturedItem>
                                    <FeaturedImg src={image}>
                                    </FeaturedImg>
                                    <FeaturedDetails>
                                    </FeaturedDetails>
                                    </FeaturedItem>
                                    <FeaturedItem>
                                    <FeaturedImg src={image}>
                                    </FeaturedImg>
                                    <FeaturedDetails>
                                    </FeaturedDetails>
                                    </FeaturedItem>
                                </Split>
                            </Container>
                        </Featured>
                        <OurProducts>
                            <Container>
                                <SectionTitle>
                                </SectionTitle>
                                <Product>

                                </Product>
                            </Container>
                        </OurProducts>
        </>
    )
}
