import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Button from '@mui/material/Button';
// import Typography from '@mui/material/Typography';
import { Hero, PrimaryTitle, MainTextP, Btn, Featured, SectionTitle, Split, FeaturedItem, FeaturedImg, OurProducts, Product, FeaturedDetails,Container } from './LandingPage.style';
import image from './img/乾炒牛河-removebg-preview.png'

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    height: 1200,
    bgcolor: 'background.paper',
    border: '1px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function LandingPageModal() {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <div>
            <Button onClick={handleOpen}>Open modal</Button>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <Box sx={style}>
                        {/* <Typography id="transition-modal-title" variant="h6" component="h2">
              Text in a modal
            </Typography>
            <Typography id="transition-modal-description" sx={{ mt: 2 }}>
              Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
            </Typography> */}
                        <Hero>
                            <Container>
                                <PrimaryTitle>
                                    乾炒牛河
                                </PrimaryTitle>
                                <MainTextP>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatem rerum fugiat iste architecto, porro, nam repellat necessitatibus quia, at veritatis consequuntur dolorum dolorem deleniti. Distinctio necessitatibus fuga iste quis optio.
                                </MainTextP>
                                <Btn href="#">點餐開始</Btn>
                            </Container>
                        </Hero>
                        <Featured>
                            <Container>
                                <SectionTitle>產品
                                </SectionTitle>
                                <Split>
                                    <FeaturedItem>
                                    </FeaturedItem>
                                    <FeaturedImg src={image}>
                                    </FeaturedImg>
                                    <FeaturedDetails>
                                    </FeaturedDetails>
                                    <FeaturedItem>
                                    </FeaturedItem>
                                    <FeaturedImg src={image}>
                                    </FeaturedImg>
                                    <FeaturedDetails>
                                    </FeaturedDetails>
                                    <FeaturedItem>
                                    </FeaturedItem>
                                    <FeaturedImg src={image}>
                                    </FeaturedImg>
                                    <FeaturedDetails>
                                    </FeaturedDetails>
                                </Split>
                            </Container>
                        </Featured>
                        <OurProducts>
                            <Container>
                                <SectionTitle>
                                </SectionTitle>
                                <Product>

                                </Product>
                            </Container>
                        </OurProducts>
                    </Box>
                </Fade>
            </Modal>
        </div>
    );
}