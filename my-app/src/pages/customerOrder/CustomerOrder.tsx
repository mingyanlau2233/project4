import React from 'react'
import { useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import SimpleBottomNavigation from './BottonNavBar';
import { CustomerOrderLogo, CustomerOrderCategory, CustomerOrderCategoryTitle, CustomerOrderCategoryItem, CustomerOrderContainer, CustomerOrderCategoryContainer, CustomerOrderCategoryItemContainer, CustomerOrderCategoryItemTitle, CustomerOrderCategorySubTitle, CustomerOrderCategoryLongItem, BottomNavigationContainer } from './CustomerOrder.style'
// import MultipleItems from './swiper'
import BasicModal from './CustomerOrderCategoryModal';


export default function CustomerOrder() {
    const productData = useSelector((state: IRootState) => state.category.dataFromDb)
    console.log(productData)
    // const [value, setValue] = React.useState(0);
    // const handleOpen = () => <CartModal />;
    return (
        <>
            <CustomerOrderContainer>
                <CustomerOrderLogo>LOGO
                    {/* <MultipleItems/> */}
                </CustomerOrderLogo>
                <CustomerOrderCategory>
                    <CustomerOrderCategoryTitle>
                        精選
                    </CustomerOrderCategoryTitle>
                    <CustomerOrderCategoryContainer>
                        <CustomerOrderCategoryItemContainer>

                            <CustomerOrderCategoryItem>
                                here<BasicModal category_id={1} />
                            </CustomerOrderCategoryItem>
                            <CustomerOrderCategoryItemTitle>
                                飯
                            </CustomerOrderCategoryItemTitle>

                        </CustomerOrderCategoryItemContainer>
                        <CustomerOrderCategoryItemContainer>

                            <CustomerOrderCategoryItem>
                                1
                            </CustomerOrderCategoryItem>
                            <CustomerOrderCategoryItemTitle>
                                粉
                            </CustomerOrderCategoryItemTitle>

                        </CustomerOrderCategoryItemContainer>
                        <CustomerOrderCategoryItemContainer>

                            <CustomerOrderCategoryItem>
                                1
                            </CustomerOrderCategoryItem>
                            <CustomerOrderCategoryItemTitle>
                                弱
                            </CustomerOrderCategoryItemTitle>

                        </CustomerOrderCategoryItemContainer>
                        <CustomerOrderCategoryItemContainer>

                            <CustomerOrderCategoryItem>
                                1
                            </CustomerOrderCategoryItem>
                            <CustomerOrderCategoryItemTitle>
                                面
                            </CustomerOrderCategoryItemTitle>

                        </CustomerOrderCategoryItemContainer>
                        <CustomerOrderCategoryItemContainer>

                            <CustomerOrderCategoryItem>
                                1
                            </CustomerOrderCategoryItem>
                            <CustomerOrderCategoryItemTitle>
                                飯
                            </CustomerOrderCategoryItemTitle>

                        </CustomerOrderCategoryItemContainer>

                    </CustomerOrderCategoryContainer>
                </CustomerOrderCategory>
                <CustomerOrderCategory>
                    <CustomerOrderCategorySubTitle>
                        午餐
                    </CustomerOrderCategorySubTitle>
                    <CustomerOrderCategoryContainer>
                        <CustomerOrderCategoryLongItem>
                            1
                        </CustomerOrderCategoryLongItem>
                        <CustomerOrderCategoryLongItem>
                            1
                        </CustomerOrderCategoryLongItem>
                        <CustomerOrderCategoryLongItem>
                            1
                        </CustomerOrderCategoryLongItem>
                    </CustomerOrderCategoryContainer>
                </CustomerOrderCategory>
                <CustomerOrderCategory>
                    <CustomerOrderCategorySubTitle>
                        套餐
                    </CustomerOrderCategorySubTitle>
                    <CustomerOrderCategoryContainer>
                        <CustomerOrderCategoryLongItem>
                            1
                        </CustomerOrderCategoryLongItem>
                        <CustomerOrderCategoryLongItem>
                            1
                        </CustomerOrderCategoryLongItem>
                        <CustomerOrderCategoryLongItem>
                            1
                        </CustomerOrderCategoryLongItem>
                    </CustomerOrderCategoryContainer>
                </CustomerOrderCategory>
                <CustomerOrderCategory>
                    <CustomerOrderCategorySubTitle>
                        飲品
                    </CustomerOrderCategorySubTitle>
                    <CustomerOrderCategoryContainer>
                        <CustomerOrderCategoryLongItem>
                            1
                        </CustomerOrderCategoryLongItem>
                        <CustomerOrderCategoryLongItem>
                            1
                        </CustomerOrderCategoryLongItem>
                        <CustomerOrderCategoryLongItem>
                            1
                        </CustomerOrderCategoryLongItem>
                    </CustomerOrderCategoryContainer>
                </CustomerOrderCategory>


            </CustomerOrderContainer>
            <SimpleBottomNavigation />
            {/* <BottomNavigationContainer> */}
            {/* <Box sx={{ width: 375, position: 'sticky' , bottom: 0, left: 0, right: 0 }}>
                <BottomNavigation
                    showLabels
                    value={value}
                    onChange={(event, newValue) => {
                        setValue(newValue);
                    }}
                >
                    <BottomNavigationAction label="Recents" icon={<RestoreIcon />} />
                    <BottomNavigationAction component={Link} to="/" label="Cart" icon={<ShoppingCartIcon />} />
                    <BottomNavigationAction label="Nearby" icon={<LocationOnIcon />} />
                </BottomNavigation>
            </Box> */}
            {/* </BottomNavigationContainer> */}
        </>
    )
}
