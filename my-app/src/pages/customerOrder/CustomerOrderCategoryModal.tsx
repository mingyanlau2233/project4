import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { CategoryContainer, ItemContainer, ItemImg, ItemListContainer, ItemListTitle, ItemName, ItemPriceTag } from './CustomerOrderCategory.Modal.style';
import ProductModal from './CustomerOrderProduct.Modal';

const style = {
  position: 'absolute' as 'absolute',
  top: '65%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 350,
  height:700,
  bgcolor: 'background.paper',
  border: '1px solid lightgray',
  boxShadow:3,
  p: 3,
};

interface IPros {
  category_id: number, 
}

export default function BasicModal(pros:IPros) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Button onClick={handleOpen}>Open modal</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <CategoryContainer>
            <ItemListTitle>
              飯類
            </ItemListTitle>
          <ItemListContainer>
            <ItemContainer>
              <ItemImg>
              <ProductModal/>
              </ItemImg>
              <ItemPriceTag>
                $50
              </ItemPriceTag>
              <ItemName>
                多啦A夢
              </ItemName>
            </ItemContainer>
            <ItemContainer>
              <ItemImg>

              </ItemImg>
              <ItemPriceTag>
                $50
              </ItemPriceTag>
              <ItemName>
                多啦A夢
              </ItemName>
            </ItemContainer>
            <ItemContainer>
              <ItemImg>

              </ItemImg>
              <ItemPriceTag>
                $50
              </ItemPriceTag>
              <ItemName>
                多啦A夢
              </ItemName>
            </ItemContainer>
            <ItemContainer>
              <ItemImg>

              </ItemImg>
              <ItemPriceTag>
                $50
              </ItemPriceTag>
              <ItemName>
                多啦A夢
              </ItemName>
            </ItemContainer>
            <ItemContainer>
              <ItemImg>

              </ItemImg>
              <ItemPriceTag>
                $50
              </ItemPriceTag>
              <ItemName>
                多啦A夢
              </ItemName>
            </ItemContainer>
            <ItemContainer>
              <ItemImg>

              </ItemImg>
              <ItemPriceTag>
                $50
              </ItemPriceTag>
              <ItemName>
                多啦A夢
              </ItemName>
            </ItemContainer>
            <ItemContainer>
              <ItemImg>

              </ItemImg>
              <ItemPriceTag>
                $50
              </ItemPriceTag>
              <ItemName>
                多啦A夢
              </ItemName>
            </ItemContainer>
            <ItemContainer>
              <ItemImg>

              </ItemImg>
              <ItemPriceTag>
                $50
              </ItemPriceTag>
              <ItemName>
                多啦A夢
              </ItemName>
            </ItemContainer>
            <ItemContainer>
              <ItemImg>

              </ItemImg>
              <ItemPriceTag>
                $50
              </ItemPriceTag>
              <ItemName>
                多啦A夢
              </ItemName>
            </ItemContainer>
          </ItemListContainer>
          </CategoryContainer>
        </Box>
      </Modal>
    </div>
  );
}