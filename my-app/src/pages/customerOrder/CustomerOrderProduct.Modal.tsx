import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import { ProductContainer, ProductImg, ProductName, ProductOrderQuantityContainer, ProductOrderQuantityTitle, ProductOrderQuantityRight, ProductOrderQuantitySign, ProductOrderQuantity, ProductOrderTotalContainer, ProductOrderTotalPrice, ProductOrderTotalTitle, ButtonContainer } from './CustomerOrderProduct.Modal.style';
const style = {
    position: 'absolute' as 'absolute',
    top: '65%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 350,
    height: 700,
    bgcolor: 'background.paper',
    border: '1px solid lightgray',
    boxShadow: 3,
    p: 3,
};

export default function ProductModal() {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <div>
            <Button onClick={handleOpen}>Open modal</Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <ProductContainer>
                        <ProductImg>111
                        </ProductImg>
                        <ProductName>多啦A夢
                        </ProductName>
                        <ProductOrderQuantityContainer>
                            <ProductOrderQuantityTitle>
                                數量
                            </ProductOrderQuantityTitle>
                            <ProductOrderQuantityRight>
                                <ProductOrderQuantitySign>
                                    <RemoveIcon />
                                </ProductOrderQuantitySign>
                                <ProductOrderQuantity>1
                                </ProductOrderQuantity>
                                <ProductOrderQuantitySign>
                                    <AddIcon />
                                </ProductOrderQuantitySign>
                            </ProductOrderQuantityRight>
                        </ProductOrderQuantityContainer>
                        <ProductOrderTotalContainer>
                            <ProductOrderTotalTitle>
                                總計
                            </ProductOrderTotalTitle>
                            <ProductOrderTotalPrice>
                                HK$100
                            </ProductOrderTotalPrice>

                        </ProductOrderTotalContainer>
                        <ButtonContainer>

                        <Button variant="contained" color="success" fullWidth={true}> 
                            Success
                        </Button>
                        </ButtonContainer>
                        <ButtonContainer>
                        <Button variant="contained" color="error" fullWidth={true}> 
                            Success
                        </Button>
                        </ButtonContainer>

                    </ProductContainer>
                </Box>
            </Modal>
        </div>
    );
}