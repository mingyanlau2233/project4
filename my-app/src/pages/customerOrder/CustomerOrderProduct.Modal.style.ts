import styled from "styled-components"

export const ProductContainer = styled.div`
    display:flex;
    flex-direction:column;
`
export const ProductImg = styled.div`
    width:300px;
    height:200px;
    background-color:lightgray;
`
export const ProductName = styled.h2`

`
export const ProductOrderQuantityContainer = styled.div`
    display:flex;
    justify-content:space-between;
    align-items:center;
`
export const ProductOrderQuantityTitle = styled.div`

`
export const ProductOrderQuantityRight = styled.div`
    display:flex;
`
export const ProductOrderQuantitySign = styled.span`
    color:#555;
`
export const ProductOrderQuantity = styled.div`
    margin: 0 5px;
`

export const ProductOrderTotalContainer = styled.div`
display:flex;
justify-content:space-between;
align-items:center;
`

export const ProductOrderTotalTitle = styled.div`

`

export const ButtonContainer = styled.div`
    margin-top: 20px ;
`

export const ProductOrderTotalPrice = styled.div`

`