import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table } from "react-bootstrap"
import { IRootState } from "../../redux/store";
import { getOrderCartProductThunk, submitOrderThunk } from "../../redux/orderCart/thunks";
import ProductModal from "./CustomerOrderProduct.Modal";
import React from "react";
import { getAllCategoryThunk, getProductDataThunk } from "../../redux/productSetting/thunks";
import ProductComponent from "./productComponent"
import Box from '@mui/material/Box';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import "./styles.css"
import IconButton from '@mui/material/IconButton';
import InfoIcon from '@mui/icons-material/Info';
interface CategoryProps {

  id: number;
  name: string;
  //is_inSetOnly: boolean ; 
}
const { REACT_APP_SOCKET_IO_URL } = process.env;
export default function CategoryComponent(props: CategoryProps) {

  const dispatch = useDispatch();

  const productData = useSelector((state: IRootState) => state.category.productFromDb)

  // let  [currentTime, setCurrentTime] = React.useState(new Date().getHours());
  let time = new Date().getHours()
  let period = checkPeriod(time)

  let [currentPeriod, setCurrentPeriod] = React.useState(period);
  useEffect(() => {
   
    dispatch(getProductDataThunk(props.id))

  }, [])

  function checkPeriod(time: number) {
   
    if (time >= 2 && time < 11) {
      return "breakfast"
    }
    if (time >= 11 && time < 14) {
      return "lunch"
    }
    if (time >= 14 && time < 18) {
      return "afternoonTea"
    }
    if (time >= 18 && time < 22) {
     
      return "dinner"
    }
    if (time >= 22 || time < 2) {
     
      return 'nightSnack'
    }

  }
  const filterData = (datas: any[]) => datas.filter((data) => data.product_category_id == props.id
    && data.is_launched == true && data[`${currentPeriod}`] == true)

  return (


    <>
      <div className="stickyTitle">
      <h1>{props.name}</h1>
      <h3 className="text-decoration">{currentPeriod}</h3>
      </div>
      <ImageList sx={{ width: 1, height: 1}} variant="quilted"
        cols={3} gap={30} rowHeight={350} >
        {filterData(productData).map((data) => (
          <ImageListItem key={`${REACT_APP_SOCKET_IO_URL}/${data.photo}`}   >
            <img
              src={`${REACT_APP_SOCKET_IO_URL}/${data.photo}?w=248&fit=crop&auto=format`}
              srcSet={`${REACT_APP_SOCKET_IO_URL}/${data.photo}?w=248&fit=crop&auto=format&dpr=2 2x`}
              alt={data.name}
              loading="lazy"
            />
            {/* <ProductComponent name={data.name} product_category_id={data.product_category_id}
              price={data.price} photo={data.photo} product_id={data.product_id}
            /> */}

            <ImageListItemBar              actionIcon={<ProductComponent name={data.name} product_category_id={data.product_category_id}
              price={data.price} photo={data.photo} product_id={data.product_id}
            /> }/>
          </ImageListItem>
        ))}
      </ImageList>


    </>

  )
}

