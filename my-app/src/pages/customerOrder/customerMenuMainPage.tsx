import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table } from "react-bootstrap"
import { IRootState } from "../../redux/store";
import { getOrderCartProductThunk, submitOrderThunk } from "../../redux/orderCart/thunks";
import ProductModal from "../customerOrder/CustomerOrderProduct.Modal";
import { AssignContainer } from "../../components/product/product.style";
import React from "react";
import { getAllCategoryThunk, getAllDiscountSetThunk } from "../../redux/productSetting/thunks";
import CategoryComponent from "./categoryComponent";
import ProductComponent from "./discountSetComponent";
import { Photo } from "@material-ui/icons";
import { useForm } from "react-hook-form";
import "./styles.css"
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Box from '@mui/material/Box';
import { ImageList } from "@mui/material";
import IconButton from '@mui/material/IconButton';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import { Link } from "react-router-dom";

export default function MenuMainPage() {

  const dispatch = useDispatch();

  const productCategory = useSelector((state: IRootState) => state.category.dataFromDb)
  useEffect(() => {
    console.log(123)
    dispatch(getAllCategoryThunk())
    dispatch(getAllDiscountSetThunk())
  }, [dispatch])

  ///////  For discount Set 
  const discountSet = useSelector((state: IRootState) => state.category.discountSetFromDb)
  let time = new Date().getHours()
  let period = checkPeriod(time)
  let [currentPeriod, setCurrentPeriod] = React.useState(period);
  function checkPeriod(time: number) {
    console.log("period check ")
    if (time >= 2 && time < 11) {
      return "breakfast"
    }
    if (time >= 11 && time < 14) {
      return "lunch"
    }
    if (time >= 14 && time < 18) {
      return "afternoonTea"
    }
    if (time >= 18 && time < 22) {
      console.log("period check 2")
      return "dinner"
    }
    if (time >= 22 || time < 2) {
      console.log("period check999 ")
      return 'nightSnack'
    }

  }
  let [category, setCategory] = React.useState(1);


  const filterPeriodForDiscountSetData = (datas: any[]) => datas.filter((data) =>
    data[`${currentPeriod}`] == true && data.is_launched == true)

  const filterData = (datas: any[], selectedCategory: number) => datas.filter((data) => data.is_inSetOnly == false && data.id == selectedCategory)

  const filterForButton = (datas: any[]) => datas.filter((data) => data.is_inSetOnly == false)



  const chooseCategory = (e: any) => {
    e.preventDefault()
    setCategory(e.target.value)
  }

  function checkedCategoryCss(num:number):string{
      if(num==category){
      return "checkedCategory"}
      return "123"
  }
  return (
    <>   
      <div className="MenuMainPageContainer">
        <div className="discountMainContainer">
          <h1 className="stickyTitle">選擇套餐<div className="text-decoration">{period}</div></h1>
          <div className="discountWrap-this">
            {filterPeriodForDiscountSetData(discountSet).map((data) =>
              <ProductComponent name={data.name} id={data.id}
                photo={data.photo} price={data.price} />
            )}
          </div>
        </div>
        <div className="border"></div>
        <div className="ProductMainContainer">
          <div>
            <ButtonGroup fullWidth={true} variant='text' aria-label="text button group">
              {filterForButton(productCategory).map((data) =>
                <Button className={checkedCategoryCss(data.id)} value={data.id} onClick={chooseCategory}  >{data.name}</Button>)}
            </ButtonGroup>
          </div>

          {filterData(productCategory, category).map((data) =>
            <div>
              <CategoryComponent id={data.id} name={data.name} />
            </div>
          )}
        </div>
        <div >
          <IconButton className="cartContainer" component={Link} to={"/orderCart"} sx={{ position: 'fixed', bottom: 150, right: 120,width:120,height:120}} color="primary" aria-label="add to shopping cart">
            <div>
              <AddShoppingCartIcon sx={{ fontSize: 50 }} />
              <h2 className="cartText">前往購物車</h2>

            </div>
          </IconButton>
        </div>
      </div>
    </>

  )
}