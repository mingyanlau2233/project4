import styled from "styled-components"

export const CustomerOrderCartContainer = styled.div`
    display:flex;
    flex-direction:column;
`

export const CustomerOrderCartTitle = styled.div`

`

export const OrderListsContainer = styled.div`

`

export const OrderItem = styled.div`

`

export const OrderItemLeft = styled.div`

`

export const OrderItemMiddle = styled.div`

`

export const ProductName = styled.div`

`

export const OrderQuatity = styled.div`

`


export const OrderQuatitySign = styled.div`

`


export const OrderQuatityNumber = styled.div`

`


export const EachPrice = styled.div`

`

export const OrderItemRight = styled.div`

`


export const EachSubPrice = styled.div`

`

