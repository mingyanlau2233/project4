import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import React from "react";
import { postProductToOrderCartThunk } from "../../redux/menuPage/thunk";
import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import Slide, { SlideProps } from '@mui/material/Slide';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import IconButton from '@mui/material/IconButton';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';


type TransitionProps = Omit<SlideProps, 'direction'>;
interface ProductProps {

  name: string;
  product_category_id: number;
  price: number;
  photo: string
  product_id: number
}
export default function ProductComponent(props: ProductProps) {

  const dispatch = useDispatch();

  let [qty, setQty] = React.useState(0);

  function add() { setQty(qty + 1) }
  function drop() {
    if (qty > 0) { setQty(qty - 1) }
  }


  function TransitionDown(props: TransitionProps) {
    return <Slide {...props} direction="down" />;
  }
  const [open, setOpen] = React.useState(false);
  const [transition, setTransition] = React.useState<
    React.ComponentType<TransitionProps> | undefined
  >(undefined);
  const handleClick = (Transition: React.ComponentType<TransitionProps>) => () => {
    dispatch(postProductToOrderCartThunk(qty, props.price, props.product_id, props.name))
   
    setTransition(() => Transition);
    setOpen(true);


  };
  const handleClose = () => {
    setQty(qty = 0)
    setOpen(false);
  };



  function postToOrderCart() {

    dispatch(postProductToOrderCartThunk(qty, props.price, props.product_id, props.name))

    setQty(qty = 0)
  }



  return (


    < >
      <div className="cartDetails">
        <div className="ProductPriceContainer">
         
          <div className="product2">{props.name}</div>
          <div className="PriceContainer">
          <div>單價 : $</div><div>{props.price}</div>
          </div>
        </div>
        <div className="AddtoCartBtnContainer">
          <div className="quantityContainer">
          <IconButton color="primary"  className="btw-p2"onClick={drop}aria-label="add to shopping cart">
        <RemoveCircleIcon />
      </IconButton>
            <div className="quantityNumberQantity" >{qty}</div>
            {/* <div>數量</div> */}
            <IconButton color="primary"   className="btw-p2" onClick={add}aria-label="add to shopping cart">
        <AddCircleIcon />
      </IconButton>
          </div> 
          <div>
            <Button type="submit" className="btw-p"  variant="contained" color="primary"  sx={{margin:1}}onClick={handleClick(TransitionDown)}>加入購物車</Button>
            <Snackbar
              open={open}
              onClose={handleClose}
              TransitionComponent={transition}
              message={qty + "份" + props.name + "以加入購物車"}
              key={transition ? transition.name : ""}
            />
          </div>

        </div>

      </div>

    </>

  )
}