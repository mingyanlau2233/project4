import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { CustomerOrderCartContainer, CustomerOrderCartTitle, EachPrice, EachSubPrice, OrderItem, OrderItemLeft, OrderItemMiddle, OrderItemRight, OrderListsContainer, OrderQuatity, OrderQuatityNumber, OrderQuatitySign, ProductName } from './CutomerOrderCart.style';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import SimpleBottomNavigation from './BottonNavBar';


export default function Cart() {


    return (
        <>
            <CustomerOrderCartContainer>
                <CustomerOrderCartTitle>

                </CustomerOrderCartTitle>
                <OrderListsContainer>
                    <OrderItem>
                        <OrderItemLeft>
                        </OrderItemLeft>
                        <OrderItemMiddle>
                            <ProductName>
                            </ProductName>
                            <OrderQuatity>
                                <OrderQuatitySign>

                                </OrderQuatitySign>
                                <OrderQuatityNumber>

                                </OrderQuatityNumber>
                                <OrderQuatitySign>

                                </OrderQuatitySign>

                            </OrderQuatity>
                        </OrderItemMiddle>
                        <OrderItemRight>
                            <EachPrice>

                            </EachPrice>
                            <EachSubPrice>

                            </EachSubPrice>
                        </OrderItemRight>

                    </OrderItem>

                </OrderListsContainer>
            </CustomerOrderCartContainer>
            <SimpleBottomNavigation />
        </>
    );
}