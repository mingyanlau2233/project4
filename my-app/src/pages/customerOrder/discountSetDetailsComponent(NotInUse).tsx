import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table } from "react-bootstrap"
import { IRootState } from "../../redux/store";
import { getOrderCartProductThunk ,submitOrderThunk} from "../../redux/orderCart/thunks";
import ProductModal from "./CustomerOrderProduct.Modal";
import { AssignContainer } from "../../components/product/product.style";
import React from "react";
import { postProductToOrderCartThunk, } from "../../redux/menuPage/thunk";
import { getProductDataThunk } from "../../redux/productSetting/thunks";
import { useForm } from "react-hook-form";
import { Form, Button } from "react-bootstrap";

interface DiscountSetProps{
  name:string,
  discount_set_sequence_id:number,
  description: string,
  id: number,
  product_category_id:number
}
export default function  DiscountSetDetailsComponent(props:DiscountSetProps) {

    const dispatch = useDispatch();
    const productData =useSelector((state:IRootState)=>state.category.productFromDb)

    useEffect(() => {
       
        dispatch(getProductDataThunk(props.product_category_id)  )
      },[])
    
      const filterData = (datas:any[])=> datas.filter((data) =>  
      data.product_category_id == props.product_category_id)
      let  [productId,setProductId]=React.useState("0");
      let  [selected,setProductSelected]=React.useState([productId,props.discount_set_sequence_id]);
      
      function submit(){
          console.log(productId)
          console.log(selected)
      }
   return (

    
        <>
           {/* <div>{props.description}</div> */}
           <select aria-label="Default select example" value ={productId} onChange={function(e){
                setProductId(e.target.value)
               setProductSelected([productId,props.discount_set_sequence_id])
              
              }} >
           <option >請選擇{props.description}中的{props.name}</option>
                        {filterData(productData).map((product)=>
                              <option value={product.product_id}>{product.name}</option>)}
                     </select>
            <button type="submit" onClick={submit}>+</button>
          
        </>
        
    )
  }