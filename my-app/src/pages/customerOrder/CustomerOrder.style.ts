import styled from "styled-components"

export const CustomerOrderContainer = styled.div`

    display:flex
    flex-direction:column
`

export const CustomerOrderLogo = styled.div``

export const CustomerOrderCategory = styled.div`
    margin: 12px 5px;
`


export const CustomerOrderCategoryTitle = styled.h1``

export const CustomerOrderCategoryContainer = styled.div`
    display: flex;
    flex-wrap: nowrap;
    overflow-x: auto;
    margin: 5px 0;
    -webkit-overflow-scrolling: touch;
    &::-webkit-scrollbar {
    display: none;
    }
    `

export const CustomerOrderCategoryItem = styled.div`
    height: 100px;
    width: 100px;
    flex: 0 0 auto;
    background-color:lightgray;
    margin:0 5px;
`

export const CustomerOrderCategoryItemContainer = styled.div`
    disaplay:flex;
    flex-direction:column;
`

export const CustomerOrderCategoryItemTitle = styled.p`
    text-align:center;
    font-size:12px;
    margin-top:5px;
`

export const CustomerOrderCategorySubTitle = styled.h2``

export const CustomerOrderCategoryLongItem = styled.div`
height: 100px;
width: 150px;
flex: 0 0 auto;
background-color:lightgray;
margin:0 5px;
`

export const BottomNavigationContainer = styled.div`
    position: sticky;
    bottom :0;
`