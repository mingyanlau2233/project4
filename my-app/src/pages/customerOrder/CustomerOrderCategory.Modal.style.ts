import styled from "styled-components"

export const CategoryContainer = styled.div`
    display:flex;
    justify-content:center;
    flex-direction: column;

`

export const ItemListContainer = styled.div`
    display:flex;
    flex-wrap:wrap; 
    height:600px;
    overflow-y: auto;
    -webkit-overflow-scrolling: touch;
    &::-webkit-scrollbar {
    display: none;}
    justity-content: flex-start;
`

export const ItemListTitle = styled.h2`
    margin-left:15px;
`

export const ItemContainer = styled.div`
    height:180px;
    width:120px;
    margin:0 15px;

`

export const ItemImg = styled.div`
    height:120px;
    width:120px;
    background-color:lightgray;
    display:flex;
    justify-content:center;
`

export const ItemPriceTag = styled.p`

`

export const ItemName = styled.h4`

`

