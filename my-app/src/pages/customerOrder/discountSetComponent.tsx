import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table } from "react-bootstrap"
import { IRootState } from "../../redux/store";
import { getOrderCartProductThunk, submitOrderThunk } from "../../redux/orderCart/thunks";
import ProductModal from "./CustomerOrderProduct.Modal";
import { AssignContainer } from "../../components/product/product.style";
import React from "react";
import { postDiscountSetToOrderCartThunk, postProductToOrderCartThunk } from "../../redux/menuPage/thunk";
import { getDiscountSetDetailsThunk, getProductDataThunk ,} from "../../redux/productSetting/thunks";
import DiscountSetDetailsComponent from "./discountSetDetailsComponent(NotInUse)"
import Button from '@mui/material/Button';
import "./styles.css"
import Snackbar from '@mui/material/Snackbar';
import Slide, { SlideProps } from '@mui/material/Slide';

type TransitionProps = Omit<SlideProps, 'direction'>;
interface DiscountSetProps {

  name: string,
  id: number,
  photo: string;
  price: number;
}

function TransitionDown(props: TransitionProps) {
  return <Slide {...props} direction="down" />;
}
const { REACT_APP_SOCKET_IO_URL } = process.env;
export default function ProductComponent(props: DiscountSetProps) {
  const dispatch = useDispatch();
  const discountSetDetailsData = useSelector((state: IRootState) => state.category.discountSetDetailsFromDB)
  const productData = useSelector((state: IRootState) => state.category.productFromDb)
  const [open, setOpen] = React.useState(false);

  const [transition, setTransition] = React.useState<
    React.ComponentType<TransitionProps> | undefined
  >(undefined);

  const handleClick = (Transition: React.ComponentType<TransitionProps>) => () => {
    let handleData = productDataToDB.slice(0, productDataToDB.length - 1).split("?")
  

   if(count== handleData.length && handleData[0]!= ""){
   
   
    dispatch(postDiscountSetToOrderCartThunk(props.price, props.id, props.name, handleData))
    setTransition(() => Transition);
    setOpen(true);
    return 
   }
   alert("請完成選單");
  };

  const handleClose = () => {
    setOpen(false);
   // setInterval(() => { window.location.reload() }, 100);
  };

  useEffect(() => {
    dispatch(getDiscountSetDetailsThunk(props.id))
    dispatch(getProductDataThunk(1))
    
  }, [])
  const filterData = (datas: any[]) => datas.filter((data) =>
    data.discount_set_id == props.id)
  const filterData2 = (data2: any[], compare: any) => data2.filter((data2) =>
    data2.product_category_id == compare)


  let [productDataToDB, setProductId] = React.useState('');

  // function submit() {
  //   console.log(productDataToDB)
  //   let handleData = productDataToDB.slice(0, productDataToDB.length - 1).split("?")
  //   console.log(handleData)
  //   dispatch(postDiscountSetToOrderCartThunk(props.price, props.id, props.name, handleData))
   
   
  // }
  let count = filterData(discountSetDetailsData).length
  // console.log("count",count)
   
  

 
  return (
      <div className="MenuImageContainer">
        <img className="Image"
          src={`${REACT_APP_SOCKET_IO_URL}/${props.photo}`}

        />
        <div className="discountSetDetailContainer">
        
         
          
          <div className="SelectionContainer">
          <h2 className="discountSetDetail">{props.name} &nbsp;價錢 : {props.price}</h2>
           
            {filterData(discountSetDetailsData).map((data) => (
              <>
                
                
                <select aria-label="Default select example" className="option" onChange={function (e) {
                  //  console.log(e.target.value,data.discount_set_sequence_id)
                  let selectedProduct = data.discount_set_sequence_id + "," + e.target.value + "," + data.description
                  //  console.log(selectedProduct)
                  setProductId(productDataToDB + selectedProduct + "?")

                }} >
                  <option >請選擇{data.description}中的{data.name}</option>
          
                  {filterData2(productData, data.product_category_id).map((product) =>
                  <>
                    <option  value={product.product_id}>
                      {product.name}</option>
                      </>)}
                </select>
              </>
            ))}
                   <Button className="btwN" fullWidth={true} onClick={handleClick(TransitionDown)}>加入購物車</Button>
        <Snackbar
          open={open}
          onClose={handleClose}
          TransitionComponent={transition}
          message={`${props.name}下單完成`}
          key={transition ? transition.name : ''}
        />
          </div>
        </div>
 
      </div>
  );
}