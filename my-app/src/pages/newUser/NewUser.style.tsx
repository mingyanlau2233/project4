import styled from 'styled-components'

export const NewUserContainer = styled.div`
    flex:6;
`

export const NewUserTitle = styled.div`

`

export const NewUserForm = styled.form`
    display:flex;
    flex-wrap:wrap;
`

export const NewUserItem = styled.div`
width:400px;
display:flex;
flex-direction:column;
margin-top:10px;
margin-right:20px;
`