import React from 'react'
import {
    NewUserContainer,
    // NewUserForm,
    // NewUserItem,
    NewUserTitle
} from './NewUser.style'
import './NewUser.css'
import { FormProvider, useForm } from "react-hook-form";
import { FormInputText } from '../../components/form/FormInputText'
import { Button } from '@mui/material';
// import { useForm } from "react-hook-form";
// import { Button } from '@mui/material';
// import { Link } from 'react-router-dom';
interface IFormInput {
    textValue: string;
    radioValue: string;
    checkboxValue: string[];
    dateValue: Date;
    dropdownValue: string;
    sliderValue: number;
}
const defaultValues = {
    textValue: "",
    radioValue: "",
    checkboxValue: [],
    dateValue: new Date(),
    dropdownValue: "",
    sliderValue: 0,
};
// interface IFormInputs {
//     firstName: string;
//     lastName: string;
//     isDeveloper: boolean;
//     age: number;
//     email: string;
// }


export default function NewUser() {
    const methods = useForm<IFormInput>({ defaultValues: defaultValues });
    const { handleSubmit, reset, control, setValue, watch } = methods;
    const onSubmit = (data: IFormInput) => {console.log(data)};

    // const {
    //     register,
    //     handleSubmit,
    //     formState: { errors }
    // } = useForm<IFormInputs>();

    // const onSubmit = (data: IFormInputs) => {
    //     console.log(data);
    // };
    return (
        <NewUserContainer>
            <NewUserTitle>New User</NewUserTitle>
            <FormInputText name="userName" control={control} label="Username" />
            <Button onClick={handleSubmit(onSubmit)} variant={"contained"}>
                {" "}
                Submit{" "}
            </Button>
            <Button onClick={() => reset()} variant={"outlined"}>
                {" "}
                Reset{" "}
            </Button>
            {/* <form onSubmit={handleSubmit(onSubmit)}>
                    <div>
                        <label>First Name</label>
                        <input {...register("firstName" ,{required:"This is so fucked up"})} placeholder="Kotaro" />
                        {errors?.firstName && <p>{errors.firstName.message}</p>}
                    </div>

                    <div>
                        <label>Last Name</label>
                        <input {...register("lastName" ,{required:true , minLength:8})} placeholder="Sugawara" />
                    </div>
                    <div>
                        <label>Age</label>
                        <input {...register("age" ,{required:true,  
                        minLength:{
                            value:8 , 
                            message:"密碼過短"
                        }})} placeholder="Sugawara" />
                        {errors?.age && <p>{errors.age.message}</p>}
                    </div>

                    <div>
                        <label htmlFor="isDeveloper">Is an developer?</label>
                        <input
                            type="checkbox"
                            {...register("isDeveloper")}
                            placeholder="luo"
                            value="yes"
                        />
                    </div>

                    <div>
                        <label htmlFor="email">Email</label>
                        <input
                            {...register("email")}
                            placeholder="bluebill1049@hotmail.com"
                            type="email"
                        />
                    </div>
                    <input type="submit" />
                </form> */}
            {/* <NewUserForm>
                <NewUserItem>
                    <label className="NewUserItemLabel">帳號</label>
                    <input className="NewUserItemInput" type="text" placeholder="sosingho" />
                </NewUserItem>
                <NewUserItem>
                    <label className="NewUserItemLabel">姓名</label>
                    <input className="NewUserItemInput" type="text" placeholder="陳大文" />
                </NewUserItem>
                <NewUserItem>
                    <label className="NewUserItemLabel">密碼</label>
                    <input className="NewUserItemInput" type="password" placeholder="password" />
                </NewUserItem>
                <NewUserItem>
                    <label className="NewUserItemLabel">電郵</label>
                    <input className="NewUserItemInput" type="email" placeholder="sosingho@gmail.com" />
                </NewUserItem>
                <NewUserItem>
                    <label className="NewUserItemLabel">電話</label>
                    <input className="NewUserItemInput" type="text" placeholder="+852 1231 2131" />
                </NewUserItem>
                <NewUserItem>
                    <label className="NewUserItemLabel">地址</label>
                    <input className="NewUserItemInput" type="text" placeholder="Hong Kong" />
                </NewUserItem>
                <NewUserItem>


                </NewUserItem>
                <Button component={Link} to={'/newUser'} variant="contained" color="success">
                    新增
                </Button>
            </NewUserForm> */}

        </NewUserContainer>
    )

}
