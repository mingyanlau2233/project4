import styled from 'styled-components';

export const DepartmentContainer = styled.div`
    flex:6;
    padding:20px;
`

export const DepartmentTitleContainer = styled.div`
    display :flex;
    align-items:center;
    justify-content: space-between;
`

export const DepartmentTitle = styled.h1`
    
`
export const DepartmentItemContainer = styled.div`
    display:flex;
    flex-wrap: wrap;
`

export const DepartmentItemTitleContainer = styled.div`
    
`
export const DepartmentItem = styled.div`
width:45%;
margin:10px 20px;
padding: 30px;
border-radius:10px;
cursor: pointer;
-webkit-box-shadow: 0px 0px 15px -10px #000000; 
box-shadow: 0px 0px 15px -10px #000000;
display:flex;
justify-content: space-between;
`

export const DepartmentItemTitle = styled.h1`
    
`

export const DepartmentItmeSub = styled.span`
font-size: 14px;
color:gray;
`

export const DepartmentItemBtnContainer = styled.div`
    display:flex;
    align-items:center;
`