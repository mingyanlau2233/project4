import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { DepartmentContainer, DepartmentItem, DepartmentItemBtnContainer, DepartmentItemContainer, DepartmentItemTitle, DepartmentItemTitleContainer, DepartmentItmeSub, DepartmentTitle, DepartmentTitleContainer } from './Department.style'
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import { deleteDepartment, getDepartments } from '../../redux/department/thunks';
import { IRootState } from '../../redux/store';
import BasicModal from '../../components/modal/Modal';


export default function Department() {
    const departments = useSelector((state: IRootState) => state.department.department)
    const dispatch = useDispatch();
    useEffect(() => {
        if (departments.length === 0) {
            dispatch(getDepartments());
        }
        console.log("departments",departments)
    }, [ dispatch])
    // const [data,setData] = useState(departments)
    const handleDelete = (id:number) =>{
        dispatch(deleteDepartment(id));
    }

    return (
        <DepartmentContainer >
            <DepartmentTitleContainer>
                <DepartmentTitle>
                    部門
                </DepartmentTitle>

                <BasicModal />
            </DepartmentTitleContainer>
            <DepartmentItemContainer>
                {
                    // departments.length
                    departments.length > 0 && departments.map(department => (
                        <DepartmentItem key={department.id}>
                            <DepartmentItemTitleContainer>
                                <DepartmentItemTitle>{department.name}
                                </DepartmentItemTitle>
                                <DepartmentItmeSub>Create at 01-01-2021
                                </DepartmentItmeSub>
                            </DepartmentItemTitleContainer>
                            <DepartmentItemBtnContainer>
                                <Button variant="outlined" color="error"  onClick={() => handleDelete(department.id)}>
                                    刪除
                                </Button>
                            </DepartmentItemBtnContainer>
                        </DepartmentItem>
                    ))
                }

            </DepartmentItemContainer>


        </DepartmentContainer>
    )
}
