import { useForm } from "react-hook-form";
import { Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../../redux/store";
import React, { useEffect } from "react";
import { putOrderCartProductThunk, deleteOrderCartProductThunk, getOrderCartProductThunk } from "../../../redux/orderCart/thunks";
import Button from '@mui/material/Button';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import IconButton from '@mui/material/IconButton';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';


interface OrderCartProductProps {
  id: number,
  product_name: string
  product_id: number,
  qty: number,
  price: number
  isSubmit: boolean
}



export default function GetProductForOrderCart(props: OrderCartProductProps) {


  const dispatch = useDispatch();
  const [qty, setQty] = React.useState(props.qty);
  const [pricePreEach, setPricePreEach] = React.useState(props.price / qty);
  const [price, setPrice] = React.useState(props.price);

  console.log(props.isSubmit)

  function addProduct() {
    dispatch(putOrderCartProductThunk(props.id, qty + 1, price / qty * (qty + 1)))
    setPrice(price / qty * (qty + 1))
    setQty(qty + 1)
    setInterval(() => { window.location.reload() }, 100);
  }
  function dropProduct() {
    if (qty > 1) {
      dispatch(putOrderCartProductThunk(props.id, qty - 1, price / qty * (qty - 1)))

      setPrice(price / qty * (qty - 1))
      setQty(qty - 1)
    }
    setInterval(() => { window.location.reload() }, 100);
  }
  function deleteProduct() {
    dispatch(deleteOrderCartProductThunk(props.id))
    dispatch(getOrderCartProductThunk())
      // setInterval(() => { window.location.reload() }, 100);
      ;
  }

function showButton() {
    if (props.isSubmit == false) {
      return (
        <>
          <td className="actionBtnContainer">
          <IconButton color="primary" onClick={dropProduct}aria-label="add to shopping cart">
        <RemoveCircleIcon />
      </IconButton>
            <div className="quantityNumberQantity" >{qty}</div>
            {/* <div>數量</div> */}
            <IconButton color="primary" onClick={addProduct}aria-label="add to shopping cart">
        <AddCircleIcon />
      </IconButton>
          </td>
          <td><Button  color="error" type="submit" onClick={deleteProduct}>刪除</Button></td>
        </>)
    }
  }
  return (
      <tr>
        <td className="setName">{props.product_name}</td>
        <td className="fontColorGray">{qty}</td>
        <td className="fontColorGray">{pricePreEach}</td>
        <td className="fontColorGray">{price}</td>
        {showButton()}
      </tr>
  );
}