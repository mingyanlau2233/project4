import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import GetProductForOrderCart from "./orderCartComponents/orderCartFormForProduct";
import { IRootState } from "../../redux/store";
import {
  getOrderCartDiscountThunk,
  getOrderCartProductThunk,
  submitOrderThunk,
} from "../../redux/orderCart/thunks";
import ProductModal from "../customerOrder/CustomerOrderProduct.Modal";
import { AssignContainer } from "../../components/product/product.style";
import React from "react";
import DiscountSetComponent from "./orderCartDiscountSetOrderDetails";
import { useHistory } from "react-router-dom";
import { io } from "socket.io-client";
import socketClient from "socket.io-client";
import "./styles.css"
import { Button } from "@mui/material";

interface Details {
  discount_set_sequence_name: string;
  product_name: string;
}



export default function OrderCartMainPage() {
  const dispatch = useDispatch();

  const orderCartProductData = useSelector(
    (state: IRootState) => state.orderCart.GetOrderCartProduct
  );
  const orderCartDiscountSetData = useSelector(
    (state: IRootState) => state.orderCart.GetOrderCartDiscountSet
  );
  const ordersId = useSelector(
    (state: IRootState) => state.order.returnOrderId
  );

  useEffect(() => {
    dispatch(getOrderCartProductThunk());
    dispatch(getOrderCartDiscountThunk());


  }, []);

  let add = (data: any[]) => data.reduce((a, b) => a.price + b.price);
  // const  answer = add(orderCartProductData)

  function submitOrder() {
    if (orderCartProductData[0] || orderCartDiscountSetData[0]) {
      dispatch(submitOrderThunk());
      setButton(1)
      setIsSubmit(true)
      alert("你的帳單已經提交");


      return;
    }
    if (!orderCartProductData[0] || !orderCartDiscountSetData[0]) {
      alert("你没有選購任何商品，請到購物車選購");
      return;
    }
    //setInterval(() => { window.location.reload() }, 100);
  }

  const [button, setButton] = React.useState(0);
  const [isSubmit, setIsSubmit] = React.useState(false);

  function submitBefore() {
    if (button == 0) {
      return <Button className="btnHover"color="primary" type="submit" variant="outlined" style={{ margin: 20 }} size="medium" onClick={submitOrder}>
        確認訂單
      </Button>
    }
  }
  function submitBeforeTable() {
    if (button == 0) {
      return <><th></th>
        <th></th></>
    }
  }

  function submitBeforeButton() {
    if (button == 0) {
      return <>          <td></td>
        <td>
          {submitBefore()}
        </td>
      </>
    }
  }

  function sum() {
    let totalPrice = 0;
    orderCartProductData.map((data) => (totalPrice += Number(data.price)));
    orderCartDiscountSetData.map((data) => (totalPrice += Number(data.price)));
    return totalPrice;
  }
  const history = useHistory()
  const backToMenuPage = () => {
    let path = `/menuPage`;
    history.push(path);
  }

  function menuPage() {
    if (button == 0) {
      return <Button className="btnHover"color="primary" variant="outlined"  sx={{ width:0.55}} style={{ margin: 0 }} type="submit" size="large" onClick={backToMenuPage}>選擇更多菜品</Button>
    }else{
      return <Button className="btnHover" color="primary" variant="outlined"  sx={{ width:0.55}} style={{ margin: 0 }} type="submit" size="large" onClick={backToMenuPage}>返回菜單</Button>
    }
  }
  return (
    <div className="orderCartMainPageContainer">
      <h2>購物車</h2>
     
      <table >
        <tr>
          <th className="orderTitle">{ordersId && <h3>{"帳單編號：" + ordersId.orders_id}</h3>}</th>
          <th></th>
          <th></th>
          {submitBeforeTable()}
        </tr>
        <tr className="tableHeader">
          <th><h2>菜品</h2></th>
          <th><h2>數量</h2></th>
          <th><h2>單價</h2></th>
          <th><h2>小結</h2></th>
          {submitBeforeTable()}
        </tr>
        {orderCartProductData.map((product) => (
          <GetProductForOrderCart
            id={product.id}
            product_name={product.product_name}
            product_id={product.product_id}
            qty={product.qty}
            price={product.price}
            isSubmit={isSubmit}
          />
        ))}
        <tr className="tableHeader">
          <th><h2>套餐</h2></th>
          <th><h2>內容</h2></th>
          <th></th>
          <th><h2>價錢</h2></th>
          {submitBeforeTable()}
        </tr>
        {orderCartDiscountSetData.map((discountSet) => (
          <DiscountSetComponent
            id={discountSet.id}
            discount_set_name={discountSet.discount_set_name}
            discount_set_order_id={discountSet.discount_set_order_id}
            price={discountSet.price}
            isSubmit={isSubmit}
          />
        ))}
        <tr className="totalPriceContainer">
          <td></td>
          <td></td>
          <td ><h2 className="totalPriceName">總價</h2></td>
          <td><p className="totalPrice">{sum()}</p></td>
          {submitBeforeButton()}
        </tr>
      </table>
        {menuPage()}
    </div>
  );
}
