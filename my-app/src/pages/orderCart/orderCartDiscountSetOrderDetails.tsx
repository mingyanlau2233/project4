import { useForm } from "react-hook-form";
import { Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import React, { useEffect } from "react";
import { getOrderCartDiscountSetOrderDetails } from "../../redux/orderCart/action";
import { getOrderCartDiscountSetOrderDetailsThunk, deleteDiscountSetProductThunk } from "../../redux/orderCart/thunks";
import Button from '@mui/material/Button';

//import { } from "../../../redux/orderCart/thunks";



interface OrderCartDiscountSetOrderProps {
  id: number,
  discount_set_name: string
  discount_set_order_id: number,
  price: number,
  isSubmit: boolean,
}



export default function DiscountSetComponent(props: OrderCartDiscountSetOrderProps) {


  const dispatch = useDispatch();
  let discountSetOrderDetailsData = useSelector((state: IRootState) =>
    state.orderCart.GetOrderCartDiscountSetOrderDetails)
  useEffect(() => {

    dispatch(getOrderCartDiscountSetOrderDetailsThunk(props.discount_set_order_id))
  }, [])

  function deleteDiscountSet() {

    dispatch(deleteDiscountSetProductThunk(props.discount_set_order_id))

    setInterval(() => { window.location.reload() }, 100);

  }

  function showButton() {
    if (props.isSubmit == false) {
      return <Button type="submit" color="error" onClick={deleteDiscountSet}>刪除</Button>

    }

  }

  function show() {
    if (props.isSubmit == false) {
      return <>        <td></td>
        <td>{showButton()}</td></>

    }

  }

  return (
    <>


      <tr>
        <td><p className="setName">{props.discount_set_name}</p></td>
        <td>  {discountSetOrderDetailsData.map(details =>
            <p className="detailsProductName">
              {details.product_name}
            </p>)}</td>
        <td></td>
        <td className="fontColorGray">{props.price}</td>
      {show()}
      </tr>
    </>
  );
}