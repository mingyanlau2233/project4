import React from "react";
import { Form, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { loginThunk } from "../../redux/login/thunks";
// import ReactFacebookLogin, {
//   ReactFacebookLoginInfo,
// } from "react-facebook-login";

function LoginPage() {
  const dispatch = useDispatch();
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");

  const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // console.log(123433333444)
    dispatch(loginThunk(username, password));
  };

  // const fBCallback = (
  //   userInfo: ReactFacebookLoginInfo & { accessToken: string }
  // ) => {
  //   if (userInfo.accessToken) {
  //     dispatch(loginFacebookThunk(userInfo.accessToken));
  //   }
  //   return null;
  // };

  return (
    <div>
      <h1>This is Login Page</h1>
      <Form onSubmit={submitHandler}>
        <Form.Group className="mb-3" controlId="formBasicUsername">
          <Form.Label>Username</Form.Label>
          <Form.Control
            type="text"
            placeholder="name"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        {/* <Form.Group>
          <div className="fb-button">
            <ReactFacebookLogin
              appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""}
              autoLoad={false}
              fields="name,email,picture"
              onClick={() => null}
              callback={fBCallback}
            />
          </div>
        </Form.Group> */}

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </div>
  );
}

export default LoginPage;
