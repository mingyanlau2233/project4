import React from 'react'
import './Login.css'
import { useForm } from "react-hook-form";
import Button from '@mui/material/Button';
import { UsernameInput } from './UsernameInput';
import { PasswordInpiut } from './PasswordInput';
import { useDispatch } from 'react-redux';
import { loginThunk } from '../../redux/login/thunks';

let renderCount = 0

interface IFormInput {
    username: string;
    password: string;
}

const defaultValues = {
    username: '',
    password: ''
};

export default function Login() {
    renderCount++;
    const methods = useForm<IFormInput>({ defaultValues: defaultValues });
    const { handleSubmit, reset, control, setValue, watch } = methods;
    const dispatch = useDispatch()
    const onSubmit = (data: IFormInput) => {
        console.log(data);
        dispatch(loginThunk(data.username,data.password));
    }
        
    return (
        <div className="loginContainer">
           <div className="name">點 。 點 。 易</div>
            <div className="smallBox">
            <div className="small">簡單 &nbsp;  &nbsp;&nbsp; 方便  &nbsp;&nbsp; &nbsp;準確 &nbsp; &nbsp; &nbsp;快捷</div>
       
           </div>
          
            <UsernameInput  name="username" control={control}  label="職員名稱"/>
            <PasswordInpiut name="password" control={control} label="密碼"/>
            <Button onClick={handleSubmit(onSubmit)} className="btw">登入</Button>
        </div>
    )
}
