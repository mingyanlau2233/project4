import { Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import React, { useEffect, useState } from "react";
import { getDepartments } from "../../redux/department/thunks";
import { HandleOrdersBoardContainer, DepartmentTitle, DepartmentTitleContainer, DepartmentItem, DepartmentItemContainer, DepartmentItemTitle, DepartmentItemTitleContainer } from './HandleOrders.style'
import { getDiscountSetDepartmentThunk, getOrderDetailsDepartmentThunk, putOrderDetailsStageThunk, putDiscountSetStageThunk } from "../../redux/handleOrders/thunks";
import HandleOrdersProduct from "./handleOrder2(notInUse)"
import "./handleOrder.css"
import webSocket, { Socket } from 'socket.io-client'
import { useForm } from "react-hook-form";

interface HandleOrdersProps {
    department_id: number,
    isSubmit: boolean,

}

interface order_details_idForm {
    id:number

  }

interface discount_set_details_idForm {
    id:number

  }
  const { REACT_APP_SOCKET_IO_URL } = process.env;
export default function HandleOrdersBoard(props: HandleOrdersProps) {
    const [ws, setWs] = useState<Socket | null>(null)
 

    const dispatch = useDispatch();
    const departments = useSelector((state: IRootState) => state.department.department);
    const orderDetails = useSelector((state: IRootState) => state.handleOrders.getOrderDetailsDepartment);
    const discountSetDetails = useSelector((state: IRootState) => state.handleOrders.getDiscountSetDepartment);
    const passStatus = useSelector((state: IRootState) => state.passMessage.orderCartPass);
    const [value, setValue] = useState(1);
    const [id, setOrderDetailsId] = useState(1);
    const [detailsId, setDiscountSetOrderDetailsId] = useState(1);
    const [finishYet, setFinishYet] = useState("");
    const connectWebSocket = () => {
     
        if (REACT_APP_SOCKET_IO_URL){
          //console.log(42353454356643,REACT_APP_SOCKET_IO_URL)
        setWs(webSocket(REACT_APP_SOCKET_IO_URL))
      }
      
    }

    const handleChange = (e: any) => {

        setValue(e.target.value);

        dispatch(getOrderDetailsDepartmentThunk(e.target.value));

        dispatch(getDiscountSetDepartmentThunk(e.target.value));
       

    };

    useEffect(() => {
        if (ws) {
            //連線成功在 console 中打印訊息
            console.log('success connect!')
            //設定監聽
            ws.on('postedOrder', message => {
                dispatch(getOrderDetailsDepartmentThunk(value))
                dispatch(getDiscountSetDepartmentThunk(value))
            })

            ws.on('handledOrder', message => {
                let stage =3
                dispatch(getOrderDetailsDepartmentThunk(value))
                dispatch(getDiscountSetDepartmentThunk(value))
              })
              ws.on('packed', message => {
                let stage =3
                dispatch(getOrderDetailsDepartmentThunk(value))
                dispatch(getDiscountSetDepartmentThunk(value))
              })
        } else {
            connectWebSocket()
        }

    }, [ws])
    
    useEffect(() => {
  
        dispatch(getDepartments())
        dispatch(getOrderDetailsDepartmentThunk(value))
        dispatch(getDiscountSetDepartmentThunk(value))
        setTimeout(() => { dispatch(getOrderDetailsDepartmentThunk(value)) }, 400);
        
    }, [dispatch])


    const filterData = (datas: any[]) => datas.filter((data) =>
        data.department_id == value)

        const { register, handleSubmit, reset } = useForm<order_details_idForm>({

            defaultValues: {
            id:1,
            },
        
          });

          const onSubmit = (data: order_details_idForm) => {
          
           dispatch(putOrderDetailsStageThunk(id, value));
           setTimeout(() => { dispatch(getOrderDetailsDepartmentThunk(value)) }, 400);
        };

    function numOfOrder(){
          let ans = filterData(orderDetails).length+ filterData(discountSetDetails).length
        return ans
    }

    function finished(){
        
      return  <Form.Label className="finishYet">{finishYet}  &nbsp;已完成</Form.Label> 
  }

    return (
        <>
            <div className="width">

                <div className="department">
                    <Form.Label className="department">製作部門：</Form.Label>
                    <Form.Select aria-label="Default select example" className="department" onChange={handleChange} >
                        <option >請選擇所屬部門</option>
                        {departments.map((departments) =>
                            <option value={departments.id}>{departments.name}</option>)}
                            
                    </Form.Select>
                    <Form.Label className="numOfOrder">未處理：{numOfOrder()}張</Form.Label> 
                    {finished()} 
                    
                </div>

                <HandleOrdersBoardContainer className="bigContainer">

                    <DepartmentItemContainer className="divide">
                        {
                            // departments.length
                            orderDetails.length > 0 && filterData(orderDetails).map(getOrderDetailsDepartment => (
                                <DepartmentItem key={getOrderDetailsDepartment.id} className="memo">
                                    <DepartmentItemTitleContainer className="memo">
                                        <DepartmentItemTitle ><div className="orderNo">單號: {getOrderDetailsDepartment.orders_id}&nbsp;&nbsp;&nbsp;數量: {getOrderDetailsDepartment.qty}</div>
                                        
                                            <div className="product">{getOrderDetailsDepartment.product_name}</div>
                                            <Form  onSubmit={handleSubmit(onSubmit)}>
                                            <Button type="submit" onClick={function put(){
                                                setOrderDetailsId(getOrderDetailsDepartment.order_details_id)
                                                setFinishYet(getOrderDetailsDepartment.product_name)
                                            }
                                        }
                                            className="btw">完成</Button>
                                            </Form>
                                        </DepartmentItemTitle>
                                    </DepartmentItemTitleContainer>
                                </DepartmentItem>
                            ))
                        }

                    </DepartmentItemContainer>

                    <DepartmentItemContainer className="divide">
                        {
                            // departments.length
                            discountSetDetails.length > 0 && filterData(discountSetDetails).map(getDiscountSetDepartment => (
                                <DepartmentItem key={getDiscountSetDepartment.id} className="memo2">
                                    <DepartmentItemTitleContainer className="memo">
                                        <DepartmentItemTitle><div className="orderNo">單號: {getDiscountSetDepartment.orders_id}</div>
                                            <div className="orderNo">{getDiscountSetDepartment.discount_set_name} {getDiscountSetDepartment.discount_set_sequence_name}</div>

                                            <div className="product">{getDiscountSetDepartment.product_name}</div>
                                            <form onSubmit={function(e){e.preventDefault();
                                            console.log(detailsId,value)
                                            dispatch(putDiscountSetStageThunk(detailsId, value));
                                            setTimeout(() => {
                                                dispatch(getDiscountSetDepartmentThunk(value))
                                            }, 400);
                                            setFinishYet(getDiscountSetDepartment.product_name)
                                            }}>
                                            <Button type="submit" onClick={() => (setDiscountSetOrderDetailsId(getDiscountSetDepartment.id))} 
                                            className="btw">完成</Button>
                                            </form>
                                        </DepartmentItemTitle>
                                    </DepartmentItemTitleContainer>
                                </DepartmentItem>
                            ))
                        }

                    </DepartmentItemContainer>

                </HandleOrdersBoardContainer>
            </div>
        </>
    );
}


