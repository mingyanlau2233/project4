import styled from 'styled-components';

export const HandleOrdersBoardContainer = styled.div`
   
    padding:20px;
     width:100vw
  
	
`

export const DepartmentTitleContainer = styled.div`
    display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: flex-start;
	align-items: flex-start;
	align-content: flex-start;
    w
}
`

export const DepartmentTitle = styled.h1`
    
`
export const DepartmentItemContainer = styled.div`
    display:flex;
    flex-wrap: wrap;
    width:45vw
`

export const DepartmentItemTitleContainer = styled.div`
    
`
export const DepartmentItem = styled.div`
width:230px
height:130px
margin:15px 15px;15px ;15px
padding: 15px;
border-radius:10px;
cursor: pointer;
-webkit-box-shadow: 0px 0px 15px -10px #000000; 
box-shadow: 0px 0px 15px -10px #000000;
display:flex;

justify-content: flex;
`

export const DepartmentItemTitle = styled.h1`
    
`

export const DepartmentItmeSub = styled.span`
font-size: 14px;
color:gray;
`

export const DepartmentItemBtnContainer = styled.div`
    display:flex;
    align-items:center;
`