import { Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import React, { useEffect, useState } from "react";
import { getDepartments } from "../../redux/department/thunks";
import { HandleOrdersBoardContainer, DepartmentTitle, DepartmentTitleContainer, DepartmentItem, DepartmentItemContainer, DepartmentItemTitle, DepartmentItemTitleContainer } from './HandleOrders.style'
import { getDiscountSetDepartmentThunk, getOrderDetailsDepartmentThunk, putOrderDetailsStageThunk, putDiscountSetStageThunk } from "../../redux/handleOrders/thunks";
import "./HandleOrders.css"


interface ProductProps {
    department_id: number,
    orders_id:number,
    qty:number,
    product_name:string,
    order_details_id:number
   
}

export default function HandleOrdersProduct(props:ProductProps) {

    const dispatch = useDispatch();
   
    const orderDetails = useSelector((state: IRootState) => state.handleOrders.getOrderDetailsDepartment);
 
    useEffect(() => {
        dispatch(getOrderDetailsDepartmentThunk(props.department_id))
        dispatch(getDiscountSetDepartmentThunk(props.department_id));
    }, [])

    function finishOrder() {
        
        if (orderDetails[0]) {
            console.log("dsgcdsku",props.order_details_id)
            dispatch(putOrderDetailsStageThunk(props.order_details_id,props.department_id));
            
           
           setInterval(() => { dispatch(getOrderDetailsDepartmentThunk(props.department_id))
            dispatch(getDiscountSetDepartmentThunk(props.department_id)) }, 200);
        }
    
    }

    // const filterData = (datas: any[]) => datas.filter((data) =>
    // data.department_id == props.department_id)

    return (
         <>

            <DepartmentItemContainer>
                
                    
                        <DepartmentItem >
                            <DepartmentItemTitleContainer>
                                <DepartmentItemTitle>單號: {props.orders_id}
                                    <div>菜品: {props.product_name}</div>
                                    <div>數量: {props.qty}</div>
                                    <div>訂單狀態: 已入單</div>
                                    <Button type="submit" onClick={finishOrder}>已製作</Button>
                                </DepartmentItemTitle>
                            </DepartmentItemTitleContainer>
                        </DepartmentItem>
                    
                
            </DepartmentItemContainer>
            </>

    );
}