
import { UserContainer } from './Users.style'
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
// import { userRows } from '../../dummyDate';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { deleteStaff, getAllStaffThunk } from '../../redux/staff/thunks';
import { EditUserTitle, EditUserTitleContainer } from '../editUser/EditUser.style';
import BasicModal from '../../components/modal/Modal';
import StaffModal from '../../components/modal/StaffModal';


export default function User() {
    const staffs = useSelector((state: IRootState) => state.staff.staff)
    const dispatch = useDispatch();
    console.log(staffs)
    useEffect(() => {



        dispatch(getAllStaffThunk())
        console.log(12234345435, staffs)
    }
        , [dispatch])
    // console.log(staffs.map(staff=>staff.id))
    // const [data,setData] = useState(userRows)
    const handleDelete = (id: number) => {
        // setData(data.filter((item:any) => item.id !== id))
        dispatch(deleteStaff(id))
    }
    const columns: GridColDef[] = [
        { field: 'id', headerName: 'ID', width: 200 },
        { field: 'name', headerName: 'Name', width: 200 },
        {
            field: 'is_onboard',
            headerName: 'Active',
            width: 200,
        },
        {
            field: 'position',
            headerName: '職位',
            width: 200,
        },
        {
            field: 'created_at',
            headerName: '入職日期',
            width: 200,
        },
        {
            field: 'action',
            headerName: 'Actions',
            width: 250,
            renderCell: (params: any) => {
                return (
                    <Stack direction="row" spacing={3}>
                        <Button component={Link} to={'/user/' + params.id} variant="contained" color="success">
                            修改
                        </Button>
                        <Button variant="outlined" color="error" onClick={() => handleDelete(params.id)}>
                            刪除
                        </Button>
                    </Stack>
                )
            }
        }

    ];
    return (
        <UserContainer>
            <EditUserTitleContainer>
                <EditUserTitle>員工</EditUserTitle>
                <StaffModal />
            </EditUserTitleContainer>
            <DataGrid
                rows={staffs}
                disableSelectionOnClick
                columns={columns}
                pageSize={8}
                rowsPerPageOptions={[5]}
                checkboxSelection
            />
        </UserContainer>
    )
}
