import FeaturedInfo from "../../../components/interface/featuredInfo/FeaturedInfo";
import { HomeContainer } from "./Home.style";


export default function Home() {
    return (
        <HomeContainer>
        
            <FeaturedInfo/>
        </HomeContainer>
    )
}
