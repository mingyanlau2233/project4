import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Container, Card, Button } from 'react-bootstrap';
import { IRootState } from "../../redux/store";
import { postSeatThunk, deleteDeskThunk, getSeatThunk } from '../../redux/editDesk/thunks';
import './EditDesk.css';
import { SeatState } from '../../redux/editDesk/state'

export interface SeatProps {
    desk_id: number
    sequence: number
    seats: SeatState[] | null
}

export default function Desks(props: SeatProps) {
    const dispatch = useDispatch();
    console.log(props.desk_id)
    let seatData = useSelector((state: IRootState) => state.editDesk.desks)
    // useEffect(() => {
    //     dispatch(getSeatThunk());
    // }, [dispatch])

    function addSeat() {
        dispatch(postSeatThunk(props.desk_id))
    }

    function deleteDesk() {
        dispatch(deleteDeskThunk(props.desk_id))
    }
    function deleteSeat(){

    }
    return (
        <div>
            Desk sequence : {props.sequence}
            {
                props.seats !== null && <Container className="deskContainer" fluid>
                    {props.seats.length > 0 && props.seats.map((seat: SeatState) => (
                        <>
                        <div>{seat.sequence}</div>
                     
                        </>
                    ))
                    }
                    <Button className="seatButton" variant="primary" type="submit" onClick={addSeat}>加位</Button>
                    <Button className="seatButton" variant="primary" type="submit" onClick={deleteDesk}>刪Desk</Button>
                    
                </Container>
            }

            {
                 props.seats === null && <Container className="deskContainer" fluid>
                 <Button className="seatButton" variant="primary" type="submit" onClick={addSeat}>加位</Button>
                 <Button className="seatButton" variant="primary" type="submit" onClick={deleteDesk}>刪位</Button>
                </Container>
            }
        </div>
    )
}