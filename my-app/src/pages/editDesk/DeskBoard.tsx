import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Container, Card, Button } from 'react-bootstrap';
import { IRootState } from "../../redux/store";
import { getDeskThunk, postDeskThunk, deleteAllDeskThunk } from '../../redux/editDesk/thunks'
import './EditDesk.css';
import Desks from './Desks'
import { DeskState } from "../../redux/editDesk/state"
interface DeskProps {
    id: number;
    sequence: number;
}


export default function DeskBoard(props: DeskProps) {
    const dispatch = useDispatch();

    let deskData = useSelector((state: IRootState) => state.editDesk.desks)
    useEffect(() => {
        dispatch(getDeskThunk());

    }, [])

    function addDesk() {
        console.log("addDesk")
        dispatch(postDeskThunk())
    }

    function deleteAllDesk() {
        dispatch(deleteAllDeskThunk());
    }
    return (
        <div className="blockContainer">
            <Button className="button" variant="primary" type="submit" onClick={addDesk}>加枱</Button>
            <Button className="button" variant="primary" type="submit" onClick={deleteAllDesk}>刪枱All</Button>
            <div>
                {deskData.length > 0 && deskData.map((desk: DeskState) => (
                    // Array.isArray(desk.seats) && <Desks key={desk.id} desk_id={desk.id} seats={desk.seats} />
                    <Desks key={desk.id} sequence={desk.sequence} desk_id={desk.id} seats={desk.seats} />
                ))}
            </div>
        </div>

    )
}


