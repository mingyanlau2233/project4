import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table } from "react-bootstrap"
import { useForm } from "react-hook-form";
import { IRootState } from "../../redux/store";

import { AssignContainer } from "../../components/product/product.style";
import React from "react";
import { changeStageDiscountSetOrderDetailsWaiterThunk ,getDiscountSetOrderDetailsWaiterThunk} from "../../redux/waiter/thunks";

interface DiscountSetOrderDetails {
    id:number,
    
    discount_set_sequence_name:string,
    product_name:string,
    stage:number,
    created_at:number,
    updated_at:number,
   
    
}
export default function DiscountSetOrderDetailsComponent(Props:DiscountSetOrderDetails) {

    const dispatch = useDispatch();
    let  [stage,setStage]=React.useState(whichStage(Props.stage));

    function whichStage(stage:number){
      if(stage==1){
        return "製作中"
      }
      if(stage==2){
        return "已完成"
      }
      if(stage==3){
        return "已上菜"
      }
    }

    function changeStageOfDiscountSetOrderDetail(){
        // console.log("checkStage",Props.id)
         dispatch( changeStageDiscountSetOrderDetailsWaiterThunk(Props.id) )
         setTimeout(()=> dispatch(getDiscountSetOrderDetailsWaiterThunk(4)), 300);
       
       
    }

    function packingButton(){
      if(Props.stage<3){
        return <button  className="btw-packing" type="submit" onClick={changeStageOfDiscountSetOrderDetail}>已包裝</button>
      }
    }

    function containerColour(){
      if(Props.stage <=1){
        return "cooking"}
      if(Props.stage ==2){
      return "containerCooked"}
      if(Props.stage ==3){
        return "containerFinish"}
    }

   return (

    <>
       <tr className={containerColour()}>
        
        <td >{Props.discount_set_sequence_name}</td>
        <td >{Props.product_name}</td>
        <td className="stage">{stage}</td>
        <td>{packingButton()}</td>
        
        
       </tr>
      </>
    )
  }