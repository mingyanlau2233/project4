import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table } from "react-bootstrap"
import { useForm } from "react-hook-form";
import { IRootState } from "../../redux/store";
import { getOrderCartDiscountThunk, getOrderCartProductThunk ,submitOrderThunk} from "../../redux/orderCart/thunks";
import ProductModal from "../customerOrder/CustomerOrderProduct.Modal";
import { AssignContainerColumn , AssignContainer } from "../../components/product/product.style";
import React from "react";
import { getOrderDetailsForDiscountSetOrderWaiterThunk, getOrderDetailsForProductThunk,    } from "../../redux/waiter/thunks";
import OrdersDetailsForProductComponent from "./OrderDetailsForproduct";
import OrdersDetailsForDiscountSetComponent from "./orderDetailsForDiscountSet";
import webSocket, { Socket } from 'socket.io-client'
import { ContactSupportOutlined } from "@material-ui/icons";
import { ConnectedTvOutlined } from "@mui/icons-material";
interface Orders {
    id:number,
    session_id:number,
    desk_sequence:number,
    seat_sequence:number,
  
    
}
const { REACT_APP_SOCKET_IO_URL } = process.env;
export default function OrdersComponent(Props:Orders) {
  const [ws, setWs] = useState<Socket | null>(null)
   
    const dispatch = useDispatch();

    const ordersDetailsForProduct =useSelector((state:IRootState)=>state.order.getOrderDetailsForProductFromDb)
    const  ordersDetailsForDiscountSet =useSelector((state:IRootState)=>state.order.getOrderDetailsForDiscountSetWaiterFromDb)
 
    const connectWebSocket = () => {
     
      if (REACT_APP_SOCKET_IO_URL){
        
      setWs(webSocket(REACT_APP_SOCKET_IO_URL))
    }
    
  }

  useEffect(() => {
   
    if (ws) {
        //連線成功在 console 中打印訊息
        console.log('success connect!')
        //設定監聽
        ws.on('postedOrder', message => {
          console.log(message)
          let stage =4
          dispatch(getOrderDetailsForProductThunk(stage))
          dispatch(getOrderDetailsForDiscountSetOrderWaiterThunk(stage))
        })

        ws.on('handledOrder', message => {
          let stage =4
          console.log(message)
          dispatch(getOrderDetailsForProductThunk(stage))
          dispatch(getOrderDetailsForDiscountSetOrderWaiterThunk(stage))
        })

        ws.on('packed', message => {
          let stage =4
          console.log(message)
          dispatch(getOrderDetailsForProductThunk(stage))
          dispatch(getOrderDetailsForDiscountSetOrderWaiterThunk(stage))
        })
    } else {
        connectWebSocket()
    }

}, [ws])

    useEffect(() => {
       let stage =4
      dispatch(getOrderDetailsForProductThunk(stage))
      dispatch(getOrderDetailsForDiscountSetOrderWaiterThunk(stage))
      },[])
  
      const filterData = (datas:any[])=> datas.filter((data) => data.orders_id == Props.id)
   
   return (

    <>
     
       <div className="orderContainer">
        <div className="center ">單號：{Props.id}</div>

        
        
        
        {filterData(ordersDetailsForProduct).map((orderDetails)=>
            
            <table className="customers">
                 <OrdersDetailsForProductComponent id={orderDetails.id}   qty={orderDetails.qty}
                   price={orderDetails.price} stage={orderDetails.stage} created_at={orderDetails.created_at}
                   updated_at={orderDetails.updated_at} product_name={orderDetails.product_name}/>  
               </table> 
          )}

        {filterData(ordersDetailsForDiscountSet).map((orderDetails)=>
            
            <table className="customers setBorder">
            <OrdersDetailsForDiscountSetComponent id={orderDetails.id}   discount_set_order_id={orderDetails.discount_set_order_id}
              price={orderDetails.price} stage={orderDetails.stage} created_at={orderDetails.created_at}
              updated_at={orderDetails.updated_at} discount_set_name={orderDetails.discount_set_name}/>  
            </table> 
          )}
         </div>
        
        </>
    )
  }