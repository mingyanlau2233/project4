import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table } from "react-bootstrap"
import { useForm } from "react-hook-form";
import { IRootState } from "../../redux/store";

import { AssignContainer } from "../../components/product/product.style";
import React from "react";
import { changeStageOrderCartWaiterThunk,getDiscountSetOrderDetailsWaiterThunk,getOrderDetailsForProductThunk,getOrderDetailsForDiscountSetOrderWaiterThunk } from "../../redux/waiter/thunks";
import "./waiter.css"



interface OrdersDetailsForProduct {
    id:number,
    qty:number,
    price:number,
   
     stage:number,
     created_at:number,
     updated_at:number,
     product_name:string,
    
}
export default function OrdersDetailsForProductComponent(Props:OrdersDetailsForProduct) {

    const dispatch = useDispatch();

    let  [stage,setStage]=React.useState(whichStage(Props.stage));

    function whichStage(stage:number){
      if(stage==1){
        return "製作中"
      }
      if(stage==2){
        return "已完成"
      }
      if(stage==3){
        return "已包裝"
      }
    }

 
  
    function changeStageOfOrderDetail(){
        console.log("checkStage",Props.id)
        dispatch( changeStageOrderCartWaiterThunk(Props.id) )
        
          setTimeout(()=> dispatch(getDiscountSetOrderDetailsWaiterThunk(4)), 300);
       // setInterval(()=> {window.location.reload()}, 100);
    }

    function packingButton(){
      if(Props.stage<3){
        return <button  className="btw-packing" type="submit" onClick={changeStageOfOrderDetail}>已包裝</button>
      }
    }

    function containerColour(){
      if(Props.stage ==1){
        return "cooking"}
      if(Props.stage ==2){
      return "containerCooked"}
      if(Props.stage ==3){
        return "containerFinish"}
    }

   return (

    <>
     <tr className={containerColour()} >
        <td>{Props.product_name}</td>
        <td >{Props.qty}</td>
        <td className="stage">{whichStage(Props.stage)}</td>
        <td>{packingButton()}</td>
        </tr>
      </>
    )
  }