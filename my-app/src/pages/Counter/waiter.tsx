import { useEffect ,useState} from "react";
import { useSelector, useDispatch} from "react-redux";
import { Table } from "react-bootstrap"
import "./waiter.css"
import { IRootState } from "../../redux/store";
import { getOrderCartDiscountThunk, getOrderCartProductThunk ,submitOrderThunk} from "../../redux/orderCart/thunks";
import ProductModal from "../customerOrder/CustomerOrderProduct.Modal";
import { AssignContainer } from "../../components/product/product.style";
import React from "react";
import { getOrdersThunk } from "../../redux/waiter/thunks";
import OrdersComponent from "./ordersComponent";
import webSocket, { Socket } from 'socket.io-client'


const { REACT_APP_SOCKET_IO_URL } = process.env;

export default function  WaiterMainPage() {
  const [ws, setWs] = useState<Socket | null>(null)
    const dispatch = useDispatch();

    const ordersData =useSelector((state:IRootState)=>state.order.getOrdersFromDb)
    
    const connectWebSocket = () => {
     
      if (REACT_APP_SOCKET_IO_URL){
        //console.log(42353454356643,REACT_APP_SOCKET_IO_URL)
      setWs(webSocket(REACT_APP_SOCKET_IO_URL))
    }
    
  }
 
  useEffect(() => {
    if (ws) {
        //連線成功在 console 中打印訊息
       
        //設定監聽
        ws.on('postedOrder', message => {
          let stage =3
          dispatch(getOrdersThunk(stage))
        })
        ws.on('handledOrder', message => {
          let stage =3
         
          dispatch(getOrdersThunk(stage))
        })
        ws.on('packed', message => {
          let stage =3
          
          dispatch(getOrdersThunk(stage))
        })
    } else {
        connectWebSocket()
    }
  
}, [ws])

    useEffect(() => {
      console.log(123)
      let stage =3
       dispatch(getOrdersThunk(stage))
      },[dispatch])
  
  function numOfOrder(){
    return  ordersData.length
  }

   return (

    
        <>
        <div className="big">
        <div className="numOfOrder"> 未處理： {numOfOrder()}張</div>
        <div className="bigContainer">
           
         <div className="countContainer">
         {ordersData.map((order)=>
             <div>
            
                 <OrdersComponent id={order.id}   session_id={order.session_id}
                   desk_sequence={order.desk_sequence} seat_sequence={order.seat_sequence} />  
                 </div>
          )}
          </div>
          </div>
          </div>
        </>
        
    )
  }