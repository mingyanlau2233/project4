import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table } from "react-bootstrap"
import { useForm } from "react-hook-form";
import { IRootState } from "../../redux/store";

import { AssignContainer } from "../../components/product/product.style";
import React from "react";
import { getDiscountSetOrderDetailsWaiterThunk} from "../../redux/waiter/thunks";
import DetailsForDiscountSetComponent from "./discountSetOrderDetailComponent"
import webSocket, { Socket } from 'socket.io-client'
interface OrdersDetailsForProduct {
    id:number,
    price:number,
   
     stage:number,
     created_at:number,
     updated_at:number,
     discount_set_name:string,
     discount_set_order_id:string,
     
    
}
const { REACT_APP_SOCKET_IO_URL } = process.env;
export default function OrdersDetailsForDiscountSetComponent(Props:OrdersDetailsForProduct) {
  const [ws, setWs] = useState<Socket | null>(null)
    const dispatch = useDispatch();
    const  discountSetOrderDetails =useSelector((state:IRootState)=>state.order.getDiscountSetOrderDetailsWaiter)
        let  [stage,setStage]=React.useState(whichStage(Props.stage));

        const connectWebSocket = () => {
     
          if (REACT_APP_SOCKET_IO_URL){
            
          setWs(webSocket(REACT_APP_SOCKET_IO_URL))
        }
        
      }
    function whichStage(stage:number){
      if(stage==1){
        return "製作中"
      }
      if(stage==2){
        return "已完成"
      }
      if(stage==3){
        return "已上菜"
      }
    }
    useEffect(() => {
      if (ws) {
          //連線成功在 console 中打印訊息
          console.log('success connect!')
          //設定監聽
          ws.on('postedOrder', message => {
            console.log(message)
            let stage =4
            dispatch(getDiscountSetOrderDetailsWaiterThunk(stage))
          })
  
          ws.on('handledOrder', message => {
            let stage =4
            console.log(message)
            dispatch(getDiscountSetOrderDetailsWaiterThunk(stage))
          })
          ws.on('packed', message => {
            let stage =4
            console.log(message)
            dispatch(getDiscountSetOrderDetailsWaiterThunk(stage))
          })
      } else {
          connectWebSocket()
      }
  
  }, [ws])


    useEffect(() => {
      let stage =4
      dispatch(getDiscountSetOrderDetailsWaiterThunk(stage))
       // dispatch()
        },[])


        const filterData = (datas:any[])=> datas.filter((data) => 
        data.discount_set_order_id == Props.discount_set_order_id)

       
   return (

    <>
        
        <tr ><td className="center discountSetName">{Props.discount_set_name}</td></tr>
       
        {filterData(discountSetOrderDetails).map((details)=>
            
           
            <DetailsForDiscountSetComponent id={details.id}   discount_set_sequence_name={details.discount_set_sequence_name}
            product_name={details.product_name} stage={details.stage} created_at ={details.created_at}
            updated_at={details.updated_at}/>  
           
          )}
        
      </>
    )
  }