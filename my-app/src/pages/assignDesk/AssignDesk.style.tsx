import styled from 'styled-components';

export const AssignDeskContainer = styled.div`
    flex:6;
    padding:20px;
`

export const AssignDeskTitleContainer = styled.div`
    display:flex;
    flex-direction: row;
    align-items:center;
    justify-content:space-between;
`

export const AssignDeskTitle = styled.h1`
    
`

export const AssignDeskDiv = styled.div`
    display:flex;
    margin-top:20px;
    width: 100%;
    height: 90%;
    -webkit-box-shadow: 0px 0px 15px -10px #000000; 
    box-shadow: 0px 0px 15px -10px #000000;
`

export const AreaBoard = styled.div`
    display:flex;
    flex: 3;
    margin-top:20px;
    width: 100%;
    height: 98%;
   
`

export const DeskShow = styled.div`
    display: flex;
    flex-wrap: wrap !important;
    justify-content: space-around;
    margin:0 ;
    padding: 30px;
    border-radius:10px;
    cursor: pointer;
`

export const DeskBox = styled.div`
    display: flex;
    flex-wrap: wrap !important;
    flex: 1;
    justify-content: space-around;
    margin: 10px;
    width: 200px;
    height: 200px;
    border: solid 2px;
    border-color: lightgrey;
    border-radius: 15px;
    padding-top: 10px;
    text-align: center;
`

export const SummaryContainer = styled.div`
    display:flex;
    flex: 1;
    flex-direction: column;
    align-items:center;
    justify-content: space-around;
    width: auto;
    height: auto;
    margin-top: 68px;
    margin-bottom: 60px;
    margin-right: 20px;
    border-top: solid lightgrey 1px;
`

export const DeskNum = styled.p`
`

export const QRCode = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    width: 200px;
    height: 200px;
`
export const PrintCode = styled.div`
    display: flex;
    justify-content: center;
    width: 150px;
    height: 50px;
    border: solid 2px lightgrey;
    border-radius: 15px;
    margin-bottom: 200px;
`