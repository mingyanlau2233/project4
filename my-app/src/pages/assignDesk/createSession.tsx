import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Container, Card, Button } from 'react-bootstrap';
import { IRootState } from "../../redux/store";
import { getDeskThunk, getSeatingThunk, postSessionThunk } from '../../redux/editDesk/thunks'
import "./AssignDesk.css"
import  QRCodeGenerate from "./QRCodeGenerate"
interface DeskProps {
    id: number;
    sequence: number;
}


export default function CreateSession() {
    const dispatch = useDispatch();

    let deskData = useSelector((state: IRootState) => state.editDesk.desks)
    //console.log(deskData)
    const [seat,setSeats] =useState([0]);
    useEffect(() => {
        console.log(123)
        dispatch(getSeatingThunk());
       
    }, [dispatch])
    
    function seatId(seat_id:number,is_occupied:boolean){
        console.log("seat_id",seat_id)
        if(is_occupied==false){
        setSeats(seat.concat(seat_id))
        }
      }
    const clearState=()=>{setSeats([0])}

    function createCustomer(){
        let seatData:any= seat.slice(1).sort((a,b)=>a-b)
        console.log(seatData)
        if (seatData){
        dispatch(postSessionThunk(seatData))
        }
        clearState()
    }
    function whichClass(is_occupied:boolean){
        if(is_occupied==false){
            return "free"
        }
        if(is_occupied==true){
            return "occupied"
        }

    }
    return (
        <>
        <div className="blockContainer">
         {deskData.map((desk) =>
                 <>
                <div >枱號{desk.sequence}
                {desk.seats.map((i)=> <button className={whichClass(i.is_occupied)}
                onClick={() => seatId(i.id,i.is_occupied)}>{i.sequence}</button>)}
                </div>   
                </>)}

                <button onClick={createCustomer} > 列印QR-CODE</button>
                <QRCodeGenerate/>
        </div>
         
         </>
    )
}