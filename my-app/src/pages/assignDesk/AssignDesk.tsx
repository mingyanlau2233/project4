import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getExistingAreaThunk } from '../../redux/assignDesk/thunks';
import { IRootState } from '../../redux/store';
import {
    AssignDeskContainer,
    AssignDeskDiv,
    AssignDeskTitle,
    AssignDeskTitleContainer,
    SummaryContainer,
    AreaBoard,
    DeskShow,
    DeskBox,
    QRCode,
    PrintCode,
    DeskNum
} from './AssignDesk.style'
import Button from '@mui/material/Button';
import './AssignDesk.css'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import QRCodeGenerate from './QRCodeGenerate';


function createRow(area: string, desk: number, seat: Array<string>) {
  return { area, desk, seat };
}

interface Row {
  area: string;
  desk: number;
  seat: Array<number>
}

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
  }

  function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  function a11yProps(index: number) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

function totalCustomer(total: number) {
  return { total };
}

function remainingSeat(remaining: number) {
    return { remaining };
}

const rows = [
  createRow('A', 1, ['5, 3, 4']),
  createRow('B', 4, ['2, 5, 6']),
  createRow('C', 2, ['1, 3']),
];


export default function AssignDesk() {
    // const dispatch = useDispatch()
    // const isLoading = useSelector(
    //     (state: IRootState) => state.assignDesk.isLoading
    // );
    // const areaFromDB = useSelector(
    //     (state: IRootState) => state.assignDesk.areaFromDB
    // );
    // const deskFromDB = useSelector(
    //     (state: IRootState) => state.assignDesk.deskFromDB
    // );
    // const seatFromDB = useSelector(
    //     (state: IRootState) => state.assignDesk.seatFromDB
    // );
    // const sessionFromDB = useSelector(
    //     (state: IRootState) => state.assignDesk.sessionFromDB
    // );

    // useEffect(() => {
    //     dispatch(getExistingAreaThunk())
    // }, [dispatch]);

    // if (isLoading) {
    //     return <h1>Loading...</h1>;
    // }
    
    const [emptySeat, selectSeat] = React.useState(() => ['']);

    const handleStatus = (
        event: React.MouseEvent<HTMLElement>,
        seat: string[],
      ) => {
        selectSeat(seat);
      };

    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
        };
   

    return (
        <AssignDeskContainer>
            <AssignDeskTitleContainer>
                <AssignDeskTitle>座位分配</AssignDeskTitle>
            </AssignDeskTitleContainer>
            <AssignDeskDiv>
                <AreaBoard>
                <Box sx={{ width: '100%' }}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                            <Tab label="Item One" {...a11yProps(0)} />
                            <Tab label="Item Two" {...a11yProps(1)} />
                            <Tab label="Item Three" {...a11yProps(2)} />
                        </Tabs>
                    </Box>
                    <TabPanel value={value} index={0}>
                    <DeskShow>
                        <DeskBox>
                            1
                            <ToggleButtonGroup
                            className="toggleButtonGroup"
                            color="primary"
                            value={emptySeat}
                            onChange={handleStatus}
                            >
                            <ToggleButton className="toggleButton" value="1.1" >
                            1
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="1.2">
                            2
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="1.3">
                            3
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="1.4">
                            4
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="1.5">
                            5
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="1.6">
                            6
                            </ToggleButton>
                            </ToggleButtonGroup>
                        </DeskBox>
                        <DeskBox> 
                            2
                            <ToggleButtonGroup
                            className="toggleButtonGroup"
                            color="primary"
                            value={emptySeat}
                            onChange={handleStatus}
                            >
                            <ToggleButton className="toggleButton" value="2.1">
                            1
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="2.2">
                            2
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="2.3">
                            3
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="2.4">
                            4
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="2.5">
                            5
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="2.6">
                            6
                            </ToggleButton>
                            </ToggleButtonGroup>
                        </DeskBox>
                        <DeskBox>
                            3
                            <ToggleButtonGroup
                            className="toggleButtonGroup"
                            color="primary"
                            value={emptySeat}
                            onChange={handleStatus}
                            >
                            <ToggleButton className="toggleButton" value="3.1">
                            1
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="3.2">
                            2
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="3.3">
                            3
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="3.4">
                            4
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="3.5">
                            5
                            </ToggleButton>
                            <ToggleButton className="toggleButton" value="3.6">
                            6
                            </ToggleButton>
                            </ToggleButtonGroup>
                        </DeskBox>
                    </DeskShow>
                
                    
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        Item Two
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                        Item Three
                    </TabPanel>
                    </Box>
                </AreaBoard>
                <SummaryContainer>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 400 }} aria-label="spanning table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center" colSpan={24}> 座位使用概況 </TableCell>
                                    </TableRow>
                                        <TableRow>
                                            <TableCell align="right" colSpan={8}>客人數目: </TableCell>
                                            <TableCell align="left" colSpan={4}>10</TableCell>
                                            <TableCell align="right" colSpan={5}>剩餘座位:</TableCell>
                                            <TableCell align="left" colSpan={7}>10</TableCell>
                                        </TableRow>
                                    <TableRow>
                                        <TableCell align="center" colSpan={8}>區</TableCell>
                                        <TableCell align="center" colSpan={6}>枱號</TableCell>
                                        <TableCell align="center" colSpan={10}>座位</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.map((row) => (
                                    <TableRow key={row.area}>
                                        <TableCell align="center" colSpan={8}>{row.area}</TableCell>
                                        <TableCell align="center" colSpan={6}>{row.desk}</TableCell>
                                        <TableCell align="center" colSpan={10}>{row.seat}</TableCell>
                                    </TableRow>
                                    ))}
                                </TableBody>
                                
                            </Table>
                        </TableContainer>
                        <QRCodeGenerate text="sdfsdfsas" />  
                        <Button variant="contained">列印 QR-Code</Button>
                    </SummaryContainer>
            </AssignDeskDiv> 
        </AssignDeskContainer>
    )
}
