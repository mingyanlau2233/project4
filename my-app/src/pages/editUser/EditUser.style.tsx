import styled from 'styled-components';

export const EditUserContainer = styled.div`
    flex:6;
    padding:20px;
`

export const EditUserTitleContainer = styled.div`
    display:flex;
    align-items:center;
    justify-content:space-between;
`

export const EditUserTitle = styled.h1`
    
`

export const EditUserDiv = styled.div`
    display:flex;
    margin-top:20px;

`
export const UserShow = styled.div`
    flex:1;
    margin:0 ;
    padding: 30px;
    border-radius:10px;
    cursor: pointer;
    -webkit-box-shadow: 0px 0px 15px -10px #000000; 
    box-shadow: 0px 0px 15px -10px #000000;
`

export const UserShowTop = styled.div`
    display:flex;
    align-items:center;
`

export const UserShowToptitle = styled.span`
    display:flex;
    flex-direction:column;
    margin-left:20px;
`

export const UserShowUsername = styled.span`
    font-weight:600;
`

export const UserShowPosition = styled.span`
    font-weight:300;
`


export const UserShowBottom = styled.div`
    margin-top: 10px;
`

export const UserUpdate = styled.div`
    flex:2;
    margin:0;  
    margin-left:20px;
    padding: 30px;
    border-radius:10px;
    cursor: pointer;
    -webkit-box-shadow: 0px 0px 15px -10px #000000; 
    box-shadow: 0px 0px 15px -10px #000000;
`

export const UserShowBottomTitle = styled.span`
    font-size: 14px;
    font-weigth:600;
    color: rgb(175,170,170);
`

export const UserShowInfo = styled.div`
    display:flex;
    align-items:center;
    margin:20px 0px;
    color: #444;

`


export const UserShowInfoTitle = styled.span`

`

export const UserUpdateTitle = styled.span`
font-size: 24px;
font-weight: 600;
`

export const UserUpdateForm = styled.form`
    display:flex;
    justify-content: space-between;
    margin-top:20px;
`

export const UserUpdateLeft = styled.div`

`

export const UesrUpdateRight = styled.div`
    display:flex;
    flex-direction: column;
    justify-content: space-between;
    
`

export const UserUpdateItem = styled.div`
    display:flex;
    flex-direction: column;
    margin-top:10px;
`

export const UserUpdateUpload = styled.div``

