import React, { useEffect } from 'react'
import {
    EditUserContainer,
    EditUserDiv,
    EditUserTitle,
    EditUserTitleContainer,
    UesrUpdateRight,
    UserShow,
    UserShowBottom,
    UserShowBottomTitle,
    UserShowInfo,
    UserShowInfoTitle,
    UserShowPosition,
    UserShowTop,
    UserShowToptitle,
    UserShowUsername,
    UserUpdate,
    UserUpdateForm,
    UserUpdateItem,
    UserUpdateLeft,
    UserUpdateTitle,
    UserUpdateUpload
} from './EditUser.style'
import Button from '@mui/material/Button';
import './EditUser.css'
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import PhoneIcon from '@mui/icons-material/Phone';
import EmailIcon from '@mui/icons-material/Email';
import HomeIcon from '@mui/icons-material/Home';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import { Link, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store'
import { getAllStaffThunk, putStaffThunk } from '../../redux/staff/thunks';
import { FormInputDropdownPosition } from './FormInputDropdownForPosition';
import { FormProvider, useForm } from 'react-hook-form';
import { FormInputText } from '../../components/form/FormInputText';
import { FormInputDropdownIsOnBoard } from '../../components/form/FormInputDropdownForOnBoard';

interface IFormInput {
    // department: string;
    // textValue: string;
    // radioValue: string;
    // checkboxValue: string[];
    // dateValue: Date;
    // dropdownValue: string;
    // sliderValue: number;
    name: string;
    password: string;
    position: string;
    is_onboard: boolean
}
const defaultValues = {
    // department: "",
    // textValue: "",
    // radioValue: "",
    // checkboxValue: [],
    // dateValue: new Date(),
    // dropdownValue: "",
    // sliderValue: 0,
    name: "",
    password: "",
    position: "管理",
    is_onboard: true,
};


export default function EditUser() {
    const { userId } = useParams<{ userId: string }>()
    const staffs = useSelector((state: IRootState) => state.staff.staff)
    const staff = staffs.filter((i) => i.id === parseInt(userId))[0]
    const id = parseInt(userId)
    
    console.log(staff)
    // console.log(staffs)
    // console.log(id)

    const methods = useForm<IFormInput>({ defaultValues: defaultValues });
    const { handleSubmit, reset, control, setValue, watch } = methods;
    const dispatch = useDispatch();
    const onSubmit = (data:IFormInput)=>{
        console.log(data)
        dispatch(putStaffThunk(data, id ))
    }

    useEffect(() => {
        dispatch(getAllStaffThunk())
    }, [])

    return (
        <EditUserContainer>

            {/* {staffs.map(staff=>(<> */}
            {staff && <EditUserDiv>
                <UserShow>
                    <UserShowTop>
                        <img
                            src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png"
                            className='userShowImg'
                        />
                        <UserShowToptitle>
                            <UserShowUsername>
                                {staff.name}
                            </UserShowUsername>
                            <UserShowPosition>
                                {staff.position}
                            </UserShowPosition>
                        </UserShowToptitle>
                    </UserShowTop>
                    <UserShowBottom>
                        <UserShowBottomTitle>
                            Account Details
                        </UserShowBottomTitle>
                        <UserShowInfo>
                            <CalendarTodayIcon className="userShowInfoIcon" />
                            <UserShowInfoTitle>
                                {staff.created_at.toString().slice(0, 10)}
                            </UserShowInfoTitle>
                        </UserShowInfo>
                        <UserShowBottomTitle>
                            Contant Details
                        </UserShowBottomTitle>
                        <UserShowInfo>
                            <PhoneIcon className="userShowInfoIcon" />
                            <UserShowInfoTitle>
                                +85297256400
                            </UserShowInfoTitle>
                        </UserShowInfo>
                        <UserShowInfo>
                            <EmailIcon className="userShowInfoIcon" />
                            <UserShowInfoTitle>
                                tecky@tecky.tecky.com
                            </UserShowInfoTitle>
                        </UserShowInfo>
                        <UserShowInfo>
                            <HomeIcon className="userShowInfoIcon" />
                            <UserShowInfoTitle>
                                {staff.is_onboard && "OnBoard"}
                            </UserShowInfoTitle>
                        </UserShowInfo>
                    </UserShowBottom>
                </UserShow>
                <UserUpdate>
                    <UserUpdateTitle>修改</UserUpdateTitle>
                    <FormProvider {...methods}>
                        <UserUpdateForm>
                            <UserUpdateLeft>
                                <UserUpdateItem>
                                    <FormInputText name="name" control={control} label="姓名" />
                                </UserUpdateItem>
                                <UserUpdateItem>
                                    <FormInputText name="password" control={control} label="密碼" />
                                </UserUpdateItem>
                                <UserUpdateItem>
                                    <FormInputDropdownPosition name="position" control={control} label="職位" />
                                </UserUpdateItem>
                                <UserUpdateItem>
                                    <FormInputDropdownIsOnBoard name="is_onboard" control={control} label="在線" />
                                </UserUpdateItem>

                            </UserUpdateLeft>
                            <UesrUpdateRight>
                                <UserUpdateUpload>
                                    <img
                                        src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png" alt=""
                                        className='userUpateImg'
                                    />
                                    <label htmlFor="file">
                                        <FileUploadIcon />
                                    </label>
                                    <input type="file" id="file" style={{ display: "none" }} />
                                </UserUpdateUpload>
                                <Button variant="contained" color="success" onClick={handleSubmit(onSubmit)}>
                                    修改
                                </Button>
                            </UesrUpdateRight>

                        </UserUpdateForm>
                    </FormProvider>
                </UserUpdate>
            </EditUserDiv>}
            
            {/* </>))} */}
        </EditUserContainer>
    )
}

