

import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../redux/store";
import { ICategoryState } from "../../redux/productSetting/state";
import { getAllCategoryThunk } from "../../redux/productSetting/thunks";
import PutCategoryForm, { PostCategoryForm } from "../../components/product/setCategoryForm";
import { originCategoryState } from "../../redux/productSetting/state";
import { fetchGetProductCategory } from "../../redux/productSetting/api";
import { ProductContainer } from "../../components/product/product.style";
import SetProduct from "../../components/product/setProduct";

export default function ProductSetting() {

  const dispatch = useDispatch();
  const categoryData = useSelector((state: IRootState) => state.category.dataFromDb)
  // const product_category_id = useSelector((state:IRootState)=>state.category.id)
  //const discountSetData = useSelector((state: IRootState) => state.category.discountSetFromDb )

  // console.log(categoryData)
  useEffect(() => {
    console.log(123)
    dispatch(getAllCategoryThunk())
    //console.log(categoryData)
  }, [dispatch])

  return (

    <ProductContainer>

      <PostCategoryForm />

      <SetProduct />

      {categoryData.map(data => <PutCategoryForm product_category_name={data.name} product_category_id={data.id}
        is_inSetOnly={data.is_inSetOnly} />)}

    </ProductContainer>

  );
}


