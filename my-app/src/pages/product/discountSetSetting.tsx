import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../redux/store";
import { ICategoryState } from "../../redux/productSetting/state";
import { getAllCategoryThunk, getAllDiscountSetThunk } from "../../redux/productSetting/thunks";
import PutCategoryForm, { PostCategoryForm } from "../../components/product/setCategoryForm";
import { originCategoryState } from "../../redux/productSetting/state";
import { fetchGetProductCategory } from "../../redux/productSetting/api";
import { DiscountSetContainer } from "../../components/product/product.style";
import SetProduct from "../../components/product/setProduct";
import { PostDiscountSetForm } from "../../components/product/discountSetSetting/setDiscountSet"


export default function DiscountSetSetting() {


  // const product_category_name = useSelector((state:IRootState)=>state.category.name)
  // const product_category_id = useSelector((state:IRootState)=>state.category.id)
  const discountSetData = useSelector((state: IRootState) => state.category.discountSetFromDb)

  const dispatch = useDispatch();
  useEffect(() => {
    console.log("here 222222")
    dispatch(getAllDiscountSetThunk())
    // console.log(categoryData)
  }, [dispatch])

  return (
    <>

      <DiscountSetContainer>
       
        <PostDiscountSetForm />

      </DiscountSetContainer>
    </>);
}



// //               {discountSetData.map(data => <PostDiscountSetForm name={data.name}  id= {data.id}  is_launched ={data.is_launched}
// breakfast ={data.breakfast} lunch ={data.lunch} afternoonTea ={data.afternoonTea} dinner= {data.dinner}
// nightSnack={data.nightSnack} photo={data.photo} />)}



