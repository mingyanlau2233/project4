import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { Link } from 'react-router-dom';
import { RemarksConatainer, RemarksTitleConatainer, RemarksTitle } from './Remark.style';
import { Button } from '@mui/material';
import { remarksRows } from '../../dummyDate';
import { useState } from 'react';

// function createData(
//     name: string,
//     calories: number,
//     fat: number,
//     carbs: number,
//     protein: number,
// ) {
//     return { name, calories, fat, carbs, protein };
// }

// const remarksRows = [
//     createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
//     createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
//     createData('Eclair', 262, 16.0, 24, 6.0),
//     createData('Cupcake', 305, 3.7, 67, 4.3),
//     createData('Gingerbread', 356, 16.0, 49, 3.9),
// ];

export default function Remarks() {

    const [data, setData] = useState(remarksRows)
    const handleDelete = (id:number) => {
        // setData(data.filter((item:any) => item.id !== id))
        // console.log(id)
        setData(data.filter((row) => row.id !== id))
        console.log(data)
    }

    return (
        <RemarksConatainer>
            <RemarksTitleConatainer>
                <RemarksTitle>
                    備註設定
                </RemarksTitle>

                <Button component={Link} to={'/newRemark'} variant="contained" color="success">
                    新增
                </Button>
            </RemarksTitleConatainer>
            <TableContainer component={Paper}>
                
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>飲品備註</TableCell>
                            <TableCell align="right">備註</TableCell>
                            <TableCell align="right">價錢&nbsp;(g)</TableCell>
                            <TableCell align="right">Actions&nbsp;</TableCell>

                        </TableRow>
                    </TableHead>@
                    <TableBody>
                    
                        {data && data.map((row) => (
                            <TableRow
                                key={row.name}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.name}</TableCell>
                                <TableCell align="right">{row.price}</TableCell>
                                <TableCell align="right">
                                    <Button variant="outlined" color="error" onClick={() => handleDelete(row.id)}>
                                        刪除
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </RemarksConatainer>
    )
}
