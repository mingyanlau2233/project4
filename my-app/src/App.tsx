import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import Topbar from "./components/interface/topbar/Topbar";
import Sidebar from "./components/interface/sidebar/Sidebar";
import SidebarForStaff from "./components/interface/sidebar/SideBarForStaff";
import NoMatchPage from "./pages/interfaces/NoMatchPage";
import Home from "./pages/interfaces/home/Home";
import User from "./pages/users/Users";
import EditUser from "./pages/editUser/EditUser";
import DeskBoard from "./pages/editDesk/DeskBoard";
import AssignDesk from "./pages/assignDesk/AssignDesk";
import { PrivateRoute } from "./components/PrivateRoutes";
import { history, IRootState } from "./redux/store";
import LoginPage from "./pages/login/LoginPage";
import { useSelector } from "react-redux";
import SetTicketType from "./pages/waitingTicket/setTicketType";
import ConfirmTicket from "./pages/waitingTicket/confirmTicket";
import TicketList from "./pages/waitingTicket/ticketList";
import GetTicket from "./pages/waitingTicket/getTicket";
import Department from "./pages/department/Department";
import NewUser from "./pages/newUser/NewUser";
import Remarks from "./pages/remarks/Remarks";
import ProductSetting from "./pages/product/productSetting";
import CustomerOrder from "./pages/customerOrder/CustomerOrder";
import Cart from "./pages/customerOrder/CutomerOrderCart";
import OrderCartMainPage from "./pages/orderCart/oderCartMainPage";
import MenuMainPage from "./pages/customerOrder/customerMenuMainPage";
import DiscountSetSetting from "./pages/product/discountSetSetting";
import WaiterMainPage from "./pages/Counter/waiter";
import CreateSession from "./pages/assignDesk/createSession";
import Customer from "./pages/assignDesk/customer";
import HandleOrders from "./pages/handleOrders/HandleOrders";
import LandingPage from "./pages/landing/LandingPage";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import jwtDecode from "jwt-decode";
import { decodeToken } from "./redux/login/actions";
import Login from "./pages/login/Login";
import { io } from "socket.io-client";
import socketClient from "socket.io-client";

function App() {
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated
  );
  const userData = useSelector((state: IRootState) => state.auth.position);

  const dispatch = useDispatch();
  useEffect(() => {
    const token: string | null = localStorage.getItem("token");
    let coded;
    async function decode() {
      if (token) {
        coded = await jwtDecode(token); 
        dispatch(decodeToken(coded));
      }
    }
    decode();
  }, [dispatch, isAuthenticated]);

  function LoginPage() {
    if (!isAuthenticated) {
      return <Route path="/" exact={true} component={Login} />;
    }
    if (isAuthenticated) {
      return <PrivateRoute path="/" exact={true} component={MenuMainPage} />;
    }
  }

  return (
    <>
      <ConnectedRouter history={history}>
        <Route path="/landingPage" exact={true} component={LandingPage} />
        <div className="App">
          {isAuthenticated && userData != "customer" && <Topbar />}
          <div className="wrapper">
            {isAuthenticated && userData == "admin" && <Sidebar />}
            {isAuthenticated && userData == "staff" && <SidebarForStaff />}

            <Switch>
              {/* <Route path="/customer/:qrCode" exact={true} component={Customer} /> */}
              {LoginPage()}

              <PrivateRoute path="/users" exact={true} component={User} />
              <PrivateRoute
                path="/desk/editDesk"
                exact={true}
                component={DeskBoard}
              />
              <PrivateRoute
                path="/desk/assignDesk"
                exact={true}
                component={AssignDesk}
              />
              <PrivateRoute
                path="/user/:userId"
                exact={true}
                component={EditUser}
              />
              {/* <PrivateRoute path="/setTicketType" exact={true} component={SetTicketType} />
            <PrivateRoute path="/confirmTicket" exact={true} component={ConfirmTicket} /> */}
              <PrivateRoute
                path="/department"
                exact={true}
                component={Department}
              />
              {/* <PrivateRoute path="/newUser" exact={true} component={NewUser} />
            <PrivateRoute path="/newRemark" exact={true} component={Remarks} /> */}
              <PrivateRoute
                path="/productSetting"
                exact={true}
                component={ProductSetting}
              />
              <PrivateRoute
                path="/discountSetSetting"
                exact={true}
                component={DiscountSetSetting}
              />
              <PrivateRoute
                path="/orderCart"
                exact={true}
                component={OrderCartMainPage}
              />
              <PrivateRoute
                path="/menuPage"
                exact={true}
                component={MenuMainPage}
              />
              <PrivateRoute
                path="/counter"
                exact={true}
                component={WaiterMainPage}
              />
              {/* <PrivateRoute path="/createSession" exact={true} component={CreateSession}/> */}
              <PrivateRoute
                path="/handleOrders"
                exact={true}
                component={HandleOrders}
              />
            </Switch>
          </div>

          <Switch>
            <Route
              path="/customerOrder"
              exact={true}
              component={CustomerOrder}
            />
            <Route path="/customerOrder/Cart" exact={true} component={Cart} />
          </Switch>
          {/* <SimpleBottomNavigation /> */}
        </div>
      </ConnectedRouter>
    </>
  );
}

export default App;

{
  /* <Switch>
<PrivateRoute path="/" exact={true} component={HomePage} />
{/* <Route path="/" exact={true} component={HomePage} /> */
}
{
  /* <Route path="/login" exact={true} component={LoginPage} />
<Route component={NoMatchPage} />
</Switch> */
}
