import styled from "styled-components"
import { Link } from 'react-router-dom'

export const NavbarContainer = styled.nav`
    width: 100%;
    height: 80px;
    background-color:black ;
    displayl:flex;
    flex-direction: column;
`

export const LeftContainer = styled.div`
    display:flex;
    flex:60%;
    align-items:center;
    padding-left: 5%;
    // background-color:red;
`

export const RightContainer = styled.div`
    display:flex;
    flex:40%;
    justify-content:flex-end;
    padding-left: 50px;
    // background-color:salmon;   
`
export const NavbarInnerContainer = styled.div`
    width:100%;
    height:80px;
    display:flex;
`
export const NavbarLinkContainer = styled.div`
    display:flex;
    height:80px;
    align-items:center;
`
export const NavbarLink = styled(Link)`
    color:white;
    font-size:x-large;
    font-family: Arial,sans-serif;
    text-decoration:none;
    margin-right: 10px;
`
export const UserContainer = styled.div`
    display:flex;
    height:80px;
    align-items:center;
`

export const User = styled.div`
    color: white;
    font-family: Arial,sans-serif;
    margin-right: 10px;
`
export const LogoutBtnContaier = styled.div`
    display:flex;
    height:80px;
    align-items:center;
    margin-right:10px;
`


export const NavbarExtendenContainer = styled.div`
`

