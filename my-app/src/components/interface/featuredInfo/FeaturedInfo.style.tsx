import styled from "styled-components"

export const FeaturedItemContainer = styled.div`
    width:100%;
    display:flex;
    justify-content:space-between;
    
`

export const FeaturedItem = styled.div`
    flex:1;
    margin:0 20px;
    padding: 30px;
    border-radius:10px;
    cursor: pointer;
    -webkit-box-shadow: 0px 0px 15px -10px #000000; 
    box-shadow: 0px 0px 15px -10px #000000;
`

export const FeaturedTitle = styled.span`
    font-size: 20px;
`

export const FeaturedIncomeContainer = styled.div`
    display:flex;
    margin:10px 0;
    align-items: center;
`

export const FeaturedIncome = styled.div`
    font-size:30px;
    font-weight:600;
`

export const FeaturedRate = styled.span`
    display:flex;
    align-items: center;
    margin-left:12px;
`

export const FeaturedSub = styled.div`
    font-size: 14px;
    color:gray;
`