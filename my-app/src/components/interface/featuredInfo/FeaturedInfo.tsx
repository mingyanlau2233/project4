import {
    FeaturedItem,
    FeaturedTitle,
    FeaturedIncomeContainer,
    FeaturedIncome,
    FeaturedRate,
    FeaturedSub,
    FeaturedItemContainer
}
    from './FeaturedInfo.style'
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import './FeaturedInfo.css'

export default function FeaturedInfo() {
    return (
        <FeaturedItemContainer>
            <FeaturedItem>
                <FeaturedTitle>昨天營業額</FeaturedTitle>
                <FeaturedIncomeContainer>
                    <FeaturedIncome>$8,765</FeaturedIncome>
                    <FeaturedRate>
                        12
                        <ArrowDownwardIcon className="featuredIcon"/>
                    </FeaturedRate>
                </FeaturedIncomeContainer>
                <FeaturedSub>Compared to last month</FeaturedSub>
            </FeaturedItem>
            <FeaturedItem>
                <FeaturedTitle>今天營業額</FeaturedTitle>
                <FeaturedIncomeContainer>
                    <FeaturedIncome>$8,765</FeaturedIncome>
                    <FeaturedRate>
                        12
                        <ArrowDownwardIcon className="featuredIcon"/>
                    </FeaturedRate>
                </FeaturedIncomeContainer>
                <FeaturedSub>Compared to last month</FeaturedSub>
            </FeaturedItem>
            <FeaturedItem>
                <FeaturedTitle>這星期營業額</FeaturedTitle>
                <FeaturedIncomeContainer>
                    <FeaturedIncome>$8,765</FeaturedIncome>
                    <FeaturedRate>
                        12
                        <ArrowDownwardIcon className="featuredIcon"/>
                    </FeaturedRate>
                </FeaturedIncomeContainer>
                <FeaturedSub>Compared to last month</FeaturedSub>
            </FeaturedItem>
        </FeaturedItemContainer>
        
        
    )

}
