import styled from "styled-components"

export const SidebarContainer = styled.div`
    width:100px
    min-width:100px
    height: calc(100vh - 50px);
    background-color: rgb(251,251,255);
    position:sticky;
    top:50px;
`
export const SidebarWrapper = styled.div`
    padding:20px;
    color: gray;
`
export const SidebarMenu = styled.div`
    margin-bottom:10px
`

export const SidebarTitle = styled.h3`
    font-size:13px;
    color: rgb(187,186,186);
`

export const SidebarList = styled.ul`
    list-style:none;
    padding:5px;
`

export const SidebarItem = styled.li`
    padding:5px;
    cursor:pointer;
    display:flex;
    align-items:center;
    border-radius:10px;
    &:hover, &:active{
        background-color: rgb(228,228,250)

    }
`