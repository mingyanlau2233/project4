import React from 'react'
import {
    SidebarContainer, 
    SidebarItem, 
    SidebarList, 
    SidebarMenu, 
    SidebarTitle, 
    SidebarWrapper} from './Sidebar.style'
import SettingsIcon from '@mui/icons-material/Settings';
import './Sidebar.css'
import { useDispatch } from 'react-redux';
import { logoutThunk } from '../../../redux/login/thunks';
import { Link } from 'react-router-dom';



export default function Sidebar() {
   

    const dispatch = useDispatch();
    function LogOut(){
       
         
         dispatch(logoutThunk())
         
    }
    return (
        <SidebarContainer>
            <SidebarWrapper>
                <SidebarMenu>
                    <SidebarTitle>營運設置</SidebarTitle>
                    <SidebarList>
                        <Link to={"/productSetting"}>
                        <SidebarItem >
                            <SettingsIcon className="SidebabrIcon"/>
                            菜品管理
                        </SidebarItem>
                        </Link>
                        <Link to={"/discountSetSetting"}>
                        <SidebarItem>
                            <SettingsIcon className="SidebabrIcon"/>
                            套餐管理
                        </SidebarItem>
                        </Link>
                        <Link to={"/department"}>
                        <SidebarItem>
                            <SettingsIcon className="SidebabrIcon"/>
                            部門管理
                        </SidebarItem>
                        </Link>
                        <Link to={"/users"}>
                        <SidebarItem>
                            <SettingsIcon className="SidebabrIcon"/>
                            員工管理
                        </SidebarItem>
                        </Link>
                    </SidebarList>
                </SidebarMenu>
                <SidebarMenu>
                    <SidebarTitle>下單</SidebarTitle>
                    <SidebarList>
                    <Link to={"/menuPage"}>
                        <SidebarItem>
                            <SettingsIcon className="SidebabrIcon"/>
                              餐牌
                        </SidebarItem>
                        </Link >



                        <Link to={"/orderCart"}>
                        <SidebarItem>
                            <SettingsIcon className="SidebabrIcon"/>
                            確認下單
                        </SidebarItem>
                        </Link >


                    </SidebarList>
                </SidebarMenu>
                <SidebarMenu>
                    <SidebarTitle>廚房運作</SidebarTitle>
                    <SidebarList>
                    <Link to={"/handleOrders"}>
                        <SidebarItem>
                            <SettingsIcon className="SidebabrIcon"/>
                             製作部門
                        </SidebarItem>
                        </Link>
                        <Link to={"/counter"}>
                        <SidebarItem>   
                            <SettingsIcon className="SidebabrIcon"/>
                            包裝櫃枱
                        </SidebarItem>
                        </Link>
                    </SidebarList>
                </SidebarMenu>
              
                    
                <SidebarMenu>
                <SidebarTitle>用戶：{}</SidebarTitle>
                <SidebarItem>
                <SettingsIcon className="SidebabrIcon"/>
                            <div onClick={LogOut}>登出</div>
                        </SidebarItem>
                </SidebarMenu>
            </SidebarWrapper>
        </SidebarContainer>
    )
}