import {
    SidebarContainer, 
    SidebarItem, 
    SidebarList, 
    SidebarMenu, 
    SidebarTitle, 
    SidebarWrapper} from './Sidebar.style'
import SettingsIcon from '@mui/icons-material/Settings';
import './Sidebar.css'
import { useDispatch } from 'react-redux';
import { logoutThunk } from '../../../redux/login/thunks';
import { Link } from 'react-router-dom';
console.log("Sidebar1")

export default function SidebarForStaff() {


    const dispatch = useDispatch();
    function LogOut(){
       
         
         dispatch(logoutThunk())
         
    }
    return (
        <SidebarContainer>
            <SidebarWrapper>
 
                <SidebarMenu>
                    <SidebarTitle>下單</SidebarTitle>
                    <SidebarList>
                    <Link to={"/menuPage"}>
                        <SidebarItem>
                            <SettingsIcon className="SidebabrIcon"/>
                              餐牌
                        </SidebarItem>
                        </Link >



                        <Link to={"/orderCart"}>
                        <SidebarItem>
                            <SettingsIcon className="SidebabrIcon"/>
                            確認下單
                        </SidebarItem>
                        </Link >


                    </SidebarList>
                </SidebarMenu>
                <SidebarMenu>
                    <SidebarTitle>廚房運作</SidebarTitle>
                    <SidebarList>
                    <Link to={"/handleOrders"}>
                        <SidebarItem>
                            <SettingsIcon className="SidebabrIcon"/>
                             製作部門
                        </SidebarItem>
                        </Link>
                        <Link to={"/counter"}>
                        <SidebarItem>   
                            <SettingsIcon className="SidebabrIcon"/>
                            包裝櫃枱
                        </SidebarItem>
                        </Link>
                    </SidebarList>
                </SidebarMenu>
              
                    
                <SidebarMenu>
                <SidebarTitle>用戶：{}</SidebarTitle>
                <SidebarItem>
                <SettingsIcon className="SidebabrIcon"/>
                            <div onClick={LogOut}>登出</div>
                        </SidebarItem>
                </SidebarMenu>
            </SidebarWrapper>
        </SidebarContainer>
    )
}