
import {
    NavbarContainer,
    LeftContainer,
    RightContainer,
    NavbarInnerContainer,
    NavbarExtendenContainer,
    NavbarLinkContainer,
    NavbarLink,
    UserContainer,
    User,
    LogoutBtnContaier
} from './Navbar.style'
import { Button } from '@mui/material';

export default function Navbar() {
    return (
        <NavbarContainer>
            <NavbarInnerContainer>
                <LeftContainer>
                    <NavbarLinkContainer>
                        <NavbarLink to="home">營運管理</NavbarLink>
                        <NavbarLink to="1">營動數據</NavbarLink>
                        <NavbarLink to="2">庫存出納</NavbarLink>
                        <NavbarLink to="3">日出操作</NavbarLink>
                    </NavbarLinkContainer>
                </LeftContainer>
                <RightContainer>
                    <UserContainer>
                        <User>員工 : Maggie</User>
                    </UserContainer>
                    <UserContainer>
                        <User>職位 : 打雜</User>
                    </UserContainer>
                    <LogoutBtnContaier>
                        <Button variant="outlined" color="error">
                            Logout
                        </Button>
                    </LogoutBtnContaier>
                </RightContainer>
            </NavbarInnerContainer>
            <NavbarExtendenContainer></NavbarExtendenContainer>
        </NavbarContainer>
    )
}
