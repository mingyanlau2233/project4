import styled from "styled-components"

export const TopbarContainer = styled.div`
    width:100%;
    height:50px;
    background-color: white;
    position:sticky;
    top:0;
    z-index:1000;
`

export const TopbarWrapper= styled.div`
    height:100%;
    padding:0 20px;
    display:flex;
    align-items:center;
    justify-content:space-between;
`

export const TopLeft = styled.div`

`

export const TopRight =styled.div`

`

export const Logo = styled.span`
    font-weight:300;
    font-size:30px;
    color: darkblue;
    cursor: pointer;
    word-spacing: 13px;
    text-shadow: 5px 5px 13px rgb(120, 184, 214);
    
`

export const TopbarIconContainer = styled.div`
    display:flex;
`

export const TopIconBag = styled.div`
`


export const User = styled.div`
    display:flex;
    align-items:center;
    margin-right: 10px;
    word-spacing: 1px;
   
    
`