
import {
    Logo,
    TopbarContainer,
    TopbarIconContainer,
    TopbarWrapper,
    TopLeft,
    TopRight,
    User
} from './Topbar.style'
import "./topbar.css"
import { useEffect } from "react";
import { useDispatch,useSelector } from "react-redux";
import jwtDecode from "jwt-decode";
import { decodeToken } from "../../../redux/login/actions"
import { IRootState } from '../../../redux/store';

console.log("Topbar 1")


export default function Topbar() {
    console.log("Topbar 2")

    const userName = useSelector((state: IRootState ) => state.auth.name)
    const userPosition = useSelector((state: IRootState) => state.auth.position)
     console.log(userName)

   
        return (
        <TopbarContainer>
            <TopbarWrapper>
                <TopLeft><Logo>點 點 易</Logo></TopLeft>
                <TopRight>
                    <TopbarIconContainer className= "box">
                        <div className="branch">餐 廳 營 運 好 幫 手</div>
                        <div className="user">職員：{userName}</div>
                        <div className="user">職級：{userPosition}</div>
                    </TopbarIconContainer>
                   
                </TopRight>
            </TopbarWrapper>
        </TopbarContainer>
    )
}
