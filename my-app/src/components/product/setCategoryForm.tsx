import { Form, Table } from "react-bootstrap";
import { deleteCategoryThunk, getAllCategoryThunk, getProductDataThunk, postCategoryThunk, putCategoryThunk } from "../../redux/productSetting/thunks"
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import "./inline.css";
import { ContainerBorder } from "./product.style"
import React, { useEffect, useState } from 'react'
import { IRootState } from "../../redux/store";
import GetProduct from "./getProduct"
import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';



interface CategoryProps {
  product_category_id: number;
  product_category_name: string;
  is_inSetOnly: boolean;
}
interface ProductCategoryForm {
  name: string;
  id: number;
  is_inSetOnly: boolean;
}


export default function PutCategoryForm(props: CategoryProps) {
  const dispatch = useDispatch();
  const buttonState = {
    button: 1
  };

  const [open, setOpen] = React.useState(false);
  const [transition, setTransition] = React.useState(undefined);


  let productData = useSelector((state: IRootState) => state.category.productFromDb)
  console.log(productData)
  useEffect(() => {
    dispatch(getProductDataThunk(props.product_category_id));
  }, [dispatch])

  const { register, handleSubmit, reset } = useForm<ProductCategoryForm>({
    defaultValues: {
      name: props.product_category_name,
      id: props.product_category_id,
      is_inSetOnly: props.is_inSetOnly

    },
  });
  const onSubmit = (data: ProductCategoryForm) => {
    console.log(data.name, data.id)
    if (buttonState.button === 1) {
      console.log("Button 1 clicked!");
      let is_inSetOnly = Boolean(data.is_inSetOnly)
      dispatch(putCategoryThunk(data.id, data.name, is_inSetOnly))
      alert(`菜品 : ${data.name} 已修改`)
   
      dispatch(getAllCategoryThunk())
    }
    if (buttonState.button === 2) {
      console.log("Button 2 clicked!")
      dispatch(deleteCategoryThunk(data.id))
      alert(`菜品 : ${data.name} 已刪除`)
      
      setTimeout(() => { dispatch(getAllCategoryThunk())  }, 300);
    }
  };




 

  const filterData = (datas: any[]) => datas.filter((data) => data.product_category_id == props.product_category_id)


  return (


    // <Form onSubmit={submitHandler}>
    <div className="putContainer">
      <Form onSubmit={handleSubmit(onSubmit)} >
        <Form.Group className="inTitle" controlId="formInput">
          <Table responsive="md" style={{ width: '100%', textAlign: 'left' }}>
            <thead>
              <tr >
                <th >
                  <Form.Label style={{ paddingLeft: "10px"}}>菜品類別編號：</Form.Label >
                  <Form.Control type="text" className="tableContent" disabled {...register("id", {})} />
                  <Form.Label>菜品類別：</Form.Label>
                  <Form.Control type="text" placeholder="請輸入菜品類別" {...register("name", {})} />
                  <Form.Label>組合素材：</Form.Label>
                  <Form.Select aria-label="Default select example" className="inputWidthWithNumber" {...register("is_inSetOnly", {})} required={true}>
                    <option value="false">否</option>
                    <option value="true">是</option>
                  </Form.Select>
                  </th>
                <th style={{textAlign: "right"}}>
                  <button className="submitButton" type="submit" onClick={() => (buttonState.button = 1)}>
                    修改
                  </button>
             
                  <button className="deleteButton" type="submit" onClick={() => (buttonState.button = 2)}>
                    刪除
                  </button>
                </th>
              </tr>
            </thead>
          </Table>
        </Form.Group>
      </Form>

      {
        filterData(productData).map(data => (<div className="getProductDiv">{
          <GetProduct product_id={data.product_id} name={data.name} is_launched={data.is_launched}
            is_kiosk={data.is_kiosk} is_time_consuming={data.is_time_consuming} department_id={data.department_id}
            price={data.price} breakfast={data.breakfast} lunch={data.lunch}
            afternoonTea={data.afternoonTea} dinner={data.dinner} nightSnack={data.nightSnack}
            photo={data.photo}
          />}</div>))
      }





    </div >
  );
}


export function PostCategoryForm() {
  const dispatch = useDispatch();
  //const dispatch = useDispatch();
  //const user = useSelector((state:User)=>state.user)
  const [name, setName] = React.useState("");
  const [subitem, setSubitem] = React.useState("");

  const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    //console.log(name,id)
    console.log("onclick success")
    console.log("here", name, subitem)


    dispatch(postCategoryThunk(name, Boolean(subitem)))
    setTimeout(() => {   dispatch(getAllCategoryThunk()) }, 300);
    // dispatch(getAllCategoryThunk())
  };
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  // const action = (
  //   <React.Fragment>
  //     <Button color="secondary" size="small" onClick={handleClose}>
  //       UNDO
  //     </Button>
  //     <IconButton
  //       size="small"
  //       aria-label="close"
  //       color="inherit"
  //       onClick={handleClose}
  //     >
  //       <CloseIcon fontSize="small" />
  //     </IconButton>
  //   </React.Fragment>
  // );


  return (

    <Form onSubmit={submitHandler}>
      <Form.Group className="categoryBorder" controlId="formInput">
        <Form.Label>新增菜品類別：</Form.Label>
        <Form.Control type="text" placeholder="請輸入菜品類別" className="inputWidthWithTest"
          value={name} onChange={(e) => setName(e.target.value)} required={true} />
        <Form.Label>組合素材：</Form.Label>
        <Form.Select aria-label="Default select example" value={subitem} onChange={(e) => setSubitem(e.target.value)} >
          <option value="false">否</option>
          <option value="true">是</option>
        </Form.Select>
        <button className="submitButton" type="submit" onClick={handleClick}>
          提交
        </button>
        <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message="已新增菜品類別"
        // action={action}
      />
      </Form.Group>
    </Form>

  );
}