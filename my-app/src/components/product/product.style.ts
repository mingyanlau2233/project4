import styled from 'styled-components';

export const AssignDeskContainer = styled.div`
    flex:6;
    padding:20px;
`
export const ProductContainer = styled.div`
    flex:6;
    padding:50px;
    padding-left: 60px;
    padding-right: 60px;
    background-color: rgb(245, 245, 245 , 0.8);
    border-radius: 15px;
    height:100%;
    overflow-y: scroll;
`

export const DiscountSetContainer = styled.div`
    flex:6;
    padding:50px;
    padding-left: 170px;
    padding-right: 170px;
    background-color: rgb(245, 245, 245 , 0.8);
    border-radius: 15px;
    overflow-y: scroll;
`

export const AssignContainer = styled.div`
    display:flex;
    flex-direction:column;
`
export const AssignContainerColumn = styled.div`
    display:flex;
    flex-direction: column;
    align-items:center;
    justify-content:space-between;
    width:80vw
`

export const ContainerBorder = styled.div`
    border-radius: 3px;
    width: 100%;
    margin-top: 25px;
     border: 1px solid rgb(151, 147, 147,0.7);

`