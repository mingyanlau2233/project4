import { Form, Table } from "react-bootstrap";
//import { useDispatch } from "react-redux"
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { ContainerBorder } from "../product.style";
import { IRootState } from "../../../redux/store";
import React, { useEffect, useState } from 'react'
import { deleteDiscountSetThunk, getAllCategoryThunk, getDiscountSetDetailsThunk, postDiscountSetDetailsThunk, putDiscountSetThunk } from "../../../redux/productSetting/thunks";
import { EditDiscountSetDetails } from "./discountSetDetails"
import { display, textAlign } from "@mui/system";
interface DiscountSetForm {
  name: string;
  is_launched: boolean;
  id: number
  breakfast: boolean;
  lunch: boolean;
  afternoonTea: boolean;
  dinner: boolean;
  nightSnack: boolean;
  price: number;
  photo: any

}
const { REACT_APP_SOCKET_IO_URL } = process.env;
export function PutDiscountSetForm(Props: DiscountSetForm) {
  const dispatch = useDispatch();
  const buttonState = {
    button: 0
  };
  const { register, handleSubmit, reset } = useForm<DiscountSetForm>({

    defaultValues: {
      name: Props.name,
      is_launched: Props.is_launched,
      id: Props.id,
      breakfast: Props.breakfast,
      lunch: Props.lunch,
      afternoonTea: Props.afternoonTea,
      dinner: Props.dinner,
      nightSnack: Props.nightSnack,
      price: Props.price,
      photo: Props.photo
    },

  });
  const [content, setContent] = React.useState("");
  const [description, setDescription] = React.useState("");
  const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    //console.log(name,id)
    console.log("onclick success")
    console.log("here", content, Props.id)
    dispatch(postDiscountSetDetailsThunk(Props.id, Number(content), description))
    window.location.reload()
  };

  const onSubmit = (data: DiscountSetForm) => {
    console.log("here123456", data.breakfast, data.lunch, data.afternoonTea, data.dinner,
      data.nightSnack)
    if (buttonState.button === 3) {
      dispatch(putDiscountSetThunk(data.id, data.breakfast, data.lunch, data.afternoonTea, data.dinner,
        data.nightSnack, data.name, data.price, data.is_launched, data.photo))
      alert(`套餐 : ${data.name} 已修改`)

      return
    }
    if (buttonState.button === 4) {
      dispatch(deleteDiscountSetThunk(data.id))
      alert(`套餐 : ${data.name} 已刪除`)
    }
    window.location.reload()


  };
  const categoryData = useSelector((state: IRootState) => state.category.dataFromDb)
  const discountSetDetailsData = useSelector((state: IRootState) => state.category.discountSetDetailsFromDB)

  const filterData = (datas: any[]) => datas.filter((data) => data.discount_set_id == Props.id)
  useEffect(() => {
    dispatch(getAllCategoryThunk())
    dispatch(getDiscountSetDetailsThunk(Props.id))
    // console.log(categoryData)
  }, [])
  return (
    <div className="containerBorder">
      <Form className="discountTable" encType="multipart/form-data" onSubmit={handleSubmit(onSubmit)}>
        <Table responsive="md" style={{ width: '100%', textAlign: 'center' }}>
          <thead>

            <tr>
              <th >編號</th>
              <th >套餐名稱</th>
              <th >價錢</th>
              <th >上市中</th>
              <th >圖片</th>
              <th >更換圖片</th>
              <th >早餐</th>
              <th >午餐</th>
              <th >下午茶</th>
              <th >晚餐</th>
              <th >宵夜</th>
              <th></th>
            </tr>

          </thead>


          <tbody>

            <tr>

              <td>
                <Form.Control type="text" placeholder="" className="tableContent"
                  {...register("id", {})} disabled />
              </td>

              <td>
                <Form.Control type="text" placeholder="" className="inputWidthWithTest"
                  {...register("name", {})} />
              </td>

              <td>
                <Form.Control type="number" placeholder="" className="tableContent"
                  {...register("price", {})} />
              </td>

              <td>
                <Form.Select aria-label="Default select example" className="tableContent"
                  {...register("is_launched", {})}>

                  <option value="true">是</option>
                  <option value="false">否</option>
                </Form.Select>
              </td>

              <td>
                <a href={`${REACT_APP_SOCKET_IO_URL}/${Props.photo}`} target="_blank">查閱 </a>
              </td>


              <td>
                <Form.Control type="file" className="tableForMulter" multiple {...register("photo", {})} />
              </td>



              <td ><Form.Check className="tableForButton" {...register("breakfast", {})} /></td>
              <td ><Form.Check className="tableForButton" {...register("lunch", {})} /></td>
              <td style={{ paddingLeft: "10px" }}><Form.Check className="tableForButton" {...register("afternoonTea", {})} /></td>
              <td ><Form.Check className="tableForButton" {...register("dinner", {})} /></td>
              <td ><Form.Check className="tableForButton" {...register("nightSnack", {})} /></td>

              <td style={{ display: "flex" }}>

                <button className="submitButton" type="submit" onClick={() => (buttonState.button = 3)} >
                  修改
                </button>
                <button className="deleteButton" type="submit" onClick={() => (buttonState.button = 4)}>
                  刪除
                </button>

              </td>
            </tr>
          </tbody>


        </Table>
      </Form>

      <div>
        {filterData(discountSetDetailsData).map((data) =>
          <EditDiscountSetDetails description={data.description} name={data.name}
            id={data.id} product_category_id={data.product_category_id}
            discount_set_sequence_id={data.discount_set_sequence_id} discount_set_id={data.discount_set_id}
            price={data.price} photo={data.photo} />
        )}
      </div>
      <Form className="addProductRow" onSubmit={submitHandler}>
        <Form.Label>新增套餐菜品內容:</Form.Label>
        <Form.Control type="text" placeholder="請輸入菜品名稱" className="inputWidthWithTest"
          value={description} onChange={(e) => setDescription(e.target.value)} required={true} />
        <Form.Select aria-label="Default select example" value={content} onChange={(e) => setContent(e.target.value)} required={true} >
          <option >-請選擇菜品類別-</option>
          {categoryData.map((categoryData) =>
            <option value={categoryData.id}>{categoryData.name}</option>)}
        </Form.Select>

        <button className="submitButton" type="submit">
          新增內容
        </button>
      </Form>

    </div>

  );
} 