import { Form } from "react-bootstrap";
//import { useDispatch } from "react-redux"
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { IRootState } from "../../../redux/store";

import React, { useEffect, useState } from 'react'
import { getAllDiscountSetThunk, postDiscountSetThunk,deleteDiscountSetDetails ,
  postDiscountSetDetailsThunk2} from "../../../redux/productSetting/thunks";
import { propTypes } from "react-bootstrap/esm/Image";


interface DiscountSetDetails {
     description: string, 
     name:string,
     id:number,
     product_category_id:number,
     discount_set_sequence_id:number,
     discount_set_id:number,
     price:number,
     photo:string
  }

  interface NewDiscountSetDetails {
    description: string, 
    name:string,
    id:number,
    product_category_id:number,
    new_product_category_id:number,
    discount_set_sequence_id:number
    price:number;
    photo:string;
 }

  export  function  EditDiscountSetDetails(Props:DiscountSetDetails) {
    const dispatch = useDispatch()
    const buttonState = {
      button: 0
    };
    const categoryData =useSelector((state: IRootState) => state.category.dataFromDb )
    const { register, handleSubmit, reset } = useForm<NewDiscountSetDetails>({
  
      defaultValues: {
        description: Props.description ,
        name:Props.name,
        id:Props.id,
        product_category_id:Props.product_category_id,
        new_product_category_id:0,
        price:Props.price,
        photo:Props.photo
     },
     })

     const onSubmit = (data:NewDiscountSetDetails) => {

      if (buttonState.button === 1) {
        console.log(data.new_product_category_id)
        console.log("discount_details_testing",Props.discount_set_sequence_id)
        dispatch(postDiscountSetDetailsThunk2(Props.discount_set_id,data.new_product_category_id,Props.discount_set_sequence_id))
        window.location.reload()
      }
      if (buttonState.button === 2) {
        console.log("discount_details_testing",Props.discount_set_sequence_id)
        dispatch(deleteDiscountSetDetails(Props.discount_set_sequence_id))
        window.location.reload()
               

    }
 
    };

    return (
       
      <Form className="discountContent" encType="multipart/form-data" onSubmit={handleSubmit(onSubmit)}>
         <Form.Label>套餐內容：</Form.Label>
         <Form.Control type="text" placeholder="" className="inputWidthWithTest"
          disabled {...register("description", {}) }/>
          <Form.Control type="text" placeholder="" className="inputWidthWithTest"
         disabled  {...register("name", {}) }/>
        

        <Form.Select placeholder="請選擇菜品類別" aria-label="Default select example" {...register("new_product_category_id", {}) } >
                      <option >-請選擇菜品類別-</option>
               {categoryData.map((categoryData)=>
                     <option value={categoryData.id}>{categoryData.name}</option>)}
            </Form.Select>

        <button className="submitButton" type="submit" onClick={() => (buttonState.button = 1)}>
          新增
        </button>
        <button className="deleteButton" type="submit" onClick={() => (buttonState.button = 2)} >
          刪除
        </button>
      </Form>
      
    );
  } 