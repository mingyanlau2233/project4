import { Form } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import Stack from '@mui/material/Stack';
import React, { useEffect } from 'react'
import { getAllDiscountSetThunk, postDiscountSetThunk } from "../../../redux/productSetting/thunks";
import { PutDiscountSetForm } from "./putdiscountSet";
import { IRootState } from "../../../redux/store"
import Snackbar from '@mui/material/Snackbar';

interface DiscountSetForm {
  name: string;
  is_launched: boolean;
  price: number;

  breakfast: boolean;
  lunch: boolean;
  afternoonTea: boolean;
  dinner: boolean;
  nightSnack: boolean;

  photo: any

}

export function PostDiscountSetForm() {
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const dispatch = useDispatch();
  const discountSetData = useSelector((state: IRootState) => state.category.discountSetFromDb)
  const onSubmit = (data: DiscountSetForm) => {
    
    dispatch(postDiscountSetThunk(data.breakfast, data.lunch, data.afternoonTea, data.dinner,
      data.nightSnack, data.name, data.is_launched, data.photo, data.price))

    // setInterval(() => { window.location.reload() }, 300);
  };

  useEffect(() => {
    
    dispatch(getAllDiscountSetThunk())
  }, [dispatch])
  const handleClose = (event: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  const { register, handleSubmit, reset } = useForm<DiscountSetForm>({
    defaultValues: {
      name: "",
      is_launched: true,
      price: 0,
      breakfast: false,
      lunch: false,
      afternoonTea: false,
      dinner: false,
      nightSnack: false,
      photo: ""
    },
  });

  return (
    <>
      <Form encType="multipart/form-data" onSubmit={handleSubmit(onSubmit)} className='setDiscountForm'>
       
        <Form.Group className="mb-3 setupDiscountForm" controlId="formInput">

          <Form.Label>套餐名稱：</Form.Label>
          <Form.Control type="test" placeholder="請輸入套餐名稱"
            {...register("name", {})} required={true} />
          <Form.Label>價錢：</Form.Label>
          <Form.Control className="price" type="number" placeholder="" min={0}
            {...register("price", {})} required={true} />
          <Form.Label>上市中：</Form.Label>
          <Form.Select aria-label="Default select example"
            {...register("is_launched", {})} required={true}>

            <option value="true">是</option>
            <option value="false">否</option>
          </Form.Select>

          < Form.Label >上傳圖片：</Form.Label>
          <Form.Control  id="fileUpload" type="file" multiple {...register("photo", {})} required={true} />
        </Form.Group>

        <Form.Group className="saleTime">
          <Form.Label >銷售時間：</Form.Label>
            <Form.Label >早餐</Form.Label>
            <Form.Check className="inline"   {...register("breakfast", {})} />
            <Form.Label>午餐</Form.Label>
            <Form.Check className="inline"    {...register("lunch", {})} />
            <Form.Label >下午茶</Form.Label>
            <Form.Check className="inline"    {...register("afternoonTea", {})} />
            <Form.Label  >晚餐</Form.Label>
            <Form.Check className="inline"    {...register("dinner", {})} />
            <Form.Label>宵夜</Form.Label>
            <Form.Check className="inline"    {...register("nightSnack", {})} />
            <button className="submitButton" type="submit" onClick={handleClick}>
              提交
            </button>
            <Snackbar
              open={open}
              autoHideDuration={3000}
              message="已新增套餐"
              onClose={handleClose}
            />
       
        </Form.Group>
      </Form>

      {
        discountSetData.map((data) => (

          <PutDiscountSetForm name={data.name} id={data.id} is_launched={data.is_launched}
            breakfast={data.breakfast} lunch={data.lunch} afternoonTea={data.afternoonTea} dinner={data.dinner}
            nightSnack={data.nightSnack} photo={data.photo} price={data.price} />
        ))
      }
    </>
  );
} 