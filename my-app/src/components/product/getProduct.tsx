import { useForm } from "react-hook-form";
import { Form, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import React, { useEffect } from "react";
import { getDepartments } from "../../redux/department/thunks";
import "./inline.css"
import { deleteProductThunk, getAllCategoryThunk, postCategoryThunk, postProductThunk, putProductThunk } from "../../redux/productSetting/thunks";
import { Snackbar } from "@material-ui/core";


interface ProductProps {
  name: string,
  product_id: number,
  is_launched: boolean,
  is_kiosk: boolean,
  is_time_consuming: boolean,
  department_id: number,
  price: number,
  breakfast: boolean,
  lunch: boolean,
  afternoonTea: boolean,
  dinner: boolean,
  nightSnack: boolean,
  photo: string
}

interface PutProductForm {
  name: string;
  product_id: number;
  is_launched: boolean;
  is_kiosk: boolean;
  is_time_consuming: boolean;
  department_id: number;
  price: number;
  breakfast: boolean;
  lunch: boolean;
  afternoonTea: boolean;
  dinner: boolean;
  nightSnack: boolean;
  photo: any;
  // product_category_id:number;

}
const { REACT_APP_SOCKET_IO_URL } = process.env;
export default function GetProduct(props: ProductProps) {
  const buttonState = {
    button: 0
  };

  const dispatch = useDispatch();
  const departments = useSelector((state: IRootState) => state.department.department)
  const { register, handleSubmit, reset } = useForm<PutProductForm>({
    defaultValues: {
      name: props.name,
      product_id: props.product_id,
      is_launched: props.is_launched,
      is_kiosk: props.is_kiosk,
      is_time_consuming: props.is_time_consuming,
      department_id: props.department_id,
      price: props.price,
      breakfast: props.breakfast,
      lunch: props.lunch,
      afternoonTea: props.afternoonTea,
      dinner: props.dinner,
      nightSnack: props.nightSnack,
      photo: props.photo,
      //product_category_id:props.product_category_id;
    },
  });


  // data.breakfast ,data.lunch,data.afternoonTea,data.dinner,
  // data.nightSnack,data.department_id,data.name,data.price,data.is_launched,
  // data.is_kiosk,data.is_time_consuming,data.photo,data.product_category_id))


  useEffect(() => {
    dispatch(getDepartments())
  }, [dispatch])

  const onSubmit = (data: PutProductForm) => {
    console.log("here123456", data.breakfast, data.lunch, data.afternoonTea, data.dinner,
      data.nightSnack)
    if (buttonState.button === 3) {

      dispatch(putProductThunk(data.product_id, data.breakfast, data.lunch, data.afternoonTea, data.dinner,
        data.nightSnack, data.department_id, data.name, data.price, data.is_launched,
        data.is_kiosk, data.is_time_consuming, data.photo))
        alert(`菜品 : ${data.name} 已修改`)
      // setInterval(() => { window.location.reload() }, 300);
      return
    } 
    if (buttonState.button === 4) {
      alert(`菜品 : ${data.name} 已刪除`)
      
      dispatch(deleteProductThunk(data.product_id))
    }

    setTimeout(() => { window.location.reload() }, 300);

  };


  return (
    <>
      <Form className="getProductForm" encType="multipart/form-data" onSubmit={handleSubmit(onSubmit)}>
        <Table responsive="md" style={{ width: '100%', textAlign: 'center' }}>
          <thead>
            <tr >
              <th >編號</th>
              <th >菜品名稱</th>
              <th >價錢</th>
              <th >上市中</th>
              <th >小賣部</th>
              <th >耗時</th>
              <th >部門</th>
              <th >圖片</th>
              <th >更換圖片</th>
              <th >早餐</th>
              <th >午餐</th>
              <th >下午茶</th>
              <th >晚餐</th>
              <th >宵夜</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>
                <Form.Control type="text" placeholder="" className="tableContent" disabled
                  {...register("product_id", {})} />
              </td>

              <td>
                <Form.Control type="text" placeholder="" className="inputWidthWithTest product3"
                  {...register("name", {})} />
              </td>

              <td>
                <Form.Control type="number" placeholder="" min="0" className="tableContent"
                  {...register("price", {})} />

              </td>

              <td>
                <Form.Select aria-label="Default select example" className="tableContent"
                  {...register("is_launched", {})}>

                  <option value="true">是</option>
                  <option value="false">否</option>
                </Form.Select>
              </td>

              <td>
                <Form.Select aria-label="Default select example" className="tableContent"
                  {...register("is_kiosk", {})}>

                  <option value="true">是</option>
                  <option value="false">否</option>
                </Form.Select>
              </td>

              <td>
                <Form.Select aria-label="Default select example" className="tableContent"
                  {...register("is_time_consuming", {})}>

                  <option value="true">是</option>
                  <option value="false">否</option>
                </Form.Select>
              </td>

              <td>
                <Form.Select aria-label="Default select example" {...register("department_id", {})}>

                  {departments.map((department) =>
                    <option value={department.id}>{department.name}</option>)}
                </Form.Select>
              </td>

              <td >
                <a className="clickPhoto" href={`${REACT_APP_SOCKET_IO_URL}/${props.photo}`} target="_blank">查閱 </a>
              </td>

              <td>
                <Form.Control type="file" className="tableForMulter" multiple {...register("photo", {})} />
              </td>

              <td><Form.Check  {...register("breakfast", {})} /></td>

              <td><Form.Check  {...register("lunch", {})} /></td>

              <td style={{ paddingLeft: "5px" }}><Form.Check    {...register("afternoonTea", {})} /></td>

              <td><Form.Check  {...register("dinner", {})} /></td>

              <td><Form.Check  {...register("nightSnack", {})} /></td>

              <td style={{ display: "flex", justifyContent: "flex-end" }}>
                <button className="submitButton" type="submit" onClick={() => (buttonState.button = 3)} >
                  修改
                </button>
                <button className="deleteButton" type="submit" onClick={() => (buttonState.button = 4)}>
                  刪除
                </button>
              </td>

            </tr>
          </tbody>
        </Table>
      </Form>

    </>
  );
}
