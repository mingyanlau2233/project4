import { useForm } from "react-hook-form";
import { Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import React, { useEffect } from "react";
import { getDepartments } from "../../redux/department/thunks";
import "./inline.css"
import { postProductThunk } from "../../redux/productSetting/thunks";
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';

interface ProductForm {
  name: string;
  product_category_id: number;
  is_launched: boolean;
  is_kiosk: boolean;
  is_time_consuming: boolean;
  department_id: number;
  price: number;
  breakfast: boolean;
  lunch: boolean;
  afternoonTea: boolean;
  dinner: boolean;
  nightSnack: boolean;
  photo: any

}
export default function SetProduct() {
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };
  const dispatch = useDispatch();
  const categoryData = useSelector((state: IRootState) => state.category.dataFromDb)
  const departments = useSelector((state: IRootState) => state.department.department)
  const { register, handleSubmit, reset } = useForm<ProductForm>({
    defaultValues: {
      name: "",
      product_category_id: 0,
      is_launched: true,
      is_kiosk: false,
      is_time_consuming: false,
      department_id: 0,
      price: 0,
      breakfast: false,
      lunch: false,
      afternoonTea: false,
      dinner: false,
      nightSnack: false,
      photo: ""
    },
  });
  const onSubmit = (data: ProductForm) => {
    console.log(data)
    dispatch(postProductThunk(data.breakfast, data.lunch, data.afternoonTea, data.dinner,
      data.nightSnack, data.department_id, data.name, data.price, data.is_launched,
      data.is_kiosk, data.is_time_consuming, data.photo, data.product_category_id))
    //window.location.reload()
    setTimeout(() => { window.location.reload() }, 300);

  };
  useEffect(() => {
    dispatch(getDepartments())
  }, [])

  const handleClose = (event: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  
  return (
    <>
      <Form encType="multipart/form-data" onSubmit={handleSubmit(onSubmit)} className="setProductForm">
      
        <Form.Group className="mb-3 setupForm" controlId="formInput">

          <Form.Label>菜品類別：</Form.Label>
          <Form.Select aria-label="Default select example" {...register("product_category_id", {})} required={true}>
            <option >-請選擇菜品類別-</option>
            {categoryData.map((categoryData) =>
              <option value={categoryData.id}>{categoryData.name}</option>)}
          </Form.Select>

          <Form.Label>菜品名稱：</Form.Label>
          <Form.Control type="test" placeholder="請輸入菜品名稱"
            {...register("name", {})} required={true} />

          <Form.Label>價錢：</Form.Label>
          <Form.Control className="price" type="number" placeholder="" min="0"
            {...register("price", {})} required={true} />

          <Form.Label>上市中：</Form.Label>
          <Form.Select aria-label="Default select example"
            {...register("is_launched", {})} required={true}>

            <option value="true">是</option>
            <option value="false">否</option>
        </Form.Select>

        </Form.Group>

        <Form.Group className="setupForm">

        <Form.Label>小賣部：</Form.Label>
        <Form.Select aria-label="Default select example"
          {...register("is_kiosk", {})} required={true}>

          <option value="true">是</option>
          <option value="false">否</option>
        </Form.Select>
        <Form.Label>製作耗時：</Form.Label>
        <Form.Select aria-label="Default select example"
          {...register("is_time_consuming", {})} required={true}>

          <option value="true">是</option>
          <option value="false">否</option>
        </Form.Select>

        <Form.Label>製作部門：</Form.Label>
        <Form.Select aria-label="Default select example" {...register("department_id", {})} required={true}>
          <option >-請選擇製作部門-</option>
          {departments.map((department) =>
            <option value={department.id}>{department.name}</option>)}
        </Form.Select>
        < Form.Label>上傳圖片：</Form.Label>
        <Form.Control type="file" multiple {...register("photo", {})} required={true} id="fileUpload"/>
      </Form.Group>

      <Form.Group className="saleTime">
        <Form.Label >銷售時間：</Form.Label>
            <Form.Label >早餐</Form.Label>
            <Form.Check className="inline"   {...register("breakfast", {})} />
            <Form.Label>午餐</Form.Label>
            <Form.Check className="inline"    {...register("lunch", {})} />
            <Form.Label >下午茶</Form.Label>
            <Form.Check className="inline"    {...register("afternoonTea", {})} />
            <Form.Label >晚餐</Form.Label>
            <Form.Check className="inline"    {...register("dinner", {})} />
            <Form.Label>宵夜</Form.Label>
            <Form.Check className="inline"    {...register("nightSnack", {})} />
            <button className="submitButton" type="submit" onClick={handleClick}>
              提交
            </button>
            <Snackbar
              open={open}
              autoHideDuration={3000}
              message="已新增菜品"
              onClose={handleClose}
            />
      </Form.Group>
    </Form>
    </>
  );
}
