import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { FormInputText } from '../form/FormInputText';
import { FormProvider, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { addDepartment } from '../../redux/department/thunks';
import { postStaffAction } from '../../redux/staff/action';
import {postStaffThunk} from '../../redux/staff/thunks';
import { FormInputDropdownPosition } from '../form/FormInputDropdownForPosition';
import { FormInputDropdownIsOnBoard } from '../form/FormInputDropdownForOnBoard';


const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const DropdownContainerStyle = {
    display: 'flex',
    gap: "150px"
    
    

}
interface IFormInput {
    // department: string;
    // textValue: string;
    // radioValue: string;
    // checkboxValue: string[];
    // dateValue: Date;
    // dropdownValue: string;
    // sliderValue: number;
    name: string;
    password: string;
    position:string;
    is_onboard:boolean
}
const defaultValues = {
    // department: "",
    // textValue: "",
    // radioValue: "",
    // checkboxValue: [],
    // dateValue: new Date(),
    // dropdownValue: "",
    // sliderValue: 0,
    name: "",
    password:"",
    position: "管理",
    is_onboard: true,
};

export default function StaffModal() {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const dispatch = useDispatch();
    const methods = useForm<IFormInput>({ defaultValues: defaultValues });
    const { handleSubmit, reset, control, setValue, watch } = methods;
    const onSubmit = (data: IFormInput) => { 
        // console.log(data.department,data.dropdownValue)
        dispatch(postStaffThunk(data))
        // console.log(data)
        handleClose()
    };

    return (
        <div>
            <Button onClick={handleOpen} variant="contained" color="success">新增</Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >

                <Box sx={style}>
                    <FormProvider {...methods}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            新增會員
                        </Typography>
                        <br />
                        <FormInputText name="name" control={control} label="姓名" />
                        <br />
                        <br />
                        <FormInputText name="password" control={control} label="密碼" />
                        <br />
                        <br />
                        <div style={DropdownContainerStyle}>
                        <FormInputDropdownIsOnBoard name="is_onboard" control={control} label="在線"/>
                        <FormInputDropdownPosition name="position" control={control} label="職位"/>
                        </div>
                        <br />
                        <br />
                    </FormProvider>
                        <Button onClick={handleSubmit(onSubmit)} variant={"contained"}>
                            {" "}
                            Submit{" "}
                        </Button>

                </Box>
            </Modal>
        </div>
    );
}