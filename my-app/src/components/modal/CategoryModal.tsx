import * as React from 'react';
import Box from '@mui/material/Box';
import { Form, Button } from "react-bootstrap";
import Modal from '@mui/material/Modal';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { useForm } from 'react-hook-form';
import { postProductThunk } from '../../redux/productSetting/thunks';
import { useEffect } from 'react';
import { getDepartments } from '../../redux/department/thunks';
import { AssignContainer } from '../product/product.style';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

interface ProductForm {
    name: string;
    product_category_id: number;
    is_launched: boolean;
    is_kiosk: boolean;
    is_time_consuming: boolean;
    department_id: number;
    price: number;
    breakfast: boolean;
    lunch: boolean;
    afternoonTea: boolean;
    dinner: boolean;
    nightSnack: boolean;
    photo: any

}



export default function SetProductModal() {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const dispatch = useDispatch();
    const categoryData = useSelector((state: IRootState) => state.category.dataFromDb)
    const departments = useSelector((state: IRootState) => state.department.department)
    const { register, handleSubmit, reset } = useForm<ProductForm>({
        defaultValues: {
            name: "",
            product_category_id: 0,
            is_launched: true,
            is_kiosk: false,
            is_time_consuming: false,
            department_id: 0,
            price: 0,
            breakfast: false,
            lunch: false,
            afternoonTea: false,
            dinner: false,
            nightSnack: false,
            photo: ""
        },
    });
    const onSubmit = (data: ProductForm) => {
        console.log(data)
        dispatch(postProductThunk(data.breakfast, data.lunch, data.afternoonTea, data.dinner,
            data.nightSnack, data.department_id, data.name, data.price, data.is_launched,
            data.is_kiosk, data.is_time_consuming, data.photo, data.product_category_id))
        //window.location.reload()
        setInterval(() => { window.location.reload() }, 300);

    };
    useEffect(() => {
        dispatch(getDepartments())
    }, [])

    return (
        <div>
            <Button onClick={handleOpen}>Open modal</Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box>
                    <Form encType="multipart/form-data" onSubmit={handleSubmit(onSubmit)}>
                      
                        <Form.Group className="mb-3" controlId="formInput">
                            <Form.Label>菜品類別：</Form.Label>
                            <Form.Select aria-label="Default select example" {...register("product_category_id", {})}>
                                <option >請選擇菜品類別</option>
                                {categoryData.map((categoryData) =>
                                    <option value={categoryData.id}>{categoryData.name}</option>)}
                            </Form.Select>
                            <Form.Label>菜品名稱：</Form.Label>
                            <Form.Control type="test" placeholder=""
                                {...register("name", {})} />
                            <Form.Label>價錢：</Form.Label>
                            <Form.Control type="number" placeholder="" min="0"
                                {...register("price", {})} />

                        </Form.Group>

                        <Form.Group>
                            <Form.Label>上市中：</Form.Label>
                            <Form.Select aria-label="Default select example"
                                {...register("is_launched", {})}>

                                <option value="true">是</option>
                                <option value="false">否</option>
                            </Form.Select>
                            <Form.Label>小賣部：</Form.Label>
                            <Form.Select aria-label="Default select example"
                                {...register("is_kiosk", {})}>

                                <option value="true">是</option>
                                <option value="false">否</option>
                            </Form.Select>
                            <Form.Label>製作耗時：</Form.Label>
                            <Form.Select aria-label="Default select example"
                                {...register("is_time_consuming", {})}>

                                <option value="true">是</option>
                                <option value="false">否</option>
                            </Form.Select>

                        </Form.Group>

                        <Form.Group >

                            <Form.Select aria-label="Default select example" {...register("department_id", {})}>
                                <option >請選擇制作部門</option>
                                {departments.map((department) =>
                                    <option value={department.id}>{department.name}</option>)}
                            </Form.Select>
                            < Form.Label>上傳圖片</Form.Label>
                            <Form.Control type="file" multiple {...register("photo", {})} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label >銷售時間：</Form.Label>
                            <Form.Label >早餐</Form.Label>
                            <Form.Check className="inline"   {...register("breakfast", {})} />
                            <Form.Label>午餐</Form.Label>
                            <Form.Check className="inline"    {...register("lunch", {})} />
                            <Form.Label >下午茶</Form.Label>
                            <Form.Check className="inline"    {...register("afternoonTea", {})} />
                            <Form.Label  >晚餐</Form.Label>
                            <Form.Check className="inline"    {...register("dinner", {})} />
                            <Form.Label>宵夜</Form.Label>
                            <Form.Check className="inline"    {...register("nightSnack", {})} />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            提交
                        </Button>
                    </Form>
                </Box>
            </Modal>
        </div>
    );
}