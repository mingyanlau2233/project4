import { useSelector } from "react-redux";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { IRootState } from "../redux/store";
import ProductSetting from "../pages/product/productSetting";
export function PrivateRoute({ component, ...rest }: RouteProps) {
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth
  );
  console.log(isAuthenticated);

  const Component = component;
  if (Component == null) {
    return null;
  }

  if (isAuthenticated.position=="admin"){



  }

  let render: (props: any) => JSX.Element;
  // if (Component == ProductSetting){
  //   if (isAuthenticated.position == "admin"){
  //     render = (props: any) => <Component {...props} />;
      
  //   } 
  //    else{
  //      render = (props:any)=> <div>Loading...</div>
  //      return  <Route {...rest} render={render} />;
  //   }
    




  // }  

  if (isAuthenticated.isAuthenticated ) {
    render = (props: any) => <Component {...props} />;
  }
  else {
    if(isAuthenticated.isAuthenticated){
          render = (props: any) => (
        <Redirect
          to={{
            pathname: "/waiter",
            state: { from: props.location },
          }}
        />)
    }
    else {render = (props: any) => ( 
      <Redirect
        to={{
          pathname: "/login",
          state: { from: props.location },
        }}
      />
    );}
  }

  return <Route {...rest} render={render} />;
}
